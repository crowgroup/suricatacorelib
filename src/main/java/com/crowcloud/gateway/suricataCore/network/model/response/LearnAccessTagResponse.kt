package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class LearnAccessTagResponse(
        @SerializedName("is_learned") val isLearned: Boolean = false,
        @SerializedName("state") val accessTagState: String? = null
)
