package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

/**
 * Created by michael on 2/18/18.
 */
class PaginatedResponse<T> {

    @SerializedName("count")
    val count: Int = 0

    @SerializedName("next")
    val next: String? = null

    @SerializedName("previous")
    val previous: String? = null

    @SerializedName("results")
    val results: List<T>? = null
}
