package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class AddKeypadRequest(
    @SerializedName("link_type")
    val linkType: String = "1",
    @SerializedName("device_id")
    val serialNumber: Int,
    @SerializedName("name")
    val keypadName: String,
    @SerializedName("areas")
    val areas: List<Int>
)
