package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by michael on 2/19/18.
 */

class PanelUser() : Parcelable {

    var index: Int = 1

    @SerializedName("remote")
    var remote: Boolean? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("user_code")
    var userCode: String? = null

    @SerializedName("phone_number")
    var phoneNumber: String? = null

    @SerializedName("is_access_tag_user")
    var isAccessTagUser: Boolean = false

    @SerializedName("pendant_allow")
    var pendantAllow: Boolean = false

    @SerializedName("pendant_battery_low")
    var pendantBatteryLow: Boolean = false

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("disarm")
    var disarm: Boolean? = null

    @SerializedName("disarm_stay")
    var disarmStay: Boolean? = null

    @SerializedName("arm")
    var arm: Boolean? = null

    @SerializedName("stay")
    var stay: Boolean? = null

    @SerializedName("can_edit_himself")
    var canEditHimself: Boolean? = null

    @SerializedName("pendant_id")
    var pendantId: Int? = null

    @SerializedName("active")
    var active: Boolean? = null

    @SerializedName("access_tag_id")
    var accessTagId: String? = null

    @SerializedName("can_edit_all_users")
    var canEditAllUsers: Boolean? = null

    @SerializedName("areas")
    // In your User class (or whatever the class name is)
    var areas: MutableList<Int> = mutableListOf()

    constructor(parcel: Parcel) : this() {
        remote = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        name = parcel.readString()
        userCode = parcel.readString()
        phoneNumber = parcel.readString()
        isAccessTagUser = parcel.readByte() != 0.toByte()
        pendantAllow = parcel.readByte() != 0.toByte()
        pendantBatteryLow = parcel.readByte() != 0.toByte()
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        disarm = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        disarmStay = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        arm = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        stay = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        canEditHimself = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        pendantId = parcel.readValue(Int::class.java.classLoader) as? Int
        active = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        accessTagId = parcel.readString()
        canEditAllUsers = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
    }

    override fun equals(other: Any?): Boolean {
        if (other !is PanelUser) return false

        return name == other.name
                && id == other.id
                && stay == other.stay//TODO: continue and fill all class members..
    }

    override fun hashCode(): Int {
        return "$name$id$stay".hashCode()//TODO: continue and fill all class members..
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(remote)
        parcel.writeString(name)
        parcel.writeString(userCode)
        parcel.writeString(phoneNumber)
        parcel.writeByte(if (isAccessTagUser) 1 else 0)
        parcel.writeByte(if (pendantAllow) 1 else 0)
        parcel.writeByte(if (pendantBatteryLow) 1 else 0)
        parcel.writeValue(id)
        parcel.writeValue(disarm)
        parcel.writeValue(disarmStay)
        parcel.writeValue(arm)
        parcel.writeValue(stay)
        parcel.writeValue(canEditHimself)
        parcel.writeValue(pendantId)
        parcel.writeValue(active)
        parcel.writeString(accessTagId)
        parcel.writeValue(canEditAllUsers)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PanelUser> {
        override fun createFromParcel(parcel: Parcel): PanelUser {
            return PanelUser(parcel)
        }

        override fun newArray(size: Int): Array<PanelUser?> {
            return arrayOfNulls(size)
        }
    }

}
