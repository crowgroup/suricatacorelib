package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class PanelInfo(
    @SerializedName("id")
    override var id: Int? = null,

    @SerializedName("role")
    override var role: Int? = null,

    @SerializedName("panel_user")
    var panelUserId: Int? = null,

    @SerializedName("name")
    override var name: String? = null,

    @SerializedName("receive_pictures")
    override var receivePictures: Boolean = false,

    @SerializedName("notifications_filter")
    var notificationsFilter: NotificationsEmailFilter? = null,

    @SerializedName("user")
    override var user: User? = null,

    @SerializedName("is_superuser")
    var isSuperUser: Boolean = false,

    @SerializedName("notification_language")
    var notificationLanguage: String? = null,

    @SerializedName("user_code")
    var userCode: String? = null,

    @SerializedName("control_panel")
    override var controlPanel: ControlPanel? = null,

    var isSelected: Boolean = false
) : Parcelable, BasePanel {

    // Derived properties for state, stateFull, and pairType
    val panelState: Panel.PanelState?
        get() = controlPanel?.state?.let { Panel.PanelState.valueOf(it.uppercase()) }

    val stateFull: Panel.StateFull?
        get() = controlPanel?.stateFull?.let { stateValue ->
            Panel.StateFull.entries.find { it.ordinal == stateValue }
        }

    val pairType: Panel.PairType?
        get() = controlPanel?.pairType?.let { pairValue ->
            Panel.PairType.entries.find { it.ordinal == pairValue }
        }
}
