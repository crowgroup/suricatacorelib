package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.request.AddZoneRequest
import com.crowcloud.gateway.suricataCore.network.model.request.BypassRequest
import com.crowcloud.gateway.suricataCore.network.model.request.SetDeviceNameRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigAdvAirQualityDetectorRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigAirQualityDetectorRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigCameraPirCT2035Request
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigFloodDetectorRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigGlassBreakDetectorRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigInfraredMotionDetectorRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigMagnetAndVibrationRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigMagneticContactRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigNewSmokeHeatSyncDetectorRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigPirCT2035Request
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigPirCameraRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigSmokeHeatSyncDetectorRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigSmokeSyncDetectorRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigTemperatureRequest
import com.crowcloud.gateway.suricataCore.network.model.response.Zone
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by michael on 2/4/18.
 */

interface ZonesApi {

    @GET("/panels/{panel_id}/zones/")
    suspend fun getZones(@Path("panel_id") panelId: Int): List<Zone>

    @PATCH("/panels/{panel_id}/zones/{zone_id}/")
    suspend fun bypass(@Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                   @Body bypassRequest: BypassRequest): Zone

    @PUT("/panels/{panel_id}/zones/{zone_id}/")
    suspend fun addZone(@Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                        @Body addZoneRequest: AddZoneRequest): Zone

    @PUT("/panels/{panel_id}/zones/{zone_id}/params/type/{zone_type}/")
    suspend fun configPirCamera(
                                @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                                @Path("zone_type") zoneType: Int,
                                @Body configPirCameraRequest: ConfigPirCameraRequest): Zone

    @PUT("/panels/{panel_id}/zones/{zone_id}/params/type/{zone_type}/")
    suspend fun configInfraredMotionDetector(
                                @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                                @Path("zone_type") zoneType: Int,
                                @Body configInfraredMotionDetectorRequest:
                                                 ConfigInfraredMotionDetectorRequest): Zone

    @PUT("/panels/{panel_id}/zones/{zone_id}/params/type/{zone_type}/")
    suspend fun configMagneticContact(
                                @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                                @Path("zone_type") zoneType: Int,
                                @Body configInfraredMotionDetectorRequest:
                                                 ConfigMagneticContactRequest): Zone

//    @PUT("/panels/{panel_id}/zones/{zone_id}/params/type/{zone_type}/")
//    suspend fun configSmokeDetector(
//                                @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
//                                @Path("zone_type") zoneType: Int,
//                                @Body configSmokeDetectorRequest: ConfigSmokeDetectorRequest): Zone

    @PUT("/panels/{panel_id}/zones/{zone_id}/params/type/{zone_type}/")
    suspend fun configGlassBreakDetector(
                                @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                                @Path("zone_type") zoneType: Int,
                                @Body configGlassBreakDetectorRequest:
                                                ConfigGlassBreakDetectorRequest): Zone

    @PUT("/panels/{panel_id}/zones/{zone_id}/params/type/{zone_type}/")
    suspend fun configFloodDetector(
                                @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                                @Path("zone_type") zoneType: Int,
                                @Body configFloodDetectorRequest: ConfigFloodDetectorRequest): Zone

    @PUT("/panels/{panel_id}/zones/{zone_id}/params/type/{zone_type}/")
    suspend fun configMagnetAndVibrationSensor(
                        @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                        @Path("zone_type") zoneType: Int,
                        @Body configMagnetAndVibrationRequest: ConfigMagnetAndVibrationRequest): Zone

    @PUT("/panels/{panel_id}/zones/{zone_id}/params/type/{zone_type}/")
    suspend fun configAirQualityDetector(
                        @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                        @Path("zone_type") zoneType: Int,
                        @Body configAirQualityDetectorRequest: ConfigAirQualityDetectorRequest): Zone

    @PUT("/panels/{panel_id}/zones/{zone_id}/params/type/{zone_type}/")
    suspend fun configAdvAirQualityDetector(
                        @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                        @Path("zone_type") zoneType: Int,
                        @Body configAdvAirQualityDetectorRequest:
                                    ConfigAdvAirQualityDetectorRequest): Zone

    @PUT("/panels/{panel_id}/zones/{zone_id}/params/type/{zone_type}/")
    suspend fun configTemperature(
                        @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                        @Path("zone_type") zoneType: Int,
                        @Body configTemperatureRequest: ConfigTemperatureRequest): Zone

    @PUT("/panels/{panel_id}/zones/{zone_id}/params/type/{zone_type}/")
    suspend fun configSmokeSyncDetector(
                        @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                        @Path("zone_type") zoneType: Int,
                        @Body configSmokeSyncDetectorRequest: ConfigSmokeSyncDetectorRequest): Zone

    @PUT("/panels/{panel_id}/zones/{zone_id}/params/type/{zone_type}/")
    suspend fun configSmokeHeatSyncDetector(
                        @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                        @Path("zone_type") zoneType: Int,
                        @Body configSmokeHeatSyncDetectorRequest: ConfigSmokeHeatSyncDetectorRequest): Zone

    @PUT("/panels/{panel_id}/zones/{zone_id}/params/type/{zone_type}/")
    suspend fun configNewSmokeHeatSyncDetector(
        @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
        @Path("zone_type") zoneType: Int,
        @Body configNewSmokeHeatSyncDetectorRequest: ConfigNewSmokeHeatSyncDetectorRequest): Zone

    @PUT("/panels/{panel_id}/zones/{zone_id}/params/type/{zone_type}/")
    suspend fun configPirCT2035(
                        @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                        @Path("zone_type") zoneType: Int,
                        @Body configPirCT2035Request: ConfigPirCT2035Request): Zone

    @PUT("/panels/{panel_id}/zones/{zone_id}/params/type/{zone_type}/")
    suspend fun configCameraPirCT2035(
                        @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                        @Path("zone_type") zoneType: Int,
                        @Body configPirCT2035Request: ConfigCameraPirCT2035Request): Zone

    @DELETE("/panels/{panel_id}/zones/{zone_id}/")
    suspend fun deleteZone(@Header("X-Crow-CP-User") userId: String,
                           @Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int):
            retrofit2.Response<ResponseBody>

    @GET("/panels/{panel_id}/zones/{zone_id}/")
    fun getZone(@Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int): Call<Zone>

    @PUT("/panels/{panel_id}/zones/{zone_id}/")
    suspend fun updateZoneName(
                        @Header("X-Crow-CP-User") userId: String,
                        @Path("panel_id") panelId: Int,
                        @Path("zone_id") zoneId: Int,
                        @Body setDeviceNameRequest: SetDeviceNameRequest): Zone

//    @PUT("/panels/{panel_id}/zones/{zone_id}/")
//    suspend fun updateZoneData(
//        @Header("X-Crow-CP-User") userId: String,
//        @Path("panel_id") panelId: Int,
//        @Path("zone_id") zoneId: Int, @Body zone: Zone): Zone

    @PATCH("/panels/{panel_id}/zones/{zone_id}/")
    fun bypassZone(@Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                   @Body bypassRequest: BypassRequest): Call<Zone>
}
