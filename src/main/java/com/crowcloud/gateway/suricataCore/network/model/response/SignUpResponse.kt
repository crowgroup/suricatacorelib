package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class SignUpResponse {
    @SerializedName("key")
    private val key: String? = null
}
