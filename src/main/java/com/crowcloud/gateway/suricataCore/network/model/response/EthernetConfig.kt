package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class EthernetConfig {

    @SerializedName("remote_control_port")
    private val remoteControlPort: Int = 0

    @SerializedName("static_ip")
    private val staticIp: String? = null

    @SerializedName("dns_server")
    private val dnsServer: String? = null

    @SerializedName("subnet_mask")
    private val subnetMask: String? = null

    @SerializedName("id")
    private val id: String? = null

    @SerializedName("enabled")
    private val enabled: Boolean = false

    @SerializedName("gateway")
    private val gateway: String? = null

    @SerializedName("dhcp")
    private val dhcp: Boolean = false
}