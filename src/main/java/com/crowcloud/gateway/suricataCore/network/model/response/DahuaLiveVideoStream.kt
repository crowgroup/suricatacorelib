package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class DahuaLiveVideoStreamData(
        @SerializedName("device_id")
        var deviceId: String? = null,

        @SerializedName("channel_id")
        var channelId: String? = null,

        @SerializedName("rtmp")
        var rtmp: String? = null,

        @SerializedName("rtmp_h_d")
        var rtmpHd: String? = null
)

/*
{"device_id":"5F03BD9PAGFA5A4",
"channel_id":"0","rtmp":"rtmp://118.194.233.247:12966/live/28de595627d6d45c552c301555e37be63d9b093a13259f10",
"rtmp_h_d":"rtmp://118.194.233.247:12966/live/28de595627d6d45ca0f4f3b72e40a8532329e23c9f40f9cd"}
 */