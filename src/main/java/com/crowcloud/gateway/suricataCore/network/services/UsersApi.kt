package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.request.AcceptInvitationRequest
import com.crowcloud.gateway.suricataCore.network.model.request.LocalUser
import com.crowcloud.gateway.suricataCore.network.model.response.IncomingInvitation
import com.crowcloud.gateway.suricataCore.network.model.response.Invitation
import com.crowcloud.gateway.suricataCore.network.model.response.Invitee
import com.crowcloud.gateway.suricataCore.network.model.response.LearnAccessTagResponse
import com.crowcloud.gateway.suricataCore.network.model.response.Panel
import com.crowcloud.gateway.suricataCore.network.model.response.PanelUserV92
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

interface UsersApi {

    //combined list of cloud and panel users
    @GET("/panels/{panel_id}/users/")
    suspend fun getUsers(@Path("panel_id") panelId: Int): List<PanelUserV92>

    @POST("/panels/{panel_id}/users/{user_id}/")
    suspend fun forceSyncUser(@Path("panel_id") panelId: Int,
                              @Path("user_id") userId: Int): PanelUserV92

    @GET("/panels/{panel_id}/users/{user_id}/")
    suspend fun getUser(@Path("panel_id") panelId: Int,
                        @Path("user_id") userId: Int): PanelUserV92

    @GET("/panels/{panel_id}/users/me/")
    suspend fun getMyUser(@Path("panel_id") panelId: Int): PanelUserV92

    @PUT("/panels/{panel_id}/users/{user_id}/")
    suspend fun updateUser(@Path("panel_id") panelId: Int,
                           @Path("user_id") userId: Int,
                           @Body user: PanelUserV92): PanelUserV92

    @DELETE("/panels/{panel_id}/users/{user_id}/")
    suspend fun deleteUser(@Path("panel_id") panelId: Int,
                           @Path("user_id") userId: Int): Response<ResponseBody>

    @POST("/panels/{panel_id}/users/")
    suspend fun createUser(@Path("panel_id") panelId: Int,
                           @Body localUser: LocalUser): List<PanelUserV92>

    @GET("/panels/{panel_id}/users/invitations/")
    suspend fun getInvitations(@Path("panel_id") panelId: Int): List<Invitation>

    @POST("/panels/{panel_id}/users/invitations/")
    suspend fun createInvitation(@Path("panel_id") panelId: Int,
                                 @Body invitee: Invitee): Invitation

    @DELETE("/panels/{panel_id}/users/invitations/{email}/")
    suspend fun deleteInvitation(@Path("panel_id") panelId: Int,
                                @Path("email") email: String): Response<ResponseBody>

    @GET("/invitations/accept/{invitation_code}/")
    suspend fun getInvitation(@Path("invitation_code") invitationCode: String): IncomingInvitation

    @POST("/invitations/accept/{invitation_code}/")
    suspend fun acceptInvitation(@Path("invitation_code") invitationCode: String,
                                 @Body acceptInvitationRequest: AcceptInvitationRequest): Panel

    @DELETE("/invitations/accept/{invitation_code}/")
    suspend fun rejectInvitation(@Path("invitation_code") invitationCode: String):
            Response<ResponseBody>

    @POST("/panels/{panel_id}/users/{user_id}/learn_access_tag/")
    suspend fun learnUserAccessTag(@Header("X-Crow-CP-User") userCode: String,
                                   @Path("panel_id") panelId: Int,
                                   @Path("user_id") userId: Int) : LearnAccessTagResponse

    @GET("/panels/{panel_id}/access_tags/learn_state/")
    suspend fun getAccessTagLearnState(@Path("panel_id") panelId: Int) : LearnAccessTagResponse

    @POST("/panels/{panel_id}/access_tags/learn_state/")
    suspend fun finishLearningAccessTag(@Path("panel_id") panelId: Int) : LearnAccessTagResponse

    @DELETE("/panels/{panel_id}/users/{user_id}/learn_access_tag/")
    suspend fun deleteUserAccessTag(@Header("X-Crow-CP-User") userCode: String,
                                    @Path("panel_id") panelId: Int,
                                    @Path("user_id") userId: Int) : LearnAccessTagResponse

    @DELETE("/panels/{panel_id}/users/{user_id}/learn_access_tag/")
    suspend fun cancelLearningAccessTag(@Path("panel_id") panelId: Int,
                                        @Path("user_id") userId: Int) : LearnAccessTagResponse

}