package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class AddLockRequest(@SerializedName("serial") val serial: String? = null)