package com.crowcloud.gateway.suricataCore.network.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by michael on 2/18/18.
 */

public class BypassRequest {

    @SerializedName("bypass")
    public boolean bypass;

    public BypassRequest(boolean bypass) {
        this.bypass = bypass;
    }
}
