package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigFogGenerator(
    @SerializedName("ext_command")
    val command: Int
)