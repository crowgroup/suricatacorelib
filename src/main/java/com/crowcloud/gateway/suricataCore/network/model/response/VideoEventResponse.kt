package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class VideoEventResponse(@SerializedName("videoevent") val videoevent: VideoEvent?,
                              @SerializedName("thumbnail_base64") val thumbnailBase64: String?)
