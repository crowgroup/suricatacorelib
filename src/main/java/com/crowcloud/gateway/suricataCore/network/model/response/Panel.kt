package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

/**
 * Created by michael on 2/18/18.
 */

class Panel : BasePanel {

    // BasePanel Interface Fields
    @SerializedName("id")
    override var id: Int? = null

    @SerializedName("name")
    override var name: String? = null

    @SerializedName("role")
    override var role: Int? = null

    @SerializedName("receive_pictures")
    override var receivePictures: Boolean = false

    @SerializedName("control_panel")
    override var controlPanel: ControlPanel? = null

    @SerializedName("user")
    override var user: User? = null

    // Panel-Specific Fields
    @SerializedName("panel_user")
    var panelUser: Int? = null

    @SerializedName("user_code")
    var userCode: String? = null

    @SerializedName("mac")
    var mac: String? = null

    @SerializedName("remote_access_password")
    var remoteAccessPassword: String = ""

    @SerializedName("notifications_filter")
    var notificationsFilter: NotificationsEmailFilter? = null

    @SerializedName("is_superuser")
    var isSuperuser: Boolean = false

    @SerializedName("notification_language")
    var notificationLanguage: String = ""

    // Geographic Fields
    @SerializedName("longitude")
    var longitude: Double? = null

    @SerializedName("latitude")
    var latitude: Double? = null

    @SerializedName("geo_timezone")
    var geoTimezone: String? = null

    @SerializedName("geo_country")
    var geoCountry: String? = null

    // State and Connection Fields
    @SerializedName("state")
    var state: PanelState? = null

    @SerializedName("state_full")
    var stateFull: StateFull? = null

    @SerializedName("pair_type")
    var pairType: PairType? = null

    @SerializedName("panic_enabled")
    var panicEnabled: Boolean? = null

    @SerializedName("report_heartbeat_expired")
    var reportHeartbeatExpired: Boolean? = null

    @SerializedName("last_report_heartbeat")
    var lastReportHeartbeat: String? = null

    // Metadata Fields
    @SerializedName("version")
    var version: String? = null

    @SerializedName("api_version")
    var apiVersion: Int = 1

    @SerializedName("has_ip_cams_associated")
    var hasIpCamsAssociated: Boolean = false

    @SerializedName("has_doorbell_cams_associated")
    var hasDoorbellCamsAssociated: Boolean = false

    // Enums
    enum class PanelState {
        @SerializedName("ONLINE")
        Online,

        @SerializedName("OFFLINE")
        Offline
    }

    enum class StateFull {
        @SerializedName("0")
        Offline,

        @SerializedName("1")
        Online,

        @SerializedName("2")
        Connected
    }

    enum class PairType {
        @SerializedName("0")
        SAPI,

        @SerializedName("1")
        PC,

        @SerializedName("2")
        CLOUD
    }
}

