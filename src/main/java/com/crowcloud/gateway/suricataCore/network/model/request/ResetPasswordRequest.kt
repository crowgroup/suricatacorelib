package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ResetPasswordRequest(@SerializedName("email") val email: String)