package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.request.AreaConfigRequest
import com.crowcloud.gateway.suricataCore.network.model.request.AreaStateRequest
import com.crowcloud.gateway.suricataCore.network.model.response.Area
import retrofit2.http.*

/**
 * Created by michael on 2/4/18.
 */

interface AreasApi {

    @GET("/panels/{panel_id}/areas/")
    suspend fun getPanelAreas(@Path("panel_id") panelId: Int): List<Area>

    @GET("/panels/{panel_id}/areas/{area_id}/")
    suspend fun getAreaByAreaId(@Path("panel_id") panelId: Int,
                                @Path("area_id") areaId: Int):Area

    @PUT("/panels/{panel_id}/areas/{area_id}/")
    suspend fun updateAreaConfig(
                        @Header("X-Crow-CP-remote") remoteAccessPassword: String,
                        @Header("X-Crow-CP-User") userId: String,
                        @Path("panel_id") panelId: Int,
                        @Path("area_id") areaId: Int,
                        @Body areaConfig: AreaConfigRequest): Area

    @PATCH("/panels/{panel_id}/areas/{area_id}/")
    suspend fun setNewAreaState(@Path("panel_id") panelId: Int,
                                @Path("area_id") areaId: Int,
                                @Body stateRequest: AreaStateRequest):Area

    @PATCH("/panels/{panel_id}/areas/{area_id}/")
    suspend fun setNewAreaStateWithUserCode(@Header("X-Crow-CP-User") userId: String,
                                            @Path("panel_id") panelId: Int,
                                            @Path("area_id") areaId: Int,
                                            @Body stateRequest: AreaStateRequest):Area

}