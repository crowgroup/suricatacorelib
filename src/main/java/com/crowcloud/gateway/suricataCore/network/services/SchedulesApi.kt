package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.response.PaginatedResponse
import com.crowcloud.gateway.suricataCore.network.model.response.ScheduleInfo
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface SchedulesApi {

    @GET("/panels/{panel_id}/schedules/")
    suspend fun getSchedules(@Path("panel_id") id: Int,
                             @Query("page") page: Int,
                             @Query("page_size") pageSize: Int,
                             @Query("ordering") ordering: String? = null):
            PaginatedResponse<ScheduleInfo>

    @POST("/panels/{panel_id}/schedules/")
    suspend fun addSchedule(@Path("panel_id") panelId: Int,
                            @Body data: ScheduleInfo): ScheduleInfo

    @PATCH("/panels/{panel_id}/schedules/{schedule_id}/")
    suspend fun updateSchedule(@Path("panel_id") panelId: Int,
                               @Path("schedule_id") scheduleId: Int,
                               @Body data: ScheduleInfo): ScheduleInfo

    @DELETE("/panels/{panel_id}/schedules/{schedule_id}/")
    suspend fun deleteSchedule(@Path("panel_id") panelId: Int,
                               @Path("schedule_id") scheduleId: Int): Response<ResponseBody>
}