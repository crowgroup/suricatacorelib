package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class AccessRequestResponse {

    @SerializedName("result")
    val result: AccessRequest? = null
}