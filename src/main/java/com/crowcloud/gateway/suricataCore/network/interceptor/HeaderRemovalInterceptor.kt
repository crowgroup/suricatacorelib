package com.crowcloud.gateway.suricataCore.network.interceptor

import android.util.Log
import com.crowcloud.gateway.suricataCore.persistence.DataRepo
import okhttp3.Interceptor
import okhttp3.Response


class HeaderRemovalInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        val apiVersion = DataRepo.instance.apiVersion
        val shouldRemoveHeader = apiVersion >= 2

        val newRequestBuilder = originalRequest.newBuilder()

        // Conditionally remove the header
        if (shouldRemoveHeader) {
            newRequestBuilder.removeHeader("X-Crow-CP-User")
            Log.d("HeaderRemovalInterceptor",
                "Removed X-Crow-CP-User header for API version: $apiVersion")
        }

        // Proceed with the modified request
        val newRequest = newRequestBuilder.build()
        return chain.proceed(newRequest)
    }
}
