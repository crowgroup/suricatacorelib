package com.crowcloud.gateway.suricataCore.network.model.response

import com.crowcloud.gateway.suricataCore.util.CrowDeviceType
import kotlinx.parcelize.Parcelize
import java.util.ArrayList

@Parcelize
class Pendant(
    override var deviceType: CrowDeviceType = CrowDeviceType.PENDANT,

    var userName: String = "",
    var batteryLow: Boolean = false,
) : BaseDevice() {

    constructor(userName: String, batteryLow: Boolean) : this() {
        this.userName = userName
        this.batteryLow = batteryLow
    }

    override val troublesList: ArrayList<String>
        get() {
            val troubles = super.troublesList
            if (batteryLow) troubles.add("pendant_battery_low")
            return troubles
        }
}

/* Pendant fields in PanelUser
            "pendant_repeated": false,
            "pendant_id": 2623805,
            "pendant_rssi_bars": 0,
            "pendant_allow": true,
            "pendant_hardware_version": "0",
            "pendant_rssi": 0,
            "pendant_software_version": null,
            "pendant_rssi_unit": "%",
 */