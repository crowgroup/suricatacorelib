package com.crowcloud.gateway.suricataCore.network.model.response

data class ArmData (
    var areaId: Int? = null,
    var timeStamp: Long? = null
)
