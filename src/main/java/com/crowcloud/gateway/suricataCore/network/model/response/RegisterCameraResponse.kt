package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class RegisterCameraResponse {

    @SerializedName("message")
    var message: String? = null

    @SerializedName("api")
    var api: ApiData? = null

    @SerializedName("ws_entry_point")
    var wsEntryPoint: String? = null

    @SerializedName("rest_url")
    var restEntryPoint: String? = null

    class ApiData {

        @SerializedName("key")
        var key: String? = null

        @SerializedName("secret")
        var secret: String? = null
    }
}