package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ZoomVideoRequest(@SerializedName("top") val top: Double,
                            @SerializedName("left") val left: Double,
                            @SerializedName("size") val size: Double)
//{ top: 0.2, left: 0.4, size: 0.5 }