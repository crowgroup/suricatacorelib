package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class RadioInfo {

    @SerializedName("module_id")
    val moduleId: Int = 0

    @SerializedName("freq")
    val freq: Int = 0

    @SerializedName("software_version")
    val softwareVersion: String? = null

    @SerializedName("id")
    val id: String? = null

    @SerializedName("hardware_version")
    val hardwareVersion: String? = null
}