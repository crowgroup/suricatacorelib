package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.request.*
import com.crowcloud.gateway.suricataCore.network.model.response.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

/**
 * Created by michael on 2/4/18.
 */

interface AuthApi {

    @GET("/auth/user/")
    fun user(): Call<User>

    @GET("/auth/user/")
    suspend fun getCurrentUser(): User

    @PUT("/auth/user/")
    suspend fun updateCurrentUser(@Body user: User): User

    @POST("/user_actions/delete_account/")
    suspend fun deleteAccount(@Body deleteAccountRequest: DeleteAccountRequest): Response<ResponseBody>

    @FormUrlEncoded
    @POST("/o/token/")
    suspend fun signIn(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String,
        @Field("grant_type") grantType: String = "password" // Default grant type
    ): Response<LoginResponse>


    @FormUrlEncoded
    @Headers("Accept: */*", "Content-Type: application/x-www-form-urlencoded", "charset: utf-8")
    @POST("/o/token/")
    suspend fun refreshToken(
        @Field("grant_type") grantType: String = "refresh_token",
        @Field("refresh_token") refreshToken: String,
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String
    ): Response<LoginResponse>


//    @Headers("Accept: */*", "Content-Type: application/x-www-form-urlencoded", "charset: utf-8")
//    @POST("/o/token/?grant_type=refresh_token")
//    fun refreshToken(@Query("refresh_token") refreshToken: String): Call<LoginResponse>

    @POST("/auth/password/reset/")
    suspend fun resetPassword(@Body resetPasswordRequest: ResetPasswordRequest): MessageResponse

    @POST("/auth/registration/")
    suspend fun register(@Body signUpRequest: SignUpRequest): SignUpResponse

    @GET("/general/tos/")
    suspend fun getLatestTOS(): TosData

    @FormUrlEncoded
    @POST("/o/revoke_token/")
    suspend fun revokeToken(
        @Field("token") token: String,
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String
    ): Response<ResponseBody>

    @POST("/auth/password/change/")
    suspend fun changePassword(@Body changePasswordRequest: ChangePasswordRequest): MessageResponse

    @POST("/auth/password/reset/confirm/")
    suspend fun confirmResetPasswordConfirm(@Body request: ConfirmPasswordResetRequest): MessageResponse

    @Headers("Accept: */*", "Content-Type: application/json", "charset: utf-8")
    @POST("/user_actions/endpoint/")
    suspend fun updateFcmToken(@Body body: Map<String, String>): Response<ResponseBody>

    @Headers("Accept: */*", "Content-Type: application/json", "charset: utf-8")
    @POST("/user_actions/endpoint/")
    fun updateFcmTokenSync(@Body body: Map<String, String>): Call<ResponseBody>

}