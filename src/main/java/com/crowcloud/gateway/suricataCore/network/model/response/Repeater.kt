package com.crowcloud.gateway.suricataCore.network.model.response

import com.crowcloud.gateway.suricataCore.util.CrowDeviceType
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.util.ArrayList

@Parcelize
class Repeater(

    override var deviceType: CrowDeviceType = CrowDeviceType.REPEATER,

    @SerializedName("tamper_alarm")
    val tamperAlarm: Boolean = false,

    @SerializedName("battery_low")
    val batteryLow: Boolean = false,

    @SerializedName("supervision_alarm")
    var supervisionAlarm: Boolean = false,

    @SerializedName("battery")
    var battery: Int = 0,

    @SerializedName("temperature")
    var temperature: Int = 0
    ) : BaseDevice(){

    override val troublesList: ArrayList<String>
        get() {
            val troubles = super.troublesList
            if (tamperAlarm) troubles.add("tamper_alarm")
            if (batteryLow) troubles.add("battery_low")
            if (supervisionAlarm) troubles.add("supervision_alarm")
            return troubles
        }
}