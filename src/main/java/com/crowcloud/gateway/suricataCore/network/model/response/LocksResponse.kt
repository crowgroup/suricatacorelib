package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class LocksResponse {

    @SerializedName("result")
    val result: ArrayList<LockInAssa> = ArrayList()
}