package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.response.Troubles
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by michael on 2/4/18.
 */

interface TroublesApi {

    @GET("/panels/{panel_id}/troubles/")
    suspend fun getPanelTroubles(@Path("panel_id") panelId: Int): Troubles

    @DELETE("/panels/{panel_id}/troubles/")
    suspend fun resetTroubles(@Path("panel_id") panelId: Int): Response<ResponseBody>

}
