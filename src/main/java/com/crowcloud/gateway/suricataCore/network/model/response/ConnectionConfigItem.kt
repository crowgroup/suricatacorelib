package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class ConnectionConfigItem {

    @SerializedName("security")
    var security: Int = 0

    @SerializedName("password")
    var password: String? = null

    @SerializedName("id")
    val id: Int = 0

    @SerializedName("ssid")
    var ssid: String? = null

    @SerializedName("rssi")
    var rssi: Int = 0
}