package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.response.PaginatedResponse
import com.crowcloud.gateway.suricataCore.network.model.response.Picture
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

/**
 * Created by michael on 2/4/18.
 */

interface PicturesApi {

    @GET("/panels/{panel_id}/pictures/")
    suspend fun getAllPictures(
        @Path("panel_id") panelId: Int,
        @Query("page") page: Int,
        @Query("page_size") pageSize: Int,
        @Query("ordering") ordering: String? = null): PaginatedResponse<Picture>

    @GET("/panels/{panel_id}/zones/{zone_id}/pictures/")
    suspend fun getCameraPictures(
        @Path("panel_id") panelId: Int,
        @Path("zone_id") zoneId: Int,
        @Query("page") page: Int,
        @Query("page_size") pageSize: Int,
        @Query("ordering") ordering: String? = null): PaginatedResponse<Picture>


    @GET("/panels/{panel_id}/pictures/latest/by_zone/")
    fun getLatestPicturesByZone(@Path("panel_id") panelId: Int): Call<HashMap<String, Picture>>

    @GET("/panels/{panel_id}/pictures/latest/by_zone/")
    suspend fun getLatestPictures(@Path("panel_id") panelId: Int): HashMap<String, Picture>

    @Headers("CONNECT_TIMEOUT:30000", "READ_TIMEOUT:45000", "WRITE_TIMEOUT:45000")
    @POST("/panels/{panel_id}/zones/{zone_id}/pictures/")
    fun takePicture(@Path("panel_id") panelId: Int,
                    @Path("zone_id") zoneId: Int,
                    @Body emptyBody: JsonObject): Call<Picture>

    @Headers("CONNECT_TIMEOUT:30000", "READ_TIMEOUT:45000", "WRITE_TIMEOUT:45000")
    @POST("/panels/{panel_id}/zones/{zone_id}/pictures/")
    fun takePictureAsFlow(@Path("panel_id") panelId: Int,
                    @Path("zone_id") zoneId: Int,
                    @Body emptyBody: JsonObject): Response<Picture>

    @Headers("CONNECT_TIMEOUT:30000", "READ_TIMEOUT:45000", "WRITE_TIMEOUT:45000")
    @POST("/panels/{panel_id}/zones/{zone_id}/pictures/")
    suspend fun takePirCameraPicture(@Path("panel_id") panelId: Int, @Path("zone_id") zoneId: Int,
                                     @Body emptyBody: JsonObject): Picture

    @DELETE("/panels/{panel_id}/pictures/{id}/")
    suspend fun deleteTakenPicture(@Path("panel_id") panelId: Int,
                          @Path("id") id: Int): Response<ResponseBody>

    @DELETE("/panels/{panel_id}/pictures/{id}/")
    suspend fun deletePirCameraPicture(@Path("panel_id") panelId: Int,
                          @Path("id") pictureId: Int): Response<ResponseBody>

    @DELETE("/panels/{panel_id}/pictures/{id}/")
    fun deletePictureById(@Path("panel_id") panelId: Int,
                          @Path("id") id: Int): Call<ResponseBody>

    @DELETE("/panels/{panel_id}/pictures/{id}/")
    fun deletePictureByIdAsResponse(@Path("panel_id") panelId: Int,
                          @Path("id") id: Int): Response<ResponseBody>

    @DELETE("/panels/{panel_id}/zones/{zone_id}/pictures/")
    fun cancelTakePicture(@Path("panel_id") panelId: Int,
                          @Path("zone_id") zoneId: Int): Call<ResponseBody>

    @DELETE("/panels/{panel_id}/zones/{zone_id}/pictures/")
    suspend fun cancelTakePirCameraPicture(@Path("panel_id") panelId: Int,
                          @Path("zone_id") zoneId: Int): Response<ResponseBody>

    @GET("/panels/{panel_id}/pictures/{id}/")
    fun getPanelPictureById(@Path("panel_id") panelId: Int,
                            @Path("id") id: Int): Call<PaginatedResponse<Picture>>

}
