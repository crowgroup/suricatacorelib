package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.request.SetDeviceNameRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigAcPlugRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigFogGenerator
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigIndoorSirenRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigSmartSiren2Request
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigSmartSirenRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ConfigZigbeeRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ToggleOutputRequest
import com.crowcloud.gateway.suricataCore.network.model.response.Output
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by michael on 2/4/18.
 */

interface OutputsApi {

    @GET("/panels/{panel_id}/outputs/")
    suspend fun getOutputs(@Path("panel_id") panelId: Int): List<Output>

    @PATCH("/panels/{panel_id}/outputs/{output_id}/")
    suspend fun updateOutputState(@Path("panel_id") panelId: Int, @Path("output_id") outputId: Int,
                                  @Body toggleOutputRequest: ToggleOutputRequest): Output

    @PATCH("/panels/{panel_id}/outputs/{output_id}/")
    suspend fun updateZigbeeConfig(@Path("panel_id") panelId: Int, @Path("output_id") outputId: Int,
                                  @Body configZigbeeRequest: ConfigZigbeeRequest): Output

    @GET("/panels/{panel_id}/outputs/{output_id}/")
    suspend fun getOutputById(@Path("panel_id") panelId: Int, @Path("output_id") outputId: Int): Output

    @PATCH("/panels/{panel_id}/outputs/{output_id}/")
    suspend fun updateFogGeneratorConfig(@Path("panel_id") panelId: Int, @Path("output_id") outputId: Int,
                                         @Body configRequest: ConfigFogGenerator): Output

    @GET("/panels/{panel_id}/outputs/{output_id}/")
    fun getOutput(@Path("panel_id") panelId: Int, @Path("output_id") outputId: Int): Call<Output>

    @PUT("/panels/{panel_id}/outputs/{output_id}/")
    suspend fun updateOutputName(
                                @Header("X-Crow-CP-User") userId: String,
                                @Path("panel_id") panelId: Int, @Path("output_id") outputId: Int,
                                @Body setDeviceNameRequest: SetDeviceNameRequest
                            ) : Output

//    @PUT("/panels/{panel_id}/outputs/{output_id}/")
//    suspend fun updateOutputData(
//                        @Header("X-Crow-CP-User") userId: String,
//                        @Path("panel_id") panelId: Int, @Path("output_id") outputId: Int,
//                        @Body output: Output): Output

    @PATCH("/panels/{panel_id}/outputs/{output_id}/")
    fun toggleOutput(@Path("panel_id") panelId: Int, @Path("output_id") outputId: Int,
                     @Body toggleOutputRequest: ToggleOutputRequest): Call<Output>

    @PUT("/panels/{panel_id}/outputs/{output_id}/")
    suspend fun addOutput(@Header("X-Crow-CP-User") userId: String,
                          @Path("panel_id") panelId: Int, @Path("output_id") outputId: Int,
                          @Body output: Output): Output

    @DELETE("/panels/{panel_id}/outputs/{output_id}/")
    suspend fun deleteOutput(@Header("X-Crow-CP-User") userId: String,
                             @Path("panel_id") panelId: Int,
                             @Path("output_id") outputId: Int):
            retrofit2.Response<ResponseBody>

    @PUT("/panels/{panel_id}/outputs/{output_id}/params/type/{output_type}/")
    suspend fun configSmartSiren2(@Path("panel_id") panelId: Int, @Path("output_id") outputId: Int,
                                  @Path("output_type") outputType: Int,
                                  @Body configSirenRequest: ConfigSmartSiren2Request): Output

    @PUT("/panels/{panel_id}/outputs/{output_id}/params/type/{output_type}/")
    suspend fun configSmartSiren(@Path("panel_id") panelId: Int, @Path("output_id") outputId: Int,
                                  @Path("output_type") outputType: Int,
                                  @Body configSirenRequest: ConfigSmartSirenRequest): Output

    @PUT("/panels/{panel_id}/outputs/{output_id}/params/type/{output_type}/")
    suspend fun configIndoorSiren(@Path("panel_id") panelId: Int, @Path("output_id") outputId: Int,
                                 @Path("output_type") outputType: Int,
                                 @Body configSirenRequest: ConfigIndoorSirenRequest): Output

    @PUT("/panels/{panel_id}/outputs/{output_id}/params/type/{output_type}/")
    suspend fun configAcPlug(@Path("panel_id") panelId: Int, @Path("output_id") outputId: Int,
                                  @Path("output_type") outputType: Int,
                                  @Body configAcPlugRequest: ConfigAcPlugRequest): Output

}


/*
PATCH method available with requested new ext_command - can only go from 0 to 4
e.g.:
PATCH outputs/0 - DATA: {'ext_command': 1}

Fog generator commands:
0 - Disarm (if armed:true, will change to armed: false)
1 - Arm (Needs to be executed before command "2 - Explode")
2 - Explode (will "explode" and will set explosion: true temporary, will also reset armed to false)
3 - Charge (Will update empty to false, to be used by customer after manually changing the fog bomb)
4 - Empty (Will update empty to true, to be used by customer after manually removing the fog bomb)
 */