package com.crowcloud.gateway.suricataCore.network.model.response;

import java.util.HashMap;
import java.util.List;

/**
 * Created by michael on 4/23/18.
 */

public class ErrorFieldMapResponse extends HashMap<ErrorField, List<String>> {

}
