package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

enum class ScheduleAction {

    @SerializedName("area__arm")
    Arm,

    @SerializedName("area__stayarm")
    StayArm,

    @SerializedName("area__disarm")
    Disarm,

    @SerializedName("output__on")
    OutputOn,

    @SerializedName("output__off")
    OutputOff
}
