package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class DeviceConfig(
    @SerializedName("dimm_time")
    var dimmerTime: Int,
    @SerializedName("dimm_value")
    var dimmerValue: Int,
    @SerializedName("newColor")
    var isNewColor: Boolean,
    @SerializedName("temperature")
    var temperature: Int,
    @SerializedName("current_x")
    var currentX: Int,
    @SerializedName("current_y")
    var currentY: Int
) : Parcelable
