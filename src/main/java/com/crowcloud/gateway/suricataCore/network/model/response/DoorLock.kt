package com.crowcloud.gateway.suricataCore.network.model.response

import com.crowcloud.gateway.suricataCore.R
import com.crowcloud.gateway.suricataCore.util.CrowDeviceType
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.util.ArrayList

@Parcelize
data class DoorLock(
        override var deviceType: CrowDeviceType = CrowDeviceType.DOOR_LOCK,

        @SerializedName("locked")
        var isLocked: Boolean = false,

        @SerializedName("unlock_to_disarm")
        var unlockToDisarm: Boolean = false,

        @SerializedName("temperature")
        val temperature: Int = 0,

        @SerializedName("battery_low")
        val batteryLow: Boolean = false,

        @SerializedName("missing")
        var isMissing: Boolean = false,

        @SerializedName("tamper_alarm")
        val tamperAlarm: Boolean = false,

        @SerializedName("trouble_alarm")
        var troubleAlarm: Boolean = false,

        @SerializedName("open_trouble")
        var openTrouble: Boolean = false,

        @SerializedName("broken_trouble")
        var brokenTrouble: Boolean = false,

        @SerializedName("bolt_trouble")
        var boltTrouble: Boolean = false,

        @SerializedName("communication_alarm")
        var communicationAlarm: Boolean = false,

        @SerializedName("secure_sensor_trouble")
        var secureSensorTrouble: Boolean = false,

        @SerializedName("device_config")
        var config: DoorLockConfig? = null

) : BaseDevice() {

        constructor(id: Int, name: String) : this() {
                this.id = id
                this.name = name
        }

        override val troublesList: ArrayList<String>
                get() {
                        val troubles = super.troublesList
                        if (tamperAlarm) troubles.add("tamper_alarm")
                        if (batteryLow) troubles.add("battery_low")
                        if (isMissing) troubles.add("missing")
                        return troubles
                }

        fun getTroubles(): ArrayList<Int> {
                val arr = ArrayList<Int>()
                if (tamperAlarm) arr.add(R.string.trouble_tamper_alarm)
                if (batteryLow) arr.add(R.string.trouble_low_battery)
                if (isMissing) arr.add(R.string.trouble_missing)
                return arr
        }

        override fun deepCopy(): DoorLock {
                return DoorLock(
                        deviceType = this.deviceType,
                        isLocked = this.isLocked,
                        unlockToDisarm = this.unlockToDisarm,
                        temperature = this.temperature,
                        batteryLow = this.batteryLow,
                        isMissing = this.isMissing,
                        tamperAlarm = this.tamperAlarm,
                        troubleAlarm = this.troubleAlarm,
                        openTrouble = this.openTrouble,
                        brokenTrouble = this.brokenTrouble,
                        boltTrouble = this.boltTrouble,
                        communicationAlarm = this.communicationAlarm,
                        secureSensorTrouble = this.secureSensorTrouble,
                        config = this.config?.deepCopy()
                ).also {
                        it.id = this.id
                        it.name = this.name
                        it.state = this.state
                        it.softwareVersion = this.softwareVersion
                        it.hardwareVersion = this.hardwareVersion
                        it.rssi = this.rssi
                        it.rssiBars = this.rssiBars
                        it.rssiUnit = this.rssiUnit
                }
        }
}


//TODO
//    override val troublesList: ArrayList<String>
//        get() {
//            val troubles = super.troublesList
//            if (isMissing) troubles.add("missing")
//            if (tamperAlarm) troubles.add("tamper_alarm")
//            if (batteryLow) troubles.add("battery_low")
//            if (troubleAlarm) troubles.add("trouble_alarm")
//            if (openTrouble) troubles.add("open_trouble")
//            if (brokenTrouble) troubles.add("broken_trouble")
//            if (boltTrouble) troubles.add("bolt_trouble")
//            if (communicationAlarm) troubles.add("communication_alarm")
//            if (secureSensorTrouble) troubles.add("secure_sensor_trouble")
//            return troubles
//        }


/*
{"battery":0,"secure_sensor_trouble":false,"rssi_bars":5,
"device_config":{"user_code_blocking":false,
"alarm_system":false,"volume":1,"auto_relock":false,
"alarm_mode":0,"alarm_hold_off_time":100},
"tamper_alarm":false,"id":0,"temperature":25,"rssi_unit":"%",
"open_trouble":false,"type":161,"link_type":0,"battery_low":false,
"communication_alarm":false,"software_version":"0.1.0.4",
"missing":false,"repeated_rssi":0,"repeated":false,"repeated_rssi_bars":0,
"bolt_trouble":true,"broken_trouble":false,"unlock_to_disarm":false,"device_id":2641780,"locked":false,
"name":"Doorlock 1","trouble_alarm":false,"hardware_version":"42","rssi":100}]
 */