package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class UserAccount(
        @SerializedName("id") val id: Int,
        @SerializedName("is_superuser") val isSuperUser: Boolean?,
        @SerializedName("user") val user: User?
)
