package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class PanelResponse(
    @SerializedName("count")
    val count: Int,
    @SerializedName("next")
    val next: String?,
    @SerializedName("previous")
    val previous: String?,
    @SerializedName("results")
    val results: List<Panel>
)
