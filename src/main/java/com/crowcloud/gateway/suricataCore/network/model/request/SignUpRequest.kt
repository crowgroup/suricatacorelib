package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class SignUpRequest(

        @SerializedName("email")
        private val email: String?,

        @SerializedName("first_name")
        private val firstName: String?,

        @SerializedName("last_name")
        private val lastName: String?,

        @SerializedName("login")
        private val login: String = "",

        @SerializedName("password1")
        private val password1: String?,

        @SerializedName("password2")
        private val password2: String?
)