package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigAirQualityDetectorRequest(
    @SerializedName("supervision")
    val supervision: Int = 0
)

/*
 "device_config": {
            "supervision": 0
        }
 */