package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class LoginResponse(

        @SerializedName("access_token")
        val accessToken: String?,

        @SerializedName("token_type")
        val tokenType: String?,

        @SerializedName("expires_in")
        val expiresIn: Int = 0,

        @SerializedName("refresh_token")
        val refreshToken: String?,

        @SerializedName("scope")
        val scope: String?
)
