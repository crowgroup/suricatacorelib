package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.request.AcceptAccessRequest
import com.crowcloud.gateway.suricataCore.network.model.request.LocaleRequest
import com.crowcloud.gateway.suricataCore.network.model.request.SerialNumber
import com.crowcloud.gateway.suricataCore.network.model.request.ToggleLockRequest
import com.crowcloud.gateway.suricataCore.network.model.response.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface LocksApi {

    @GET("/panels/{panel_id}/doorlocks/")
    suspend fun getDoorLocks(@Path("panel_id") panelId: Int): List<DoorLock>

    @PATCH("/panels/{panel_id}/doorlocks/{lock_id}/")
    suspend fun updateDoorLockState(
                    @Path("panel_id") panelId: Int,
                    @Path("lock_id") lockId: Int,
                    @Body toggleLockRequest: ToggleLockRequest): DoorLock

    @PUT("/panels/{panel_id}/doorlocks/{lock_id}/")
    suspend fun updateDoorLock(@Header("X-Crow-CP-User") userId: String,
                               @Path("panel_id") panelId: Int,
                               @Path("lock_id") lockId: Int,
                               @Body doorLock: DoorLock): DoorLock

    @GET("/panels/{panel_id}/doorlocks/{lock_id}/")
    suspend fun getDoorLockById(@Path("panel_id") panelId: Int,
                               @Path("lock_id") lockId: Int): DoorLock

    @GET("/panels/{panel_id}/doorlocks/")
    fun getLocks(@Path("panel_id") panelId: Int): Call<List<DoorLock>>

    @GET("/panels/{panel_id}/doorlocks/{lock_id}/")
    fun getDoorLock(@Path("panel_id") panelId: Int, @Path("lock_id") lockId: Int): Call<DoorLock>

    @PATCH("/panels/{panel_id}/doorlocks/{lock_id}/")
    fun toggleLock(@Path("panel_id") panelId: Int,
                   @Path("lock_id") lockId: Int,
                   @Body toggleLockRequest: ToggleLockRequest): Call<DoorLock>

    /*
//https://api.crowcloud.xyz/panels/0013A120ABB3/defaults/device_params/161/
     */
    @GET("/panels/{panel_id}/defaults/device_params/{type}/")
    fun getLockParameters(@Path("panel_id") panelId: Int,
                          @Path("type") doorLockType: Int): Call<LockParams>

    //Assa - not in use
    @GET("/assa/access_request/")
    suspend fun getAccessRequests(): AccessRequestsResponse

    @GET("/assa/access_request/")
    fun getAccessRequestsOld(): Call<AccessRequestsResponse>

    @PATCH("/assa/access_request/{access_request_id}/")
    fun acceptAccessRequest(@Path("access_request_id") id: String,
                            @Body body: AcceptAccessRequest): Call<ResponseBody>

    @GET("/assa/access_request/{access_request_id}/")
    fun getAccessRequest(@Path("access_request_id") id: String): Call<AccessRequestResponse>

    @DELETE("/assa/access_request/{access_request_id}/")
    fun rejectAccessRequest(@Path("access_request_id") id: String): Call<ResponseBody>

    //get list of registered locks
    @GET("/assa/lock/")
    fun getYaleLocks(): Call<LocksResponse>

    @POST("/assa/panel/{control_panel_mac}/lock/")
    fun addYaleLockToAssa(@Path("control_panel_mac") mac: String?, @Body body: SerialNumber) : Call<LockInAssa>

    //get lock by id
    @GET("/assa/lock/{lock_id}/")
    fun getYaleLockById(@Path("lock_id") id: String): Call<LockInAssa>

    @DELETE("/assa/lock/{lock_id}/")
    fun deleteYaleLockById(@Path("lock_id") id: String): Call<ResponseBody>

    @POST("/assa/user/")
    fun registerUserInAssa(@Body body: LocaleRequest): Call<RegisterUserInAssaResponse>

    //Get current logged user info about registration in ASSA
    @GET("/assa/user/")
    fun getUserRegistrationInAssaInfo(): Call<RegisterUserInAssaResponse>

}
