package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.response.NotificationsEmailData
import com.crowcloud.gateway.suricataCore.network.model.response.NotificationsEmailListResponse
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

interface NotificationsApi {

    @GET("/panels/{id}/notifications/")
    suspend fun getNotificationsEmailList(@Path("id") panelId: Int): NotificationsEmailListResponse

    @GET("/panels/{id}/notifications/{email_id}/")
    suspend fun getNotificationsEmail(
        @Path("id") panelId: Int,
        @Path("email_id") emailId: Int): NotificationsEmailData

    @DELETE("/panels/{id}/notifications/{email_id}/")
    suspend fun deleteNotificationsEmail(
        @Path("id") panelId: Int,
        @Path("email_id") emailId: Int): Response<ResponseBody>

    @PUT("/panels/{id}/notifications/{email_id}/")
    suspend fun updateNotificationsEmail(
        @Path("id") panelId: Int,
        @Path("email_id") emailId: Int,
        @Body body: NotificationsEmailData): NotificationsEmailData


    @POST("/panels/{id}/notifications/")
    suspend fun addNotificationsEmail(@Path("id") panelId: Int,
                              @Body body: NotificationsEmailData): NotificationsEmailData
}