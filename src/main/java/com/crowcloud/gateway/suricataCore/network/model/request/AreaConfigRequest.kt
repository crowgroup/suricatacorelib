package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class AreaConfigRequest(@SerializedName("name") val name: String?,
                            @SerializedName("exit_delay") val exitDelay: Int?,
                            @SerializedName("stay_exit_delay") val stayExitDelay: Int?)
