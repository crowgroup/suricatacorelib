package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigSmartSirenRequest(
    @SerializedName("zone_led")
    val zoneLed: Boolean = true,
    @SerializedName("sndr_loud")
    val sndrLoud: Int = 3
)


/*
{
  "zone_led": true,
  "sndr_loud": 3
}
 */