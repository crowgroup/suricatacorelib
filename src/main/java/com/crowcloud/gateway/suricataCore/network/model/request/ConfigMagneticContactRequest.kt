package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigMagneticContactRequest(
    @SerializedName("zone_led")
    val enableLed: Boolean = true,
    @SerializedName("switch_and")
    val switchAnd: Boolean = false,
    @SerializedName("ext_switch")
    val extSwitch: Boolean = false,
    @SerializedName("int_switch")
    val intSwitch: Boolean = true,
    @SerializedName("supervision")
    val supervision: Int = 0
)

/*
 "device_config": {
            "zone_led": true,
            "switch_and": false,
            "ext_switch": false,
            "int_switch": true,
            "supervision": 0
        }
 */