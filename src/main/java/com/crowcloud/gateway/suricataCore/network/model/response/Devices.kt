package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName
import java.util.*

class Devices {

    @SerializedName("panel")
    var panel: PanelDevice? = null

    @SerializedName("zones")
    var zones: ArrayList<Zone>? = null

    @SerializedName("keypads")
    var keypads: ArrayList<Keypad>? = null

    @SerializedName("users")
    var users: ArrayList<PanelUser>? = null // pendant, access_tag

    @SerializedName("outputs")
    var outputs: ArrayList<Output>? = null

    @SerializedName("voiceboxes")
    var voiceboxes: ArrayList<VoiceBox>? = null

    @SerializedName("extenders")
    var extenders: ArrayList<Extender>? = null

    @SerializedName("repeaters")
    var repeaters: ArrayList<Repeater>? = null

    @SerializedName("dect")
    var dectDevices: ArrayList<Dect>? = null //not in use for now

}