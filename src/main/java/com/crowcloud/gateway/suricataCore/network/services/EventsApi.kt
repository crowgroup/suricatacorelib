package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.response.Event
import com.crowcloud.gateway.suricataCore.network.model.response.PaginatedResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface EventsApi {

    @GET("/panels/{id}/events/")
    suspend fun getEvents(@Path("id") id: Int,
                          @Query("page") page: Int,
                          @Query("page_size") pageSize: Int,
                          @Query("ordering") ordering: String? = null): PaginatedResponse<Event>
}