package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class AcceptInvitationRequest(
    @SerializedName("user_code")
    val userCode: String,
    @SerializedName("panel_name")
    val panelName: String,
    @SerializedName("user_name")
    val userName: String
)
