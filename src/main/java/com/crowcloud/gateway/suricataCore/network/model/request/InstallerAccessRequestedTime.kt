package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class InstallerAccessRequestedTime(@SerializedName("requested_time") val requestedTime: Int)

