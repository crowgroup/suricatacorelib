package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.response.DahuaDevicesResponse
import com.crowcloud.gateway.suricataCore.network.model.response.DahuaIpCamera
import com.crowcloud.gateway.suricataCore.network.model.response.DahuaLiveVideoStreamData
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface DahuaApi {

    //Not in use
    @POST("/dahua/{panel_id}/")
    fun registerDahuaDevice(@Path("panel_id") panelId: Int, @Body body: DahuaIpCamera): Call<ResponseBody>

    @GET("/dahua/{panel_id}/")
    suspend fun getDahuaDevices(@Path("panel_id") panelId: Int): DahuaDevicesResponse

    @GET("/dahua/{panel_id}/{device_id}/")
    fun getDahuaDevice(@Path("panel_id") panelId: Int,
                     @Path("device_id") id: String): Call<DahuaIpCamera>

    @DELETE("/dahua/{panel_id}/{device_uid}/")
    fun deleteDahuaDevice(@Path("panel_id") panelId: Int,
                        @Path("device_uid") id: String): Call<ResponseBody>

    //Get live video stream
    //GET /dahua/<panel_id>/<device_id>/stream/<protocol (rtmp, hls) optional>/
    @GET("/dahua/{panel_id}/{device_uid}/stream/{video_type}/")
    fun getDahuaLiveVideoStream(@Path("panel_id") panelId: Int,
                           @Path("device_uid") id: String, @Path("video_type") videoType: String):
                                                                Call<DahuaLiveVideoStreamData>

    //Turn on motion detection (arm)
    @POST("/dahua/{panel_id}/{device_uid}/arm/")
    fun turnOnMotionDetection(@Path("panel_id") panelId: Int,
                              @Path("device_uid") id: String)

    //Turn off motion detection (disarm)
    @DELETE("/dahua/{panel_id}/{device_uid}/arm/")
    fun turnOffMotionDetection(@Path("panel_id") panelId: Int,
                              @Path("device_uid") id: String)
}