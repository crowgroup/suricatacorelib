package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.request.RegisterCameraRequest
import com.crowcloud.gateway.suricataCore.network.model.request.ZoomVideoRequest
import com.crowcloud.gateway.suricataCore.network.model.response.*
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface IpCamerasApi {

    enum class StreamType(val value: String) {
        HLS("hls"),
        RTMP("rtmp"),
        RTMPS("rtmps")
    }

    @GET("/camera/{panel_id}/")
    fun getCameras(@Path("panel_id") panelId: Int,
                   @Query("page") page: Int,
                   @Query("page_size") pageSize: Int,
                   @Query("ordering") ordering: String? = null): Call<PaginatedResponse<IpCamera>>

    @POST("/camera/{panel_id}/{device_uid}/arm/")
    fun armIpCamera(@Path("panel_id") panelId: Int,
                    @Path("device_uid") id: String): Call<ResponseBody>

    @DELETE("/camera/{panel_id}/{device_uid}/arm/")
    fun disarmIpCamera(@Path("panel_id") panelId: Int,
                    @Path("device_uid") id: String): Call<ResponseBody>

    @POST("/camera/{panel_id}/{device_uid}/roi/")
    fun zoomInIpCamera(@Path("panel_id") panelId: Int,
                    @Path("device_uid") id: String,
                    @Body body: ZoomVideoRequest): Call<ResponseBody>

    @DELETE("/camera/{panel_id}/{device_uid}/roi/")
    fun zoomOutIpCamera(@Path("panel_id") panelId: Int,
                       @Path("device_uid") id: String): Call<ResponseBody>

    @PUT("/camera/{panel_id}/{device_uid}/")
    fun upgradeIpCamera(@Path("panel_id") panelId: Int,
                        @Path("device_uid") id: String): Call<ResponseBody>

    @Multipart
    @PUT("/camera/{panel_id}/{device_uid}/audio/")
    fun sendIpCameraAudio(@Path("panel_id") panelId: Int,
                          @Path("device_uid") id: String,
                          @Part file: MultipartBody.Part?): Call<ResponseBody>

    @GET("/camera/{panel_id}/{device_uid}/stream/{stream_type}/")
    fun getCameraStream(@Path("panel_id") panelId: Int,
                        @Path("device_uid") id: String,
                        @Path("stream_type") streamType: String? = "rtmp"): Call<StreamData>

    @POST("/camera/{panel_id}/")
    fun registerCamera(@Path("panel_id") panelId: Int, @Body body: RegisterCameraRequest): Call<RegisterCameraResponse>

    @PATCH("/camera/{panel_id}/{device_id}/")
    fun updateCamera(@Path("panel_id") panelId: Int,
                     @Path("device_id") deviceId: String,
                     @Body body: RegisterCameraRequest): Call<RegisterCameraResponse>

    @DELETE("/camera/{panel_id}/{device_uid}/")
    fun deleteCamera(@Path("panel_id") panelId: Int,
                     @Path("device_uid") id: String): Call<ResponseBody>

    @GET("/camera/{panel_id}/{device_id}/")
    fun isCameraExist(@Path("panel_id") panelId: Int,
                      @Path("device_id") id: String): Call<ResponseBody>

    @GET("/camera/{panel_id}/{device_id}/videoevents/")
    fun getCameraVideoEvents(@Path("panel_id") panelId: Int,
                             @Path("device_id") deviceId: String,
                             @Query("page") page: Int,
                             @Query("page_size") pageSize: Int,
                             @Query("ordering") ordering: String? = null): Call<PaginatedResponse<VideoEvent>>

    @GET("/camera/videoevents/{event_id}/")
    fun getVideoEventWithThumbnail(@Path("event_id") eventId: String): Call<VideoEventResponse>

    @DELETE("/camera/videoevents/{event_id}/")
    fun deleteVideoEvent(@Path("event_id") eventId: String): Call<ResponseBody>

    @GET("/camera/videoevents/{event_id}/{video_name}/stream/")
    fun getCameraStream(@Path("event_id") eventId: String,
                        @Path("video_name") videoName: String): Call<StreamData>
}