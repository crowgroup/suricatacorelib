package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class AcceptAccessRequest(@SerializedName("lock_id") val lockId: String? = null)
