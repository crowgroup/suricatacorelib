package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigCameraPirCT2035Request(
    @SerializedName("num_pictur")
    val numberOfPictures: Int = 2,
    @SerializedName("conf_time_1p_2p_53")
    val confTime1p2p53: Int = 1,
    @SerializedName("pict_rate")
    val pictureRate: Int = 1,
    @SerializedName("quality")
    val quality: Int = 4,
    @SerializedName("diff_jpeg")
    val diffJpeg: Boolean = false,
    @SerializedName("sharpness")
    val sharpness: Boolean = true,
    @SerializedName("pls_dir_mode_53")
    val plsDirMode53: Int = 1,
    @SerializedName("time_2p_3p_53")
    val time2p3p53: Int = 4,
    @SerializedName("time_1p_2p_53")
    val time1p2p53: Int = 6,
    @SerializedName("camera")
    val camera: Boolean = true,
    @SerializedName("gain_level")
    var gainLevel: Int = 2,
    @SerializedName("pet_immuni")
    var petImmuni: Boolean = false,
    @SerializedName("hold_off")
    val holdOff: Int = 4,
    @SerializedName("res_color")
    val resColor: Int = 3,
    @SerializedName("zone_led")
    val enableLed: Boolean = true,
    @SerializedName("infrared_leds")
    val enableInfraredLeds: Boolean = true,
    @SerializedName("contrast")
    val contrast: Boolean = true,
    @SerializedName("num_pulses")
    val numPulses: Int = 1
)

/*
 "device_config": {
            "num_pictur": 2,
            "conf_time_1p_2p_53": 1,
            "pict_rate": 1,
            "quality": 4,
            "diff_jpeg": false,
            "sharpness": true,
            "pls_dir_mode_53": 1,
            "time_2p_3p_53": 4,
            "time_1p_2p_53": 6,
            "camera": true,
            "gain_level": 2,
            "pet_immuni": false,
            "hold_off": 4,
            "res_color": 3,
            "zone_led": true,
            "infrared_leds": true,
            "contrast": true,
            "num_pulses": 1
        }
 */