package com.crowcloud.gateway.suricataCore.network.model.response

import com.crowcloud.gateway.suricataCore.R
import com.crowcloud.gateway.suricataCore.util.CrowDeviceType
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class Extender(
    override var deviceType: CrowDeviceType = CrowDeviceType.EXTENDER,

    @SerializedName("tamper_alarm")
    val tamperAlarm: Boolean = false,

    @SerializedName("battery_low")
    val batteryLow: Boolean = false,

    @SerializedName("battery")
    var battery: Int = 0,

    @SerializedName("missing")
    var isMissing: Boolean = false,

    @SerializedName("ac_fail")
    var acFail: Boolean = false,

    @SerializedName("trouble_alarm")
    var troubleAlarm: Boolean = false

    ) : BaseDevice() {

        constructor(id: Int, name: String) : this() {
            this.id = id
            this.name = name
        }

        override val troublesList: ArrayList<String>
            get() {
                val troubles = super.troublesList
                if (tamperAlarm) troubles.add("tamper_alarm")
                if (batteryLow) troubles.add("battery_low")
                if (troubleAlarm) troubles.add("trouble_alarm")
                if (isMissing) troubles.add("missing")
                if (acFail && battery <= 0) troubles.add("ac_fail")
                return troubles
            }

        fun getTroubles(): ArrayList<Int> {
            val arr = ArrayList<Int>()
            if (tamperAlarm) arr.add(R.string.trouble_tamper_alarm)
            if (batteryLow) arr.add(R.string.trouble_low_battery)
            if (troubleAlarm) arr.add(R.string.trouble_alarm)
            if (isMissing) arr.add(R.string.trouble_missing)
            if (acFail && battery <= 0) arr.add(R.string.power_failure)
            return arr
        }
}