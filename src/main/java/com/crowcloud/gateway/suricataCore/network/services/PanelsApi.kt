package com.crowcloud.gateway.suricataCore.network.services


import com.crowcloud.gateway.suricataCore.network.model.request.ControlPanelRequest
import com.crowcloud.gateway.suricataCore.network.model.request.InstallerAccessRequestedTime
import com.crowcloud.gateway.suricataCore.network.model.request.PanelCredentials
import com.crowcloud.gateway.suricataCore.network.model.request.PanicRequestBody
import com.crowcloud.gateway.suricataCore.network.model.response.ControlPanelUser
import com.crowcloud.gateway.suricataCore.network.model.response.MaintenanceState
import com.crowcloud.gateway.suricataCore.network.model.response.NotificationsEmailData
import com.crowcloud.gateway.suricataCore.network.model.response.PaginatedResponse
import com.crowcloud.gateway.suricataCore.network.model.response.PanelConnectionState
import com.crowcloud.gateway.suricataCore.network.model.response.PanelInfo
import com.crowcloud.gateway.suricataCore.network.model.response.PanelResponse
import com.crowcloud.gateway.suricataCore.network.model.response.PanicResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by michael on 2/4/18.
 */

interface PanelsApi {

    @GET("/panel_users/")
    suspend fun getPanelList(
                            @Query("page") page: Int,
                            @Query("page_size") pageSize: Int,
                            @Query("ordering") ordering: String? = null):
            PaginatedResponse<PanelInfo>

    @GET("/panels/")
    suspend fun getPanels(): PanelResponse

    @DELETE("/panels/{id}/")
    suspend fun deletePanel(@Path("id") id: Int): Response<ResponseBody>

    @PUT("/panel_users/{id}/")
    suspend fun updatePanel(@Path("id") panelUserId: Int, @Body requestBody: PanelInfo): PanelInfo

    @PATCH("/panel_users/{id}/")
    suspend fun confirmPanelCredentials(@Header("X-Crow-CP-remote") remoteAccessPassword: String,
                                        @Path("id") panelId: Int,
                                        @Body requestBody: PanelCredentials): PanelInfo

    @PATCH("/panel_users/{id}/")
    suspend fun updatePanelLocation(@Header("X-Crow-CP-remote") remoteAccessPassword: String,
                                       @Header("X-Crow-CP-User") userId: String,
                                       @Path("id") panelId: Int,
                                       @Body requestBody: PanelCredentials): PanelInfo

    @POST("/panels/")
    suspend fun createPanel(@Body requestBody: ControlPanelRequest): PanelInfo

    @GET("/panel_users/{id}/")
    suspend fun getPanelByUserId(@Path("id") panelUserId: Int): PanelInfo

    @GET("/panels/{id}/panel_users/")
    suspend fun getPanelUserAccounts(
                            @Path("id") id: Int,
                            @Query("page") page: Int,
                            @Query("page_size") pageSize: Int,
                            @Query("ordering") ordering: String? = null):
            PaginatedResponse<ControlPanelUser>

    @DELETE("/panels/{id}/panel_users/{account_id}/")
    suspend fun deletePanelUserAccount(@Path("id") id: Int, @Path("account_id") accountId: Int):
                                                                            Response<ResponseBody>
    @PUT("/panels/{id}/panic/")
    fun panic(@Path("id") id: Int, @Body requestBody: PanicRequestBody): Call<PanicResponse>

    @PUT("/panels/{id}/panic/")
    suspend fun activatePanic(@Path("id") id: Int, @Body requestBody: PanicRequestBody): PanicResponse

    @GET("/panels/{id}/connection/")
    suspend fun getConnectionState(@Path("id") panelId: Int): PanelConnectionState

    @PUT("/panels/{id}/connection/")
    suspend fun connectToPanel(@Path("id") panelId: Int): PanelConnectionState

    @DELETE("/panels/{id}/connection/")
    suspend fun disconnectPanel(@Path("id") panelId: Int): PanelConnectionState

    //{"maintenance_until":null}
    @GET("/panels/{id}/maintenance/")
    fun getMaintenanceState(@Path("id") panelId: Int): Call<MaintenanceState>

    @POST("/panels/{id}/maintenance/")
    fun openMaintenance(@Path("id") panelId: Int): Call<MaintenanceState>

    @POST("/panels/{mac}/maintenance/")
    fun setMaintenanceDuration(@Path("mac") macAddress: String,
                               @Body body: InstallerAccessRequestedTime): Call<MaintenanceState>

    @DELETE("/panels/{id}/maintenance/")
    fun closeMaintenance(@Path("id") panelId: Int): Call<MaintenanceState>

    @GET("/panels/{id}/maintenance/")
    suspend fun getPanelMaintenanceState(@Path("id") panelId: Int): MaintenanceState

    @POST("/panels/{id}/maintenance/")
    suspend fun openPanelMaintenance(@Path("id") panelId: Int): MaintenanceState

    @POST("/panels/{mac}/maintenance/")
    suspend fun setPanelMaintenanceDuration(@Path("mac") macAddress: String,
                               @Body body: InstallerAccessRequestedTime): MaintenanceState

    @DELETE("/panels/{id}/maintenance/")
    suspend fun closePanelMaintenance(@Path("id") panelId: Int): MaintenanceState

    @GET("/panels/{id}/notifications/{email_id}/")
    fun getNotificationsEmail(
            @Path("id") panelId: Int,
            @Path("email_id") emailId: Int): Call<NotificationsEmailData>

    @DELETE("/panels/{id}/notifications/{email_id}/")
    fun deleteNotificationsEmail(
            @Path("id") panelId: Int,
            @Path("email_id") emailId: Int): Call<NotificationsEmailData>

    @PUT("/panels/{id}/notifications/{email_id}/")
    fun updateNotificationsEmail(
            @Path("id") panelId: Int,
            @Path("email_id") emailId: Int,
            @Body body: NotificationsEmailData): Call<NotificationsEmailData>

    @POST("/panels/{id}/notifications/")
    fun addNotificationsEmail(@Path("id") panelId: Int,
                              @Body body: NotificationsEmailData): Call<NotificationsEmailData>

    @POST("/panels/{id}/connection/restart_router/")
    suspend fun restartRouter(@Path("id") panelId: Int): Response<ResponseBody>
}
