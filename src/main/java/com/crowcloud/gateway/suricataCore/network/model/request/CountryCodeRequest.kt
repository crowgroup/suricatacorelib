package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class CountryCodeRequest(
    @SerializedName("countryCode")
    val countryCode: String?
)
