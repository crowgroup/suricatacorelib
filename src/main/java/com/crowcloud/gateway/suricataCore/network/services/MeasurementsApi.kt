package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.response.Measurement
import com.crowcloud.gateway.suricataCore.network.model.response.MeasuresByZone
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by michael on 2/4/18.
 */

interface MeasurementsApi {

    @GET("/panels/{panel_id}/dect/measurements/latest/by_zone/")
    fun getLatestMeasurementsByZone(@Path("panel_id") panelId: Int): Call<HashMap<String, MeasuresByZone>>

    @GET("/panels/{panel_id}/dect/measurements/latest/by_zone/")
    suspend fun getLatestMeasurements(@Path("panel_id") panelId: Int): HashMap<String, MeasuresByZone>

    //    @GET("/panels/{panel_id}/measurements/{device_type}/{device_id}/interface/{interface_id}/")
//    fun getMeasurementsByPeriod(@Path("panel_id") panelId: Int,
//                                @Path("device_type") deviceType: Measurement.DeviceType,
//                                @Path("device_id") deviceId: Int,
//                                @Path("interface_id") interfaceId: Int,
//                                @Query("duration") duration: String,
//                                @Query("offset") offset: Int = 0): Call<List<Measurement>>

    @GET("/panels/{panel_id}/dect/measurements/zone/{device_id}/interface/{interface_id}/")
    suspend fun getMeasurementsByPeriod(@Path("panel_id") panelId: Int,
                                        @Path("device_id") deviceId: Int,
                                        @Path("interface_id") interfaceId: Int,
                                        @Query("duration") duration: String,
                                        @Query("offset") offset: Int = 0): List<Measurement>

}