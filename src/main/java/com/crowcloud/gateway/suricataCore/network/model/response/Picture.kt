package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.util.*

/**
 * Created by michael on 2/18/18.
 */
@Parcelize
data class Picture(

    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("control_panel")
    var controlPanel: Int = 0,

    @SerializedName("created")
    var created: Date? = null,

    @SerializedName("panel_time")
    var panelTime: Date? = null,

    @SerializedName("zone")
    var zone: Int = 0,

    @SerializedName("zone_name")
    var zoneName: String? = null,

    @SerializedName("event")
    var event: Int = 0,

    @SerializedName("type")
    var type: Int = 0,

    @SerializedName("pic_total")
    var picTotal: Int = 0,

    @SerializedName("pic_index")
    var picIndex: Int = 0,

    @SerializedName("url")
    var url: String? = null,

    @SerializedName("key")
    var key: String? = null
) : Parcelable