package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class User( //Authenticated user
    @SerializedName("pk")
    val pk: Int = 0,

    @SerializedName("username")
    val username: String = "",

    @SerializedName("email")
    val email: String = "",

    @SerializedName("first_name")
    var firstName: String = "",

    @SerializedName("last_name")
    var lastName: String = "",

    @SerializedName("is_superuser")
    val isSuperuser: Boolean = false,

    @SerializedName("is_staff")
    val isStaff: Boolean = false,

    @SerializedName("permissions")
    val permissions: List<String> = emptyList(),

    @SerializedName("groups")
    val groups: List<Int> = emptyList(),

    @SerializedName("panel_group")
    val panelGroup: PanelGroup? = null,

    @SerializedName("deploy_level")
    val deployLevel: Int = 0,

    @SerializedName("employee_info")
    val employeeInfo: EmployeeInfo? = null
) : Parcelable

