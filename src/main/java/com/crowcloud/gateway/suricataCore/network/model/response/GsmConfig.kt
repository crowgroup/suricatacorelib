package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class GsmConfig {

    @SerializedName("password")
    private val password: String? = null

    @SerializedName("pin_code")
    private val pinCode: String? = null

    @SerializedName("sms")
    private val sms: Boolean = false

    @SerializedName("ussd")
    private val ussd: String? = null

    @SerializedName("id")
    private val id: String? = null

    @SerializedName("user")
    private val user: String? = null

    @SerializedName("enabled")
    private val enabled: Boolean = false

    @SerializedName("apn")
    private val apn: String? = null
}