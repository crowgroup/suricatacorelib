package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigNewSmokeHeatSyncDetectorRequest(
    @SerializedName("enable_burglary_alarm")
    val enableBurglaryAlarm: Boolean = false,
    @SerializedName("del_time")
    val delTime: Int = 10,
    @SerializedName("sensitivity")
    val sensitivity: Int = 60,
    @SerializedName("enable_fire_alarm")
    val enableFireAlarm: Boolean = true,
    @SerializedName("enable_water_alarm")
    val enableWaterAlarm: Boolean = false,
    @SerializedName("indication")
    val indication: Boolean = true,
    @SerializedName("heat_enabled")
    val enableHeat: Boolean = true,
    @SerializedName("buzzer_loudness")
    val buzzerLoudness: Int = 3
)

/*

"device_config": {
    "enable_burglary_alarm": false,
    "del_time": 10,
    "sensitivity": 60,
    "enable_fire_alarm": true,
    "enable_water_alarm": false,
    "indication": true,
    "heat_enabled": true,
    "buzzer_loudness": 3
}
 */

