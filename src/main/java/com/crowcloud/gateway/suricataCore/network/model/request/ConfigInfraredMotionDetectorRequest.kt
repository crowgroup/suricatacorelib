package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigInfraredMotionDetectorRequest(
    @SerializedName("zone_led")
    val enableLed: Boolean = true,
    @SerializedName("pet_immuni")
    var petImmuni: Boolean = false,
    @SerializedName("gain_level")
    var gainLevel: Int = 2,
    @SerializedName("supervision")
    val supervision: Int = 0,
    @SerializedName("num_pulses")
    val numPulses: Int = 2
)


/*
   "device_config": {
            "zone_led": true,
            "pet_immuni": false,
            "gain_level": 2,
            "supervision": 0,
            "num_pulses": 1
        },
 */