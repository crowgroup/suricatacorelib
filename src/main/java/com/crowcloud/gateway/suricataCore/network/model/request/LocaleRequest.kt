package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class LocaleRequest(@SerializedName("locale") val locale: String? = null)
