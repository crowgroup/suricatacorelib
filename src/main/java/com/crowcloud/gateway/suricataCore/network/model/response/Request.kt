package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName

abstract class Request: Parcelable {

    @SerializedName("accessRequestId")
    val accessRequestId: String? = null
}