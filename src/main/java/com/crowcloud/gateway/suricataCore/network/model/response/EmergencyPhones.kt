package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class EmergencyPhones(
    @SerializedName("phone")
    var phones: MutableList<String> = mutableListOf()
)