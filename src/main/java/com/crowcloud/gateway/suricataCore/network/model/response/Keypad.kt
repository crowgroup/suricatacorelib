package com.crowcloud.gateway.suricataCore.network.model.response

import com.crowcloud.gateway.suricataCore.util.CrowDeviceType
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.util.ArrayList

@Parcelize
class Keypad(

    override var deviceType: CrowDeviceType = CrowDeviceType.KEYPAD,

    @SerializedName("tamper_alarm")
    val tamperAlarm: Boolean = false,

    @SerializedName("battery_low")
    val batteryLow: Boolean = false,

    @SerializedName("fast_arm")
    val fastArm: Boolean = false,

    @SerializedName("fast_stay_arm")
    val fastStayArm: Boolean = true,

    @SerializedName("enable_beeps")
    val enableBeeps: Boolean = false,

    @SerializedName("armed_indication")
    val armedIndication: Boolean = false,

    @SerializedName("temperature")
    val temperature: Int = 0,

    @SerializedName("active")
    val isActive: Boolean = false,

    @SerializedName("missing")
    val isMissing: Boolean = false,

    @SerializedName("type_friendly")
    val typeFriendly: String = "0",

    @SerializedName("device_config")
    var config: KeypadConfig? = null,

    @SerializedName("areas")
    var areas: List<Int>? = null

    ) : BaseDevice() {

    constructor(id: Int, name: String) : this() {
        this.id = id
        this.name = name
    }
    override val troublesList: ArrayList<String>
        get() {
            val troubles = super.troublesList
            if (tamperAlarm) troubles.add("tamper_alarm")
            if (batteryLow) troubles.add("battery_low")
            if (isMissing) troubles.add("missing")
            if (!isActive) troubles.add("active")
            return troubles
        }
}

//POST /keypads -- PUT,DELETE /keypads/<keypad_id>
//
//{
//    "name": "Keypad {id}",
//    "device_id": 0,
//    "type": 0,
//    "areas": [0, ],
//    "fast_arm": false,
//    "fast_stay_arm": false,
//    "enable_beeps": false,
//    "armed_indication": false,
//    "device_config": {}, // Device Type Dependant
//}