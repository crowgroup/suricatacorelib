package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class VideoEvents(@SerializedName("videoevents") val videoEvents: List<VideoEvent>)

class VideoEvent {

    @SerializedName("id")
    var id: String? = null
    @SerializedName("timestamp")
    var timestamp: Long? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("meta")
    var meta: Meta? = null
    @SerializedName("device")
    var device: IpCamera.Device? = null
    @SerializedName("thumbnail_id")
    var thumbnailId: String? = null
    @SerializedName("images")
    var images: List<Image>? = null
    @SerializedName("videos")
    var videos: List<Video>? = null
    @SerializedName("thumbnail_base64")
    var thumbnailBase64: String? = null

    class Image {
        @SerializedName("upload_id")
        var uploadId: String? = null
        @SerializedName("size")
        var size: Int? = null
        @SerializedName("format")
        var format: String? = null
        @SerializedName("timestamp")
        var timestamp: Long? = null
        @SerializedName("time_rel")
        var timeRel: Long? = null
    }

    class Meta {
        @SerializedName("trigger")
        var trigger: String? = null
        @SerializedName("navi")
        var navi: Navi? = null
    }

    class Navi {
        @SerializedName("lon")
        var lon: Float? = null
        @SerializedName("lat")
        var lat: Float? = null
        @SerializedName("s")
        var s: Float? = null
    }

    class Status {
        @SerializedName("state")
        var state: String? = null
        @SerializedName("bytes_uploaded")
        var bytesUploaded: Int? = null
        @SerializedName("bytes_total")
        var bytesTotal: Int? = null
        @SerializedName("progress_human")
        var progressHuman: String? = null
    }

    class Video {
        @SerializedName("upload_id")
        var uploadId: String? = null
        @SerializedName("size")
        var size: Int? = null
        @SerializedName("format")
        var format: String? = null
        @SerializedName("name")
        var name: String? = null
        @SerializedName("transcoded")
        var transcoded: Boolean? = null
        @SerializedName("start_time")
        var startTime: Long? = null
        @SerializedName("duration")
        var duration: Int? = null
        @SerializedName("status")
        var status: Status? = null
    }
}