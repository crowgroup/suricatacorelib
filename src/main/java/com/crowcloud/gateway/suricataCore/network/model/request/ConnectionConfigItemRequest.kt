package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

class ConnectionConfigItemRequest {

    @SerializedName("id")
    var id: Int = 0

    @SerializedName("password")
    var password: String? = null

    @SerializedName("security")
    var security: Int = 0

    @SerializedName("ssid")
    var ssid: String? = null
}