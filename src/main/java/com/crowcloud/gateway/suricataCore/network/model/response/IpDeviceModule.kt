package com.crowcloud.gateway.suricataCore.network.model.response

import com.crowcloud.gateway.suricataCore.util.IpDeviceType

interface IpDeviceModule {
    fun getDeviceType(): IpDeviceType
    fun getDeviceId(): String?
    fun getName(): String?
    fun setName(name: String)
}