package com.crowcloud.gateway.suricataCore.network.model.response

enum class ActionType {
    Area,
    Output
}