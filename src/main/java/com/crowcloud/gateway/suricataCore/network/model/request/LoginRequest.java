package com.crowcloud.gateway.suricataCore.network.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by michael on 2/4/18.
 */

public class LoginRequest {

    @SerializedName("username")
    public String username;

    @SerializedName("password")
    public String password;

    @SerializedName("grant_type")
    public String grantType;

    @SerializedName("client_id")
    public String clientId;

    @SerializedName("client_secret")
    public String clientSecret;

    public LoginRequest() {
    }

    public LoginRequest(String username, String password, String grantType, String clientId, String clientSecret) {
        this.username = username;
        this.password = password;
        this.grantType = grantType;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }
}
