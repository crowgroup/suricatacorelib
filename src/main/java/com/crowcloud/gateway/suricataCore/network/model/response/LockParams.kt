package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class LockParams(
    @SerializedName("user_code_blocking")
    val userCodeBlocking: Boolean = false,

    @SerializedName("alarm_system")
    val alarmSystem: Boolean = false,

    @SerializedName("volume")
    val volume: Int? = null,

    @SerializedName("auto_relock")
    val autoRelock: Boolean = false,

    @SerializedName("alarm_mode")
    val alarmMode: Int? = null,

    @SerializedName("alarm_hold_off_time")
    val alarmHoldOffTime: Int? = null
)

/*
    {
    "user_code_blocking": false,
    "alarm_system": false,
    "volume": 1,
    "auto_relock": false,
    "alarm_mode": 0,
    "alarm_hold_off_time": 100
}
 */
