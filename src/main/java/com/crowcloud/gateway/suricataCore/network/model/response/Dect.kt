package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class Dect {
    @SerializedName("id")
    var id = 0

    @SerializedName("name")
    var name: String? = null
}