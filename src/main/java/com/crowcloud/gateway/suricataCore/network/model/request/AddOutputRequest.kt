package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class AddOutputRequest(
    @SerializedName("link_type")
    val linkType: String = "1",
    @SerializedName("device_id")
    val serialNumber: Int,
    @SerializedName("name")
    val outputName: String
)