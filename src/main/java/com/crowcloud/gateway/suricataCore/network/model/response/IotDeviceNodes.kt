package com.crowcloud.gateway.suricataCore.network.model.response


import com.google.gson.annotations.SerializedName

data class IotDeviceNodes(
        @SerializedName("switch")
        val switch: NodeData? = null
) {

    data class NodeData(
            @SerializedName("on")
            val on: String?,
            @SerializedName("name")
            val name: String?,
            @SerializedName("type")
            val type: String?,
            @SerializedName("properties")
            val properties: String?
    )
}