package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class LoginMeariCloudResponse(
    @SerializedName("redirect") val redirect:  MeariRedirectResponse,
    @SerializedName("login") val login: MeariLoginResponse?
)

data class  MeariRedirectResponse(
    @SerializedName("result") val result: RedirectResult,
    @SerializedName("t") val t: Long,
    @SerializedName("resultCode") val resultCode: String
)

data class RedirectResult(
    @SerializedName("pointUrlNew") val pointUrlNew: String,
    @SerializedName("countryCode") val countryCode: String,
    @SerializedName("pointUrl") val pointUrl: String,
    @SerializedName("apiServer") val apiServer: String,
    @SerializedName("partnerId") val partnerId: Int,
    @SerializedName("dcCode") val dcCode: String,
    @SerializedName("gwCode") val gwCode: String,
    @SerializedName("gwUrl") val gwUrl: String,
    @SerializedName("pfApi") val pfApi: PfApi
)

data class PfApi(
    @SerializedName("openapi") val openapi: Openapi,
    @SerializedName("platform") val platform: Platform
)

data class Openapi(
    @SerializedName("domain") val domain: String
)

data class Platform(
    @SerializedName("signature") val signature: String,
    @SerializedName("domain") val domain: String
)

data class MeariLoginResponse(
    @SerializedName("result") val result: LoginResult,
    @SerializedName("t") val t: Long,
    @SerializedName("resultCode") val resultCode: String
)

data class LoginResult(
    @SerializedName("userID") val userID: Long,
    @SerializedName("nickName") val nickName: String,
    @SerializedName("userAccount") val userAccount: String,
    @SerializedName("password") val password: String?,
    @SerializedName("soundFlag") val soundFlag: Int,
    @SerializedName("imageUrl") val imageUrl: String,
    @SerializedName("lngType") val lngType: String?,
    @SerializedName("tyPassword") val tyPassword: String?,
    @SerializedName("sourceApp") val sourceApp: String?,
    @SerializedName("openID") val openID: String?,
    @SerializedName("deviceSecret") val deviceSecret: String?,
    @SerializedName("deviceSecretDesk") val deviceSecretDesk: String?,
    @SerializedName("countryCode") val countryCode: String,
    @SerializedName("phoneCode") val phoneCode: String,
    @SerializedName("createDate") val createDate: String?,
    @SerializedName("updateDate") val updateDate: String?,
    @SerializedName("timeFlag") val timeFlag: String?,
    @SerializedName("iotId") val iotId: String?,
    @SerializedName("iotProductKey") val iotProductKey: String?,
    @SerializedName("iotDeviceName") val iotDeviceName: String?,
    @SerializedName("iotDeviceSecret") val iotDeviceSecret: String?,
    @SerializedName("loginType") val loginType: Int,
    @SerializedName("userRecycle") val userRecycle: String?,
    @SerializedName("iotType") val iotType: Int,
    @SerializedName("thirdLoginId") val thirdLoginId: String?,
    @SerializedName("emailAuthFlag") val emailAuthFlag: String,
    @SerializedName("promotion") val promotion: Int,
    @SerializedName("iot") val iot: Iot,
    @SerializedName("userToken") val userToken: String,
    @SerializedName("ringDuration") val ringDuration: String,
    @SerializedName("jpushAlias") val jpushAlias: String,
    @SerializedName("iosUserId") val iosUserId: String?
)

data class Iot(
    @SerializedName("pfKey") val pfKey: PfKey,
    @SerializedName("mqtt_aws") val mqttAws: MqttAws,
    @SerializedName("mqtt") val mqtt: Mqtt,
    @SerializedName("iotVersion") val iotVersion: Int
)

data class PfKey(
    @SerializedName("accessid") val accessid: String,
    @SerializedName("platformSignature") val platformSignature: String,
    @SerializedName("platformDomain") val platformDomain: String,
    @SerializedName("accesskey") val accesskey: String,
    @SerializedName("openapiDomain") val openapiDomain: String
)

data class MqttAws(
    @SerializedName("host") val host: String,
    @SerializedName("port") val port: String,
    @SerializedName("clientId") val clientId: String,
    @SerializedName("subTopic") val subTopic: String,
    @SerializedName("cognitoPoolId") val cognitoPoolId: String,
    @SerializedName("cognitoRegion") val cognitoRegion: String,
    @SerializedName("iotPolicyName") val iotPolicyName: String,
    @SerializedName("certId") val certId: String
)

data class Mqtt(
    @SerializedName("iotId") val iotId: String,
    @SerializedName("host") val host: String,
    @SerializedName("port") val port: String,
    @SerializedName("iotToken") val iotToken: String,
    @SerializedName("pk") val pk: String,
    @SerializedName("dn") val dn: String,
    @SerializedName("clientId") val clientId: String,
    @SerializedName("deviceSecret") val deviceSecret: String,
    @SerializedName("crtUrls") val crtUrls: String,
    @SerializedName("keepalive") val keepalive: Int,
    @SerializedName("protocol") val protocol: String,
    @SerializedName("subTopic") val subTopic: String,
    @SerializedName("region") val region: String
)


