package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigSmartSiren2Request(

    @SerializedName("led_flasher")
    val ledFlasher: Int = 0,
    @SerializedName("sndr_loud")
    val sndrLoud: Int = 3
)


/*
{
  "led_flasher": 0,
  "sndr_loud": 3
}
 */