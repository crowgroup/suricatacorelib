package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class EmployeeInfo(
    @SerializedName("id")
    val id: Int = 0,

    @SerializedName("group")
    val group: PanelGroup? = null,

    @SerializedName("deploy_level")
    val deployLevel: Int = 0,

    @SerializedName("is_developer")
    val isDeveloper: Boolean = false,

    @SerializedName("min_firmware_view_level")
    val minFirmwareViewLevel: Int? = null,

    @SerializedName("max_firmware_deploy_level")
    val maxFirmwareDeployLevel: Int? = null
) : Parcelable
