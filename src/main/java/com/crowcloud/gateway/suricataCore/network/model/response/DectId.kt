package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class DectId(
    @SerializedName("ipud")
    val ipud: String? = null,

    @SerializedName("unit_id")
    val unitId: Int = 0
) : Parcelable