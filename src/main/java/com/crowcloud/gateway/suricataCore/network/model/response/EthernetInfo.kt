package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class EthernetInfo {

    @SerializedName("ip")
    @Expose
    val ip: String? = null

    @SerializedName("mac")
    @Expose
    val mac: String? = null

    @SerializedName("mask")
    @Expose
    val mask: String? = null

    @SerializedName("gateway")
    @Expose
    val gateway: String? = null

    @SerializedName("id")
    @Expose
    val id: String? = null
}
