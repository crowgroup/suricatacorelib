package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName
import java.util.*

data class MaintenanceState(@SerializedName("maintenance_until") val maintenanceUntil: Date?)
