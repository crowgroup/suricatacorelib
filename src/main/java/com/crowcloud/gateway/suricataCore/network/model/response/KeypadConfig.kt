package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class KeypadConfig(
    @SerializedName("kpd_loud")
    val kpdLoud: Int = 3,
    @SerializedName("pin_code_size")
    var pinCodeSize: Int = 4,
    @SerializedName("panic_alarm_audiable")
    val panicAlarmAudible: Boolean = false,
    @SerializedName("panic_alarm_visual")
    val panicAlarmVisual: Boolean = true,
    @SerializedName("button_bush_volume")
    val buttonBushVolume: Int = 3,
    @SerializedName("siren_timeout")
    val sirenTimeout: Int = 0,
    @SerializedName("24h_alarm_enable")
    val alarm24hEnable: Boolean = false,
    @SerializedName("pin_processing_type")
    val pinProcessingType: Int = 1
) : Parcelable
