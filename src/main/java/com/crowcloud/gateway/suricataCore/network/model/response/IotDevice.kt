package com.crowcloud.gateway.suricataCore.network.model.response


import com.google.gson.annotations.SerializedName

data class IotDevice(
        @SerializedName("created")
        val created: String? = null,
        @SerializedName("device")
        val device: Device? = null,
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("is_online")
        val isOnline: Boolean? = null,
        @SerializedName("mac")
        var mac: String?,
        @SerializedName("name")
        var name: String?,
        @SerializedName("type")
        var type: String?,
        @SerializedName("uid")
        var uid: String?) {

    data class Device(
            @SerializedName("id")
            val id: String?,
            @SerializedName("ip")
            val ip: String?,
            @SerializedName("mac")
            val mac: String?,
            @SerializedName("name")
            val name: String?,
            @SerializedName("state")
            val state: String?,
            @SerializedName("uptime")
            val uptime: String?
    )
}