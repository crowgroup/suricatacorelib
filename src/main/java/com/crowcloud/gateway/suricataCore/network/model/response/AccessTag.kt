package com.crowcloud.gateway.suricataCore.network.model.response

import com.crowcloud.gateway.suricataCore.util.CrowDeviceType
import kotlinx.parcelize.Parcelize

@Parcelize
data class AccessTag(
    override var deviceType: CrowDeviceType = CrowDeviceType.ACCESS_TAG,
    val accessTagUserName: String, val accessTagId: String
        ) : BaseDevice()