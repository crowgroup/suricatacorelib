package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class AccessTimeWindow() : Parcelable {

    @SerializedName("startValidity")
    var startValidity: String? = null

    @SerializedName("endValidity")
    var endValidity: String? = null

    constructor(parcel: Parcel) : this() {
        startValidity = parcel.readString()
        endValidity = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(startValidity)
        parcel.writeString(endValidity)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AccessTimeWindow> {
        override fun createFromParcel(parcel: Parcel): AccessTimeWindow {
            return AccessTimeWindow(parcel)
        }

        override fun newArray(size: Int): Array<AccessTimeWindow?> {
            return arrayOfNulls(size)
        }
    }
}