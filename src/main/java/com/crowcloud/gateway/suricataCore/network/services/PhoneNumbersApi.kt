package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.response.EmergencyPhones
import com.crowcloud.gateway.suricataCore.network.model.response.ReportChannel
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.PUT
import retrofit2.http.Path

interface PhoneNumbersApi {

    //Emergency phone numbers

    @GET("/panels/{panel_id}/communications/dect/")
    suspend fun getEmergencyPhones(@Path("panel_id") panelId: Int): EmergencyPhones

    @PUT("/panels/{panel_id}/communications/dect/")
    suspend fun updateEmergencyPhones(@Header("X-Crow-CP-User") userCode: String,
                                      @Path("panel_id") panelId: Int,
                                      @Body phones: EmergencyPhones): EmergencyPhones

    //Report channel phones

    @GET("/panels/{panel_id}/report_channels/")
    suspend fun getReportChannelPhones(@Path("panel_id") panelId: Int): List<ReportChannel>
    @PUT("/panels/{panel_id}/report_channels/{channel_id}/")
    suspend fun updateReportChannelPhone(@Header("X-Crow-CP-User") userCode: String?,
                                         @Path("panel_id") panelId: Int,
                                         @Path("channel_id") reportChannelId: Int,
                                         @Body reportChannel: ReportChannel): ReportChannel

}
