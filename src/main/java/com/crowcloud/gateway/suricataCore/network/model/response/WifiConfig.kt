package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class WifiConfig {

    @SerializedName("conn_config")
    var connectionConfig: List<ConnectionConfigItem>? = null

    @SerializedName("id")
    var id: String? = null

    @SerializedName("enabled")
    var enabled: Boolean = false
}