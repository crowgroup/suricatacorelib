package com.crowcloud.gateway.suricataCore.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response
import android.net.TrafficStats

class TrafficStatsInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        // Set a unique tag for the network operation
        TrafficStats.setThreadStatsTag(Thread.currentThread().hashCode())
        return try {
            // Proceed with the network request
            chain.proceed(chain.request())
        } finally {
            // Clear the tag after the network operation is done
            TrafficStats.clearThreadStatsTag()
        }
    }
}
