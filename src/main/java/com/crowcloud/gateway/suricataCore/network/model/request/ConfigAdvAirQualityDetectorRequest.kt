package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigAdvAirQualityDetectorRequest(
    @SerializedName("threshold_temperature_low")
    val thresholdTemperatureLow: Int = 5,
    @SerializedName("threshold_humidity_high")
    val thresholdHumidityHigh: Int = 60,
    @SerializedName("enable_temperature")
    val enableTemperature: Boolean = true,
    @SerializedName("supervision")
    val supervision: Int = 2,
    @SerializedName("threshold_air_quality")
    val thresholdAirQuality: Int = 2,
    @SerializedName("enable_airquality")
    val enableAirQuality: Boolean = true,
    @SerializedName("enable_humidity")
    val enableHumidity: Boolean = true,
    @SerializedName("threshold_temperature_high")
    val thresholdTemperatureHigh: Int = 35,
    @SerializedName("threshold_humidity_low")
    val thresholdHumidityLow: Int = 20,
    @SerializedName("enable_indication")
    val enableIndication: Boolean = true
)

/*
 "device_config": {
            "threshold_temperature_low": 5,
            "threshold_humidity_high": 60,
            "enable_temperature": true,
            "supervision": 2,
            "threshold_air_quality": 2,
            "enable_airquality": true,
            "enable_humidity": true,
            "threshold_temperature_high": 35,
            "threshold_humidity_low": 20,
            "enable_indication": true
        }
 */
