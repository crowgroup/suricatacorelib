package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ToggleLockRequest(@SerializedName("locked") val locked: Boolean? = null)

