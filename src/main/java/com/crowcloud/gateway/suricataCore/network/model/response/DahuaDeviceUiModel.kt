package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.crowcloud.gateway.suricataCore.util.IpDeviceType
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue

@Parcelize
class DahuaDeviceUiModel(val device: @RawValue DahuaIpCamera): IpDeviceModule, Parcelable {
    override fun getDeviceType(): IpDeviceType {
        return IpDeviceType.DAHUA_CAMERA
    }
    override fun getDeviceId(): String? {
        return device.deviceId
    }
    override fun getName(): String? {
        return device.deviceName
    }

    override fun setName(name: String) {
        //
    }

    val devicePassword: String?
        get() = device.devicePassword
}