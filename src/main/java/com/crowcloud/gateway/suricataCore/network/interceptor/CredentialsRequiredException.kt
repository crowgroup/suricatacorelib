package com.crowcloud.gateway.suricataCore.network.interceptor

import okhttp3.Response
import java.io.IOException

class CredentialsRequiredException(
    message: String,
    val response: Response
) : IOException(message)