package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class LockOwner {

    @SerializedName("id")
    val id: String? = null

    @SerializedName("emailAddresses")
    val emailAddresses: ArrayList<String>? = null

    @SerializedName("phoneNumbers")
    val phoneNumbers: ArrayList<String>? = null

    @SerializedName("user")
    val user: Int = 0

    @SerializedName("lockOwnerId")
    val lockOwnerId: String? = null

    @SerializedName("deleted")
    val deleted: String? = null

    @SerializedName("locale")
    val locale: String? = null
}


/*
"owner": {
        "emailAddresses": [
          "tim7@tslp.me"
        ],
        "phoneNumbers": [
          "0501032417"
        ],
        "user": 144,
        "lockOwnerId": "76e48ef7-8371-499a-9267-5247be6105ec",
        "deleted": "string",
        "locale": "string",
        "id": "604658f928cec3c39785f59a"
      }
 */