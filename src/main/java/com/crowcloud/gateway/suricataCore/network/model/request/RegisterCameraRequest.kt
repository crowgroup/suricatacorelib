package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class RegisterCameraRequest(@SerializedName("device_id") val deviceId: String? = null,
                                 @SerializedName("name") val name: String)
