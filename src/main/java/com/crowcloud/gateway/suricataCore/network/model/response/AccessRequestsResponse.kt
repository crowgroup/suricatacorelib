package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class AccessRequestsResponse {

    @SerializedName("result")
    val result: ArrayList<AccessRequest> = ArrayList()
}