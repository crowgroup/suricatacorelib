package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TosData(
    @SerializedName("id")
    val id: Int,
    @SerializedName("url")
    val url: String
) : Parcelable
