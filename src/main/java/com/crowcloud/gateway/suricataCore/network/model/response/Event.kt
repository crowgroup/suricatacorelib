package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by michael on 2/19/18.
 */

data class Event(

    @SerializedName("id")
        val id: Int,

    @SerializedName("created")
        val created: String?,

    @SerializedName("panel_time")
        val panelTime: Date?,

    @SerializedName("cid")
        val cid: EventType?,

    @SerializedName("text")
        val text: String?,

    @SerializedName("control_panel")
        val controlPanel: Int,

    @SerializedName("received_via_control_panel")
        val receivedViaControlPanel: Int?,

    @SerializedName("zone")
        val zone: Int,

    @SerializedName("area")
        val area: Int,

    @SerializedName("pictures")
        val pictures: List<Picture>?,

    @SerializedName("groups")
        val groups: List<String>?,//TODO: change to list of enum : {troubles, configuration, information, arm..}

    @SerializedName("string1")
        val string1: String?,

    @SerializedName("string2")
        val string2: String?

        )

    enum class EventType(val cid: Int, val cIds: Array<Int>? = null) {

        @SerializedName("1570", alternate = ["1383", "1572", "1964", "3130", "3138", "3139", "3381",
                        "3383", "3391", "3570", "3572", "3131"])
        EVENT_ZONE(1570, arrayOf(1383, 1572, 1964, 3130, 3138, 3139, 3381, 3391, 3383, 3570, 3572, 3131)),

        @SerializedName("1302", alternate = ["1311", "1384", "4023",
            "4025", "4026", "1330", "1338", "1923", "1924", "1927", "1928", "1940", "1947"])
        EVENT_BATTERY_LOW(1302, arrayOf(1311, 1384, 4023,
                4025, 4026, 1330, 1338, 1923, 1924, 1927, 1928, 1940, 1947)),

        @SerializedName("3401", alternate = ["3400", "3403", "3409"])
        EVENT_ARMED(3401, arrayOf(3400, 3403, 3409)),

        @SerializedName("3440", alternate = ["3441"])
        EVENT_STAYARM(3440, arrayOf(3441)),

        @SerializedName("1401", alternate = ["1400", "1409", "1403", "1407", "1441"])
        EVENT_DISARMED(1401, arrayOf(1400, 1409, 1403, 1407, 1441)),

        @SerializedName("1100")
        EVENT_MEDICAL_ALARM(1100, arrayOf(4100)),

        @SerializedName("1101")
        EVENT_EMERGENCY_ALARM(1101, arrayOf(4101, 4102)),

        @SerializedName("1110", alternate = ["4110"])
        EVENT_FIRE_ALARM(1110, arrayOf(4110)),

        @SerializedName("1111")
        EVENT_24_HOUR_FIRE_ALARM(1111),

        @SerializedName("1120")
        EVENT_PANIC_ALARM_FROM_KEYPAD(1120),

        @SerializedName("1121")
        EVENT_DURESS_ALARM(1121),

        @SerializedName("1123", alternate = ["4111", "4120"])
        EVENT_PANIC_ALARM(1123, arrayOf(4111, 4120)),

        @SerializedName("1130")
        EVENT_ZONE_ALARM(1130),

        @SerializedName("1131")
        EVENT_PERIMETER_ALARM(1131),

        @SerializedName("1132")
        EVENT_INTERIOR_ALARM(1132),

        @SerializedName("1133")
        EVENT_24_HOUR_ALARM(1133),

        @SerializedName("1136")
        EVENT_OUTDOOR_ALARM(1136),

        @SerializedName("1138")
        EVENT_NEAR_ALARM(1138),

        @SerializedName("1139")
        EVENT_VERIFIED_ALARM(1139),

        @SerializedName("1145")
        EVENT_EXTENDER_TAMPER_ALARM(1145),

        @SerializedName("1151")
        EVENT_24_HOUR_GAS_DETECTED_ALARM(1151),

        @SerializedName("1154")
        EVENT_24_HOUR_WATER_ALARM(1154),

        @SerializedName("1158")
        EVENT_HIGH_TEMPERATURE_ALARM(1158),

        @SerializedName("1159")
        EVENT_LOW_TEMPERATURE_ALARM(1159),

        @SerializedName("1165")
        EVENT_ATTENTION_TEMPERATURE_ALARM(1165),

        @SerializedName("1166")
        EVENT_HIGH_PRESSURE_ALARM(1166),

        @SerializedName("1167")
        EVENT_LOW_PRESSURE_ALARM(1167),

        @SerializedName("1168")
        EVENT_HIGH_HUMIDITY_ALARM(1168),

        @SerializedName("1169")
        EVENT_LOW_HUMIDITY_ALARM(1169),

        @SerializedName("1348", alternate = ["1381", "1934", "1941", "1948", "4158"])
        EVENT_SUPERVISED_ALARM(1348, arrayOf(1381, 1934, 1941, 1948, 4158)),

        //        @SerializedName("1381")
        //        EVENT_SUPERVISION(1381),

        @SerializedName("1375")
        EVENT_PANIC_ALARM_FROM_PENDANT(1375),

        @SerializedName("1391")
        EVENT_INACTIVITY_ALARM(1391),

        @SerializedName("1404")
        EVENT_DELINQUENCY_ALARM(1404),

        @SerializedName("1935", alternate = ["4111"])
        EVENT_AC_FAIL_ALARM(1935, arrayOf(4111)),

        @SerializedName("8006")
        EVENT_RADIO_TAMPER_ALARM(8006),

        @SerializedName("8010", alternate = ["4130"])
        EVENT_ALARM_IN(8010, arrayOf(4130)),

        @SerializedName("4161")
        EVENT_TROUBLE_ALARM(4161),

        @SerializedName("3110", alternate = ["3111", "3100", "3120", "3121", "3133", "3145", "3375"])
        EVENT_RESTORE_ALARM(3110, arrayOf(3111, 3100, 3120, 3121, 3133, 3145, 3375)),

        //Medical Alarm Reset by Remote
        // @SerializedName("3100")
        // EVENT_MEDICAL_ALARM_RESET(3100),

        @SerializedName("1137")
        EVENT_TAMPER(1137),

        @SerializedName("1344")
        EVENT_RF_GAMMING(1344),

        @SerializedName("1351")
        EVENT_ETHERNET(1351),

        @SerializedName("5200")
        EVENT_TAKE_PIC(5200),

        @SerializedName("1461")
        EVENT_CODE_ATTEMPTS_ALARM(1461)
    }
