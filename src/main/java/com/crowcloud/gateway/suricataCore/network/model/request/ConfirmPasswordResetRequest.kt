package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfirmPasswordResetRequest(
    @SerializedName("uid")
    val uid: String,

    @SerializedName("new_password1")
    val newPassword1: String,

    @SerializedName("new_password2")
    val newPassword2: String,

    @SerializedName("token")
    val token: String
)
