package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class PanelUserV92(
    @SerializedName("db_data")
    val dbData: DbData?,

    @SerializedName("mediator_data")
    val mediatorData: MediatorData?
)

data class DbData(
    @SerializedName("id")
    val id: Int,

    @SerializedName("name")
    val panelName: String,

    @SerializedName("role")
    val role: Int,

    @SerializedName("panel_user_index")
    val panelUserIndex: Int,

    @SerializedName("user")
    val user: User?,

    @SerializedName("notifications_filter")
    val notificationsFilter: NotificationsFilter?,

    @SerializedName("notification_language")
    val notificationLanguage: String?
)

data class MediatorData(
    @SerializedName("name")
    val userName: String,

    @SerializedName("user_code")
    val userCode: String,

    @SerializedName("id")
    val id: Int,

    @SerializedName("pendant_id")
    val pendantId: Long,

    @SerializedName("pendant_rssi")
    val pendantRssi: Int,

    @SerializedName("pendant_rssi_bars")
    val pendantRssiBars: Int,

    @SerializedName("pendant_rssi_unit")
    val pendantRssiUnit: String,

    @SerializedName("pendant_battery_low")
    val pendantBatteryLow: Boolean,

    @SerializedName("pendant_software_version")
    val pendantSoftwareVersion: String?, // Nullable since it can be null

    @SerializedName("pendant_hardware_version")
    val pendantHardwareVersion: String,

    @SerializedName("pendant_repeated")
    val pendantRepeated: Boolean,

    @SerializedName("active")
    val active: Boolean,

    @SerializedName("arm")
    val arm: Boolean,

    @SerializedName("stay")
    val stay: Boolean,

    @SerializedName("disarm")
    val disarm: Boolean,

    @SerializedName("disarm_stay")
    val disarmStay: Boolean,

    @SerializedName("pendant_allow")
    val pendantAllow: Boolean,

    @SerializedName("remote")
    val remote: Boolean,

    @SerializedName("can_edit_himself")
    val canEditHimself: Boolean,

    @SerializedName("can_edit_all_users")
    val canEditAllUsers: Boolean,

    @SerializedName("is_access_tag_user")
    val isAccessTagUser: Boolean,

    @SerializedName("access_tag_id")
    val accessTagId: String,

    @SerializedName("phone_number")
    val phoneNumber: String,

    @SerializedName("areas")
    val areas: List<Int>
)



