package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigAcPlugRequest(
    @SerializedName("power_up_output_state")
    val powerUpOutputState: Int = 0,
    @SerializedName("block_manual_switch")
    val blockManualSwitch: Boolean = false,
    @SerializedName("out_supervision")
    val outSupervision: Int = 0
)


/*
 "device_config": {
            "power_up_output_state": 0,
            "block_manual_switch": false,
            "out_supervision": 0
        }
 */