package com.crowcloud.gateway.suricataCore.network.interceptor

import android.util.Log
import com.crowcloud.gateway.suricataCore.persistence.DataRepo
import okhttp3.Interceptor
import okhttp3.Response

class ApiVersionInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val originalUrl = originalRequest.url

        val apiVersion = DataRepo.instance.apiVersion
        val needsVersioning = apiVersion >= 2

        // Check if the path starts with /panels/ or /panel_users/
        val isPanelPath = originalUrl.encodedPath.startsWith("/panels/")
        val isPanelUsersPath = originalUrl.encodedPath.startsWith("/panel_users/")

        // Modify path to include /v2 if needed
        val newPath = if (needsVersioning && (isPanelPath || isPanelUsersPath)) {
            "/v2${originalUrl.encodedPath}"
        } else {
            originalUrl.encodedPath
        }

        // Rebuild the URL with the modified path
        val newUrl = originalUrl.newBuilder()
            .encodedPath(newPath)
            .build()

        Log.d("ApiVersionInterceptor", "Original URL: $originalUrl")
        Log.d("ApiVersionInterceptor", "Modified URL: $newUrl")

        // Proceed with the updated request
        val newRequest = originalRequest.newBuilder()
            .url(newUrl)
            .build()

        return chain.proceed(newRequest)
    }
}
