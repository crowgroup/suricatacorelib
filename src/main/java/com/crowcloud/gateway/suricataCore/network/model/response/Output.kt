package com.crowcloud.gateway.suricataCore.network.model.response

import com.crowcloud.gateway.suricataCore.R
import com.crowcloud.gateway.suricataCore.util.CrowDeviceType
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.util.*

/**
 * Created by michael on 2/19/18.
 */
@Parcelize
class Output(
    override var deviceType: CrowDeviceType = CrowDeviceType.OUTPUT,

    @SerializedName("temperature")
    val temperature: Int = 0,

    @SerializedName("chime_mode")
    val chimeMode: Int = 0,

    @SerializedName("chime_timer")
    val chimeTimer: Int = 0,

    @SerializedName("tamper_alarm")
    val tamperAlarm: Boolean = false,

    @SerializedName("battery_low")
    val batteryLow: Boolean = false,

    @SerializedName("silent")
    val silent: Boolean = false,

    @SerializedName("monitor_alarm")
    val monitorAlarm: Boolean = false,

    @SerializedName("use_temp_device_config")
    val useTempDeviceConfig: Boolean = false,

    @SerializedName("device_config")
    var deviceConfig: DeviceConfig? = null,

    @SerializedName("temp_device_config")
    var tempDeviceConfig: DeviceConfig? = null,

    @SerializedName("ext_status")
    var extensionStatus: ExtensionStatus? = null,

    @SerializedName("arm_action")
    var armAction: Map<String, Int> = emptyMap(),

    @SerializedName("stay_action")
    var stayAction: Map<String, Int> = emptyMap(),

    @SerializedName("disarm_action")
    var disarmAction: Map<String, Int> = emptyMap(),

    @SerializedName("zone_action")
    var zoneAction: Map<String, Int> = emptyMap()
) : BaseDevice() {

    constructor(id: Int, name: String) : this() {
        this.id = id
        this.name = name
    }

    fun isSiren(): Boolean = typeCodeName().contains("SRN")
    fun isLock(): Boolean = typeCodeName().contains("LOCK")
    fun isZigbeeRgbBulb(): Boolean = typeCodeName().contains("ZB_BULB_C")
    fun isZigbeeDimmableBulb(): Boolean = typeCodeName().contains("ZB_BULB_D")
    fun isZigbeeTemperatureBulb(): Boolean = typeCodeName().contains("ZB_BULB_T")
    fun isZigbeeAcPlug(): Boolean = typeCodeName().contains("AC_PLUG")
    fun isZigbeeDoorLockController(): Boolean = typeCodeName().contains("DL_CONTROLER")


    fun isAcPlug(): Boolean = typeCodeName().contains("OUT")
    fun isFogGenerator(): Boolean = typeCodeName().contains("FOG")

    fun isWaterMeter(): Boolean = typeCodeName().contains("WATER")

    override val troublesList: ArrayList<String>
        get() {
            val troubles = super.troublesList
            if (tamperAlarm) troubles.add("tamper_alarm")
            if (batteryLow) troubles.add("battery_low")
            if (monitorAlarm) troubles.add("monitor_alarm")
            return troubles
        }

    fun getTroubles(): ArrayList<Int> {
        val arr = ArrayList<Int>()
        if (tamperAlarm) arr.add(R.string.trouble_tamper_alarm)
        if (batteryLow) arr.add(R.string.trouble_low_battery)
        if (monitorAlarm) arr.add(R.string.trouble_monitor_alarm)
        return arr
    }
}

//actions
//"arm_action": {
//    "0": 1,
//    "2": 1
//  },
//  "disarm_action": {
//    "1": 1 // for area id 2, when stay arming, turn off output 0
//  },
//  "stay_action": {
//    "2": 2 // for area id 2, when stay arming, turn off output 0
//  },
//  "zone_action": {
//    "5": 2 // for zone id 5, on alarm, turn off output 0
//  },

//update output
//Using a PUT method, all these values can be updated.
//Only values that are sent are updated(not changing other values, including partial keys)
//e.g.


//GET /outputs
//[{
//  ...
//  "arm_action": {
//    "0": 1 // Possible values are 0 - do nothing, 1 - turn on, 2 - turn off, 3 - undefined.
//           // 0 will not be shown in the list, can only be used in PUT for "removing" actions
//           // the key "0" indicates the AREA id
//           // will only show for non empty areas
//  },
//  "disarm_action": {
//    "0": 1 // the key "0" indicates the AREA id
//           // will only show for non empty areas
//  },
//  "stay_action": {
//    "0": 1 // the key "0" indicates the AREA id
//           // will only show for non empty areas
//  },
//  "zone_action": {
//    "0": 1 // this key "0" indicates the ZONE id
//           // will only show for active zones
//  },
//}]