package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ControlPanelUser(
        @SerializedName("id")
        val id: Int?,
        @SerializedName("is_superuser")
        val isSuperuser: Boolean?,
        @SerializedName("name")
        val name: String?,
        @SerializedName("notifications_filter")
        val notificationsFilter: NotificationsEmailFilter?,
        @SerializedName("receive_pictures")
        val receivePictures: Boolean?,
        @SerializedName("user")
        val user: EmailUser?,
        @SerializedName("notification_language")
        val notificationLanguage: String?,
        @SerializedName("user_code")
        val userCode: String?,
) : Parcelable
