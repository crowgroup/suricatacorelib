package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName

abstract class BaseLock: Parcelable{

    @SerializedName("id")
    val id: String? = null
}