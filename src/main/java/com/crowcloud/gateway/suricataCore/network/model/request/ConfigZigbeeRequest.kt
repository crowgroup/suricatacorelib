package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

class ConfigZigbeeRequest(config: ConfigZigbee) {

    @SerializedName("temp_device_config")
    val configZigbee: ConfigZigbee = config
}