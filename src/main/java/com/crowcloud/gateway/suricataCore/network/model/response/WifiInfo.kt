package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class WifiInfo {

    @SerializedName("ssid")
    @Expose
    val ssid: String? = null
    @SerializedName("ip")
    @Expose
    val ip: String? = null
    @SerializedName("mask")
    @Expose
    val mask: String? = null
    @SerializedName("gateway")
    @Expose
    val gateway: String? = null
    @SerializedName("mac")
    @Expose
    val mac: String? = null
    @SerializedName("dns")
    @Expose
    val dns: String? = null
    @SerializedName("rssi")
    @Expose
    val rssi: Long = 0L
    @SerializedName("id")
    @Expose
    val id: String? = null
}
