package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class DoorLockConfig(
    @SerializedName("user_code_blocking")
    var userCodeBlocking: Boolean = false,

    @SerializedName("alarm_system")
    var alarmSystem: Boolean = false,

    @SerializedName("volume")
    var volume: Int = 1,

    @SerializedName("auto_relock")
    var autoRelock: Boolean = false,

    @SerializedName("alarm_mode")
    var alarmMode: Int = 0,

    @SerializedName("alarm_hold_off_time")
    var alarmHoldOffTime: Int = 100
) : Parcelable {
    fun deepCopy(): DoorLockConfig {
        return DoorLockConfig(
            userCodeBlocking = this.userCodeBlocking,
            alarmSystem = this.alarmSystem,
            volume = this.volume,
            autoRelock = this.autoRelock,
            alarmMode = this.alarmMode,
            alarmHoldOffTime = this.alarmHoldOffTime
        )
    }
}

