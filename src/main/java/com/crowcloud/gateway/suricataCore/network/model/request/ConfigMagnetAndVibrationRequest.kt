package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigMagnetAndVibrationRequest(
    @SerializedName("internal_mag_enabled")
    val internalMagEnabled: Boolean = true,
    @SerializedName("led_enabled")
    val ledEnabled: Boolean = true,
    @SerializedName("trouble_or_alarm")
    val troubleOrAlarm: Int = 0,
    @SerializedName("supervision")
    val supervision: Int = 0,
    @SerializedName("shock_sensitivity")
    var shockSensitivity: Int = 0
)

/*
"device_config": {
            "internal_mag_enabled": true,
            "led_enabled": true,
            "trouble_or_alarm": 0,
            "supervision": 0,
            "shock_sensitivity": 3
        },
 */