package com.crowcloud.gateway.suricataCore.network.model.request;

import com.crowcloud.gateway.suricataCore.network.model.response.ControlPanel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by michael on 2/18/18.
 */

public class ControlPanelRequest {

    @SerializedName("name")
    public String name;

    @SerializedName("user_code")
    public String userCode;

    @SerializedName("control_panel")
    public ControlPanel controlPanel;
}
