package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigGlassBreakDetectorRequest(
    @SerializedName("zone_led")
    val enableLed: Boolean = true,
    @SerializedName("supervision")
    val supervision: Int = 0
)


/*
    "device_config": {
            "zone_led": true,
            "supervision": 0
        },
 */