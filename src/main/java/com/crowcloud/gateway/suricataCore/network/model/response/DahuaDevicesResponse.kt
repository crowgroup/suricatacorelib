package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class DahuaDevicesResponse {
    @SerializedName("results")
    val result: List<DahuaIpCamera>? = null
}