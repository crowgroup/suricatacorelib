package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.util.Date

@Parcelize
data class Measurement(
    @SerializedName("_id") val data: Data?,
    @SerializedName("humidity") val humidity: Float?,
    @SerializedName("temperature") val temperature: Float?,
    @SerializedName("air_pressure") val airPressure: Float?,
    @SerializedName("gas_value") val gasValue: Int?
) : Parcelable {

    enum class DeviceType {
        @SerializedName("0")
        Zone,
        @SerializedName("1")
        Output
    }

    enum class MeasureType {
        Temperature, Humidity, AirPressure
    }

    @Parcelize
    data class Data(
        @SerializedName("control_panel") val controlPanel: String?,
        @SerializedName("device_type") val deviceType: DeviceType?,
        @SerializedName("device_id") val deviceId: Int?,
        @SerializedName("dect_interface") val dectInterface: Int?,
        @SerializedName("report_type") val reportType: Int?,
        @SerializedName("panel_time") val panelTime: Date?
    ) : Parcelable

    fun getValue(type: MeasureType): Float? = when (type) {
        MeasureType.Temperature -> temperature?.div(100.0f)
        MeasureType.Humidity -> humidity?.div(100.0f)
        MeasureType.AirPressure -> airPressure
    }

    fun getUnit(type: MeasureType): String = when (type) {
        MeasureType.Temperature -> "℃"
        MeasureType.Humidity -> "%RH"
        MeasureType.AirPressure -> "hPa"
    }

    fun isTemperature(): Boolean = temperature != null
    fun isHumidity(): Boolean = humidity != null
    fun isAirPressure(): Boolean = airPressure != null
    fun isAirQuality(): Boolean = gasValue != null && data?.reportType == 0x3D
    fun isAdvancedAirQuality(): Boolean = gasValue != null && data?.reportType == 0x4D
}

data class MeasuresByZone(
        @SerializedName("name") val name: String?,
        @SerializedName("values") val values: List<Measurement?>?
)