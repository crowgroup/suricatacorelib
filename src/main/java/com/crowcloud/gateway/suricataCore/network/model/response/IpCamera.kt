package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class IpCamera {

    @SerializedName("id")
    var id: Int? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("panel_id")
    var panelId: Int? = null
    @SerializedName("device_name")
    var deviceName: String? = null
    @SerializedName("created")
    var created: String? = null
    @SerializedName("device")
    var device: Device? = null

    class Device {

        @SerializedName("device_uid")
        var deviceUid: String? = null
        @SerializedName("device_name")
        var deviceName: String? = null
        @SerializedName("device_id")
        var deviceId: String? = null
        @SerializedName("device_display_name")
        var deviceDisplayName: String? = null
        @SerializedName("device_description")
        var deviceDescription: String? = null
        @SerializedName("registration_date")
        var registrationDate: Long? = null
        @SerializedName("content_start_date")
        var contentStartDate: Long? = null
        @SerializedName("online")
        var online: Boolean? = null
        @SerializedName("streaming")
        var streaming: Boolean? = null
        @SerializedName("recording")
        var recording: Boolean? = null
        @SerializedName("has_gps_data")
        var hasGpsData: Boolean? = null
        @SerializedName("last_state_change_date")
        var lastStateChangeDate: Long? = null
        @SerializedName("recent_thumb_url")
        var recentThumbUrl: String? = null
        @SerializedName("streaming_config")
        var streamingConfig: StreamingConfig? = null
        @SerializedName("streams_spec")
        var streamsSpec: List<StreamsSpec>? = null
        @SerializedName("hardware")
        var hardware: Hardware? = null
        @SerializedName("statistics")
        var statistics: Statistics? = null
        @SerializedName("user_data")
        var userData: UserData? = null
        @SerializedName("firmware")
        var firmware: Firmware? = null

    }

    class Firmware{
        @SerializedName("upgrade_confirmation_needed")
        var upgradeConfirmationNeeded: Boolean? = null
    }

    class StreamingConfig {

        @SerializedName("fps")
        var fps: Int? = null
        @SerializedName("live_bitrate")
        var liveBitrate: Int? = null
        @SerializedName("events_bitrate")
        var eventsBitrate: Int? = null

    }

    class StreamsSpec {

        @SerializedName("id")
        var id: String? = null
        @SerializedName("protocol")
        var protocol: String? = null
        @SerializedName("vod_enabled")
        var vodEnabled: Boolean? = null
        @SerializedName("streaming")
        var streaming: Boolean? = null

    }

    class Hardware {

        @SerializedName("camera_model")
        var cameraModel: String? = null
        @SerializedName("serial_number")
        var serialNumber: String? = null
        @SerializedName("firmware_version")
        var firmwareVersion: String? = null
        @SerializedName("sensors")
        var sensorsId2DataMap: HashMap<String, SensorData>? = null//Id to data mapping

    }

    class Statistics {

        @SerializedName("storyboards_count_human")
        var storyboardsCountHuman: String? = null
        @SerializedName("storyboards_size_human")
        var storyboardsSizeHuman: String? = null
        @SerializedName("thumbnails_size")
        var thumbnailsSize: Int? = null
        @SerializedName("thumbnails_count_human")
        var thumbnailsCountHuman: String? = null
        @SerializedName("storyboards_count")
        var storyboardsCount: Int? = null
        @SerializedName("thumbnails_count")
        var thumbnailsCount: Int? = null
        @SerializedName("thumbnails_size_human")
        var thumbnailsSizeHuman: String? = null
        @SerializedName("storyboards_size")
        var storyboardsSize: Int? = null

    }

    class UserData {

        @SerializedName("events_overflow")
        var eventsOverflow: EventsOverflow? = null
        @SerializedName("product_tag")
        var productTag: ProductTag? = null
        @SerializedName("frames_skipped")
        var framesSkipped: FramesSkipped? = null
        @SerializedName("wifi_state")
        var wifiState: WifiState? = null
        @SerializedName("roi_current")
        var roiCurrent: RoiCurrent? = null
        @SerializedName("motion")
        var motion: Motion? = null
    }

    class ProductTag {

        @SerializedName("product_tag")
        var productTag: String? = null

    }

    class FramesSkipped {

        @SerializedName("frames_skipped")
        var framesSkipped: Boolean? = null

    }

    class EventsOverflow {

        @SerializedName("events_overflow")
        var eventsOverflow: Boolean? = null

    }

    class SensorData {
        @SerializedName("id")
        var id: String? = null
        @SerializedName("name")
        var name: String? = null
        @SerializedName("track")
        var track: Int? = null
        @SerializedName("max_width")
        var maxWidth: Int? = null
        @SerializedName("max_height")
        var maxHeight: Int? = null
    }

    class WifiState {

        @SerializedName("wifi_ssid")
        var wifiSsid: String? = null
        @SerializedName("wifi_security_type")
        var wifiSecurityType: String? = null
        @SerializedName("wifi_signal_strength")
        var wifiSignalStrength: Float? = null
        @SerializedName("wifi_channel")
        var wifiChannel: Float? = null
        @SerializedName("wifi_ip_ddress")
        var wifiIpDdress: String? = null

    }

    class RoiCurrent {

        @SerializedName("left-top-corner-y-fraction")
        var leftTopCornerYFraction: Float? = null
        @SerializedName("left-top-corner-x-fraction")
        var leftTopCornerXFraction: Float? = null
        @SerializedName("size-fraction")
        var sizeFraction: Float? = null
    }

    class Motion {
        @SerializedName("state")
        var state: Boolean? = null
    }

}