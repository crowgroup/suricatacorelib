package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.crowcloud.gateway.suricataCore.util.CrowDeviceType
import com.crowcloud.gateway.suricataCore.util.deviceTypeCodeMap
import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by michael on 2/18/18.
 */

abstract class BaseDevice : Parcelable {

    abstract var deviceType: CrowDeviceType

    @SerializedName("id")
    var id: Int = 0

    @SerializedName("device_id")
    var deviceId: Int = 0

    @SerializedName("name")
    var name: String? = null

    @SerializedName("type")
    var type: Int = 0

    @SerializedName("state")
    var state: Boolean = false

    @SerializedName("software_version")
    var softwareVersion: String? = null

    @SerializedName("hardware_version")
    var hardwareVersion: String? = null

    @SerializedName("rssi")
    var rssi: Int = 0

    @SerializedName("rssi_bars")
    var rssiBars: Int = 0

    @SerializedName("rssi_unit")
    var rssiUnit: String? = null

    open val troublesList: ArrayList<String>
        get() {
            return ArrayList()
        }

    override fun equals(other: Any?): Boolean {
        if (other !is BaseDevice) return false

        return id == other.id
                && state == other.state
                && name == other.name
                && troublesList.containsAll(other.troublesList) //TODO: continue and fill all class members..
    }

    override fun hashCode(): Int {
        return ("" + id + state + name).hashCode() //TODO: continue and fill all class members.. //+ tamperAlarm + batteryLow
    }

    fun typeName(): String = deviceTypeCodeMap[type]?.second ?: "None"
    fun typeCodeName(): String = deviceTypeCodeMap[type]?.first ?: "None"

    open fun deepCopy(): BaseDevice {
        return this // This should be overridden in subclasses
    }
}
