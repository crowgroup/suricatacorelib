package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class GsmInfo {

    @SerializedName("rssi")
    val rssi: Long = 0L

    @SerializedName("status_desc")
    val statusDesc: String? = null

    @SerializedName("ip")
    val ip: String? = null

    @SerializedName("dns")
    val dns: String? = null

    @SerializedName("provider")
    val provider: String? = null

    @SerializedName("imei")
    val imei: String? = null

    @SerializedName("band")
    val band: String? = null

    @SerializedName("id")
    val id: String? = null

    @SerializedName("net")
    val net: String? = null

    @SerializedName("gateway")
    val gateway: String? = null

    @SerializedName("status")
    val status: Int = 0

    @SerializedName("mask")
    val mask: String? = null

    @SerializedName("module_hw")
    val moduleHw: String? = null
}