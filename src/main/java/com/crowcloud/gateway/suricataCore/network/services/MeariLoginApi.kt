package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.request.CountryCodeRequest
import com.crowcloud.gateway.suricataCore.network.model.response.LoginMeariCloudResponse
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

interface MeariLoginApi {

    @POST("/panels/{panel_id}/meari/login/")
    suspend fun loginMeariCloud2Cloud(@Path("panel_id") panelId: Int?,
                                      @Body countryCode: CountryCodeRequest
    ): LoginMeariCloudResponse
}