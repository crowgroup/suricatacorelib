package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigTemperatureRequest(
    @SerializedName("enable_external")
    val enableExternal: Boolean = true,
    @SerializedName("threshold_temperature_low")
    val thresholdTemperatureLow: Int = 5,
    @SerializedName("supervision")
    val supervision: Int = 1,
    @SerializedName("threshold_temperature_high")
    val thresholdTemperatureHigh: Int = 35,
    @SerializedName("alert_time")
    val alertTime: Int = 20,
    @SerializedName("threshold_temperature_normal")
    val thresholdTemperatureNormal: Int = 35
)

/*
  "device_config": {
            "enable_external": true,
            "threshold_temperature_low": 500,
            "supervision": 1,
            "threshold_temperature_high": 3500,
            "alert_time": 20,
            "threshold_temperature_normal": 700
        }
 */