package com.crowcloud.gateway.suricataCore.network

import com.crowcloud.gateway.suricataCore.auth.LoginManagerConfig
import com.crowcloud.gateway.suricataCore.network.interceptor.ApiVersionInterceptor
import com.crowcloud.gateway.suricataCore.network.interceptor.AuthorizationInterceptor
import com.crowcloud.gateway.suricataCore.network.interceptor.Code204Interceptor
import com.crowcloud.gateway.suricataCore.network.interceptor.ErrorResponseInterceptor
import com.crowcloud.gateway.suricataCore.network.interceptor.HeaderRemovalInterceptor
import com.crowcloud.gateway.suricataCore.network.interceptor.TimeHeaderInterceptor
import com.crowcloud.gateway.suricataCore.network.interceptor.TrafficStatsInterceptor
import com.crowcloud.gateway.suricataCore.network.interceptor.UserAgentInterceptor
import com.crowcloud.gateway.suricataCore.network.services.*
import com.crowcloud.gateway.suricataCore.util.AccessTokenProvider
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

import com.crowcloud.gateway.suricataCore.util.*


class NetworkManager private constructor(
    private val userAgent: String,
    private val accessTokenProvider: AccessTokenProvider
) {

    private lateinit var config: LoginManagerConfig

    private val retrofit: Retrofit by lazy {
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(UserAgentInterceptor(userAgent))
            .addInterceptor(AuthorizationInterceptor(config, accessTokenProvider))
            .addInterceptor(ApiVersionInterceptor())
            .addInterceptor(HeaderRemovalInterceptor())
            .addInterceptor(Code204Interceptor())
            .addInterceptor(TrafficStatsInterceptor())
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .connectTimeout(DEFAULT_CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(DEFAULT_WRITE_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(DEFAULT_READ_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(TimeHeaderInterceptor())
            .addInterceptor(ErrorResponseInterceptor())
            .build()

        Retrofit.Builder()
            .baseUrl(ApiConfig.currentBaseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()
    }

    val authApi: AuthApi get() = createApi()
    val areasApi: AreasApi get() = createApi()
    val internalUsersApi: InternalUsersApi get() = createApi()
    val usersApi: UsersApi get() = createApi()
    val eventsApi: EventsApi get() = createApi()
    val devicesApi: DevicesApi get() = createApi()
    val locksApi: LocksApi get() = createApi()
    val outputsApi: OutputsApi get() = createApi()
    val panelCommApi: PanelCommApi get() = createApi()
    val panelsApi: PanelsApi get() = createApi()
    val panelsApiJvm: PanelsApiJvm get() = createApi()
    val picturesApi: PicturesApi get() = createApi()
    val troublesApi: TroublesApi get() = createApi()
    val zonesApi: ZonesApi get() = createApi()
    val keypadsApi: KeypadsApi get() = createApi()
    val measurementsApi: MeasurementsApi get() = createApi()
    val camerasApi: IpCamerasApi get() = createApi()
    val iotApi: IotApi get() = createApi()
    val dahuaApi: DahuaApi get() = createApi()
    val phoneNumbersApi: PhoneNumbersApi get() = createApi()
    val meariLoginApi: MeariLoginApi get() = createApi()

    val schedulesApi: SchedulesApi get() = createApi()
    val timeZonesApi: TimeZonesApi get() = createApi()
    val notificationsApi: NotificationsApi get() = createApi()

    companion object {
        const val TAG = "NetworkManager"

        @Volatile
        private var INSTANCE: NetworkManager? = null

        fun initialize(userAgent: String, config: LoginManagerConfig,
                       accessTokenProvider: AccessTokenProvider) {
            synchronized(this) {
                if (INSTANCE == null) {
                    INSTANCE = NetworkManager(userAgent, accessTokenProvider).also {
                        it.config = config
                    }
                } else {
                    INSTANCE?.config = config
                }
            }
        }

        fun getInstance(): NetworkManager =
            INSTANCE ?: throw IllegalStateException("NetworkManager must be initialized before use.")

    }

    private inline fun <reified T> createApi(): T = retrofit.create(T::class.java)

    val gson: Gson = GsonBuilder().create()
}