package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.crowcloud.gateway.suricataCore.util.CrowDeviceType
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue
import java.util.*

@Parcelize
class ExtenderUiModel(val extender: @RawValue Extender) : Parcelable {

    val deviceType: CrowDeviceType
        get() = extender.deviceType

    val id: Int
        get() = extender.id

    val deviceId: Int
        get() = extender.deviceId

    val name: String?
        get() = extender.name

    val type: Int
        get() = extender.type

    val troublesList: ArrayList<String>
        get() = extender.troublesList
}