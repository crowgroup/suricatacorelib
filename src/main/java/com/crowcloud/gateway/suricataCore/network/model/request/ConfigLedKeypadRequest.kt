package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigLedKeypadRequest(
    @SerializedName("kpd_loud")
    val kpdLoud: Int = 3
)


/* SDEVTYPE_ICON_KPD 0x97
   "device_config": {
      "kpd_loud": 3
    }
 */
