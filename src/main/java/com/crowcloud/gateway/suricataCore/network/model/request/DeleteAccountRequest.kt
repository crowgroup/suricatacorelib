package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class DeleteAccountRequest(

    @SerializedName("password")
    private val password: String?

)
