package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
class ScheduleParams(

    @SerializedName(value = "area_ids", alternate = ["areaIds"])
    var areaIds: MutableList<Int>? = null,                          // each integer range: 0-3

    @SerializedName(value = "area_id", alternate = ["areaId"])
    var areaId // each integer range: 0-3
            : Int? = null,

    @SerializedName(value = "output_ids", alternate = ["outputIds"])
    var outputIds: MutableList<Int>? = null,                        // each integer range: 0-3

    @SerializedName(value = "output_id", alternate = ["outputId"])
    var outputId: Int? = null                                       // each integer range: 0-3
) : Parcelable