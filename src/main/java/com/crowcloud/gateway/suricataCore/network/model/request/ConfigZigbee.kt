package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigZigbee(
    @SerializedName("dimm_time")
    val dimmerTime: Int,
    @SerializedName("dimm_value")
    val dimmerValue: Int,
    @SerializedName("newColor")
    val isNewColor: Boolean,
    @SerializedName("temperature")
    val temperature: Int,
    @SerializedName("current_y")
    val currentY: Int,
    @SerializedName("current_x")
    val currentX: Int
)

