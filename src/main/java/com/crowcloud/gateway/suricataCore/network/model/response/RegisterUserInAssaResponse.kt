package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

class RegisterUserInAssaResponse {

    @SerializedName("user")
    var userId: Int? = null

    @SerializedName("locale")
    var locale: String? = null

    @SerializedName("created")
    var created: String? = null

    @SerializedName("emailAddresses")
    var emailAddresses: ArrayList<String>?= null

    @SerializedName("phoneNumbers")
    var phoneNumbers: ArrayList<String>?= null
}


/*
{
  "user": 100,
  "locale": "en",
  "created": "2021-04-06 09:16:01.057Z",
  "emailAddresses": [
    "tim7@tslp.me"
  ],
  "phoneNumbers": [
    "0501032417"
  ]
}
 */