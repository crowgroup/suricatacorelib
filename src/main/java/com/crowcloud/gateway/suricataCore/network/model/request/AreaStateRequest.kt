package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class AreaStateRequest(@SerializedName("state") val state: State?,
                            @SerializedName("force") val force: Boolean = false) {

    enum class State {
        @SerializedName("arm")
        ARM,
        @SerializedName("disarm")
        DISARM,
        @SerializedName("stay")
        STAY
    }
}
