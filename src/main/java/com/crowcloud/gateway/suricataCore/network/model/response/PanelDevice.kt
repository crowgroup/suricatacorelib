package com.crowcloud.gateway.suricataCore.network.model.response

import com.crowcloud.gateway.suricataCore.util.CrowDeviceType
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class PanelDevice(
    @SerializedName("deviceType")
    override var deviceType: CrowDeviceType = CrowDeviceType.PANEL,

    @SerializedName("battery_percentage")
    var batteryPercentage: Int = 0,

    @SerializedName("nZONES")
    var nZones: Int = 0,

    @SerializedName("nWIFIS")
    var nWifis: Int = 0,

    @SerializedName("nALARM_PHONE_NUMBERS")
    var nAlarmPhoneNumbers: Int = 0,

    @SerializedName("nLAST_RF_ZONE_IDS")
    var nLastRfZoneIds: Int = 0,

    @SerializedName("nZIGBEEDEVICES")
    var nZigbeeDevices: Int = 0,

    @SerializedName("nUSERS")
    var nUsers: Int = 0,

    @SerializedName("wifi_rssi")
    var wifiRssi: Int = 0,

    @SerializedName("nKEYPADS")
    var nKeypads: Int = 0,

    @SerializedName("mobile_rssi")
    var mobileRssi: Int = 0,

    @SerializedName("dect_enabled")
    var dectEnabled: Boolean = false,

    @SerializedName("nAREAS")
    var nAreas: Int = 0,

    @SerializedName("nDECTHSS")
    var nDectHss: Int = 0,

    @SerializedName("nFIRST_RF_ZONE_IDS")
    var nFirstRfZoneIds: Int = 0,

    @SerializedName("nFIRST_RF_OUTPUT_IDS")
    var nFirstRfOutputIds: Int = 0,

    @SerializedName("nVOICEBOXES")
    var nVoiceBoxes: Int = 0,

    @SerializedName("duress_digit")
    var duressDigit: Int = 0,

    @SerializedName("connection_type")
    var connectionType: Int = 0,

    @SerializedName("hardware")
    var hardware: Hardware = Hardware(),

    @SerializedName("nDECTDEVICES")
    var nDectDevices: Int = 0,

    @SerializedName("nLAST_DECT_OUTPUT_IDS")
    var nLastDectOutputIds: Int = 0,

    @SerializedName("language")
    var language: Int = 0,

    @SerializedName("nFIRST_DECT_ZONE_IDS")
    var nFirstDectZoneIds: Int = 0,

    @SerializedName("nEXTENDERS")
    var nExtenders: Int = 0,

    @SerializedName("panel_title")
    var panelTitle: String = "",

    @SerializedName("battery_voltage")
    var batteryVoltage: Int = 0,

    @SerializedName("panel_cfg")
    var panelCfg: Int = 0,

    @SerializedName("nTIME_ZONES")
    var nTimeZones: Int = 0,

    @SerializedName("nLAST_DECT_ZONE_IDS")
    var nLastDectZoneIds: Int = 0,

    @SerializedName("nDECTCONTACTNUMBERS")
    var nDectContactNumbers: Int = 0,

    @SerializedName("nOUTPUTS")
    var nOutputs: Int = 0,

    @SerializedName("version")
    var version: String = "",

    @SerializedName("nHOLIDAYS")
    var nHolidays: Int = 0,

    @SerializedName("dect_version")
    var dectVersion: String = "",

    @SerializedName("nFIRST_DECT_OUTPUT_IDS")
    var nFirstDectOutputIds: Int = 0,

    @SerializedName("can_change_language")
    var canChangeLanguage: Boolean = false,

    @SerializedName("nLAST_RF_OUTPUT_IDS")
    var nLastRfOutputIds: Int = 0,

    @SerializedName("rf_module_id")
    var rfModuleId: Int = 0,

    @SerializedName("nDECT_UNITS")
    var nDectUnits: Int = 0,

    @SerializedName("troubles")
    var troubles: PanelTroubles = PanelTroubles(),

//    @SerializedName("timezones")
//    var timezones: List<TimeZone> = emptyList(),

    @SerializedName("areas")
    var areas: List<Area> = emptyList()
) : BaseDevice(), Parcelable {

    constructor(id: Int, name: String) : this() {
        this.id = id
        this.name = name
    }
}


@Parcelize
data class Hardware(
    @SerializedName("flash_2m")
    var flash2m: Boolean = false,

    @SerializedName("sms")
    var sms: Boolean = false,

    @SerializedName("dect")
    var dect: Boolean = false,

    @SerializedName("mobile")
    var mobile: Boolean = false,

    @SerializedName("local_voice")
    var localVoice: Boolean = false,

    @SerializedName("local_press")
    var localPress: Boolean = false,

    @SerializedName("wifi")
    var wifi: Boolean = false,

    @SerializedName("wired_ext")
    var wiredExt: Boolean = false,

    @SerializedName("power_check")
    var powerCheck: Boolean = false,

    @SerializedName("stm232f437")
    var stm232f437: Boolean = false,

    @SerializedName("wired")
    var wired: Boolean = false,

    @SerializedName("local_io")
    var localIo: Boolean = false,

    @SerializedName("ethernet")
    var ethernet: Boolean = false,

    @SerializedName("voice")
    var voice: Boolean = false,

    @SerializedName("rfm")
    var rfm: Boolean = false
) : Parcelable

@Parcelize
data class PanelTroubles(
    @SerializedName("fire_alarm")
    var fireAlarm: Boolean = false,

    @SerializedName("panic_alarm")
    var panicAlarm: Boolean = false,

    @SerializedName("fuse_fail")
    var fuseFail: Boolean = false,

    @SerializedName("sdcard_fail")
    var sdcardFail: Boolean = false,

    @SerializedName("wifi_fail")
    var wifiFail: Boolean = false,

    @SerializedName("gprs_fail")
    var gprsFail: Boolean = false,

    @SerializedName("code_attempts_alarm")
    var codeAttemptsAlarm: Boolean = false,

    @SerializedName("license_over")
    var licenseOver: Boolean = false,

    @SerializedName("rf_module_failure")
    var rfModuleFailure: Boolean = false,

    @SerializedName("duress_alarm")
    var duressAlarm: Boolean = false,

    @SerializedName("mains_fail")
    var mainsFail: Boolean = false,

    @SerializedName("ethernet_fail")
    var ethernetFail: Boolean = false,

    @SerializedName("gsm_line_fail")
    var gsmLineFail: Boolean = false,

    @SerializedName("rf_jamming_alarm")
    var rfJammingAlarm: Boolean = false,

    @SerializedName("dect_fail")
    var dectFail: Boolean = false,

    @SerializedName("panel_tamper")
    var panelTamper: Boolean = false,

    @SerializedName("medical_alarm")
    var medicalAlarm: Boolean = false,

    @SerializedName("low_battery")
    var lowBattery: Boolean = false
) : Parcelable

//@Parcelize
//data class TimeZone(
//    @SerializedName("id")
//    var id: Int = 0,
//
//    @SerializedName("name")
//    var name: String = ""
//) : Parcelable
