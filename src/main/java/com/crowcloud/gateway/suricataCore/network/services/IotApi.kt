package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.request.UpdateIotDeviceRequestBody
import com.crowcloud.gateway.suricataCore.network.model.response.IotDevice
import com.crowcloud.gateway.suricataCore.network.model.response.IotDeviceNodes
import com.crowcloud.gateway.suricataCore.network.model.response.PaginatedResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface IotApi {

    @GET("/iot/{panel_id}/")
    fun getIotDevices(@Path("panel_id") panelId: Int,
                   @Query("page") page: Int,
                   @Query("page_size") pageSize: Int,
                   @Query("ordering") ordering: String? = null): Call<PaginatedResponse<IotDevice>>

    @GET("/iot/{panel_id}/{device_id}/")
    fun getIotDevice(@Path("panel_id") panelId: Int,
                        @Path("device_id") id: String): Call<IotDevice>

    @GET("/iot/{panel_id}/{device_id}/nodes/")
    fun getIotDeviceNodes(@Path("panel_id") panelId: Int,
                          @Path("device_id") id: String): Call<IotDeviceNodes>


    @POST("/iot/{panel_id}/")
    fun registerIotDevice(@Path("panel_id") panelId: Int, @Body body: IotDevice): Call<IotDevice>

    @POST("/iot/{panel_id}/{device_id}/")
    fun updateIotDevice(@Path("panel_id") panelId: Int,
                     @Path("device_id") deviceId: String,
                     @Body body: UpdateIotDeviceRequestBody): Call<IotDevice>

    @DELETE("/iot/{panel_id}/{device_uid}/")
    fun deleteIotDevice(@Path("panel_id") panelId: Int,
                     @Path("device_uid") id: String): Call<ResponseBody>

}