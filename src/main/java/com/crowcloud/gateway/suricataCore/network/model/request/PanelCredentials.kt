package com.crowcloud.gateway.suricataCore.network.model.request

import android.os.Parcelable
import com.crowcloud.gateway.suricataCore.network.model.response.ControlPanel
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class PanelCredentials(
    @SerializedName("user_code")
    var userCode: String? = null,

    @SerializedName("control_panel")
    var controlPanel: ControlPanel? = null
): Parcelable
