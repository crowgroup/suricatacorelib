package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


class YaleLockInfo() : Parcelable{

    @SerializedName("location")
    var location: String? = null

    @SerializedName("model")
    var model: String? = null

    @SerializedName("lockingTechnology")
    var lockingTechnology: String? = null

    @SerializedName("portalType")
    var portalType: String? = null

    constructor(parcel: Parcel) : this() {
        location = parcel.readString()
        model = parcel.readString()
        lockingTechnology = parcel.readString()
        portalType = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(location)
        parcel.writeString(model)
        parcel.writeString(lockingTechnology)
        parcel.writeString(portalType)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<YaleLockInfo> {
        override fun createFromParcel(parcel: Parcel): YaleLockInfo {
            return YaleLockInfo(parcel)
        }

        override fun newArray(size: Int): Array<YaleLockInfo?> {
            return arrayOfNulls(size)
        }
    }
}