package com.crowcloud.gateway.suricataCore.network.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by michael on 2/19/18.
 */

public class ToggleOutputRequest {

    @SerializedName("state")
    public boolean state;

    public ToggleOutputRequest(boolean state) {
        this.state = state;
    }
}
