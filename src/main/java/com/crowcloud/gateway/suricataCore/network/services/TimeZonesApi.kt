package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.response.TimeZoneInfo
import okhttp3.ResponseBody
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

interface TimeZonesApi {

    @GET("/panels/{panel_id}/timezones/")
    suspend fun getTimeZones(@Path("panel_id") panelId: Int): List<TimeZoneInfo>

    @POST("/panels/{panel_id}/timezones/")
    suspend fun addTimeZone(@Path("panel_id") panelId: Int,
                            @Body data: TimeZoneInfo): TimeZoneInfo

    @PUT("/panels/{panel_id}/timezones/{timezone_id}/")
    suspend fun updateTimeZone(@Path("panel_id") panelId: Int,
                               @Path("timezone_id") timezoneId: Int,
                               @Body data: TimeZoneInfo): TimeZoneInfo

    @PATCH("/panels/{panel_id}/timezones/{timezone_id}/")
    suspend fun bypassTimeZone(@Path("panel_id") panelId: Int,
                               @Path("timezone_id") timezoneId: Int,
                               @Body data: TimeZoneInfo): TimeZoneInfo

    @DELETE("/panels/{panel_id}/timezones/{timezone_id}/")
    suspend fun deleteTimeZone(@Path("panel_id") panelId: Int,
                               @Path("timezone_id") timezoneId: Int): retrofit2.Response<ResponseBody>
}