package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class SetDeviceNameRequest (
    @SerializedName("name")
    val newName: String? = "",
)
