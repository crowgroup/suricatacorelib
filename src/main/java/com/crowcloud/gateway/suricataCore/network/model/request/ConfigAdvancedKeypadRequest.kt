package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigAdvancedKeypadRequest(
    @SerializedName("panic_alarm_audiable")
    val panicAlarmAudible: Boolean = false,
    @SerializedName("kpd_loud")
    val kpdLoud: Int = 3,
    @SerializedName("pin_code_size")
    var pinCodeSize: Int = 0,
    @SerializedName("button_bush_volume")
    val buttonBushVolume: Int = 3,
    @SerializedName("siren_timeout")
    val sirenTimeout: Int = 0,
    @SerializedName("panic_alarm_visual")
    val panicAlarmVisual: Boolean = true,
    @SerializedName("24h_alarm_enable")
    val alarm24hEnable: Boolean = false,
    @SerializedName("pin_processing_type")
    val pinProcessingType: Int = 1
)


/* SDEVTYPE_ICON_KPD2 0x99
   "device_config": {
      "panic_alarm_audiable": false,
      "kpd_loud": 3,
      "pin_code_size": 0,
      "button_bush_volume": 3,
      "siren_timeout": 0,
      "panic_alarm_visual": true,
      "24h_alarm_enable": false,
      "pin_processing_type": 1
    }
 */