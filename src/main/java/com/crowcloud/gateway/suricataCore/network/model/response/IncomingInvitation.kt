package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class IncomingInvitation(
    @SerializedName("control_panel")
    val controlPanel: ControlPanelName? = null,

    @SerializedName("invited_by")
    val invitedBy: InvitedBy? = null,

    @SerializedName("created")
    val created: String? = null,

    @SerializedName("name")
    val name: String = "",

    @SerializedName("role")
    val role: Int? = null,

    @SerializedName("email")
    val email: String = "",

    @SerializedName("invitation_code")
    val invitationCode: String? = null
)

data class ControlPanelName(
    @SerializedName("mac")
    val mac: String = "",

    @SerializedName("name")
    val name: String = ""
)
