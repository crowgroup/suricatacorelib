package com.crowcloud.gateway.suricataCore.network.model.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class NotificationsEmailData(
        @SerializedName("control_panel_user")
        val controlPanelUser: ControlPanelUser? = null,
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("key")
        var key: String? = "",
        @SerializedName("notifications_filter")
        var notificationsFilter: NotificationsEmailFilter? = NotificationsEmailFilter(),
        @SerializedName("notify_cids")
        var notifyCids: List<Int>? = ArrayList(),
        @SerializedName("receive_pictures")
        var receivePictures: Boolean? = false
) : Parcelable