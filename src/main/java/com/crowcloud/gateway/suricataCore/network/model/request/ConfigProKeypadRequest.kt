package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigProKeypadRequest(
    @SerializedName("pin_code_size")
    var pinCodeSize: Int = 0,
    @SerializedName("panic_alarm_visual")
    val panicAlarmVisual: Boolean = true,
    @SerializedName("button_bush_volume")
    val buttonBushVolume: Int = 3,
    @SerializedName("kpd_loud")
    val kpdLoud: Int = 3,
    @SerializedName("panic_alarm_audiable")
    val panicAlarmAudible: Boolean = true
)


/* PRO KEYPAD 0x95
    "device_config": {
            "pin_code_size": 0,
            "panic_alarm_visual": true,
            "button_bush_volume": 3,
            "kpd_loud": 2,
            "panic_alarm_audiable": true
        }
 */

