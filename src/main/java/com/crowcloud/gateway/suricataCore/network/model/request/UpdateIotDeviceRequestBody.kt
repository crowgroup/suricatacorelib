package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

open class UpdateIotDeviceRequestBody(
    @SerializedName("node")
    var node: String,
    @SerializedName("property")
    var property: String,
    @SerializedName("value")
    var value: String
)

class ToggleSmartPlugRequest(var state: Boolean) : UpdateIotDeviceRequestBody("switch", "on", "$state".lowercase())
