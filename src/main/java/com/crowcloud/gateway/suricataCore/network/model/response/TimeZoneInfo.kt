package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class TimeZoneInfo(
    @SerializedName("id")
    var id: Int? = null,

    @SerializedName("bypass")
    var bypass: Boolean = false,

    @SerializedName("start_time")
    var startTime: String? = null,

    @SerializedName("end_time")
    var endTime: String? = null,

    @SerializedName("days")
    var daysOfWeek: List<Int>? = null,   //range of each integer is 0-6

    @SerializedName("outputs")
    var outputIds: List<Int>? = null,

    @SerializedName("doorlocks")
    var doorLockIds: List<Int>? = null,

    @SerializedName("users")
    var userIds: List<Int>? = null,

    @SerializedName("arm_areas")
    var armAreaIds: List<Int>? = null,

    @SerializedName("stay_areas")
    var stayAreaIds: List<Int>? = null,

    @SerializedName("notified")
    var notified: Boolean = false

) : Parcelable, ScheduleListItem {
    override fun getId(): Int = id ?: -1
    override fun getType(): ScheduleListItemType = ScheduleListItemType.TIME_ZONE_INFO
}


/*
{
        "bypass": false,
        "outputs": [
            3
        ],
        "start_time": "10:00",
        "doorlocks": [],
        "users": [],
        "arm_areas": [],
        "days": [
            0,
            1,
            2,
            3,
            4,
            5,
            6
        ],
        "id": 5,
        "notified": false,
        "end_time": "21:00",
        "stay_areas": []
    }
 */