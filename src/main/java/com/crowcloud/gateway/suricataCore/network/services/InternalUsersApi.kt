package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.response.LearnAccessTagResponse
import com.crowcloud.gateway.suricataCore.network.model.response.PanelUser
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by michael on 2/4/18.
 */

interface InternalUsersApi {

    @GET("/panels/{panel_id}/users/")
    suspend fun getPanelUsers(@Path("panel_id") panelId: Int): List<PanelUser>

    @GET("/panels/{panel_id}/users/{user_id}/")
    suspend fun getPanelUser(@Path("panel_id") panelId: Int, @Path("user_id") userId: Int): PanelUser

    @PUT("/panels/{panel_id}/users/{user_id}/")
    suspend fun updatePanelUser(@Header("X-Crow-CP-User") userCode: String,
                          @Path("panel_id") panelId: Int, @Path("user_id") userId: Int,
                          @Body user: PanelUser): PanelUser

    @POST("/panels/{panel_id}/users/")
    suspend fun createPanelUser(@Header("X-Crow-CP-User") userCode: String,
                          @Path("panel_id") panelId: Int,
                          @Body user: PanelUser): PanelUser

    @DELETE("/panels/{panel_id}/users/{user_id}/")
    suspend fun deletePanelUser(@Header("X-Crow-CP-User") userCode: String,
                                @Path("panel_id") panelId: Int,
                                @Path("user_id") userId: Int): PanelUser


    @GET("/panels/{panel_id}/users/")
    fun getUsersForPanel(@Path("panel_id") panelId: Int): Call<List<PanelUser>>

    @GET("/panels/{panel_id}/users/{user_id}/")
    fun getUserOfPanel(@Path("panel_id") panelId: Int, @Path("user_id") userId: Int): Call<PanelUser>

    @PUT("/panels/{panel_id}/users/{user_id}/")
    fun updateUserOfPanel(@Header("X-Crow-CP-User") userCode: String,
                          @Path("panel_id") panelId: Int, @Path("user_id") userId: Int,
                          @Body user: PanelUser): Call<PanelUser>

    @POST("/panels/{panel_id}/users/")
    fun createUserOfPanel(@Header("X-Crow-CP-User") userCode: String,
                          @Path("panel_id") panelId: Int,
                          @Body user: PanelUser): Call<PanelUser>

    @DELETE("/panels/{panel_id}/users/{user_id}/")
    fun deleteUserOfPanel(@Header("X-Crow-CP-User") userCode: String,
                          @Path("panel_id") panelId: Int, @Path("user_id") userId: Int): Call<PanelUser>

    @POST("/panels/{panel_id}/users/{user_id}/learn_access_tag/")
    fun learnUserAccessTag(@Header("X-Crow-CP-User") userCode: String,
            @Path("panel_id") panelId: Int, @Path("user_id") userId: Int) : Call<LearnAccessTagResponse>

    @GET("/panels/{panel_id}/access_tags/learn_state/")
    fun getAccessTagLearnState(//@Header("X-Crow-CP-User") userCode: String,
            @Path("panel_id") panelId: Int) : Call<LearnAccessTagResponse>

    @DELETE("/panels/{panel_id}/users/{user_id}/learn_access_tag/")
    fun deleteUserAccessTag(@Header("X-Crow-CP-User") userCode: String,
            @Path("panel_id") panelId: Int, @Path("user_id") userId: Int) : Call<LearnAccessTagResponse>

    @DELETE("/panels/{panel_id}/users/{user_id}/learn_access_tag/")
    fun cancelLearningAccessTag(@Path("panel_id") panelId: Int,
                                @Path("user_id") userId: Int) : Call<LearnAccessTagResponse>

}
