package com.crowcloud.gateway.suricataCore.network.model.response;

import com.crowcloud.gateway.suricataCore.R;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by michael on 2/19/18.
 */

public class Troubles {

    @SerializedName("fire_alarm")
    public boolean fireAlarm;

    @SerializedName("chime_is_off")
    public boolean chimeIsOff;

    @SerializedName("fuse_fail")
    public boolean fuseFail;

    @SerializedName("sdcard_fail")
    public boolean sdcardFail;

    @SerializedName("gsm_line_fail")
    public boolean gsmLineFail;

    @SerializedName("duress_alarm")
    public boolean duressAlarm;

    @SerializedName("wifi_fail")
    public boolean wifiFail;

    @SerializedName("panic_alarm")
    public boolean panicAlarm;

    @SerializedName("code_attempts_alarm")
    public boolean codeAttemptsAlarm;

    @SerializedName("can_bus_fail")
    public boolean canBusFail;

    @SerializedName("gprs_fail")
    public boolean gprsFail;

    @SerializedName("mains_fail")
    public boolean mainsFail;

    @SerializedName("ethernet_fail")
    public boolean ethernetFail;

    @SerializedName("real_time_clock_status")
    public boolean realTimeClockStatus;

    @SerializedName("phone_line_fail")
    public boolean phoneLineFail;

    @SerializedName("rf_jamming_alarm")
    public boolean rfJammingAlarm;

    @SerializedName("dect_fail")
    public boolean dectFail;

    @SerializedName("low_battery")
    public boolean lowBattery;

    @SerializedName("panel_tamper")
    public boolean panelTamper;

    @SerializedName("medical_alarm")
    public boolean medicalAlarm;

    public ArrayList<String> getTroubleNames() {
        ArrayList<String> arr = new ArrayList<>();
        if (fireAlarm) arr.add("fire_alarm");
        if (chimeIsOff) arr.add("chime_is_off");
        if (fuseFail) arr.add("fuse_fail");
        if (sdcardFail) arr.add("sdcard_fail");
        if (gsmLineFail) arr.add("gsm_line_fail");
        if (duressAlarm) arr.add("duress_alarm");
        if (wifiFail) arr.add("wifi_fail");
        if (panicAlarm) arr.add("panic_alarm");
        if (codeAttemptsAlarm) arr.add("code_attempts_alarm");
        if (canBusFail) arr.add("can_bus_fail");
        if (gprsFail) arr.add("gprs_fail");
        if (mainsFail) arr.add("mains_fail");
        if (ethernetFail) arr.add("ethernet_fail");
        if (realTimeClockStatus) arr.add("real_time_clock_status");
        if (phoneLineFail) arr.add("phone_line_fail");
        if (rfJammingAlarm) arr.add("rf_jamming_alarm");
        if (dectFail) arr.add("dect_failed");
        if (lowBattery) arr.add("trouble_low_battery");
        if (panelTamper) arr.add("panel_tamper");
        if (medicalAlarm) arr.add("medical_alarm");
        return arr;
    }

    public ArrayList<Integer> getTroubleDescriptions() {
        ArrayList<Integer> arr = new ArrayList<>();
        if (fireAlarm) arr.add(R.string.fire_alarm);
        if (chimeIsOff) arr.add(R.string.chime_is_off);
        if (fuseFail) arr.add(R.string.fuse_fail);
        if (sdcardFail) arr.add(R.string.sdcard_fail);
        if (gsmLineFail) arr.add(R.string.gsm_line_fail);
        if (duressAlarm) arr.add(R.string.duress_alarm);
        if (wifiFail) arr.add(R.string.wifi_fail);
        if (panicAlarm) arr.add(R.string.panic_alarm);
        if (codeAttemptsAlarm) arr.add(R.string.code_attempts_alarm);
        if (canBusFail) arr.add(R.string.can_bus_fail);
        if (gprsFail) arr.add(R.string.gprs_fail);
        if (mainsFail) arr.add(R.string.mains_fail);
        if (ethernetFail) arr.add(R.string.ethernet_fail);
        if (realTimeClockStatus) arr.add(R.string.real_time_clock_status);
        if (phoneLineFail) arr.add(R.string.phone_line_fail);
        if (rfJammingAlarm) arr.add(R.string.rf_jamming_alarm);
        if (dectFail) arr.add(R.string.dect_failed);
        if (lowBattery) arr.add(R.string.trouble_low_battery);
        if (panelTamper) arr.add(R.string.panel_tamper);
        if (medicalAlarm) arr.add(R.string.medical_alarm);
        return arr;
    }
}
