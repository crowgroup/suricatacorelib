package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigSmokeHeatSyncDetectorRequest(
    @SerializedName("enable_burglary_alarm")
    val enableBurglaryAlarm: Boolean = false,
    @SerializedName("del_time")
    val delTime: Int = 10,
    @SerializedName("enable_fire_alarm")
    val enableFireAlarm: Boolean = true,
    @SerializedName("enable_water_alarm")
    val enableWaterAlarm: Boolean = false,
    @SerializedName("indication")
    val enableIndication: Boolean = true,
    @SerializedName("heat_enabled")
    val enableHeat: Boolean = true,
    @SerializedName("buzzer_loudness")
    val buzzerLoudness: Int = 1
)


/* Smoke heat (sync)
  "device_config": {
            "enable_burglary_alarm": false,
            "del_time": 0,
            "enable_fire_alarm": true,
            "enable_water_alarm": false,
            "indication": true,
            "heat_enabled": true,
            "buzzer_loudness": 1
        }
 */
