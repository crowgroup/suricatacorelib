package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

/**
 * Created by michael on 2/20/18.
 */
class PanelConnectionState {
    @SerializedName("interface")
    var interfaceId: Int? = null

    @SerializedName("connected")
    var connected = false
}
