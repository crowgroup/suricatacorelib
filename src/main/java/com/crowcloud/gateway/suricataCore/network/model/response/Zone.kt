package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.crowcloud.gateway.suricataCore.R
import com.crowcloud.gateway.suricataCore.util.CrowDeviceType
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.util.*

/**
 * Created by michael on 2/18/18.
 */

@Parcelize
open class Zone(
    override var deviceType: CrowDeviceType = CrowDeviceType.ZONE,

    @SerializedName("tamper_alarm")
    val tamperAlarm: Boolean = false,

    @SerializedName("battery_low")
    val batteryLow: Boolean = false,

    @SerializedName("temperature")
    val deviceTemperature : Int = 0,

    @SerializedName("dect_id")
    val dectId: DectId? = null,

    @SerializedName("stay_entry_delay")
    val stayEntryDelay: Int = 0,

    @SerializedName("device")
    val device: String? = null,          //zone type

    @SerializedName("chime")
    val chime: Boolean = false,

    @SerializedName("permanent_chime")
    val permanentChime: Boolean = false,

    @SerializedName("zone_24H_alarm")
    val zone24HAlarm: Boolean = false,

    @SerializedName("work_mode")
    val workMode: Int = 0,

    @SerializedName("areas")
    val areas: List<Int>? = null,

    @SerializedName("bypass")
    val bypass: Boolean = false,

    @SerializedName("supervision_alarm")
    val supervisionAlarm: Boolean = false,

    @SerializedName("active")
    val active: Boolean = false,

    @SerializedName("stay_mode_zone")
    val stayModeZone: Boolean = false,

    @SerializedName("sensor_watch_alarm")
    val sensorWatchAlarm: Boolean = false,

    @SerializedName("arm_entry_delay")
    val armEntryDelay: Int = 0

): BaseDevice(), Parcelable {

    fun isWired(): Boolean = typeCodeName().contains("Wired") //type == 0x01
    fun isCamera(): Boolean = typeCodeName().contains("CAM") //|| typeCodeName().contains("PIR_CT2035")
    fun isSmokeDetector(): Boolean = typeCodeName().contains("SMK")
    fun isFloodDetector(): Boolean = typeCodeName().contains("FLD")
    fun isGlassBreakDetector(): Boolean = typeCodeName().contains("GBD")
    fun isGasDetector(): Boolean = typeCodeName().contains("GAS")
    fun isMotionDetector(): Boolean = typeCodeName().contains("PIR") && !typeCodeName().contains("CAM")
                                                                   // && !typeCodeName().contains("PIR_CT2035")
    fun isHomeAutomation(): Boolean = typeCodeName().contains("HAM") // || typeCodeName().contains("IO_2_2")
    fun isTemperatureDetector(): Boolean = typeCodeName().contains("TMP") ||
            (typeCodeName().contains("TFM") && typeName().contains("Temperature"))
    fun isAirQualityDetector(): Boolean = type == 0x3D
    fun isAdvancedAirQualityDetector(): Boolean = type == 0x4D
    fun isMagnet(): Boolean = typeCodeName().contains("MAG") || typeCodeName().contains("VIB")

    // TODO: 5/28/21
    fun isOccupancy(): Boolean = typeCodeName().contains("OCP")
    fun isDectTempHumAir(): Boolean =
                typeCodeName().contains("TFM") || typeCodeName().contains("ARP")

    override val troublesList: ArrayList<String>
        get() {
            val troubles = super.troublesList
            if (tamperAlarm) troubles.add("tamper_alarm")
            if (batteryLow) troubles.add("battery_low")
            if (supervisionAlarm) troubles.add("supervision_alarm")
            if (sensorWatchAlarm) troubles.add("sensor_watch_alarm")
            if (zone24HAlarm) troubles.add("zone_24H_alarm")
            if (state) troubles.add("open")
            if (!active) troubles.add("active")
            return troubles
        }

    fun getTroubles(): ArrayList<Int> {
        val arr = ArrayList<Int>()
        if (tamperAlarm) arr.add(R.string.trouble_tamper_alarm)
        if (batteryLow) arr.add(R.string.trouble_low_battery)
        if (supervisionAlarm) arr.add(R.string.supervision_alarm)
        if (sensorWatchAlarm) arr.add(R.string.sensor_watch_alarm)
        if (zone24HAlarm) arr.add(R.string.zone_24H_alarm)
        if (state) arr.add(R.string.open)
        return arr
    }
}