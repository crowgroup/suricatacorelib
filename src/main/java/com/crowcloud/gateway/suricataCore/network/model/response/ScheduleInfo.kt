package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class ScheduleInfo(

    @SerializedName("id")
    var id: Int? = null,

    @SerializedName("created")
    var created: Date? = null,

    @SerializedName("control_panel_user")
    var scheduleUser: ScheduleUser? = null,

    @SerializedName("updated")
    var updated: Date? = null,

    @SerializedName("timezone")
    var timezone: String? = null,        // form: "[Continent]/[City]" i.e. "Asia/Jerusalem"

    @SerializedName("disabled")
    var disabled: Boolean? = null,

    @SerializedName("next_schedule")
    var nextSchedule: Date? = null,

    @SerializedName("days_of_week")
    var daysOfWeek: List<Int>? = null,   //range of each integer is 0-6

    @SerializedName("hour")
    var hour: Int? = null,              //0-23

    @SerializedName("minute")
    var minute: Int? = null,             //0-59

    @SerializedName("action")
    var action: ScheduleAction? = null,

    @SerializedName("params")
    var params: ScheduleParams? = null

) : Parcelable, ScheduleListItem {
    override fun getId(): Int = id ?: -1
    override fun getType(): ScheduleListItemType = ScheduleListItemType.SCHEDULE_INFO
}