package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class PanelGroup(
    @SerializedName("id")
    val id: Int = 0,

    @SerializedName("name")
    val name: String = "",

    @SerializedName("parent")
    val parent: Int? = null,

    @SerializedName("level")
    val level: Int = 0,

    @SerializedName("lft")
    val left: Int = 0,

    @SerializedName("rght")
    val right: Int = 0,

    @SerializedName("block_panel_listing")
    val blockPanelListing: Boolean = false,

    @SerializedName("deploy_level")
    val deployLevel: Int = 0,

    @SerializedName("min_firmware_view_level")
    val minFirmwareViewLevel: Int? = null
) : Parcelable