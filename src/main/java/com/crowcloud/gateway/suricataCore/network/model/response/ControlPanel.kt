package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ControlPanel(

    @SerializedName("id")
    var id: Int? = null,

    @SerializedName("mac")
    var mac: String? = null,

    @SerializedName("remote_access_password")
    var remoteAccessPassword: String? = null,

    @SerializedName("state")
    var state: String? = null,

    @SerializedName("version")
    var version: String? = null,

    @SerializedName("hw_variant")
    var hwVariant: Int? = null,

    @SerializedName("host")
    var host: String? = null,

    @SerializedName("state_full")
    var stateFull: Int? = null,

    @SerializedName("pair_type")
    var pairType: Int? = null,

    @SerializedName("panel_usage_status_desc")
    var panelUsageStatusDesc: String? = null,

    @SerializedName("longitude")
    var longitude: Double? = null,

    @SerializedName("latitude")
    var latitude: Double? = null,

    @SerializedName("geo_timezone")
    var geoTimezone: String? = null,

    @SerializedName("geo_country")
    var geoCountry: String? = null,

    @SerializedName("group_id")
    var groupId: Int? = null,

    @SerializedName("foreign_key")
    var foreignKey: String? = null,

    @SerializedName("base_reporting_account")
    var baseReportingAccount: String? = null,

    @SerializedName("panic_enabled")
    var panicEnabled: Boolean? = null,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("last_report_heartbeat")
    var lastReportHeartbeat: String? = null,

    @SerializedName("report_heartbeat_expired")
    var reportHeartbeatExpired: Boolean? = null,

    @SerializedName("meari_uid")
    var meariUid: String? = null,

    @SerializedName("meari_country")
    var meariCountry: String? = null,

    @SerializedName("meari_exists")
    var meariExists: Boolean = false,

    @SerializedName("meari_doorbell_exists")
    var meariDoorbellExists: Boolean = false,

    @SerializedName("has_ip_cams_associated")
    var hasIpCamsAssociated: Boolean = false,

    @SerializedName("has_doorbell_cams_associated")
    var hasDoorbellCamsAssociated: Boolean = false,

    @SerializedName("mediator_version")
    var mediatorVersion: Int = 1,

    @SerializedName("api_version")
    var apiVersion: Int = 1

) : Parcelable
