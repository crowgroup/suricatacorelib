package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class Area(

    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("state")
    var state: AreaState? = null,

    @SerializedName("fire_alarm")
    var isFireAlarm: Boolean = false,

    @SerializedName("panic_alarm")
    var isPanicAlarm: Boolean = false,

    @SerializedName("zone_alarm")
    var isZoneAlarm: Boolean = false,

    @SerializedName("en_alarm")
    var isEnAlarm: Boolean = false,

    @SerializedName("duress_alarm")
    var isDuressAlarm: Boolean = false,

    @SerializedName("zone24H_alarm")
    var isZone24HAlarm: Boolean = false,

    @SerializedName("code_attempts_alarm")
    var isCodeAttemptsAlarm: Boolean = false,

    @SerializedName("medical_alarm")
    var isMedicalAlarm: Boolean = false,

    @SerializedName("emergency_alarm")
    var isEmergencyAlarm: Boolean = false,

    @SerializedName("ready_to_arm")
    var isReadyToArm: Boolean = false,

    @SerializedName("ready_to_stay")
    var isReadyToStay: Boolean = false,

    @SerializedName("empty")
    var isEmpty: Boolean = false,

    @SerializedName("exit_delay")
    var exitDelay: Int = 0,

    @SerializedName("stay_exit_delay")
    var stayExitDelay: Int = 0

) : Parcelable {

    @Parcelize
    enum class AreaState : Parcelable {
        @SerializedName("disarmed")
        Disarmed,

        @SerializedName("armed")
        Armed,

        @SerializedName("stay_armed")
        StayArmed1,

        @SerializedName("stay")
        StayArmed2,

        @SerializedName("arm in progress")
        ArmInProgress1,

        @SerializedName("arm_in_progress")
        ArmInProgress2,

        @SerializedName("stay arm in progress")
        StayArmInProgress1,

        @SerializedName("arm_stay_in_progress")
        StayArmInProgress2
    }
}
