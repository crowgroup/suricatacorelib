package com.crowcloud.gateway.suricataCore.network.model.response

import com.crowcloud.gateway.suricataCore.util.CrowDeviceType
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.util.ArrayList

@Parcelize
class VoiceBox(

    override var deviceType: CrowDeviceType = CrowDeviceType.VOICE_BOX,

    @SerializedName("tamper_alarm")
    val tamperAlarm: Boolean = false,

    @SerializedName("battery_low")
    val batteryLow: Boolean = false,

    @SerializedName("ac_fail")
    var acFail: Boolean = false,

    @SerializedName("trouble_alarm")
    var troubleAlarm: Boolean = false,

    @SerializedName("missing")
    var isMissing: Boolean = false,

    @SerializedName("battery")
    var battery: Int = 0,

    @SerializedName("areas")
    var areas: List<Int>? = null

    ) : BaseDevice() {

    override val troublesList: ArrayList<String>
        get() {
            val troubles = super.troublesList
            if (acFail) troubles.add("ac_fail")
            if (isMissing) troubles.add("missing")
            if (tamperAlarm) troubles.add("tamper_alarm")
            if (batteryLow) troubles.add("battery_low")
            if (troubleAlarm) troubles.add("trouble_alarm")
            return troubles
        }
}