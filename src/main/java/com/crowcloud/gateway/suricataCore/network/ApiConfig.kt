package com.crowcloud.gateway.suricataCore.network

import com.crowcloud.gateway.suricataCore.BuildConfig

object ApiConfig {
    const val DEV_BASE_URL = "https://api.crowcloud.xyz" //"https://api.crowcloud.xyz" //"https://api.dev-env.crowcloud.xyz"
    const val PROD_BASE_URL = "https://api.crowcloud.xyz" //"https://api.crowcloud.xyz"

    val currentBaseUrl: String
        get() = if (BuildConfig.DEBUG) DEV_BASE_URL else PROD_BASE_URL
}
