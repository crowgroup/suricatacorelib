package com.crowcloud.gateway.suricataCore.network.model.response

/**
 * Created by michael on 2/18/18.
 */

import com.google.gson.annotations.SerializedName

class NotificationsFilter {

    @SerializedName("information")
    var information: Boolean = false

    @SerializedName("alarm")
    var alarm: Boolean = false

    @SerializedName("troubles")
    var troubles: Boolean = false

    @SerializedName("take_picture")
    var takePicture: Boolean = false

    @SerializedName("user_association")
    var userAssociation: Boolean = false

    @SerializedName("configuration")
    var configuration: Boolean = false

    @SerializedName("arm")
    var arm: Boolean = false

    @SerializedName("all")
    var all: Boolean = false
}