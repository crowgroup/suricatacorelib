package com.crowcloud.gateway.suricataCore.network

import org.jdeferred.Promise
import org.jdeferred.android.AndroidDeferredObject
import org.jdeferred.android.AndroidExecutionScope
import org.jdeferred.impl.DeferredObject

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Wraps Network callback with a promise
 */
class DeferredCallback<T> : Callback<T> {

    private val deferred: AndroidDeferredObject<T, Throwable, Void> =
            AndroidDeferredObject(DeferredObject(), AndroidExecutionScope.BACKGROUND)

    override fun onResponse(call: Call<T>, response: Response<T>) {
        deferred.resolve(response.body())
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        deferred.reject(t)
    }

    fun promise(): Promise<T, Throwable, Void> {
        return deferred.promise()
    }
}
