package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.request.WifiConfigRequest
import com.crowcloud.gateway.suricataCore.network.model.response.*
import okhttp3.ResponseBody
import retrofit2.http.*

/**
 * Created by michael on 2/20/18.
 */

interface PanelCommApi {

    @GET("/panels/{panel_id}/connection/")
    suspend fun getConnectionState(@Path("panel_id") panelId: Int): PanelConnectionState

    //INFO

    @GET("/panels/{panel_id}/module/ethernet/info/")
    suspend fun getEthernetInfo(@Path("panel_id") panelId: Int): EthernetInfo

    @GET("/panels/{panel_id}/module/wifi/info/")
    suspend fun getWifiInfo(@Path("panel_id") panelId: Int): WifiInfo

    @GET("/panels/{panel_id}/module/gsm/info/")
    suspend fun getGsmInfo(@Path("panel_id") panelId: Int): GsmInfo

    @GET("/panels/{panel_id}/module/radio/info/")
    suspend fun getRadioInfo(@Path("panel_id") panelId: Int): RadioInfo

    @POST("/panels/{panel_id}/module/wifi/list/")
    suspend fun getWifiScanList(@Path("panel_id") panelId: Int): List<ConnectionConfigItem>

    @GET("/panels/{panel_id}/module/wifi/config/")
    suspend fun getWifiConfig(@Path("panel_id") panelId: Int): WifiConfig

    @GET("/panels/{panel_id}/module/ethernet/config/")
    suspend fun getEthernetConfig(@Path("panel_id") panelId: Int): EthernetConfig

    @GET("/panels/{panel_id}/module/gsm/config/")
    suspend fun getGsmConfig(@Path("panel_id") panelId: Int): GsmConfig

    @GET("/panels/{panel_id}/config_mode/")
    suspend fun getConfigMode(
                            @Header("X-Crow-CP-remote") remoteAccessPassword: String,
                            @Path("panel_id") panelId: Int) : ConfigResponse

    @POST("/panels/{panel_id}/config_mode/")
    suspend fun setConfigMode(
                            @Header("X-Crow-CP-remote") remoteAccessPassword: String,
                            @Header("X-Crow-CP-installer") installerCode: String,
                            @Path("panel_id") panelId: Int) : retrofit2.Response<ResponseBody>

    @DELETE("/panels/{panel_id}/config_mode/")
    suspend fun deleteConfigMode(
                            @Header("X-Crow-CP-remote") remoteAccessPassword: String,
                            @Header("X-Crow-CP-installer") installerCode: String,
                            @Path("panel_id") panelId: Int) : retrofit2.Response<ResponseBody>

    @PUT("/panels/{panel_id}/module/wifi/config/")
    suspend fun setWifiConfig(@Path("panel_id") panelId: Int,
                            @Body wifiConfigRequest: WifiConfigRequest): WifiConfig

    @PUT("/panels/{panel_id}/module/ethernet/config/")
    suspend fun setEthernetConfig(@Path("panel_id") panelId: Int, @Body ethernetConfig: EthernetConfig)
                                                                    : EthernetConfig

    @PUT("/panels/{panel_id}/module/gsm/config/")
    suspend fun setGsmConfig(@Path("panel_id") panelId: Int, @Body gsmConfig: GsmConfig): GsmConfig
}
