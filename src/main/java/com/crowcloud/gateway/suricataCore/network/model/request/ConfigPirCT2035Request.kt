package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigPirCT2035Request(
    @SerializedName("time_2p_3p_53")
    val time2p3p53: Int = 4,
    @SerializedName("time_1p_2p_53")
    val time1p2p53: Int = 6,
    @SerializedName("conf_time_1p_2p_53")
    val confTime1p2p53: Int = 1,
    @SerializedName("pls_dir_mode_53")
    val plsDirMode53: Int = 1,
    @SerializedName("supervision")
    val supervision: Int = 0,
    @SerializedName("gain_level")
    var gainLevel: Int = 2,
    @SerializedName("pet_immuni")
    var petImmuni: Boolean = false,
    @SerializedName("zone_led")
    val enableLed: Boolean = true,
    @SerializedName("num_pulses")
    val numPulses: Int = 1
)


/*
  "device_config": {
            "time_2p_3p_53": 4,
            "time_1p_2p_53": 6,
            "conf_time_1p_2p_53": 1,
            "pls_dir_mode_53": 1,
            "supervision": 0,
            "gain_level": 2,
            "pet_immuni": true,
            "zone_led": true,
            "num_pulses": 1
        }
 */