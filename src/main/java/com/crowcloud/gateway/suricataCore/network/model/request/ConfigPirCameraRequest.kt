package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigPirCameraRequest(
    @SerializedName("num_pictur")
    val numberOfPictures: Int = 2,
    @SerializedName("pict_rate")
    val pictureRate: Int = 1,
    @SerializedName("quality")
    val quality: Int = 4,
    @SerializedName("diff_ratio")
    val diffRatio: Boolean = false,
    @SerializedName("diff_jpeg")
    val diffJpeg: Boolean = false,
    @SerializedName("sharpness")
    val sharpness: Boolean = true,
    @SerializedName("camera")
    val camera: Boolean = true,
    @SerializedName("gain_level")
    var gainLevel: Int = 2,
    @SerializedName("pet_immuni")
    var petImmuni: Boolean = false,
    @SerializedName("hold_off")
    val holdOff: Int = 4,
    @SerializedName("res_color")
    val resColor: Int = 3,
    @SerializedName("zone_led")
    val zoneLed: Boolean = true,
    @SerializedName("contrast")
    val contrast: Boolean = true,
    @SerializedName("num_pulses")
    val numPulses: Int = 3
    )

/*
    "device_config": {
            "num_pictur": 2,
            "pict_rate": 1,
            "quality": 4,
            "diff_ratio": false,
            "diff_jpeg": false,
            "sharpness": true,
            "camera": true,
            "gain_level": 2,
            "pet_immuni": false,
            "hold_off": 4,
            "res_color": 3,
            "zone_led": true,
            "contrast": true,
            "num_pulses": 1
        },
 */