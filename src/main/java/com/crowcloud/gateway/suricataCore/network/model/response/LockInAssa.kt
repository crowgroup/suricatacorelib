package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
class LockInAssa : BaseLock() {

//    @SerializedName("owner")
//    val owner: LockOwner? = null                    //not in use

    @SerializedName("name")
    val name: String? = null

    @SerializedName("serial")
    val serial: String? = null

    @SerializedName("control_panel")
    val controlPanel: Int = 0                      //not in use

    @SerializedName("control_panel_user")
    val controlPanelUser: Int = 0                  //not in use

    @SerializedName("mac")
    val mac: String? = null                        //name of panel will be added later

    @SerializedName("lockChain")
    val lockChain: List<YaleLockInfo> = ArrayList() //Only one item
}

/*
{
  "result": [
    {
      "id": "60465ef4cfe71184c195fecd",
      "owner": {
        "emailAddresses": [
          "tim7@tslp.me"
        ],
        "phoneNumbers": [
          "0501032417"
        ],
        "user": 144,
        "lockOwnerId": "76e48ef7-8371-499a-9267-5247be6105ec",
        "deleted": "string",
        "locale": "string",
        "id": "604658f928cec3c39785f59a"
      },
      "lockOwnerId": "76e48ef7-8371-499a-9267-5247be6105ec",
      "control_panel": 8070,
      "control_panel_user": 17425,
      "mac": "0013a120132c",
      "lockChain": [
        {
          "location": "Rehovot",
          "model": "CROW-01",
          "lockingTechnology": "PIN",
          "portalType": "DOOR"
        }
      ]
    }
  ]
}
 */