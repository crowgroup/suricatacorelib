package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName
import java.util.*

enum class ErrorField(vararg values: String) {

    @SerializedName("non_field_errors")
    NonFieldError("non_field_errors"),

    @SerializedName("username")
    UserName("username"),

    @SerializedName(value = "old_password")
    OldPassword("old_password"),

    @SerializedName(value = "password1", alternate = ["new_password1", "password"])
    Password("password1", "new_password1", "password"),

    @SerializedName(value = "password2", alternate = ["new_password2"])
    Password2("password2", "new_password2"),

    @SerializedName("last_name")
    LastName("last_name"),

    @SerializedName("first_name")
    FirstName("first_name"),

    @SerializedName("email")
    Email("email");

    private val valueList = values.toList()

    companion object {
        fun fromValue(value: String): ErrorField =
            ErrorField.values().firstOrNull { it.valueList.contains(value) } ?: NonFieldError
    }
}
