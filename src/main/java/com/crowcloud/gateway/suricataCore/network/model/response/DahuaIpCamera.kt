package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName
import java.util.*

class DahuaIpCamera {
    @SerializedName("device_id")
    var deviceId: String? = null
    @SerializedName("device_name")
    var deviceName: String? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("code")
    var devicePassword: String? = null

    @SerializedName("id")
    var id: Int? = null
//    @SerializedName("panel_id")
//    var panelId: String? = null
    @SerializedName("created")
    var created: Date? = null
    @SerializedName("is_online")
    var isOnline: Boolean? = null
}

/*
  "id": 3,
            "name": "Test",
            "panel_id": 9776,
            "device_name": "Test",
            "device_id": "5F03BD9PAGFA5A4",
            "created": "2022-08-31T13:53:14.799641Z",
            "device": {
                "device_id": "5F03BD9PAGFA5A4",
                "status": "online",
                "device_model": "IPC-HFW4239T-ASE",
                "catalog": "IPC",
                "brand": "easy4Ip",
                "version": "Eng_PN_V2.800.0000005.0.R.20190325",
                "name": "IPC-HFW4239T-ASE-A5A4",
                "ability": "Auth,DHP2P,HSEncrypt,CloudStorage,LocalStorage,PlaybackByFilename,MT,CK,RD,LocalRecord,XUpgrade,TimeSync,ModifyPassword,LocalStorageEnable,DaySummerTime,WeekSummerTime,RemoteControl,FrameReverse,AudioEncode,AudioEncodeControl,AlarmMD,MDW,MDS,FaceDetect,AudioTalk,AlarmSound",
                "access_type": "PaaS",
                "encrypt_mode": "0",
                "play_token": "4SCmAWDp/HpZeVq0aKzHPnRPvgoxO8RKztjoGURbdBvZDWV0WSirph1x4max3bwmGFJ01xj5tclzvbe7wb8EBwXLAp2lVhmXC+0SJN6o7FS9f/XgBZbgvhHBxBoCU5raMa+QxHg5m3e6eP8Mism0TCXrToYaxKST+RlwEZCENUnl+JzxPTrEJCjd8E1gMnmUOsdy7apJ+0UlZyQiihNZaVy7cqn5Ahs0HOY2a6Ir5i7KZVkGtOxyBqEU4a6N8FOZW0PoSzvZrNpsT4W2GFqLpAIInqeBymvWCxjt6GSCKXlEggLZHckga+o2snrTXJGBgS9zLbX3F3QKUiqj4nc4daBXLI2MTIM9RRfhKQXI5B9DPLGXfCkFn36yFiA/eNQieH7w1hb5V84/KNMz0ZA4FSWiR2glxsAohkJ/aum/JBoWbKCUWfMN077jB8yWlZQnPNhLfhhUMbKQpALWE8SCSnTtsTe338Z+QIuFvF0ya2M/r3HC/7gPqQhncwypqMpAbgsqp9MdC6R49cwXz8iaGofxL0KBaKEqe2QTvYONK0xXz1+OBAWXGfRk/HqM4MXMjQcYVCCNf5Kd6ASPw2OCg1dITOm+ciG4pA2WDV39YLQfu1YvVaPnLC6IsbUkMjMzqwXlhb43qa0anL7g0TiY9M0grEKSZEvzFiQyG6xNYPehcdv1g29Rb90kFmLqFY2a3ogF5EXhn1HZ8GKx21tVED5uYfAZocbvhPKnfA4rW0zuJEHReHw24oiSXuwRuSQ5e+FUdoiwRkpMXpbnlFvmYLdwRCowZIs2urD8gQ5odtXVqjv8/wgzFKm1cv36o+oj2YavSlbCZDVJrmfvfcZoJdNoa5HEBxQNIdlPPE9F9X8psOoT/EHbgtxHTmVWD59VCynVb7c76P4/0tHj+lOAdGaTWHIfjJqY8XW+sypvFix+qFVmPCyeQwjBQ+PN4KJuAU3heavhKDaaFQDRcIX4DlyD5v4Kv9c316c9xhNYG+XZ07mZ7IrUSTboy1Wzb9P5YoD96Z6QfgBovGpFqKWDgeKnWHtPZXtGEglcvUe8Gy+kFF/R5QsvFMUeU3Z5yyO6Va7n8APV/TzH2zi/fgrzkQ==",
                "http_port": "80",
                "private_port": "37777",
                "rtsp_port": "554",
                "tls_private_port": "37778",
                "channels": [
                    {
                        "channel_id": "0",
                        "device_id": "5F03BD9PAGFA5A4",
                        "channel_name": "5F03BD9PAGFA5A4-1",
                        "ability": "",
                        "status": "online",
                        "pic_url": "",
                        "remind_status": "off",
                        "camera_status": "off",
                        "storage_strategy_status": "notExist",
                        "share_status": "",
                        "share_functions": "",
                        "resolutions": []
                    }
                ],
                "aplist": []
            }
        }
 */