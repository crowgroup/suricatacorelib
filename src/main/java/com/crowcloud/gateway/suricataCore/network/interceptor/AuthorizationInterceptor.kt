package com.crowcloud.gateway.suricataCore.network.interceptor

import android.util.Base64
import android.util.Log
import com.crowcloud.gateway.suricataCore.auth.LoginManagerConfig
import com.crowcloud.gateway.suricataCore.events.EventSessionExpired
import com.crowcloud.gateway.suricataCore.extensions.post
import com.crowcloud.gateway.suricataCore.util.AccessTokenProvider
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import java.util.Locale
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

/**
 * Created by michael on 2/4/18.
 *
 * Handles Authorization and refresh token
 * (Adds Authorization header to request , refreshes token if needed.)
 */


class AuthorizationInterceptor(
    private val config: LoginManagerConfig,
    private val accessTokenProvider: AccessTokenProvider
) : Interceptor {

    companion object {
        private const val TAG = "AuthorizationInterceptor"
    }

    private val refreshMutex = Mutex() // Mutex to synchronize token refresh

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val requestBuilder = request.newBuilder()
            .addHeader("X-Crow-Language", Locale.getDefault().language)

        Log.d(TAG, "intercept: Request intercepted for URL: ${request.url}")

        return when {
            request.url.toString().contains("/o/token/") -> handleTokenRequest(chain, requestBuilder)
            request.url.toString().contains("/auth/registration/") -> handleRegistrationRequest(chain, requestBuilder)
            request.url.toString().contains("/general/tos/") -> handleGetLatestTOSRequest(chain, requestBuilder)
            request.url.toString().contains("/auth/password/reset/") -> handleResetPasswordRequest(chain, requestBuilder)
            else -> handleAuthenticatedRequest(chain, request, requestBuilder)
        }
    }

    private fun handleTokenRequest(chain: Interceptor.Chain, requestBuilder: Request.Builder): Response {
        Log.d(TAG, "Intercepting token-related request")
        val authValue = Base64.encodeToString(
            "${config.clientId}:${config.clientSecret}".toByteArray(),
            Base64.NO_WRAP
        )
        requestBuilder.addHeader("Authorization", "Basic $authValue")
        return chain.proceed(requestBuilder.build())
    }

    private fun handleRegistrationRequest(chain: Interceptor.Chain, requestBuilder: Request.Builder): Response {
        Log.d(TAG, "Intercepting registration request")
        return chain.proceed(requestBuilder.build())
    }

    private fun handleGetLatestTOSRequest(chain: Interceptor.Chain, requestBuilder: Request.Builder): Response {
        Log.d(TAG, "Intercepting getLatestTOS request")
        return chain.proceed(requestBuilder.build())
    }

    private fun handleResetPasswordRequest(chain: Interceptor.Chain, requestBuilder: Request.Builder): Response {
        Log.d(TAG, "Intercepting resetPassword request")
        return chain.proceed(requestBuilder.build())
    }

    private fun handleAuthenticatedRequest(
        chain: Interceptor.Chain,
        request: Request,
        requestBuilder: Request.Builder
    ): Response {
        runBlocking {
            Log.d(TAG, "Intercepting authenticated request")
            val bearerToken = ensureValidAccessToken()
            requestBuilder.addHeader("Authorization", "Bearer $bearerToken")
        }

        val response = chain.proceed(requestBuilder.build())
        return handleUnauthorizedResponse(chain, request, response)
    }

    private fun handleUnauthorizedResponse(
        chain: Interceptor.Chain,
        request: Request,
        response: Response
    ): Response {
        if (response.code == 401) {
            Log.d(TAG, "Unauthorized response received (401). Attempting token refresh.")

            runBlocking {
                if (!refreshTokenWithLock()) {
                    throw IOException("Unauthorized - session expired. Redirect to login.")
                }
            }

            val newToken = accessTokenProvider.getCurrentAccessToken()
            val retryRequest = request.newBuilder()
                .addHeader("Authorization", "Bearer $newToken")
                .build()

            Log.d(TAG, "Retrying request with new token")
            return chain.proceed(retryRequest)
        }

        return response
    }

    private suspend fun refreshTokenWithLock(): Boolean {
        // Synchronize refresh token calls
        return refreshMutex.withLock {
            if (accessTokenProvider.hasValidAccessToken()) {
                // Token was refreshed while waiting, no need to refresh again
                Log.d(TAG, "Token already refreshed by another request")
                return@withLock true
            }

            Log.d(TAG, "Refreshing token...")
            if (!accessTokenProvider.refreshTokenIfNeeded()) {
                Log.e(TAG, "Token refresh failed. Clearing session.")
                accessTokenProvider.clearSession()
                post(EventSessionExpired("Your session has expired. Please log in again."))
                return@withLock false
            }
            Log.d(TAG, "Token refresh successful")
            true
        }
    }

    private suspend fun ensureValidAccessToken(): String {
        if (!accessTokenProvider.hasValidAccessToken()) {
            Log.d(TAG, "Access token invalid. Triggering token refresh.")
            refreshTokenWithLock()
        }
        return accessTokenProvider.getCurrentAccessToken()
            ?: throw IOException("Access token is missing after refresh.")
    }
}





