package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName


data class SerialNumber(@SerializedName("serial") val serial: Int? = null)