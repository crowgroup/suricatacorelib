package com.crowcloud.gateway.suricataCore.network.model.response

interface BasePanel {
    val id: Int?
    val name: String?
    val role: Int?
    val receivePictures: Boolean
    val controlPanel: ControlPanel?
    val user: User?
}
