package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class LocalUser (
    @SerializedName("db_data")
    val dbData: LocalDbData?,

    @SerializedName("mediator_data")
    val mediatorData: LocalMediatorData?
)

data class LocalDbData(
    @SerializedName("role")
    val role: Int,
)

data class LocalMediatorData(
    @SerializedName("name")
    val name: String,

    @SerializedName("user_code")
    val userCode: String,
)
