package com.crowcloud.gateway.suricataCore.network.model.response

import com.crowcloud.gateway.suricataCore.util.smil.VideoLink
import com.google.gson.annotations.SerializedName

class StreamData {

    @SerializedName("smil")
    var smil: HashMap<String, VideoLink>? = null //xml

    fun parse(): ArrayList<VideoLink> {
        val links = smil?.values?.let { ArrayList(it) } ?: return arrayListOf()
        links.sortByDescending { it.width }
        return links
    }
}
