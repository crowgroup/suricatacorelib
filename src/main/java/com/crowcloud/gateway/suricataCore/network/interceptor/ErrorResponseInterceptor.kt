package com.crowcloud.gateway.suricataCore.network.interceptor

import android.util.Log
import com.crowcloud.gateway.suricataCore.exception.CrowNetworkException
import com.crowcloud.gateway.suricataCore.extensions.post
import com.crowcloud.gateway.suricataCore.network.NetworkManager
import com.crowcloud.gateway.suricataCore.network.model.response.ErrorResponse
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * Handles response errors
 */

class ErrorResponseInterceptor : Interceptor {

    companion object {
        private const val TAG = "ErrorResponseInterceptor"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        Log.d(TAG, "Processing request for URL: ${request.url}")

        val response = try {
            chain.proceed(request)
        } catch (e: IOException) {
            Log.e(TAG, "Network error occurred: ${e.message}", e)
            handleNetworkError(e) // Handle network-level exceptions
            throw e // Re-throw to propagate
        }

        if (!response.isSuccessful) {
            Log.d(TAG, "HTTP error received: ${response.code}")
            handleApplicationError(response) // Handle HTTP-level errors
        }

        return response
    }

    private fun handleApplicationError(response: Response) {
        // Generate an appropriate exception for the HTTP error
        val exception = when (response.code) {
            403 -> {
                Log.d(TAG, "403 Forbidden - Credentials required")
                CredentialsRequiredException("403 - Forbidden: Credentials required, 403", response)
            }
            429 -> {
                Log.d(TAG, "429 Too Many Requests - Rate limited")
                TooManyAttemptsException("Too many attempts, please try later, 429", response)
            }
            503 -> {
                Log.d(TAG, "503 Service Unavailable")
                CrowNetworkException(503, "Cannot connect to server, 503", null, null)
            }
            500 -> {
                Log.d(TAG, "500 Internal Server Error")
                CrowNetworkException(500, "Internal server error, 500", null, null)
            }
            408 -> {
                Log.d(TAG, "408 Request Timeout")
                CrowNetworkException(408, "Request timeout, 408", null, null)
            }
            405 -> {
                Log.d(TAG, "405 Method Not Allowed - Wrong installer code")
                CrowNetworkException(405, "Wrong installer code, 405", null, null)
            }
            else -> {
                Log.d(TAG, "Unhandled HTTP error: ${response.code}")
                parseErrorBody(response)
            }
        }

        // Optionally post the error to an event bus
        post(exception)

        // Throw the exception to propagate to the caller
        throw exception
    }

    private fun handleNetworkError(error: Throwable) {
        val exception = when (error) {
            is SocketTimeoutException -> CrowNetworkException(0, "Network timeout", null, error)
            is UnknownHostException -> CrowNetworkException(0, "No internet connection", null, error)
            else -> CrowNetworkException(0, "Network error", null, error)
        }

        // Post the network error to an event bus (optional)
        post(exception)

        // Throw the network exception
        throw exception
    }

    private fun parseErrorBody(response: Response): CrowNetworkException {
        val reader = response.body.charStream()
        return try {
            val errorBody = reader.readText()
            Log.d(TAG, "Error body: $errorBody")

            // Check if body is non-blank and JSON
            if (errorBody.isNotBlank()) {
                val gson = NetworkManager.getInstance().gson
                val errorResponse = gson.fromJson(errorBody, ErrorResponse::class.java)

                // Use detail or error message as fallback
                val errorMessage = errorResponse.detail ?: errorResponse.error ?: "Unknown server error"
                CrowNetworkException(response.code, errorMessage, errorMessage)
            } else {
                CrowNetworkException(response.code, "Empty error response", null)
            }
        } catch (e: Exception) {
            Log.e(TAG, "Failed to parse error body: ${e.message}", e)
            CrowNetworkException(response.code, "Unable to parse error response", null, e)
        }
    }

}




