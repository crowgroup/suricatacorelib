package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.request.SetDeviceNameRequest
import com.crowcloud.gateway.suricataCore.network.model.response.Keypad
import okhttp3.ResponseBody
import retrofit2.http.*

interface KeypadsApi {

    @GET("/panels/{id}/keypads/")
    suspend fun getKeypads(@Path("id") id: Int): List<Keypad>

    @PUT("/panels/{panel_id}/keypads/{keypad_id}/")
    suspend fun updateKeypadName(@Header("X-Crow-CP-User") userId: String,
                                 @Path("panel_id") panelId: Int, @Path("keypad_id") keypadId: Int,
                                 @Body setDeviceNameRequest: SetDeviceNameRequest): Keypad

//    @PUT("/panels/{panel_id}/keypads/{keypad_id}/")
//    suspend fun updateKeypadData(@Header("X-Crow-CP-User") userId: String,
//                     @Path("panel_id") panelId: Int, @Path("keypad_id") keypadId: Int,
//                     @Body keypad: Keypad): Keypad

    @PUT("/panels/{panel_id}/keypads/{keypad_id}/")
    suspend fun addKeypad(@Header("X-Crow-CP-User") userId: String,
                          @Path("panel_id") panelId: Int, @Path("keypad_id") keypadId: Int,
                          @Body keypad: Keypad): Keypad

    @PUT("/panels/{panel_id}/keypads/{keypad_id}/")
    suspend fun updateKeypad(@Header("X-Crow-CP-User") userId: String,
                          @Path("panel_id") panelId: Int, @Path("keypad_id") keypadId: Int,
                          @Body keypad: Keypad): Keypad

    @DELETE("/panels/{panel_id}/keypads/{keypad_id}/")
    suspend fun deleteKeypad(@Header("X-Crow-CP-User") userId: String,
                             @Path("panel_id") panelId: Int, @Path("keypad_id") keypadId: Int):
            retrofit2.Response<ResponseBody>

//    @PUT("/panels/{panel_id}/keypads/{keypad_id}/params/type/{keypad_type}/")
//    suspend fun configAdvancedKeypad(@Header("X-Crow-CP-User") userId: String,
//                                    @Path("panel_id") panelId: Int, @Path("keypad_id") keypadId: Int,
//                                    @Path("keypad_type") keypadType: Int,
//                                    @Body configAdvancedKeypadRequest: ConfigAdvancedKeypadRequest): Keypad
//
//    @PUT("/panels/{panel_id}/keypads/{keypad_id}/params/type/{keypad_type}/")
//    suspend fun configLedKeypad(@Header("X-Crow-CP-User") userId: String,
//                               @Path("panel_id") panelId: Int, @Path("keypad_id") keypadId: Int,
//                               @Path("keypad_type") keypadType: Int,
//                               @Body configLedKeypadRequest: ConfigLedKeypadRequest): Keypad
//
//    @PUT("/panels/{panel_id}/keypads/{keypad_id}/params/type/{keypad_type}/")
//    suspend fun configIconKeypad(@Header("X-Crow-CP-User") userId: String,
//                                @Path("panel_id") panelId: Int, @Path("keypad_id") keypadId: Int,
//                                @Path("keypad_type") keypadType: Int,
//                                @Body configProKeypadRequest: ConfigProKeypadRequest): Keypad
}