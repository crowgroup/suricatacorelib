package com.crowcloud.gateway.suricataCore.network.model.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class EmailUser(
        @SerializedName("email")
        val email: String?,
        @SerializedName("first_name")
        val firstName: String?,
        @SerializedName("last_name")
        val lastName: String?,
        @SerializedName("pk")
        val pk: Int?,
        @SerializedName("username")
        val username: String?
) : Parcelable