package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class AddZoneRequest(
    @SerializedName("link_type")
    val linkType: String = "1",
    @SerializedName("device_id")
    val serialNumber: Int,
    @SerializedName("name")
    val zoneName: String,
    @SerializedName("areas")
    val areas: List<Int>
)

/*
AddZoneRequest is the body of request

zone id is included in params

  Body: {
  "link_type":"1",
  "device_id":1234568,
  "name":"test2",
  "areas":[0]}
   */