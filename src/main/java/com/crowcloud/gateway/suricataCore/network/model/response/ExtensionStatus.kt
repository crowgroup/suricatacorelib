package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ExtensionStatus(

    // is explosion in progress - this value automatically resets to false after
    // few seconds (when explosion is done)
    @SerializedName("explosion")
    var explosion: Boolean,

    //is armed - if not armed, explosion command will fail
    @SerializedName("armed")
    var armed: Boolean,

    // after explosion this will be set to true
    // you can use commands to change it manually to true,false
    // (example, in event of manual discharge or recharge with new cartridge)
    @SerializedName("empty")
    var empty: Boolean
) : Parcelable
