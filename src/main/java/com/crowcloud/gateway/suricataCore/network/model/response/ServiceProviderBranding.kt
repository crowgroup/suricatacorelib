package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class ServiceProviderBranding() : Parcelable {

    @SerializedName("logoURL")
    var logoUrl: String? = null

    constructor(parcel: Parcel) : this() {
        logoUrl = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(logoUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ServiceProviderBranding> {
        override fun createFromParcel(parcel: Parcel): ServiceProviderBranding {
            return ServiceProviderBranding(parcel)
        }

        override fun newArray(size: Int): Array<ServiceProviderBranding?> {
            return arrayOfNulls(size)
        }
    }
}