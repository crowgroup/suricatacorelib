package com.crowcloud.gateway.suricataCore.network.services

import com.crowcloud.gateway.suricataCore.network.model.response.Devices
import retrofit2.http.GET
import retrofit2.http.Path

interface DevicesApi {

    @GET("/panels/{panel_id}/devices/")
    suspend fun getPanelDevices(@Path("panel_id") panelId: Int): Devices
}