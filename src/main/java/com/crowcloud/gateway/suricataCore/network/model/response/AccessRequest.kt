package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
class AccessRequest : Request(){

    @SerializedName("customerId")
    val customerId: String? = null

    @SerializedName("serviceProviderLabel")
    val serviceProviderLabel: String? = null

    @SerializedName("accessTimeWindow")
    val accessTimeWindow: AccessTimeWindow? = null

    @SerializedName("serviceDescription")
    val serviceDescription: String? = null

    @SerializedName("access_granted")
    val accessGranted: String? = null

    @SerializedName("rejected")
    val rejected: Boolean? = null

    @SerializedName("serviceProviderBranding")
    val serviceProviderBranding: ServiceProviderBranding? = null

    @SerializedName("lockId")
    val lockId: String? = null

    @SerializedName("grant_token")
    val grantToken: String? = null

    @SerializedName("serviceProviderId")
    val serviceProviderId: String? = null

    @SerializedName("keyUsageTime")
    val keyUsageTime: Int = 0                               //not used

    @SerializedName("validUntil")
    val validUntil: String? = null                          //not used

    @SerializedName("serviceDescriptionDetailURL")
    val serviceDescriptionDetailURL: String? = null         //not used

    @SerializedName("serviceAgreement")
    val serviceAgreement: String? = null                    //not used

}