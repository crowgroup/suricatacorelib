package com.crowcloud.gateway.suricataCore.network.model.response

import android.os.Parcelable
import com.crowcloud.gateway.suricataCore.util.IpDeviceType
import com.meari.sdk.bean.CameraInfo
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
class MeariDeviceUiModel(val cameraInfo: CameraInfo) : IpDeviceModule, Parcelable {

    override fun getDeviceType(): IpDeviceType {
        return IpDeviceType.MEARI_DEVICE
    }
    override fun getDeviceId(): String? {
        return cameraInfo.deviceID
    }
    override fun getName(): String? {
        return deviceName
    }
    override fun setName(name: String) {
        deviceName = name
    }

    @IgnoredOnParcel
    var deviceName: String? = cameraInfo.deviceName

    val id: String?
        get() = cameraInfo.deviceID
    val deviceUID: String
        get() = cameraInfo.deviceUUID
    val ip: String
        get() = cameraInfo.ip
    val mac: String
        get() = cameraInfo.mac
    val timeZone: String
        get() = cameraInfo.timeZone
    val serialNumber: String    //virtual id
        get() = cameraInfo.snNum
    val type: Int
        get() = cameraInfo.devTypeID
    val version: String
        get() = cameraInfo.version
    val icon: String
        get() = cameraInfo.deviceIcon
    val hasAlertMsg: Boolean
        get() = cameraInfo.isHasAlertMsg
    val isIot: Boolean
        get() = cameraInfo.isIot
    val iotType: Int
        get() = cameraInfo.iotType
    val alertType: Int
        get() = cameraInfo.evt
    val talkType: Int
        get() {
            val vtk = cameraInfo.vtk
            return if (vtk == 0) {
                1
            } else {
                2
            }
        }
    val motionDetSupported: Boolean
        get() {
            if (cameraInfo.md == 1){ //int md; motion detection: 0-not supported; 1-support
                return true
            }
            return false
        }
}