package com.crowcloud.gateway.suricataCore.network.model.response

interface ScheduleListItem {
    fun getId(): Int
    fun getType(): ScheduleListItemType
}

enum class ScheduleListItemType {
    TIME_ZONE_INFO, SCHEDULE_INFO
}