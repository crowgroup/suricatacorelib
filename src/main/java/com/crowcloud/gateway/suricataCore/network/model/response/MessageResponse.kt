package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class MessageResponse(
        @SerializedName("detail") val detail: String? = null
)