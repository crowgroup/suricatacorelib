package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ConfigSmokeSyncDetectorRequest(
    @SerializedName("enable_burglary_alarm")
    val enableBurglaryAlarm: Boolean = false,
    @SerializedName("del_time")
    val delTime: Int = 10,
    @SerializedName("enable_fire_alarm")
    val enableFireAlarm: Boolean = true,
    @SerializedName("enable_water_alarm")
    val enableWaterAlarm: Boolean = false,
    @SerializedName("sndr_loud")
    val sndrLoud: Int = 1,
    @SerializedName("led_enabled")
    val ledEnabled: Boolean = true
)


/*
"device_config": {
            "enable_burglary_alarm": false,
            "del_time": 0,
            "enable_fire_alarm": true,
            "enable_water_alarm": false,
            "sndr_loud": 1,
            "led_enable": true
        }
 */