package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

class WifiConfigRequest {

    @SerializedName("conn_config")
    var connectionConfig: List<ConnectionConfigItemRequest>? = null

    @SerializedName("enabled")
    var enabled: Boolean = false
}