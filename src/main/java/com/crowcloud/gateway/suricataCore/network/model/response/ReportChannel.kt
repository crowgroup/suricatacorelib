package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class ReportChannel(
    @SerializedName("id") var id: Int,
    @SerializedName("destination") var phoneNumber: String,
    @SerializedName("active") var isActive: Boolean,
    @SerializedName("type") var type: Int,
    @SerializedName("protocol") var protocol: Int,
    @SerializedName("backup_for") var backupFor: Int,
    @SerializedName("port") var port: Int
)

/*
        "protocol": 0,
        "backup_for": 0,
        "destination": "1212552",
        "port": 4700,
        "active": true,
        "type": 4,
        "id": 4
 */