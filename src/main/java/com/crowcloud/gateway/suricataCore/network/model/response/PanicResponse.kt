package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class PanicResponse(@SerializedName("panic_alarm") val panicAlarm: Boolean)
