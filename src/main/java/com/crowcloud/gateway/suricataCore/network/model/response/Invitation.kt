package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class Invitation(
    @SerializedName("created") val created: String,
    @SerializedName("name") val name: String,
    @SerializedName("email") val email: String,
    @SerializedName("role") val role: Int,
    @SerializedName("invited_by") val invitedBy: InvitedBy
)

data class InvitedBy(
    @SerializedName("email") val email: String,
    @SerializedName("name") val name: String
)

data class Invitee(
    @SerializedName("name") val name: String,
    @SerializedName("email") val email: String,
    @SerializedName("role") val role: Int
)


