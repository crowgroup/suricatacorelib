package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class ChangePasswordRequest(

        @SerializedName("old_password")
        private val oldPassword: String?,

        @SerializedName("new_password1")
        private val newPassword1: String?,

        @SerializedName("new_password2")
        private val newPassword2: String?
)