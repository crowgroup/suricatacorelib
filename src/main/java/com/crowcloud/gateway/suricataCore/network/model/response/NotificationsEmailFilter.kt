package com.crowcloud.gateway.suricataCore.network.model.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class NotificationsEmailFilter(

        @SerializedName("all")
        var all: Boolean? = false,

        @SerializedName("alarm")
        var alarm: Boolean? = false,

        @SerializedName("arm")
        var arm: Boolean? = false,

        @SerializedName("configuration")
        var configuration: Boolean? = false,

        @SerializedName("information")
        var information: Boolean? = false,

        @SerializedName("take_picture")
        var takePicture: Boolean? = false,

        @SerializedName("troubles")
        var troubles: Boolean? = false,

        @SerializedName("user_association")
        var userAssociation: Boolean? = false
) : Parcelable