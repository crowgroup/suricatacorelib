package com.crowcloud.gateway.suricataCore.network.interceptor

import android.util.Log
import com.crowcloud.gateway.suricataCore.exception.CrowNetworkException
import okhttp3.Interceptor
import okhttp3.Response
import java.net.SocketException
import java.util.concurrent.TimeUnit

class TimeHeaderInterceptor : Interceptor {

    companion object {
        private const val CONNECT_TIMEOUT = "CONNECT_TIMEOUT"
        private const val READ_TIMEOUT = "READ_TIMEOUT"
        private const val WRITE_TIMEOUT = "WRITE_TIMEOUT"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        var connectTimeout = chain.connectTimeoutMillis()
        var readTimeout = chain.readTimeoutMillis()
        var writeTimeout = chain.writeTimeoutMillis()

        val builder = request.newBuilder()

        request.header(CONNECT_TIMEOUT)?.also {
            connectTimeout = it.toInt()
            builder.removeHeader(CONNECT_TIMEOUT)
        }

        request.header(READ_TIMEOUT)?.also {
            readTimeout = it.toInt()
            builder.removeHeader(READ_TIMEOUT)
        }

        request.header(WRITE_TIMEOUT)?.also {
            writeTimeout = it.toInt()
            builder.removeHeader(WRITE_TIMEOUT)
        }

        return try {
            chain
                .withConnectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
                .withReadTimeout(readTimeout, TimeUnit.MILLISECONDS)
                .withWriteTimeout(writeTimeout, TimeUnit.MILLISECONDS)
                .proceed(builder.build())
        } catch (e: SocketException) {
            // Log the exception
            Log.e("TimeHeaderInterceptor", "SocketException during request", e)

            // You can decide what to do here:
            // 1. Retry the request (maybe with a delay or a limited number of attempts).
            // 2. Return a custom response to indicate the error.
            // 3. Rethrow the exception if you want it to be handled elsewhere.

            // For example, to return a custom response:
//            Response.Builder()
//                .code(503) // Service Unavailable or any other appropriate code
//                .message("Network error")
//                .request(request)
//                .protocol(Protocol.HTTP_1_1)
//                .body(ResponseBody.create(null, ""))
//                .build()

            throw CrowNetworkException(500, "Network error", null)

            // Or simply rethrow the exception if you want the calling code to handle it
            // throw e
        }
     }
}