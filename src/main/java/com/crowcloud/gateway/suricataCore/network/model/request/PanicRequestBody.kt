package com.crowcloud.gateway.suricataCore.network.model.request

import com.google.gson.annotations.SerializedName

data class LongLat(@SerializedName("longitude") val longitude: Double, @SerializedName("latitude") val latitude: Double)

data class PanicRequestBody(@SerializedName("point") val point: LongLat)
