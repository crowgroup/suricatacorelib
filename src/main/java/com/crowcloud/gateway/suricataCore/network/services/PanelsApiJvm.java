package com.crowcloud.gateway.suricataCore.network.services;

import com.crowcloud.gateway.suricataCore.network.model.response.PaginatedResponseJvm;
import com.crowcloud.gateway.suricataCore.network.model.response.PanelInfo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


//For panic widget
public interface PanelsApiJvm {

    @GET("/panel_users/")
    Call<PaginatedResponseJvm<PanelInfo>> getPanelList(
            @Query("page") int page,
            @Query("page_size") int pageSize,
            @Query("ordering") String ordering
    );
}
