package com.crowcloud.gateway.suricataCore.network.model.response

import com.google.gson.annotations.SerializedName

data class ErrorResponse(
    @SerializedName("error")
    val error: String? = null,

    @SerializedName(value = "detail", alternate = ["error_description", "message"])
    val detail: String? = null
)
