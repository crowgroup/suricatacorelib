package com.crowcloud.gateway.suricataCore.exception

class UnauthorizedException(message: String) : Exception(message)
