package com.crowcloud.gateway.suricataCore.exception

import java.io.IOException

open class CrowNetworkException(open var code: Int, var error: String?,
                                var detail: String?, var e: Throwable? = null)
    : IOException("code: $code error: $error detail: $detail", e) {

    fun message(): String? = detail ?: error
}
