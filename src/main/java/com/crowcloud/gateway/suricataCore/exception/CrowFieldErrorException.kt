package com.crowcloud.gateway.suricataCore.exception

import com.crowcloud.gateway.suricataCore.network.model.response.ErrorField
import com.crowcloud.gateway.suricataCore.util.concat

/**
 * Created by michael on 2/26/18.
 */
class CrowFieldErrorException(override var code: Int, val fieldErrorsMap: Map<ErrorField, List<String>>) :
        CrowNetworkException(code, fieldErrorsMap.entries.iterator().next().key.name, concat(fieldErrorsMap.entries.iterator().next().value, "\n"))
