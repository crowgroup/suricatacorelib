package com.crowcloud.gateway.suricataCore.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
//import androidx.navigation.Navigation
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.crowcloud.gateway.suricataCore.R
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.EventReceivedData
import com.crowcloud.gateway.suricataCore.network.model.response.PaginatedResponse
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

abstract class BaseFragment : Fragment() {

    companion object {
        const val TAG = "BaseFragment"
        const val DEBUG_LIFECYCLE = false
    }

    enum class UIState {
        Loading, Content, Empty, NoConnection
    }

    abstract fun dataType(): DataType?
    protected abstract fun requestData()
    abstract fun layoutResId(): Int
    abstract fun initView()
    open fun showContentOnly(): Boolean = false
    open fun swipeRefresh(): SwipeRefreshLayout? = swipeRL
    //open fun errorTintColor(): Int? = getColor(R.color.gunmetal)
    open fun emptyStateText(): String = getString(R.string.no_data)
    open fun emptyStateActionText(): String = getString(R.string.try_again)
    open fun emptyStateAction(view: View) {
        requestData(); showState(UIState.Loading)
    }

    //fun navController() = Navigation.findNavController(requireView())
    private var progressDialog: AlertDialog? = null

    //VIEWS
    var swipeRL: SwipeRefreshLayout? = null
    //var errorIcon: ImageView? = null
    var errorTextView: TextView? = null
    var loadingDataPB: ProgressBar? = null
    var tryAgainBtn: Button? = null
    var contentContainer: FrameLayout? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (DEBUG_LIFECYCLE) Log.i(TAG, "${javaClass.simpleName} - onAttach")
        //requestData()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (DEBUG_LIFECYCLE) Log.i(TAG, "${javaClass.simpleName} - onCreate")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (DEBUG_LIFECYCLE) Log.i(TAG, "${javaClass.simpleName} - onCreateView")
        return if (showContentOnly()) {
            inflater.inflate(layoutResId(), container, false)
        } else {
            val baseView = inflater.inflate(R.layout.content_fragment_base, container, false)
            swipeRL = baseView.findViewById(R.id.swipeRL)
            //errorIcon = baseView.findViewById(R.id.errorIcon)
            errorTextView = baseView.findViewById(R.id.errorTextView)
            loadingDataPB = baseView.findViewById(R.id.loadingDataPB)
            tryAgainBtn = baseView.findViewById(R.id.tryAgainBtn)
            contentContainer = baseView.findViewById(R.id.contentContainer)

            inflater.inflate(layoutResId(), baseView.findViewById(R.id.contentContainer), true)
            baseView
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (DEBUG_LIFECYCLE) Log.i(TAG, "${javaClass.simpleName} - onViewCreated")

        showState(if (dataType() != null) UIState.Loading else UIState.Content)
        if (dataType() != null) {
            swipeRefresh()?.setOnRefreshListener {
                requestData()
            }
            swipeRefresh()?.isEnabled = true
        }
        //errorIcon?.setColorFilter(errorTintColor())
        //errorTintColor()?.let { errorTextView?.setTextColor(it) }

        initView()
        registerSubscribers()
    }

    private fun registerSubscribers() {
        EventBus.getDefault().register(this)
    }

    private fun unRegisterSubscribers() {
        EventBus.getDefault().unregister(this)
    }

    private var currState: UIState = UIState.Loading
    fun showLoading() = showState(UIState.Loading)
    fun showEmpty() = showState(UIState.Empty)

    private fun showState(uiState: UIState) {
        if (showContentOnly()) return

        currState = uiState
        when (uiState) {
            UIState.Loading -> {
                loadingDataPB?.visibility = View.VISIBLE
//                arrayOf(errorIcon, errorTextView, contentContainer, tryAgainBtn).forEach { it?.visibility = View.GONE }
                arrayOf(errorTextView, contentContainer, tryAgainBtn).forEach { it?.visibility = View.GONE }
            }
            UIState.Content -> {
                contentContainer?.visibility = View.VISIBLE
                arrayOf(errorTextView, loadingDataPB, tryAgainBtn).forEach { it?.visibility = View.GONE }
               // arrayOf(errorIcon, errorTextView, loadingDataPB, tryAgainBtn).forEach { it?.visibility = View.GONE }
            }
            UIState.Empty -> {
                //arrayOf(errorIcon, errorTextView, tryAgainBtn).forEach { it?.visibility = View.VISIBLE }
                arrayOf(errorTextView, tryAgainBtn).forEach { it?.visibility = View.VISIBLE }
                arrayOf(contentContainer, loadingDataPB).forEach { it?.visibility = View.GONE }
                //errorIcon?.setImageResource(R.drawable.no_data_logo)
                errorTextView?.text = emptyStateText()
                tryAgainBtn?.text = emptyStateActionText()
                tryAgainBtn?.setOnClickListener { v ->
                    emptyStateAction(v)
                }
            }
            UIState.NoConnection -> {
                arrayOf(errorTextView, tryAgainBtn).forEach { it?.visibility = View.VISIBLE }
                //arrayOf(errorIcon, errorTextView, tryAgainBtn).forEach { it?.visibility = View.VISIBLE }
                arrayOf(contentContainer, loadingDataPB).forEach { it?.visibility = View.GONE }
                //errorIcon?.setImageResource(R.drawable.no_wifi_logo)
                errorTextView?.setText(R.string.error_no_connection)
                tryAgainBtn?.text = getString(R.string.try_again)
                tryAgainBtn?.setOnClickListener {
                    requestData()
                    showState(UIState.Loading)
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (DEBUG_LIFECYCLE) Log.i(TAG, "${javaClass.simpleName} - onStart")

        registerSubscribers()
        requestData()
    }

    override fun onStop() {
        if (DEBUG_LIFECYCLE) Log.i(TAG, "${javaClass.simpleName} - onStop")

        unRegisterSubscribers()

        super.onStop()
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun onRequestDataDone(event: EventReceivedData) {
        if (showContentOnly()) {
            return
        }

        if (event.type == dataType()) {
            swipeRefresh()?.isRefreshing = false
            if (event.data == null || ((event.data is Collection<*> && (event.data as Collection<*>).isEmpty())
                        || (event.data is Map<*, *>) && (event.data as Map<*, *>).isEmpty())
                        || (event.data is PaginatedResponse<*>
                        && (event.data as PaginatedResponse<*>).results?.isEmpty() == true
                        && (event.data as PaginatedResponse<*>).previous == null)) {
                showState(UIState.Empty)
            } else if (event.data is Throwable) {
//                if (!NetworkUtil.isNetworkAvailable && currState != UIState.Content) {
//                    showState(UIState.NoConnection)
//                } else if (currState != UIState.Content) {
//                    showState(UIState.Empty)//create new generic error view and show it here
//                }
            } else {
                showState(UIState.Content)
            }
        }
    }

    //protected fun postRequestData(vararg dataTypes: DataType) = dataTypes.forEach { post(EventRequestData(it)) }

   // protected fun post(event: Any?) = com.crowcloud.gatewaymobile.App.get()?.eventBus?.post(event)

    //protected fun postSticky(event: Any?) = com.crowcloud.gatewaymobile.App.get()?.eventBus?.postSticky(event)

    protected fun showProgressDialog(message: String) {
        progressDialog?.setMessage(message)
        progressDialog?.show()
    }

    protected fun dismissProgressDialog() {
        progressDialog?.dismiss()
        progressDialog = null
    }

}