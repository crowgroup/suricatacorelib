package com.crowcloud.gateway.suricataCore.events

data class EventWrongUserCodeSupplied(val userId: String)
