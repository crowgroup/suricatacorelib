package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.RegisterUserInAssaResponse

class EventGetUserRegistrationInAssaInfo

data class EventGetUserRegistrationInAssaInfoDone(val response: RegisterUserInAssaResponse?)
