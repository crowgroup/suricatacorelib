package com.crowcloud.gateway.suricataCore.events

data class EventSetSdRecordingDuration(val type: Int, val duration: Int)

data class EventSetSdRecordingDurationDone(val success: Boolean, val duration: Int, val errorCode: Int?, val msg: String?)
