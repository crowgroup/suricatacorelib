package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.DoorLock
import com.crowcloud.gateway.suricataCore.network.model.response.LockInAssa

data class EventGetLocks(val panelId: Int?)

data class EventGetLocksDone(val doorLocks: ArrayList<DoorLock>?)

class EventGetLocksInAssa

data class EventGetLocksInAssaDone(val locksInAssa: ArrayList<LockInAssa>?)