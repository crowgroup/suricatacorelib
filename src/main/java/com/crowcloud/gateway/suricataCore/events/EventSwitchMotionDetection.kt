package com.crowcloud.gateway.suricataCore.events

data class EventSwitchMotionDetection(val enable: Int, val sensitivity: Int)

data class EventSwitchMotionDetectionDone(val success: Boolean, val enable: Int, val errorCode: Int?,
                                          val msg: String?)

data class EventSetMotionDetSensitivity(val motionDetEnable: Int?, val motionDetSensitivity: Int?)

data class EventSetMotionDetSensitivityDone(val success: Boolean, val sensitivity: Int,
                                            val errorCode: Int?, val msg: String?)

data class EventSetDayNightMode(val mode: Int)

data class EventSetDayNightModeDone(val success: Boolean, val errorCode: Int?, val msg: String?)
