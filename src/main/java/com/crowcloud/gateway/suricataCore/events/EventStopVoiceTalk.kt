package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.MeariDeviceController

data class EventStopVoiceTalk(val deviceController: MeariDeviceController?)

data class EventStopVoiceTalkDone(val success: Boolean, val msg: String?)
