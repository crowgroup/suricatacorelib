package com.crowcloud.gateway.suricataCore.events

data class EventAddMeariDeviceFailed(val msg: String?)
