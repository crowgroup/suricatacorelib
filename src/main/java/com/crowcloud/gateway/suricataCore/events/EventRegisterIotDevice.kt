package com.crowcloud.gateway.suricataCore.events

data class EventRegisterIotDevice(var panelId: Int, var name: String, var type: String?, var mac: String?, var uid: String)