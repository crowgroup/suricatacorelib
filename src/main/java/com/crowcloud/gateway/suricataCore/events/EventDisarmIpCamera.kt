package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.IpCamera

data class EventDisarmIpCamera(val camera: IpCamera)

data class EventDisarmIpCameraDone(val error: Throwable? = null)