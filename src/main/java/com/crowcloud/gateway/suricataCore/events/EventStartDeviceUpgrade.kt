package com.crowcloud.gateway.suricataCore.events

data class EventStartDeviceUpgrade(val upgradeUrl: String, val newVersion: String)
