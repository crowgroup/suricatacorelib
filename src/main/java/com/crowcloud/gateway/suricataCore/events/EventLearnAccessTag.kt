package com.crowcloud.gateway.suricataCore.events

data class EventLearnAccessTag(val userCode: String, val userId: Int)
