package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.Event
import com.crowcloud.gateway.suricataCore.network.model.response.Picture

data class EventShowPictures(val event: Event? = null, val picture: Picture? = null)
