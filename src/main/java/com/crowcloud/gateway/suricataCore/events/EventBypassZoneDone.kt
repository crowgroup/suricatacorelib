package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.Zone

data class EventBypassZoneDone(val zone: Zone, val error: Throwable? = null)

data class EventActionDone<EVENT, DATA>(val event: EVENT, val data: DATA? = null, val error: Throwable? = null)
