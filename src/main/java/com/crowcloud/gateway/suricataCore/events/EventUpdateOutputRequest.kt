package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.Output

data class EventUpdateOutputRequest(val userId: String, val output: Output)

data class EventUpdateOutputDone(val output: Output, val error: Throwable? = null)

class EventRefreshOutputs