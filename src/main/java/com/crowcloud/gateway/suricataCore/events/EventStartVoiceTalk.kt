package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.MeariDeviceController

data class EventStartVoiceTalk(val deviceController: MeariDeviceController?, val talkType: Int?)

data class EventStartVoiceTalkDone(val success: Boolean, val msg: String?)
