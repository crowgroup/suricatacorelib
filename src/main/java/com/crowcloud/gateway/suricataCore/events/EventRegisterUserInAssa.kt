package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.RegisterUserInAssaResponse

data class EventRegisterUserInAssa(val locale: String)

data class EventRegisterUserInAssaDone(val response: RegisterUserInAssaResponse?)
