package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.IpCamera

data class EventArmIpCamera(val camera: IpCamera)

data class EventArmIpCameraDone(val error: Throwable? = null)