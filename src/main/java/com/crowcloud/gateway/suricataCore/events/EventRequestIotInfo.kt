package com.crowcloud.gateway.suricataCore.events

class EventRequestIotInfo
data class EventRequestIotInfoDone(var res: HashMap<String, String>)