package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.Output

data class EventGetOutputById(val id: Int, val index: Int)

data class EventGetOutputByIdDone(
    val success: Boolean, val output: Output?, val index: Int,
    val errorMsg: String? = null)

data class EventToggleOutputRequest(val output: Output, val index: Int)

data class EventToggleOutputDone(val success: Boolean, val output: Output, val index: Int,
                                 val errorMsg: String? = null)

class EventExplodeFogGenerator
