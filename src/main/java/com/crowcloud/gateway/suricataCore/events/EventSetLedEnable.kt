package com.crowcloud.gateway.suricataCore.events

data class EventSetLedEnable(val enable: Int)
