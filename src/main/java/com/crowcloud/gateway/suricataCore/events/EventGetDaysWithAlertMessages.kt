package com.crowcloud.gateway.suricataCore.events

data class EventGetDaysWithAlertMessages(val deviceId: Long)
