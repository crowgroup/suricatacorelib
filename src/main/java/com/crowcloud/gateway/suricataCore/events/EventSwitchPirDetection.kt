package com.crowcloud.gateway.suricataCore.events

data class EventSwitchPirDetection(val enable: Int, val sensitivity: Int)

data class EventSwitchPirDetectionDone(val success: Boolean, val enable: Int, val errorCode: Int?,
                                       val msg: String?)

data class EventSetPirDetSensitivity(val sensitivity: Int)

data class EventSetPirDetSensitivityDone(val success: Boolean, val pirLevel: Int,
                                         val errorCode: Int?, val msg: String?)
