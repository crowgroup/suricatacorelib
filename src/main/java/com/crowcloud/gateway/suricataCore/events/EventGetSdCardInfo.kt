package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.bean.SDCardInfo

class EventGetSdCardInfo

data class EventGetSdCardInfoDone(val success: Boolean, val sdCardInfo: SDCardInfo?,
                                  val errorCode: Int?, val msg: String?)
