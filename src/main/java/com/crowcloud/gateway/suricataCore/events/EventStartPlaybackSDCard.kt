package com.crowcloud.gateway.suricataCore.events

data class EventStartPlaybackSDCard(//val deviceController: MeariDeviceController?,
                                    //val ppsGLSurfaceView: PPSGLSurfaceView,
                                    //val videoId: Int,
                                    val playTime: String,
                                    val videoId: Int)
