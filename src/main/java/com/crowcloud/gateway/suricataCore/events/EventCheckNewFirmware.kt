package com.crowcloud.gateway.suricataCore.events

data class EventCheckNewFirmware(val firmwareVersion: String, val snNum: String, val tp: String)
