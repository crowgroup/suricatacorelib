package com.crowcloud.gateway.suricataCore.events

data class EventSessionExpired(val message: String)

