package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.request.ZoomVideoRequest

data class EventZoomInIpCamera(val uid: String, val zoomVideoRequest: ZoomVideoRequest)

data class EventZoomInIpCameraDone(val error: Throwable? = null)

data class EventZoomOutIpCamera(val uid: String)

data class EventZoomOutIpCameraDone(val error: Throwable? = null)