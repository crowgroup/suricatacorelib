package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.Output

data class EventToggleLockOutputRequest(val output: Output, val index: Int)
