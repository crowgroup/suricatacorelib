package com.crowcloud.gateway.suricataCore.events

data class EventUpdateUserData(val firstName: String, val lastName: String)
class EventRefreshUserList
