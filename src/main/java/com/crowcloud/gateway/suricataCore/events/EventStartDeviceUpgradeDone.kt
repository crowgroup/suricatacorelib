package com.crowcloud.gateway.suricataCore.events

data class EventStartDeviceUpgradeDone(val success: Boolean, val errorCode: Int, val errorMsg: String?)
