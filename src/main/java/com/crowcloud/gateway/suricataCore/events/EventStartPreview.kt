package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.MeariDeviceController
import com.ppstrong.ppsplayer.PPSGLSurfaceView

data class EventStartPreview(val deviceController: MeariDeviceController?,
                             val videoSurfaceView: PPSGLSurfaceView?, val videoId: Int)

data class EventStartPreviewDone(val success: Boolean, val msg: String?)
