package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.NotificationsEmailData

data class EventUpdateNotificationsEmail(val email: NotificationsEmailData)
data class EventAddNotificationsEmail(val email: NotificationsEmailData)