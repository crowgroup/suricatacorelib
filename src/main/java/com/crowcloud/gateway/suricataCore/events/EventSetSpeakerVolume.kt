package com.crowcloud.gateway.suricataCore.events

data class EventSetSpeakerVolume(val volume: Int)

data class EventSetSpeakerVolumeDone(val success: Boolean, val errorCode: Int?, val msg: String?)
