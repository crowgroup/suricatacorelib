package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.bean.DeviceMessageStatus

class EventGetAlertMessageStatusList

data class EventGetAlertMessageStatusListDone(val deviceMessageStatusList: List<DeviceMessageStatus>)

data class EventGetAlertMessageStatusListFailed(val errorCode: Int?, val error: String?)

