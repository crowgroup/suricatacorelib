package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.MeariDeviceController

data class EventResumePlaybackSDCard(val deviceController: MeariDeviceController?)