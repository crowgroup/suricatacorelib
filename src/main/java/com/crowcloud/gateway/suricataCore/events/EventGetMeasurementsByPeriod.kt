package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.Measurement

data class EventGetMeasurementsByPeriod(val type: Measurement.DeviceType,
                                        val deviceId: Int?,
                                        val dectInterface: Int?,
                                        val duration: String,
                                        val offset: Int)

data class EventGetMeasurementsByPeriodDone(val success: Boolean, val errorMsg: String? = null)
