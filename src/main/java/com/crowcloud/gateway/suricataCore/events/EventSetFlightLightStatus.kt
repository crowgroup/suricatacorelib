package com.crowcloud.gateway.suricataCore.events

//status  0-off; 1-on
data class EventSetFlightLightStatus(val status: Int)

data class EventSetLightStatusDone(val success: Boolean,
                                   val errorCode: Int? = null, val msg: String? = null)

//status  0-off; 1-on
data class EventSwitchFlightSiren(val status: Int)

data class EventSwitchFlightSirenDone(val success: Boolean,
                                      val errorCode: Int? = null, val msg: String? = null)

data class EventSetFloodVoiceLightAlarmEnable(val enable: Int)

data class EventSetFloodVoiceLightAlarmEnableDone(val success: Boolean,
                                                  val errorCode: Int? = null, val msg: String? = null)

data class EventSetFloodVoiceLightAlarmType(val alarmType: Int)

data class EventSetFloodVoiceLightAlarmTypeDone(val success: Boolean,
                                                  val errorCode: Int? = null, val msg: String? = null)