package com.crowcloud.gateway.suricataCore.events

data class EventDeleteAccessTag(val userCode: String, val userId: Int)

data class EventDeleteAccessTagDone(val accessTagState: String?, val error: Throwable? = null)
