package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.IpCamera

data class EventUpgradeIpCamera(val camera: IpCamera)

data class EventUpgradeIpCameraDone(val error: Throwable? = null)