package com.crowcloud.gateway.suricataCore.events

import android.location.Location

data class EventUpdatePanelLocation(val location: Location? = null)
