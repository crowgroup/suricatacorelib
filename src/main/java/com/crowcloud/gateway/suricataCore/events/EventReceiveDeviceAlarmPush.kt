package com.crowcloud.gateway.suricataCore.events

data class EventReceiveDeviceAlarmPush(val deviceId: String?, val status: Int)

data class EventReceiveDeviceAlarmPushDone(val success: Boolean, val status: Int?,
                                           val errorCode: Int?, val errorMsg: String?)
