package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.MeariDeviceController

data class EventSnapshot(val deviceController: MeariDeviceController?, val fullPath: String)

data class EventSnapshotDone(val success: Boolean, val msg: String?)
