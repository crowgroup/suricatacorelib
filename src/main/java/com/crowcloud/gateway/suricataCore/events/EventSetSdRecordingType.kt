package com.crowcloud.gateway.suricataCore.events

data class EventSetSdRecordingType(val recordingType: Int, val sdRecordDuration: Int?)

data class EventSetSdRecordingTypeDone(val success: Boolean, val errorCode: Int?, val msg: String?)
