package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.MeariDeviceController

data class EventConnectMeariDevice(val deviceController: MeariDeviceController?)

data class EventConnectMeariDeviceDone(val success: Boolean, val successMsg: String?,
                                       val errorMsg: String?)
