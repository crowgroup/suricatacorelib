package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.DoorLock

data class EventGetDoorLockById(val id: Int, val index: Int)

data class EventGetDoorLockByIdDone(
    val success: Boolean, val doorLock: DoorLock?, val index: Int,
    val errorMsg: String? = null)

data class EventToggleLockRequest(val doorLock: DoorLock, val index: Int)

data class EventToggleDoorLockDone(val success: Boolean, val doorLock: DoorLock, val index: Int,
                                   val errorMsg: String? = null)

data class EventUpdateDoorLockRequest(val userId: String, val doorLock: DoorLock?)

class EventRefreshDoorLocks