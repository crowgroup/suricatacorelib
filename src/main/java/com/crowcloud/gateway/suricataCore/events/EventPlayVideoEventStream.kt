package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.VideoEvent

data class EventPlayVideoEventStream(val videoEvent: VideoEvent)