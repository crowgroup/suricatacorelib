package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.Picture
import com.crowcloud.gateway.suricataCore.network.model.response.Zone

data class EventSaveNewCameraPicture(val picture: Picture, val zone: Zone)
