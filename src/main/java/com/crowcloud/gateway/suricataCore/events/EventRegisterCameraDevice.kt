package com.crowcloud.gateway.suricataCore.events

data class EventRegisterCameraDevice(val name: String, val serialNumber: String)
