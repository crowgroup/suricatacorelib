package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.IpCamera

data class EventOpenCameraScreen(val item: IpCamera)