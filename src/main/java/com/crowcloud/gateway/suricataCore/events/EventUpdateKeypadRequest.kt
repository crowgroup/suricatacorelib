package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.Keypad

data class EventUpdateKeypadRequest(val userId: String, val keypad: Keypad)

class EventRefreshKeypads
