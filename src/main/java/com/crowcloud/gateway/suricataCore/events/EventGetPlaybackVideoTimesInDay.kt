package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.MeariDeviceController

data class EventGetPlaybackVideoTimesInDay(val deviceController: MeariDeviceController?,
                                           val year: Int, val month: Int, val day: Int, val videoId: Int)

data class EventGetPlaybackVideoTimesInDayDone(val msg: String, val success: Boolean)

