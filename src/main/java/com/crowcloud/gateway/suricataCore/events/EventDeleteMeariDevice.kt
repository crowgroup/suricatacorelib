package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.bean.CameraInfo

data class EventDeleteMeariDevice(val cameraInfo: CameraInfo?)

data class EventDeleteMeariDeviceDone(val success: Boolean, val deviceName: String?,
                                      val errorCode: Int?, val error: String?)

