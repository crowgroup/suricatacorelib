package com.crowcloud.gateway.suricataCore.events

data class EventRequestIotDeviceBind(var name: String, var wifiSsid: String, var wifiPassword: String, var settingUid: String)
data class EventRequestIotDeviceBindDone(var res: HashMap<String, String>)
