package com.crowcloud.gateway.suricataCore.events

data class EventDeleteNotificationsEmail(val emailId: Int)
