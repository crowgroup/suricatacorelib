package com.crowcloud.gateway.suricataCore.events

data class EventDeleteDevice(val deviceId: Int?)
