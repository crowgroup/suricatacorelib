package com.crowcloud.gateway.suricataCore.events

data class EventGetDaysWithAlertMessagesDone(val days: List<String>?,
                                             val errorCode: Int, val errorMsg: String?)
