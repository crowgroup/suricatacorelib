package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.Area

data class EventRequestUpdateAreaStateNotReady(val event: EventRequestUpdateAreaState)

class EventEditAreaCanceled

data class EventEditArea(val remoteAccessPassword: String, val userCode: String, val area: Area)

data class UpdateAreaName(val area: Area)

data class EventAllAreasPanic(val isPanicAlarmInAllAreas: Boolean)
