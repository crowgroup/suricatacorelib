package com.crowcloud.gateway.suricataCore.events

data class EventRejectAssaRequest(val accessRequestId: String)

data class EventRejectAssaRequestDone(val accessRequestId: String?)
