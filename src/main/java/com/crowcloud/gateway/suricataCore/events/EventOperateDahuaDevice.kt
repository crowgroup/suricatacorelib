package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.DahuaIpCamera
import com.crowcloud.gateway.suricataCore.network.model.response.DahuaLiveVideoStreamData

data class EventRegisterDahuaDevice(val panelId: Int, val dahuaDevice: DahuaIpCamera)

data class EventRegisterDahuaDeviceDone(val success: Boolean, val message: String? = null)

data class EventGetLiveVideoStream(val panelId: Int, val deviceId: String, val videoType: String)

data class EventGetLiveVideoStreamDone(val success: Boolean, val response: DahuaLiveVideoStreamData? = null, val message: String? = null)

data class EventTurnOnMotionDetection(val panelId: Int, val deviceId: String)

data class EventTurnOffMotionDetection(val panelId: Int, val deviceId: String)

