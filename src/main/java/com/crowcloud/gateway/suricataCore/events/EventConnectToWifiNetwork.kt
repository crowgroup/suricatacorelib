package com.crowcloud.gateway.suricataCore.events

data class EventConnectToWifiNetwork(var SSID: String)
class EventConnectToPreviousWifiNetwork

data class EventConnectToWifiNetworkDone(var data: EventConnectToWifiNetwork, var error: Throwable? = null)
