package com.crowcloud.gateway.suricataCore.events

data class EventAcceptAssaRequest(val accessRequestId: String, val lockId: String)

data class EventAcceptAssaRequestDone(val lockId: String?)
