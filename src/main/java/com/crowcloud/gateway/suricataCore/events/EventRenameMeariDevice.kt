package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.MeariDeviceUiModel

data class EventRenameMeariDevice(val meariDeviceUiModel: MeariDeviceUiModel?, val newName: String)

class EventRenameMeariDeviceCanceled

data class EventRenameMeariDeviceDone(val success: Boolean, val newName: String,
                                      val errorCode: Int?, val error: String?)

