package com.crowcloud.gateway.suricataCore.events

data class EventSetHumanDetectionDay(val status: Int)

data class EventSetHumanDetectionDayDone(val success: Boolean, val errorCode: Int?, val msg: String?)

data class EventSetHumanDetectionNight(val status: Int)

data class EventSetHumanDetectionNightDone(val success: Boolean, val errorCode: Int?, val msg: String?)


