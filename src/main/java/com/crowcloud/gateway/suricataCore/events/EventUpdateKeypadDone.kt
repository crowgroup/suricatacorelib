package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.Keypad

data class EventUpdateKeypadDone(val keypad: Keypad, val error: Throwable? = null)
