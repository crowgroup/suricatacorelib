package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.request.InstallerAccessRequestedTime

class EventGetMaintenanceState()

data class EventGetMaintenanceStateDone(val success: Boolean, val message: String?)

class EventCloseMaintenance()

data class EventCloseMaintenanceDone(val success: Boolean, val message: String?)

data class EventSetMaintenanceDuration(val requestedTime: InstallerAccessRequestedTime)

data class EventSetMaintenanceDurationDone(val success: Boolean, val message: String?)

data class EventResetMaintenanceDuration(val requestedTime: InstallerAccessRequestedTime)

data class EventResetMaintenanceDurationDone(val success: Boolean, val message: String?)


/*
   //{"maintenance_until":null}
    @GET("/panels/{id}/maintenance/")
    fun getMaintenanceState(@Path("id") panelId: Int): Call<MaintenanceState>

    @POST("/panels/{id}/maintenance/")
    fun openMaintenance(@Path("id") panelId: Int): Call<MaintenanceState>

    //{"maintenance_until":"2022-09-16T12:00:13.952840Z"}
    @POST("/panels/{id}/maintenance/")
    fun setMaintenanceDuration(@Path("id") panelId: Int,
                               @Path("requested_time") requestedTime: Date): Call<MaintenanceState>

    @DELETE("/panels/{id}/maintenance/")
    fun closeMaintenance(@Path("id") panelId: Int): Call<MaintenanceState>
 */