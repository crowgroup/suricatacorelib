package com.crowcloud.gateway.suricataCore.events

data class EventDeleteCameraPicture(val pictureId: Int, val isLatestPicture: Boolean = false)

data class EventDeleteCameraPictureDone(val pictureId: Int?, val isLatestPicture: Boolean = false,
                                        val error: Throwable? = null)
