package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.Panel

data class EventUpdateNotificationFilters(val body: Panel)
