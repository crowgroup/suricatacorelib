package com.crowcloud.gateway.suricataCore.events

data class EventUpdateMaintenanceState(val isOpen: Boolean)
