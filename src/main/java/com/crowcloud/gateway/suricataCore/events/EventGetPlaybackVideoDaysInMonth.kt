package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.MeariDeviceController

data class EventGetPlaybackVideoDaysInMonth(val deviceController: MeariDeviceController?,
                                            val year: Int, val month: Int, val videoId: Int)

data class EventGetPlaybackVideoDaysInMonthDone(val year: Int, val month: Int,
                                                val days: ArrayList<Int>?,
                                                val success: Boolean, val msg: String?)
