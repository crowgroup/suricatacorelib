package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.PanelUser

data class EventDeletePanelUser(val userCode: String, val user: PanelUser?)
