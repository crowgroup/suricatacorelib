package com.crowcloud.gateway.suricataCore.events

import android.content.Intent

data class EventActivityResult(val requestCode: Int, val resultCode: Int, val data: Intent?)