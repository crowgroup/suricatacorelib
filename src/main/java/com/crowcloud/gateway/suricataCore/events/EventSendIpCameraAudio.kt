package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.IpCamera

data class EventSendIpCameraAudio(val camera: IpCamera, val fileName: String)

data class EventSendIpCameraAudioDone(val error: Throwable? = null)