package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.MeariDeviceController

data class EventSeekPlaybackSDCard(val deviceController: MeariDeviceController?,
                                   val seekTime: String)
