package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.MeariDeviceController

data class EventStartVideoRecording(val deviceController: MeariDeviceController?, val fullPath: String)

data class EventStartVideoRecordingDone(val success: Boolean, val msg: String?)
