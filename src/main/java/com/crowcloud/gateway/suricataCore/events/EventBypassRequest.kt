package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.Zone

data class EventBypassRequest(val zone: Zone)

class EventRefreshZones

data class EventRefreshZone(val zone: Zone)

class EventRefreshAllZones