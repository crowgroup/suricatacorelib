package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.Zone

data class EventUpdateZoneDone(val zone: Zone, val error: Throwable? = null)
