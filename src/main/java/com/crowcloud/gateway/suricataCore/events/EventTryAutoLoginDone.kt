package com.crowcloud.gateway.suricataCore.events

data class EventTryAutoLoginDone(val isSuccess: Boolean)