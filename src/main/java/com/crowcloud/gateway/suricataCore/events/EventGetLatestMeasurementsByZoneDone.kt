package com.crowcloud.gateway.suricataCore.events

data class EventGetLatestMeasurementsByZoneDone(val error: String? = null)
