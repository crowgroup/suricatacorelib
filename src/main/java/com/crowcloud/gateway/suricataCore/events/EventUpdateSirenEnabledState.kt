package com.crowcloud.gateway.suricataCore.events

data class EventUpdateSirenEnabledState(val sirenEnabled: Boolean)
