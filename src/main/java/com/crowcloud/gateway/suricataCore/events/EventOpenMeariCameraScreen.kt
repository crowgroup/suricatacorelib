package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.MeariDeviceUiModel

data class EventOpenMeariCameraScreen(val item: MeariDeviceUiModel)
