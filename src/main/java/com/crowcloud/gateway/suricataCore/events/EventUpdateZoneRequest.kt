package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.Zone

data class EventUpdateZoneRequest(val userId: String, val zone: Zone)
