package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.bean.DeviceParams

data class EventGetMeariDeviceParams(val deviceId: String?)

data class EventGetMeariDeviceParamsDone(
    val success: Boolean,
    val params: DeviceParams?,
    val errorCode: Int?,
    val msg: String?
)

data class EventGetMeariDevicesFailed(val codeError: Int, val errorMessage: String)

