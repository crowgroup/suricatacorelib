package com.crowcloud.gateway.suricataCore.events

class EventGetLatestPicturesByZone

data class EventGetLatestPicturesByZoneDone(val success: Boolean, val errorMsg: String? = null)
