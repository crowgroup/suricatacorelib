package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.IpCamera

data class EventDeleteCamera(val camera: IpCamera)

data class EventDeleteIpCameraDone(val error: Throwable? = null)