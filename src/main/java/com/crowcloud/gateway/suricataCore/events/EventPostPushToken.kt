package com.crowcloud.gateway.suricataCore.events

data class EventPostPushToken(val pushToken: String)

data class EventPostPushTokenDone(val success: Boolean, val errorCode: Int?, val msg: String?)
