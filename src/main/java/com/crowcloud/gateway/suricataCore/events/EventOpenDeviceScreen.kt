package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.BaseDevice

data class EventOpenDeviceScreen(val item: BaseDevice)