package com.crowcloud.gateway.suricataCore.events


data class EventCancelTakePicture(val zoneId: Int)
