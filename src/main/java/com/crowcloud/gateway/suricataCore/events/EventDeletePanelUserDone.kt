package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.PanelUser

data class EventDeletePanelUserDone(val user: PanelUser, val error: Throwable? = null)
