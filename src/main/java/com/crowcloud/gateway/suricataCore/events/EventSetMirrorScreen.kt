package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.bean.SDCardInfo

data class EventSetMirrorScreen(val mirrorEnable: Int)

data class EventSetMirrorScreenDone(val success: Boolean, val errorCode: Int?, val msg: String?)

