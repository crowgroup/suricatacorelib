package com.crowcloud.gateway.suricataCore.events

data class EventDeleteTimeZone(val timeZoneId: Int?)

class EventUpdateTimeZone