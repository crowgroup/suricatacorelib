package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.PanelUser

data class EventShowPanelUser(val panelUser: PanelUser)
//data class EventShowDevice(val device: BaseDevice)
