package com.crowcloud.gateway.suricataCore.events

data class EventGetAccessTagState(val userId: Int)

data class EventGetAccessTagStateDone(val userId: Int, val state: String?, val isLearned: Boolean)

data class EventGetAccessTagStateFailed(val userId: Int)
