package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.util.SoundType


class EventRefreshAreas

class EventRefreshArea(val areaId: Int)

class EventAreaEdited(val areaId: Int)

class EventRefreshPostArmDisarm(val areaId: Int)

class EventRefreshPostStayArmDisarm(val areaId: Int)

class EventRefreshAreaWithArmDelay(val areaId: Int)

class EventRefreshAreaWithStayDelay(val areaId: Int)

data class EventPanicAlarm(val areaId: Int)

data class EventRestoreAlarm(val areaId: Int)

class EventPlayAlarmSiren

class EventPlaySound(val soundType: SoundType, val areaId: Int)

class EventStopSiren