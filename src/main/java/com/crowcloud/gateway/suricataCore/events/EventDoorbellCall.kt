package com.crowcloud.gateway.suricataCore.events

data class EventDoorbellCall(val bellJson: String, val isUpdateScreenshot: Boolean)

data class EventDoorbellCallSnapshot(val bellJson: String)//, val isUpdateScreenshot: Boolean)
