package com.crowcloud.gateway.suricataCore.events

import android.content.Intent
import android.net.NetworkInfo
import com.crowcloud.gateway.suricataCore.components.ComponentData
import com.crowcloud.gateway.suricataCore.network.model.request.AreaStateRequest
import com.crowcloud.gateway.suricataCore.network.model.response.LoginResponse
import com.crowcloud.gateway.suricataCore.network.model.response.Panel


//region LOGIN

class EventAppStartUp

class EventRefresh

class EventRefreshNewPanel

data class EventUpdateLocation(val latitude: Double, val longitude: Double)

data class EventRecoverPassDone(val success: Boolean, val error: Throwable? = null)

data class EventFingerPrintScanDone(var isSuccess: Boolean = false)

data class EventLoginCompleted(val result: LoginResponse? = null)

data class EventSignUpDone(val success: Boolean, val error: Throwable? = null)

data class EventLoginDone(val success: Boolean, val password: String?, val error: Throwable? = null, val intent: Intent? = null)

data class EventTryLogin(val userName: String, val password: String, val rememberPassword: Boolean, val loadSavedPanel: Boolean = false)

data class EventTrySignUp(val fName: String, val lName: String, val email: String, val pass: String, val passRetype: String)

data class EventRecoverPassword(val email: String)

class EventRefreshPanels

class EventPrivacyOptionsCompleted

//endregion

//region PANEL

data class EventSelectedPanel(val panel: Panel?)


//endregion

//region DATA

data class EventRequestData(val type: DataType, val pagingInfo: ComponentData.PagingInfo? = null,
                            val requestData: Any? = null, val useCache: Boolean = true)

enum class DataType {
    LoginCreds,
    Panels,
    PanelConnectionState,
    Areas,
    Troubles,
    Measurements,
    CurrentPanel,
    Devices,
    Zones,
    Zone,
    Area,
    Events,
    Pictures,
    LatestPictures,
    Outputs,
    Output,
    Locks,
    LocksInAssa,
    Keypads,
    AllTroubles,
    RadioInfo,
    EthernetInfo,
    GsmInfo,
    WifiInfo,
    Users,
    UsersOfPanel,
    CurrentUser,
    CurrentUserOfPanel,
    NotificationFilters,
    NotificationsEmails,
    MaintenanceState,
    SirenEnabledState,
    MeasurementStats,
    Shortcuts,
    IpCameras,
    IpCamera,
    StreamData,
    BleIpCameras,
    VideoEvents,
    WifiNetworks,
    AccessRequests,
    MeariDevices
}

data class EventReceivedData(val type: DataType, var data: Any?)

//endregion

//region ACTIONS

data class EventRequestPanicAlarm(val areaId: Int)

data class EventRequestUpdateAreaState(val areaId: Int,
                                       val newState: AreaStateRequest.State,
                                       var force: Boolean = false)

//endregion

//region SYSTEM EVENTS

data class EventNetworkChanged(val networkInfo: NetworkInfo? = null)

//endregion

//CONFIG

data class EventSetConfigMode(val installerCode: String)

data class EventDeviceSettingsSetConfigMode(val installerCode: String)

class EventSetConfigModeCanceled
