package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.Picture
import com.crowcloud.gateway.suricataCore.network.model.response.Zone

data class EventTakePictureDone(val camera: Zone, val picture: Picture? = null,
                                val error: String? = null)
