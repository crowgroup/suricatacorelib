package com.crowcloud.gateway.suricataCore.events

data class EventNavigateTo(val id: Int)
