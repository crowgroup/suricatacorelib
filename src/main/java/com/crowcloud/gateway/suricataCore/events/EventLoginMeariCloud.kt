package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.bean.UserInfo

data class EventLoginMeariCloud(val countryCode: String?, val regionCode: String?,
                                val meariUID: String?)

data class EventLoginMeariCloudDone(val success: Boolean, val user: UserInfo?,
                                    val errorCode: Int?, val msg: String?)

class EventLogoutMeariCloud

class EventLoginMeariCloudOnOtherDevice

class EventFormatSdCard
data class EventFormatSdCardDone(val success: Boolean, val errorCode: Int?, val msg: String?)

class EventGetSdCardFormatPercent
data class EventGetSdCardFormatPercentDone(val success: Boolean, val percent: Int?,
                                           val errorCode: Int?, val msg: String?)
