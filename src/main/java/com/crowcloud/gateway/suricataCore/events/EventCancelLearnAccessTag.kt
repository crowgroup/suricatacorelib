package com.crowcloud.gateway.suricataCore.events

data class EventCancelLearnAccessTag(val userId: Int)
