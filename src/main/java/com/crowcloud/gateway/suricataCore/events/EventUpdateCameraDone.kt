package com.crowcloud.gateway.suricataCore.events

data class EventUpdateCameraDone(val error: Throwable? = null)