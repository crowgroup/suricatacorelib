package com.crowcloud.gateway.suricataCore.events

data class EventDeleteLock(val lockId: String)

data class EventDeleteLockDone(val lockId: String?)
