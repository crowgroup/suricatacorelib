package com.crowcloud.gateway.suricataCore.events

data class EventAddLock(val mac: String?, val serial: Int)

data class EventAddLockDone(val mac: String?)
