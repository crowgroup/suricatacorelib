package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.MeariDeviceController
import com.ppstrong.ppsplayer.PPSGLSurfaceView

data class EventSwitchResolution(val deviceController: MeariDeviceController?,
                                 val videoSurfaceView: PPSGLSurfaceView?, val videoId: Int)

data class EventSwitchResolutionDone(val success: Boolean, val videoID: Int, val msg: String?)
