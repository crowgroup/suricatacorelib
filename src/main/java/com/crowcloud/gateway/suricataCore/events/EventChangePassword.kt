package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.MessageResponse

data class EventChangePassword(val oldPassword: String, val newPassword: String)

data class EventChangePasswordDone(val success: Boolean, val response: MessageResponse? = null,
                                   val error: Throwable? = null)
