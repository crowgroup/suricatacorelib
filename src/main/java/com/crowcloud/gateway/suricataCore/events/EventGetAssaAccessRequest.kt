package com.crowcloud.gateway.suricataCore.events

import com.crowcloud.gateway.suricataCore.network.model.response.AccessRequest

data class EventGetAssaAccessRequest(val accessRequestId: String)

data class EventGetAssaAccessRequestDone(val accessRequest: AccessRequest?)
