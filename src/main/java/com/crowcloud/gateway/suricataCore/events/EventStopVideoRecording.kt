package com.crowcloud.gateway.suricataCore.events

import com.meari.sdk.MeariDeviceController

data class EventStopVideoRecording(val deviceController: MeariDeviceController?)

data class EventStopVideoRecordingDone(val success: Boolean, val msg: String?)
