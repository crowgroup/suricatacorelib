package com.crowcloud.gateway.suricataCore.events


import com.crowcloud.gateway.suricataCore.persistence.DeviceMultiItem

data class EventActionFailed(var errorCode: Int, var error: String?)
data class EventCrowNetworkException(var errorCode: Int, var error: String?)

data class EventUpdateScheduleFailed(var errorCode: Int)
data class EventAddScheduleFailed(var errorCode: Int)
data class EventDeleteScheduleFailed(var errorCode: Int)

data class EventUpdateRecipientFailed(var errorCode: Int)
data class EventDeleteRecipientFailed(var errorCode: Int)
data class EventAddRecipientFailed(var errorCode: Int)

data class EventDeleteTimeZoneFailed(var errorCode: Int)
data class EventAddTimeZoneFailed(var errorCode: Int)
data class EventUpdateTimeZoneFailed(var errorCode: Int)

data class EventForbiddenError(var errorCode: Int, var error: String?)
data class EventDeviceUpdateFailed(var device: DeviceMultiItem, var errorCode: Int, var error: String?)
data class EventAddZoneFailed(var errorCode: Int, var error: String?)
data class EventAddOutputFailed(var errorCode: Int, var error: String?)
data class EventAddKeypadFailed(var errorCode: Int, var error: String?)
data class EventDeviceDeleteFailed(var errorCode: Int, var error: String?)

class EventResetUI