package com.crowcloud.gateway.suricataCore.events

data class EventToggleSiren(val isOn: Boolean, val message: String?)
