package com.crowcloud.gateway.suricataCore.util

import android.os.Build
import androidx.annotation.ChecksSdkIntAtLeast

//class AndroidVersionUtil {

    fun getApiVersion(): Int {
        return Build.VERSION.SDK_INT
    }

    @ChecksSdkIntAtLeast(parameter = 0)
    fun isApiVersionGraterOrEqual(thisVersion: Int): Boolean {
        return Build.VERSION.SDK_INT >= thisVersion
    }
//}