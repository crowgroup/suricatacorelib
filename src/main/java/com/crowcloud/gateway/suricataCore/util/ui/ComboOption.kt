package com.crowcloud.gateway.suricataCore.util.ui

data class ComboOption(
    val id: Int,
    val text: String,
    var isSelected: Boolean
)
