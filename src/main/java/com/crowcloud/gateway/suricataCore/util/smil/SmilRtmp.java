package com.crowcloud.gateway.suricataCore.util.smil;

import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;

/**
 * SMIL file parser. Intended to work with RTMP smil file.
 */
public class SmilRtmp {
    public static final String TAG = SmilRtmp.class.getSimpleName();

    private static final String META = "meta";
    private static final String BASE = "base";
    private static final String VIDEO = "video";
    private static final String SRC = "src";
    private static final String BITRATE = "system-bitrate";
    private static final String WIDTH = "width";
    private static final String HEIGHT = "height";

    public static VideoLinksList parse(String content) {
        VideoLinksList result = new VideoLinksList(System.currentTimeMillis());

        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(new StringReader(content));
            String base = null;

            while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
                if (parser.getEventType() == XmlPullParser.START_TAG && parser.getName().equals(META)) {
                    base = parser.getAttributeValue(null, BASE);
                    Log.i(TAG, "base is " + base);
                }

                if (parser.getEventType() == XmlPullParser.START_TAG && parser.getName().equals(VIDEO) && base != null) {
                    String rtmpLink = parser.getAttributeValue(null, SRC);
                    rtmpLink = base.replace("/_definst_", "") + "/" + rtmpLink.replace("mp4:", "");
                    try {
                        result.add(new VideoLink(
                                rtmpLink,
                                Integer.parseInt(parser.getAttributeValue(null, BITRATE)),
                                Integer.parseInt(parser.getAttributeValue(null, WIDTH)),
                                Integer.parseInt(parser.getAttributeValue(null, HEIGHT))));
                    } catch (NumberFormatException e) {
                        Log.e(TAG, e.getMessage(), e);
                    }

                    Log.i(TAG, "Complete RTMP link is: " + rtmpLink);
                }
                parser.next();
            }

        } catch (XmlPullParserException | IOException | NumberFormatException e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return result;
    }
}
