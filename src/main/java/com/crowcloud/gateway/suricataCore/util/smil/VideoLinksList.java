package com.crowcloud.gateway.suricataCore.util.smil;

import java.util.ArrayList;

/**
 * Video urls list. It is combination of {@link VideoLink} list and video start timestamp in milliseconds.
 */
@SuppressWarnings("WeakerAccess")
public class VideoLinksList extends ArrayList<VideoLink> {
    public final long videoStartTimeMs;

    public VideoLinksList(long videoStartTimeMs) {
        this.videoStartTimeMs = videoStartTimeMs;
    }
}
