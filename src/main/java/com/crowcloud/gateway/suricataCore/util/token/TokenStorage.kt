package com.crowcloud.gateway.suricataCore.util.token

interface TokenStorage {
    fun saveAccessToken(token: String, expirationTime: Long)
    fun getAccessToken(): String?
    fun saveRefreshToken(token: String)
    fun getRefreshToken(): String?
    fun hasValidToken(): Boolean
    fun clearTokens()
}
