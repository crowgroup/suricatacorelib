package com.crowcloud.gateway.suricataCore.util

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*

const val TAG = "DateUtil"

const val TIME_ZONE = "UTC"

const val DateTimeFormat: String = "dd/MM/yyyy HH:mm:ss"
const val DateFormat: String = "dd/MM/yyyy"

const val PictureDateTimeFormatOneFrom: String = "yyyy-MM-dd'T'HH:mm:ss'Z'"
const val PictureDateTimeFormatTwoFrom: String = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'"
const val PictureDateFormatTo: String = "dd/MM/yyyy"
const val PictureTimeFormatTo: String = "HH:mm:ss"

const val EventDateTimeFormatFrom: String = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'"
const val EventDateTimeFormatTo: String = "dd MMM yyyy, HH:mm:ss"

fun getFormattedDateAsString(created: String?, formatFrom: String, formatTo: String): String? {
    val date = convertStringToDate(created, formatFrom)
    val dateFormat = SimpleDateFormat(formatTo, Locale.getDefault())
    if (date != null){
        val tmp = convertDateToLocalDate(date, TIME_ZONE)
        return dateFormat.format(tmp?.time)
    }
    return null
}

//Convert timestamp to Local zone
fun convertDateToLocalDate(dateFrom: Date, fromTimeZone: String): Date? {
    val sdfFrom = SimpleDateFormat(DateTimeFormat, Locale.getDefault())
    val sdfTo = SimpleDateFormat(DateTimeFormat, Locale.getDefault())
    sdfFrom.timeZone = TimeZone.getTimeZone(fromTimeZone)
    sdfTo.timeZone = TimeZone.getDefault()
    var dateTo: Date? = null
    try {
        dateTo = sdfFrom.parse(sdfTo.format(dateFrom))
    } catch (e: Throwable) {
        e.printStackTrace()
    }
    return dateTo
}

fun convertStringToDate(strDate: String?, format: String): Date? {
    return try {
        val formatter = SimpleDateFormat(format, Locale.getDefault())
        strDate?.let { formatter.parse(it) }
    } catch (exception: Exception) {
        Log.d(TAG, exception.toString())
        null
    }
}

//greater than start Unix time (1970...)
fun isAfterEpoch(panelTime: Date): Boolean {
    val givenTimeInMillis = panelTime.time
    return givenTimeInMillis > 0
}

fun convertDateToCalendar(date: Date): Calendar {
    val calendar: Calendar = Calendar.getInstance()
    calendar.time = date
    return calendar
}

fun getTodayDate(): Date? {
    return Calendar.getInstance().time
}

fun getYesterdayDate(): Date? {
    val cal = Calendar.getInstance()
    cal.add(Calendar.DATE, -1)
    return cal.time
}