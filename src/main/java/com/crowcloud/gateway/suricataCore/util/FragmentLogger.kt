package com.crowcloud.gateway.suricataCore.util

import android.content.Context
import android.util.Log
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class FragmentLogger(private val context: Context) {

    private val maxLogFileSize = 1024 * 1024 // 1 MB for example
    private val logFile = File(context.filesDir, "CrowLog.txt")
    init {
        Log.d("FragmentLogger", "Log file path: ${logFile.absolutePath}")
        if (!logFile.exists()) {
            logFile.createNewFile()
        }
    }


    private fun shouldRotateLogFile(): Boolean {
        return logFile.length() > maxLogFileSize
    }

    private fun rotateLogFile() {
        // Rename the current log file and start a new one
        val archiveFile = File(context.filesDir, "CrowLog_${System.currentTimeMillis()}.txt")
        logFile.renameTo(archiveFile)
        // Optionally, compress the old log file here
    }

    @Synchronized
    fun log(tag: String, message: String) {
        val timestamp = SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
            Locale.getDefault()).format(Date())
        val logMessage = "$timestamp [$tag]: $message\n"

        if (shouldRotateLogFile()) {
            rotateLogFile()
        }

        BufferedWriter(FileWriter(logFile, true)).use { out ->
            out.append(logMessage)
        }
    }
}