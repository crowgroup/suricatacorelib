package com.crowcloud.gateway.suricataCore.util

sealed class CustomResult<out T : Any> {
    data class Success<out T : Any>(val data: T) : CustomResult<T>()
    data class Error(val message: String) : CustomResult<Nothing>()
    object Loading : CustomResult<Nothing>()
}
