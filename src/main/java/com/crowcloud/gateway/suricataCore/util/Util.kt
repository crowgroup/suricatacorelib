package com.crowcloud.gateway.suricataCore.util

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Build
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.BulletSpan
import android.util.DisplayMetrics
import android.util.Pair
import android.util.Patterns
import android.view.View
import android.view.WindowInsets
import android.view.WindowMetrics
import android.view.inputmethod.InputMethodManager
import androidx.annotation.*
import com.crowcloud.gateway.suricataCore.R
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.meari.sdk.bean.VideoTimeRecord.*
import com.meari.sdk.common.DeviceType
import org.jdeferred.Promise
import org.jdeferred.impl.DeferredObject
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.Executors

@ChecksSdkIntAtLeast(api = Build.VERSION_CODES.O)
fun isOreoOrHigher() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O

const val DEFAULT_ACCESS_TAG_ID = "0000000000000000"

const val DEFAULT_CONNECT_TIMEOUT = 20L // 20 seconds
const val DEFAULT_WRITE_TIMEOUT = 30L   // 30 seconds
const val DEFAULT_READ_TIMEOUT = 20L    // 20 seconds

const val NUMBER_OF_ZONES = 58
const val NUMBER_OF_OUTPUTS = 30
const val NUMBER_OF_KEYPADS = 4

const val MIN_PHONE_NUMBER_LENGTH = 6
const val MAX_PHONE_NUMBER_LENGTH = 16
const val MIN_USER_CODE_LENGTH = 4
const val MAX_USER_CODE_LENGTH = 8

fun isValidPhoneNumber(phoneNumber: String): Boolean =
    phoneNumber.length in MIN_PHONE_NUMBER_LENGTH..MAX_PHONE_NUMBER_LENGTH &&
            phoneNumber.all { it.isDigit() || it == '-' || it == '+' }

fun isValidUserCode(userCode: String): Boolean =
    userCode.length in MIN_USER_CODE_LENGTH..MAX_USER_CODE_LENGTH && userCode.all { it.isDigit() }

fun concat(values: Collection<String>?, separator: String): String? {
    if (!values.isNullOrEmpty()) {
        if (values.size == 1)
            return values.iterator().next()

        val sb = StringBuilder()
        for (item in values) {
            sb.append(item).append(separator)
        }
        sb.delete(sb.length - separator.length, sb.length)//remove last separator(not needed..)
        return sb.toString()
    }

    return null
}

fun convertToBulletList(stringList: List<String>): CharSequence {
    val spannableStringBuilder = SpannableStringBuilder("")
    stringList.forEachIndexed { index, text ->
        val line: CharSequence = text + if (index < stringList.size - 1) "\n" else ""
        val spannable: Spannable = SpannableString(line)
        spannable.setSpan(
            BulletSpan(15, Color.DKGRAY),
            0,
            spannable.length,
            Spanned.SPAN_INCLUSIVE_EXCLUSIVE
        )
        spannableStringBuilder.append(spannable)
    }
    return spannableStringBuilder
}

fun validateEmail(email: String) = Patterns.EMAIL_ADDRESS.matcher(email).matches()

fun now(): Long = System.currentTimeMillis()

fun <T> asyncPromise(operation: () -> T): Promise<T, Throwable, Void> {
    val deferredObject = DeferredObject<T, Throwable, Void>()
    Executors.newSingleThreadExecutor().submit {
        try {
            val res = operation()
            deferredObject.resolve(res)
        } catch (t: Throwable) {
            deferredObject.reject(t)
        }
    }
    return deferredObject.promise()
}

fun safeParse(s: String, defVal: Int): Int = try {
    Integer.parseInt(s)
} catch (t: Throwable) {
    defVal
}

fun closeKeyboard(inputView: View) {
    val imm = inputView.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(inputView.windowToken, 0)
}

fun View.showKeyboard() {
    this.postDelayed({
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
    }, 100)
}

fun View.hideKeyboard() {
    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
}

fun getScreenWidth(activity: Activity): Int {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        val windowMetrics: WindowMetrics = activity.windowManager.currentWindowMetrics
        val insets: android.graphics.Insets = windowMetrics.windowInsets
            .getInsetsIgnoringVisibility(WindowInsets.Type.systemBars())
        windowMetrics.bounds.width() - insets.left - insets.right
    } else {
        val displayMetrics = DisplayMetrics()
        activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
        displayMetrics.widthPixels
    }
}

fun getScreenHeight(activity: Activity): Int {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        val windowMetrics: WindowMetrics = activity.windowManager.currentWindowMetrics
        val insets: android.graphics.Insets = windowMetrics.windowInsets
            .getInsetsIgnoringVisibility(WindowInsets.Type.systemBars())
        windowMetrics.bounds.height() - insets.top - insets.bottom
    } else {
        val displayMetrics = DisplayMetrics()
        activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
        displayMetrics.heightPixels
    }
}

fun insertToSP(context: Context, jsonMap: HashMap<String, List<String>>) {
    val jsonString = Gson().toJson(jsonMap)
    val sharedPreferences: SharedPreferences = context.getSharedPreferences("HashMap",
        Context.MODE_PRIVATE
    )
    val editor = sharedPreferences.edit()
    editor.putString("map", jsonString)
    editor.apply()
}

fun readFromSP(context: Context): HashMap<String?, List<String?>?>? {
    val sharedPreferences: SharedPreferences = context.getSharedPreferences("HashMap",
        Context.MODE_PRIVATE
    )
    val defValue = Gson().toJson(HashMap<String, List<String>>())
    val json = sharedPreferences.getString("map", defValue)
    val token: TypeToken<HashMap<String?, List<String?>?>?> =
        object : TypeToken<HashMap<String?, List<String?>?>?>() {}
    return Gson().fromJson(json, token.type)
}

fun convertLongToTime(time: Long, pattern: String): String {
    val date = Date(time)
    val format = SimpleDateFormat(pattern, Locale.getDefault())
    return format.format(date)
}

fun currentTimeToLong(): Long {
    return System.currentTimeMillis()
}

var userAddableZoneTypeMap = HashMap<Int, Pair<String, String>>().apply {
    put(0x31, Pair("SDEVTYPE_PIR", "Infrared motion detector (0x31)"))         // 49 in decimal
    put(0x32, Pair("SDEVTYPE_MAG", "Magnetic contact (0x32)"))
    put(0x34, Pair("SDEVTYPE_SMK", "Smoke detector (0x34)"))                   // 52 in decimal
    put(0x36, Pair("SDEVTYPE_GBD", "Glass break detector (0x36)"))
    put(0x37, Pair("SDEVTYPE_CAM", "Camera PIR (0x37)"))                       // 55 in decimal
    put(0x38, Pair("SDEVTYPE_FLD", "Flood detector (0x38)"))                   // 56 in decimal
    put(0x39, Pair("SDEVTYPE_VIB", "Magnet & Vibration sensor (0x39)"))        // 57 in decimal
    put(0x3D, Pair("SDEVTYPE_AIRQ", "Air quality detector (0x3D)"))
    put(0x4D, Pair("SDEVTYPE_AIRQ2", "Advanced Air quality detector (0x4D)"))  // 77 in decimal
    put(0x41, Pair("SDEVTYPE_TMP", "Temperature (0x41)"))
    put(0x44, Pair("SDEVTYE_SMK_SYNC–1", "Smoke detector (sync 0x44)"))        // 68 in decimal
    put(0x48, Pair("SDEVTYE_SMK_SYNC–2", "Smoke heat (sync 0x48)"))            // 72 in decimal
    put(0x4A, Pair("SDEVTYE_SMK_SYNC–3", "New Smoke heat (sync 0x4A)"))         // 74 in decimal
    put(0x52, Pair("SDEVTYPE_PIR2", "PIR based on CT2035 (0x52)"))
    put(0x53, Pair("SDEVTYPE_CAM2", "Camera PIR based on CT2035 (0x53)"))
    put(0x63, Pair("SDEVTYPE_CAM3", "Outdoor PIR (0x63)")) //same config as Indoor PIR camera 0x53
}

var userAddableOutputTypeMap = HashMap<Int, Pair<String, String>>().apply {
    put(0x40, Pair("SDEVTYPE_SMART_SRN2", "Siren (0x40)"))
    put(0x45, Pair("SDEVTYPE_SRN", "Indoor Siren (0x45)"))
    put(0x46, Pair("SDEVTYPE_SMART_SRN_IN", "Smart Siren (0x46)"))
//    put(0x47, Pair("SDEVTYPE_WATER_METER", "Water meter"))
//    put(0xE7, Pair("SDEVTYPE_DECT_OUT", "AC Plug Output(DECT)"))
//    put(0xA1, Pair("SDEVTYPE_LOCK", "Yale Lock"))
//    put(0xE8, Pair("SDEVTYPE_ZB_BULB_D", "ZB Dimmable light"))
//    put(0xE9, Pair("SDEVTYPE_ZB_BULB_C", "ZB Color light"))
//    put(0xEA, Pair("SDEVTYPE_ZB_AC_PLUG", "ZB AC plug"))
//    put(0xEB, Pair("SDEVTYPE_ZB_DL", "ZB Door lock"))
//    put(0xEC, Pair("SDEVTYPE_ZB_DL_CONTROLER", "ZB Door lock Controller"))
//    put(0xED, Pair("SDEVTYPE_ZB_BULB_T", "ZB Temperature light"))
//    put(0x56, Pair("SDEVTYPE_FOGG", "Fog Generator"))
    put(0x57, Pair("SDEVTYPE_OUT", "AC Plug Output (0x57)"))
}

var userAddableKeypadTypeMap = HashMap<Int, Pair<String, String>>().apply {
    put(0x95, Pair("SDEVTYPE_ICON_KPD3", "Pro Keypad (0x95)"))
    put(0x97, Pair("SDEVTYPE_ICON_KPD", "Led Indication Keypad (0x97)"))
    put(0x99, Pair("SDEVTYPE_AVD_KPD", "Advanced keypad (0x99)"))
}

var deviceTypeCodeMap = HashMap<Int, Pair<String, String>>().apply {
    put(0x00, Pair("SDEVTYPE_NON", "Unknown device"))
    put(0x01, Pair("Wired", "Wired"))
    put(0x31, Pair("SDEVTYPE_PIR", "Infrared motion detector"))
    put(0x32, Pair("SDEVTYPE_MAG", "Magnetic contact"))
    put(0x33, Pair("SDEVTYPE_RMT", "Pendant remote control"))
    put(0x34, Pair("SDEVTYPE_SMK", "Smoke detector"))
    put(0x35, Pair("SDEVTYPE_GAS", "Gas detector"))
    put(0x36, Pair("SDEVTYPE_GBD", "Glass break detector"))
    put(0x37, Pair("SDEVTYPE_CAM", "Camera PIR"))
    put(0x38, Pair("SDEVTYPE_FLD", "Flood detector"))
    put(0x39, Pair("SDEVTYPE_VIB", "Magnet & Vibration sensor"))
    put(0x3A, Pair("SDEVTYPE_HAM", "Home automation"))
    put(0x3B, Pair("SDEVTYPE_SPO", "Heart & saturation monitor"))
    put(0x3C, Pair("SDEVTYPE_TFM", "Temperature-flood-magnet detector")) // NOT IN USE
    put(0x3D, Pair("SDEVTYPE_AIRQ", "Air quality detector"))
    put(0x4D, Pair("SDEVTYPE_AIRQ2", "Advanced Air quality detector")) // 77
    put(0x3E, Pair("SDEVTYPE_RADON", "Radon detector"))
    put(0x3F, Pair("SDEVTYPE_AIRPRS", "Air pressure detector"))
    put(0x40, Pair("SDEVTYPE_SMART_SRN2", "Siren"))
    put(0x41, Pair("SDEVTYPE_TMP", "Temperature"))
    put(0x42, Pair("SDEVTYP_MAG_GATE", "Magnet & Gate"))
    put(0x44, Pair("SDEVTYE_SMK_SYNC-1", "Smoke detector (sync)"))
    put(0x45, Pair("SDEVTYPE_SRN", "Indoor Siren"))
    put(0x46, Pair("SDEVTYPE_SMART_SRN_IN", "Smart Siren"))
    put(0x47, Pair("SDEVTYPE_WATER_METER", "Water meter"))
    put(0x48, Pair("SDEVTYE_SMK_SYNC-2", "Smoke heat (sync)"))
    put(0x4A, Pair("SDEVTYE_SMK_SYNC-3", "Smoke heat (sync)")) //As regular smoke sync heat smoke(0x48)
    put(0x49, Pair("SDEVTYPE_OCCUPANCY", "New Occupancy")) //a new device. is it SDEVTYPE_OCCUPANCY?
    put(0x51, Pair("SDEVTYPE_BEDS", "Bed sensor"))
    put(0x52, Pair("SDEVTYPE_PIR2", "PIR based on CT2035"))
    put(0x53, Pair("SDEVTYPE_CAM2", "Camera PIR based on CT2035"))
    put(0x63, Pair("SDEVTYPE_CAM3", "Outdoor PIR (0x63)"))
    put(0x54, Pair("SDEVTYPE_OCCUPANCY", "Occupancy"))
    put(0x55, Pair("SDEVTYPE_VER_CAM", "Verification Camera"))
    put(0x57, Pair("SDEVTYPE_OUT", "AC Plug Output"))
    put(0x59, Pair("SDEVTYPE_IO_2_2", "IO 2*2 Relay Board"))
    //put(0x93, Pair("SDEVTYPE_ICON_KPD_V13", "V13 Icon Keypad")) // Not in app use! Keypad for Sector only
    put(0x95, Pair("SDEVTYPE_ICON_KPD3", "Pro Keypad")) // Keypad for Secom
    put(0x96, Pair("SDEVTYPE_BPSS_KPD", "Bypass Keypad"))
    put(0x97, Pair("SDEVTYPE_ICON_KPD", "Led Indication Keypad"))
    put(0x98, Pair("SDEVTYPE_KPD", "LCD Keypad"))
    put(0x99, Pair("SDEVTYPE_AVD_KPD", "Advanced keypad"))
    put(0xE7, Pair("SDEVTYPE_DECT_OUT", "AC Plug Output(DECT)"))
    put(0xA1, Pair("SDEVTYPE_LOCK", "Yale Lock"))
    put(0xE8, Pair("SDEVTYPE_ZB_BULB_D", "ZB Dimmable light"))
    put(0xE9, Pair("SDEVTYPE_ZB_BULB_C", "ZB Color light"))
    put(0xEA, Pair("SDEVTYPE_ZB_AC_PLUG", "ZB AC plug"))
    put(0xEB, Pair("SDEVTYPE_ZB_DL", "ZB Door lock"))
    put(0xEC, Pair("SDEVTYPE_ZB_DL_CONTROLER", "ZB Door lock Controller"))
    put(0xED, Pair("SDEVTYPE_ZB_BULB_T", "ZB Temperature light"))
    put(0x56, Pair("SDEVTYPE_FOGG", "Fog Generator"))
}

enum class CrowDeviceType {
    PANEL,
    OUTPUT,
    SIREN,
    AC_PLUG,
    ZIGBEE_RGB,
    ZIGBEE_TEMPERATURE,
    ZIGBEE_DIMMABLE,
    FOG_GENERATOR,
    WATER_METER,
    DOOR_LOCK_OUTPUT,
    DOOR_LOCK_CONTROLLER,
    DOOR_LOCK,
    CAMERA,
    ZONE,
    KEYPAD,
    REPEATER,
    VOICE_BOX,
    PENDANT,
    ACCESS_TAG,
    EXTENDER
}

enum class AlbumItemType{
    SNAPSHOT,
    VIDEO
}

const val MeariOutputDateTimeFormat: String = "yyyy-MM-dd HH:mm:ss"
const val MeariAlertDateTimeFormat: String = "yyyyMMddHHmmss"
const val DBDateTimeFormat: String = "yyyy-MM-dd HH:mm:ss"
const val MeariOutputDateFormat: String = "yyyy-MM-dd"
const val MeariInputDateFormat: String = "yyyyMMdd"
const val MeariAlertDateFormat: String = "yyyy-MM"

fun changeStringDateFormat(strDate: String): String {
    return if (isOreoOrHigher()) {
        val dateTime = LocalDateTime.parse(strDate, DateTimeFormatter.ofPattern(MeariOutputDateTimeFormat))
        dateTime.format(DateTimeFormatter.ofPattern(DBDateTimeFormat))
    } else {
        val parser = SimpleDateFormat(MeariOutputDateTimeFormat, Locale.getDefault())
        val formatter = SimpleDateFormat(DBDateTimeFormat, Locale.getDefault())
        parser.parse(strDate)?.let { formatter.format(it) } ?: ""
    }
}

fun calculateDurationInSec(startHour: Int, startMinute: Int, startSecond: Int, endHour: Int,
                            endMinute: Int, endSecond: Int): Int {
    val startSeconds = startHour*60*60 + startMinute*60 + startSecond
    val endSeconds = endHour*60*60 + endMinute*60 + endSecond
    return endSeconds - startSeconds
}

enum class IpDeviceType{
    MEARI_DEVICE, DAHUA_CAMERA
}

enum class MeariDeviceType{
    IPC, //speed camera
    DOORBELL,
    BATTERY_CAMERA,
    FLIGHT_CAMERA,
    OTHER
}

fun getMeariDeviceType(type: Int): MeariDeviceType{
    if(type == DeviceType.IPC){
        return MeariDeviceType.IPC
    }
    if(type == DeviceType.DOORBELL){
        return MeariDeviceType.DOORBELL
    }
    if(type == DeviceType.BATTERY_CAMERA){
        return MeariDeviceType.BATTERY_CAMERA
    }
    if(type == DeviceType.FLIGHT_CAMERA){
        return MeariDeviceType.FLIGHT_CAMERA
    }
    return MeariDeviceType.OTHER
}

//battery charge status: 0-uncharged; 1-charging; 2-full
enum class BatteryChargeStatus{
    UNCHARGED,
    CHARGING,
    FULL
}

enum class RecordingType{
    EVENT_RECORDING,
    FULL_DAY_RECORDING
}

// sound / motion detection
enum class AlertType{
    MOTION_DETECTION,
    SOUND_DETECTION
}

// Alarm type (PIR and Motion)
enum class ImgAlertType{
    MOTION_DETECTION_IMG,
    PIR_DETECTION_IMG
}

fun getRecordingType(type: Int): RecordingType{
    if(type == 0){
        return RecordingType.EVENT_RECORDING
    }
    return RecordingType.FULL_DAY_RECORDING
}

fun getAlertType(type: Int): AlertType{
    if(type == 1){
        return AlertType.SOUND_DETECTION
    }
    return AlertType.MOTION_DETECTION
}

fun getImgAlertType(type: Int): ImgAlertType{
    if (type == 1){
        return ImgAlertType.PIR_DETECTION_IMG
    }
    return ImgAlertType.MOTION_DETECTION_IMG
}

enum class MeariLogoutState {
    NONE,
    SUCCESS,
    FAILED,
    ERROR // Generic error state, specific error messages are not handled here
}

// Define enum for login states
enum class MeariLoginState {
    NONE,
    SUCCESS,
    FAILED,
    ERROR // Generic error state, specific error messages are not handled here
}

enum class LoginMeariState {
    IDLE,
    PROCESSING,
//    RESET_SUCCESS,
    LOGIN_SUCCESS,
    LOGOUT_SUCCESS,
//    RESET_FAILED,
    LOGIN_FAILED,
    LOGOUT_FAILED
}

enum class MeariVideoType{
    LIVE,
    ALARM,
    DOORBELL_CALL
}

enum class MeariDeviceStatus {
    OFFLINE,
    ONLINE,
    DORMANCY,
    UNKNOWN
}

//0 alarmType; 1,3 systemMsgType; 2 bellCallType
enum class MeariPushNotificationType(val type: Int) {
    ALARM_TYPE(0),
    SYSTEM_MSG_TYPE(1),  // Assuming both 1 and 3 represent system messages
    BELL_CALL_TYPE(2),
    UNKNOWN_TYPE(3);

    companion object {
        @JvmStatic
        fun fromInt(type: Int): MeariPushNotificationType = when (type) {
            0 -> ALARM_TYPE
            1, 3 -> SYSTEM_MSG_TYPE
            2 -> BELL_CALL_TYPE
            else -> UNKNOWN_TYPE
        }
    }
}

enum class RecordType{
    CALL_MSG,
    VISITOR_MSG,
    DEMOLITION_DETECTION,
    CRY_DETECTION,
    MOTION_DETECTION
}

fun getRecordType(type: Int) : RecordType{
    when(type) {
        TYPE_CALL -> { return RecordType.CALL_MSG }
        TYPE_VISIT -> { return RecordType.VISITOR_MSG }
        TYPE_TEAR -> { return RecordType.DEMOLITION_DETECTION }
        TYPE_CRY -> { return RecordType.CRY_DETECTION }
    }
    return RecordType.MOTION_DETECTION
}

enum class EditMode{
    SELECT_ALL,
    UNSELECT_ALL,
    NONE
}

enum class ImageType {
    SAVED,
    ALARM
}

fun getPanelSubVersion(version: String): Int {
    val separated = version.split(".").toTypedArray()
    var subVerStr = "0"
    if(separated.size > 2) {
        subVerStr = separated[2]
    }
    return subVerStr.toInt()
}

fun Float.roundTo(n: Int): Float {
    return String.format(Locale.US, "%.${n}f", this).toFloat()
}

fun Double.roundTo(n : Int) : Double {
    return String.format(Locale.US, "%.${n}f", this).toDouble()
}

enum class PinCodeLength(val length: Int) {
    FourDigits(0),
    SixDigits(1),
    EightDigits(2)
}

enum class SensorSensitivity(val level: Int) {
    Low(1),
    Medium(2),
    High(3)
}

enum class MagnetShockSensitivity(val level: Int) {
    Low(1),
    Medium(2),
    High(4)
}

enum class TryProcessType{
    ARM,
    FORCE_ARM,
    STAY,
    FORCE_STAY,
    DISARM,
    FORCE_DISARM,
    NONE
}

enum class SoundType{
    ARM,
    STAY,
    DISARM,
    PANIC,
    NONE
}

enum class ReportChannelType(val type: Int) {
    SMS(3),
    VOICE(4),
    UNKNOWN(-1);

    companion object {
        fun from(type: Int): ReportChannelType {
            return entries.firstOrNull { it.type == type } ?: UNKNOWN
        }
    }
}

const val GAS_LEVEL_ZERO = 0
const val GAS_LEVEL_ONE = 1
const val GAS_LEVEL_TWO = 2
const val GAS_LEVEL_THREE = 3
const val GAS_LEVEL_FOUR = 4
const val GAS_LEVEL_FIVE = 5

const val AIR_Q2_GAS_LEVEL_ONE = 1
const val AIR_Q2_GAS_LEVEL_TWO = 2
const val AIR_Q2_GAS_LEVEL_THREE = 3
const val AIR_Q2_GAS_LEVEL_FOUR = 4
const val AIR_Q2_GAS_LEVEL_FIVE = 5

fun getAirQ1GasLevel(gasValue: Int) : Int {
    var gasLevel = GAS_LEVEL_ZERO
    if (gasValue > 300) {
        gasLevel = GAS_LEVEL_FIVE
    } else if (gasValue > 200) {
        gasLevel = GAS_LEVEL_FOUR
    } else if (gasValue > 150) {
        gasLevel = GAS_LEVEL_THREE
    } else if (gasValue > 100) {
        gasLevel = GAS_LEVEL_TWO
    } else if (gasValue > 50) {
        gasLevel = GAS_LEVEL_ONE
    }
    return gasLevel
}

fun getAirQ2GasLevel(gasValue: Int) : Int {
    var gasLevel = AIR_Q2_GAS_LEVEL_ONE
    if (gasValue > 500) {
        gasLevel = AIR_Q2_GAS_LEVEL_FIVE
    } else if (gasValue > 400) {
        gasLevel = AIR_Q2_GAS_LEVEL_FOUR
    } else if (gasValue > 300) {
        gasLevel = AIR_Q2_GAS_LEVEL_THREE
    } else if (gasValue > 200) {
        gasLevel = AIR_Q2_GAS_LEVEL_TWO
    }
    return gasLevel
}

fun getAirQ1GasColorId(gasLevel: Int): Int {
    return when (gasLevel) {
        GAS_LEVEL_ZERO -> R.color.air_q1_gas_level_1
        GAS_LEVEL_ONE -> R.color.air_q1_gas_level_2
        GAS_LEVEL_TWO -> R.color.air_q1_gas_level_3
        GAS_LEVEL_THREE -> R.color.air_q1_gas_level_4
        GAS_LEVEL_FOUR -> R.color.air_q1_gas_level_5
        GAS_LEVEL_FIVE -> R.color.air_q1_gas_level_6
        else -> R.color.air_q1_gas_level_6
    }
}

fun getAirQ2GasColorId(gasLevel: Int): Int {
    return when (gasLevel) {
        AIR_Q2_GAS_LEVEL_ONE -> R.color.air_q2_gas_level_1
        AIR_Q2_GAS_LEVEL_TWO -> R.color.air_q2_gas_level_2
        AIR_Q2_GAS_LEVEL_THREE -> R.color.air_q2_gas_level_3
        AIR_Q2_GAS_LEVEL_FOUR -> R.color.air_q2_gas_level_4
        AIR_Q2_GAS_LEVEL_FIVE -> R.color.air_q2_gas_level_5
        else -> R.color.air_q2_gas_level_5
    }

}
