package com.crowcloud.gateway.suricataCore.util.ui

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.inputmethod.EditorInfo
import android.widget.Button
import com.crowcloud.gateway.suricataCore.R
import com.crowcloud.gateway.suricataCore.events.EventUserCodeCanceled
import com.crowcloud.gateway.suricataCore.extensions.post
import com.crowcloud.gateway.suricataCore.util.closeKeyboard
import com.crowcloud.gateway.suricataCore.util.showKeyboard
import com.crowcloud.gateway.suricataCore.util.simple_callback.SimpleTextWatcher
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout


class UserCodeDlg(context: Context, val validate: (code: String) ->
                                Boolean, val onDone: (code: String) -> Unit?) : Dialog(context) {

    private var passcodeInput: TextInputEditText? = null
    private var passcodeWrapper: TextInputLayout? = null
    private var confirmPasscodeButton: Button? = null
    private var cancelPasscodeButton: Button? = null

    companion object {
        private const val TAG = "UserCodeDialog"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_usercode)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        window?.decorView?.setBackgroundColor(Color.TRANSPARENT)
        window?.setGravity(Gravity.TOP)

        passcodeInput = findViewById(R.id.passcodeInput)
        passcodeWrapper = findViewById(R.id.passcodeWrapper)
        confirmPasscodeButton = findViewById(R.id.confirmPasscodeButton)
        cancelPasscodeButton = findViewById(R.id.cancelPasscodeButton)
    }

    override fun onStart() {
        super.onStart()

        passcodeInput?.requestFocus()
        passcodeInput?.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) passcodeWrapper?.isErrorEnabled = false }

        passcodeInput?.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                passcodeWrapper?.isErrorEnabled = false
            }
        })

        passcodeInput?.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) trySubmit() else false
        }

        confirmPasscodeButton?.setOnClickListener { trySubmit() }
        cancelPasscodeButton?.setOnClickListener {
            post(EventUserCodeCanceled())
            passcodeInput?.let { it1 -> closeKeyboard(it1) }
            dismiss()
        }
        setCanceledOnTouchOutside(false)
        passcodeInput?.showKeyboard()
    }

    private fun trySubmit() = if (validateInput()) {
        passcodeInput?.let { closeKeyboard(it) };
        onDone(passcodeInput?.text.toString()); dismiss(); true
    } else false

    private fun validateInput(): Boolean {
        if (passcodeInput?.text.toString().isEmpty()) {
            passcodeWrapper?.error = context.getString(R.string.error_usercode_empty)
            passcodeWrapper?.isErrorEnabled = true
            return false
        } else if (passcodeInput?.text.toString().length < 4) {
            passcodeWrapper?.error = context.getString(R.string.error_usercode_short)
            passcodeWrapper?.isErrorEnabled = true
            return false
        } else if (!validate(passcodeInput?.text.toString())) {
            passcodeWrapper?.error = context.getString(R.string.error_usercode_wrong)
            passcodeWrapper?.isErrorEnabled = true
            return false
        }
        return true
    }

}