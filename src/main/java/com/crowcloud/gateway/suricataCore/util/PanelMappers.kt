package com.crowcloud.gateway.suricataCore.util

import com.crowcloud.gateway.suricataCore.network.model.response.ControlPanel
import com.crowcloud.gateway.suricataCore.network.model.response.Panel
import com.crowcloud.gateway.suricataCore.network.model.response.PanelInfo

fun Panel.toPanelInfo(): PanelInfo {
    // Create a new ControlPanel instance and map Panel fields into it
    val controlPanel = ControlPanel(
        id = this.id,
        longitude = this.longitude,
        latitude = this.latitude,
        geoTimezone = this.geoTimezone,
        geoCountry = this.geoCountry,
        panicEnabled = this.panicEnabled,
        reportHeartbeatExpired = this.reportHeartbeatExpired,
        lastReportHeartbeat = this.lastReportHeartbeat,
        hasIpCamsAssociated = this.hasIpCamsAssociated,
        hasDoorbellCamsAssociated = this.hasDoorbellCamsAssociated,
        state = this.state?.name,
        stateFull = this.stateFull?.ordinal,
        pairType = this.pairType?.ordinal,
        version = this.version
    )

    return PanelInfo(
        id = this.id,
        name = this.name,
        role = this.role,
        receivePictures = this.receivePictures,
        controlPanel = controlPanel, // Pass the new ControlPanel
        user = this.user,
        panelUserId = this.panelUser,
        notificationsFilter = this.notificationsFilter,
        isSuperUser = this.isSuperuser ?: false,
        notificationLanguage = this.notificationLanguage,
        userCode = this.userCode
    )
}

fun PanelInfo.toPanel(): Panel {
    return Panel().apply {
        id = this@toPanel.id
        name = this@toPanel.name
        role = this@toPanel.role
        receivePictures = this@toPanel.receivePictures
        user = this@toPanel.user
        panelUser = this@toPanel.panelUserId
        notificationsFilter = this@toPanel.notificationsFilter
        isSuperuser = this@toPanel.isSuperUser
        notificationLanguage = this@toPanel.notificationLanguage ?: ""
        userCode = this@toPanel.userCode

        // Map fields from ControlPanel back to Panel
        this@toPanel.controlPanel?.let {
            longitude = it.longitude
            latitude = it.latitude
            geoTimezone = it.geoTimezone
            geoCountry = it.geoCountry
            panicEnabled = it.panicEnabled
            reportHeartbeatExpired = it.reportHeartbeatExpired
            lastReportHeartbeat = it.lastReportHeartbeat
            hasIpCamsAssociated = it.hasIpCamsAssociated
            hasDoorbellCamsAssociated = it.hasDoorbellCamsAssociated
            state = it.state?.let { stateName -> Panel.PanelState.valueOf(stateName.uppercase()) }
            stateFull = it.stateFull?.let { ordinal -> Panel.StateFull.entries.getOrNull(ordinal) }
            pairType = it.pairType?.let { ordinal -> Panel.PairType.entries.getOrNull(ordinal) }
            version = it.version
        }
    }
}




