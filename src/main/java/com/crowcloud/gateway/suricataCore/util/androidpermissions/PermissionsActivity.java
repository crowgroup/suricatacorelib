package com.crowcloud.gateway.suricataCore.util.androidpermissions;

import android.content.pm.PackageManager;
import android.os.Bundle;

import com.crowcloud.gateway.suricataCore.BuildConfig;
import com.crowcloud.gateway.suricataCore.util.androidpermissions.helpers.Logger;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import static java.util.Arrays.asList;

public class PermissionsActivity extends AppCompatActivity {

    public static final String EXTRA_PERMISSIONS_GRANTED = BuildConfig.LIBRARY_PACKAGE_NAME + ".PERMISSIONS_GRANTED";
    public static final String EXTRA_PERMISSIONS_DENIED = BuildConfig.LIBRARY_PACKAGE_NAME + ".PERMISSIONS_DENIED";
    static final int PERMISSIONS_REQUEST_CODE = 100;
    static final String EXTRA_PERMISSIONS = BuildConfig.LIBRARY_PACKAGE_NAME + ".PERMISSIONS";
    static final Logger logger = Logger.loggerFor(PermissionsActivity.class);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState == null ? new Bundle() : savedInstanceState);

        String[] permissions = getIntent().getStringArrayExtra(EXTRA_PERMISSIONS);
        if (permissions != null) {
            ActivityCompat.requestPermissions(this, permissions, PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length == 0 || permissions.length == 0) {
            logger.e("Permission request interrupted. Aborting.");

            String[] permissionsArr = getIntent().getStringArrayExtra(EXTRA_PERMISSIONS);

            if(permissionsArr != null) {
                PermissionManager.getInstance(this)
                        .removePendingPermissionRequests(asList(permissionsArr));
            }

            //TODO figure out how to finish this activity when request is interrupted to avoid duplicate dialog
            finish();
            return;
        }

        logger.i("RequestPermissionsResult, sending broadcast for permissions " + Arrays.toString(permissions));

        sendPermissionResponse(permissions, grantResults);
        finish();
    }

    private void sendPermissionResponse(@NonNull String[] permissions, @NonNull int[] grantResults) {
        Set<String> grantedPermissions = new HashSet<>();
        Set<String> deniedPermissions = new HashSet<>();

        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                grantedPermissions.add(permissions[i]);
            } else {
                deniedPermissions.add(permissions[i]);
            }
        }
    }
}