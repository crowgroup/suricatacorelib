package com.crowcloud.gateway.suricataCore.util.smil;

import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;

/**
 * SMIL file parser. Intended to work with HLS smil file.
 */
public class SmilHLS {
    public static final String TAG = SmilHLS.class.getSimpleName();

    private static final String VIDEO = "video";
    private static final String SRC = "src";
    private static final String LANG = "systemLanguage";
    private static final String NAME = "name";
    private static final String VALUE = "value";
    private static final String PARAM = "param";
    private static final String BITRATE = "videoBitrate";
    private static final String WIDTH = "width";
    private static final String HEIGHT = "height";

    public static VideoLinksList parse(String content) {
        VideoLinksList result = new VideoLinksList(System.currentTimeMillis());

        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(new StringReader(content));

            String link = null, width = null, height = null, bitRate = null;

            while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {

                if (parser.getEventType() == XmlPullParser.START_TAG && parser.getName().equals(VIDEO)) {
                    link = parser.getAttributeValue(null, SRC);
                    link = link.replace("mp4:", "");
                }

                if (parser.getEventType() == XmlPullParser.START_TAG && parser.getName().equals(PARAM)) {
                    if (WIDTH.equals(parser.getAttributeValue(null, NAME))) {
                        width = parser.getAttributeValue(null, VALUE);
                    }
                    if (HEIGHT.equals(parser.getAttributeValue(null, NAME))) {
                        height = parser.getAttributeValue(null, VALUE);
                    }
                    if (BITRATE.equals(parser.getAttributeValue(null, NAME))) {
                        bitRate = parser.getAttributeValue(null, VALUE);
                    }
                }
                if (link != null && width != null && height != null && bitRate != null) {
                    try {
                        result.add(new VideoLink(
                                link,
                                Integer.parseInt(bitRate),
                                Integer.parseInt(width),
                                Integer.parseInt(height)));
                    } catch (NumberFormatException e) {
                        Log.e(TAG, e.getMessage(), e);
                    }
                    Log.i(TAG, "Complete HLS link is: " + link);
                    link = null;
                    width = null;
                    height = null;
                    bitRate = null;
                }
                parser.next();
            }

        } catch (XmlPullParserException | IOException | NumberFormatException e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return result;
    }
}
