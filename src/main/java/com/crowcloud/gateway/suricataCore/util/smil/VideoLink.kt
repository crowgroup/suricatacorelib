package com.crowcloud.gateway.suricataCore.util.smil

import com.google.gson.annotations.SerializedName

/**
 * Video URL extended representation
 */
class VideoLink(@SerializedName("url") var url: String?,
                @SerializedName("bitrate") var bitrate: Int,
                @SerializedName("width") var width: Int,
                @SerializedName("height") var height: Int) {

    val qualityMetric: Int
        get() = width * height
}