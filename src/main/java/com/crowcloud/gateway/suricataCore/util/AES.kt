package com.crowcloud.gateway.suricataCore.util

import java.security.NoSuchAlgorithmException
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

object AES {

    private var secretKey: SecretKeySpec? = null
    private var key: ByteArray? = null

    fun setKey(myKey: String) {
        try {
            key = myKey.toByteArray(charset("UTF-8"))
            secretKey = SecretKeySpec(key, "AES")
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: Throwable) {
            e.printStackTrace()
        }

    }

    fun encrypt(secret: String, strToEncrypt: String): ByteArray? {
        try {
            setKey(secret)
            val cipher = Cipher.getInstance("AES_128/ECB/PKCS5Padding")
            cipher.init(Cipher.ENCRYPT_MODE, secretKey)
            return cipher.doFinal(strToEncrypt.toByteArray(charset("UTF-8")))
        } catch (e: Exception) {
            println("Error while encrypting: $e")
        }

        return null
    }

    fun decrypt(secret: String, strToDecrypt: ByteArray): ByteArray? {
        try {
            setKey(secret)
            val cipher = Cipher.getInstance("AES_128/ECB/NoPadding")
            cipher.init(Cipher.DECRYPT_MODE, secretKey)
            return cipher.doFinal(strToDecrypt)
        } catch (e: Exception) {
            println("Error while decrypting: $e")
        }

        return null
    }
}