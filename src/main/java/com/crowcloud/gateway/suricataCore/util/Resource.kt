package com.crowcloud.gateway.suricataCore.util

data class Resource<out T>(
    val status: UiStatus,
    val data: T?,
    val message: String?,
    val failedRequest: Any? = null
) {

    companion object {

        fun <T> success(data: T?): Resource<T> {
            return Resource(UiStatus.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?, failedRequest: Any? = null): Resource<T> {
            return Resource(UiStatus.ERROR, data, msg, failedRequest)
        }

            fun <T> loading(data: T?): Resource<T> {
            return Resource(UiStatus.LOADING, data, null)
        }

        fun <T> none(data: T?): Resource<T> {
            return Resource(UiStatus.NONE, data, null)
        }
    }
}

enum class UiStatus {
    SUCCESS,
    ERROR,
    LOADING,
    NONE
}
