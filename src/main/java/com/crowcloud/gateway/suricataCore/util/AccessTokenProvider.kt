package com.crowcloud.gateway.suricataCore.util

interface AccessTokenProvider {
    fun getCurrentAccessToken(): String?
    fun hasValidAccessToken(): Boolean
    fun hasRefreshToken(): Boolean
    suspend fun refreshTokenIfNeeded(): Boolean
    fun clearSession()
}
