package com.crowcloud.gateway.suricataCore.components

import android.util.Log
import com.crowcloud.gateway.suricataCore.events.EventGetLiveVideoStream
import com.crowcloud.gateway.suricataCore.events.EventGetLiveVideoStreamDone
import com.crowcloud.gateway.suricataCore.events.EventRegisterDahuaDevice
import com.crowcloud.gateway.suricataCore.events.EventRegisterDahuaDeviceDone
import com.crowcloud.gateway.suricataCore.extensions.promise
import com.crowcloud.gateway.suricataCore.network.NetworkManager
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class ComponentDahuaProtocol: Component() {

    companion object {
        const val TAG: String = "ComponentDahuaProtocol"
    }

    private val network = NetworkManager.getInstance()

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().register(this)
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventRegisterDahuaDevice) {
        network.dahuaApi.registerDahuaDevice(event.panelId, event.dahuaDevice).promise()
            .fail {
                Log.d(TAG, "Registration of dahua camera failed", it)
               post(EventRegisterDahuaDeviceDone(false, it.message))
            }
            .done {
                if(it == null){
                    Log.d(TAG, "Registration of dahua camera failed")
                    post(EventRegisterDahuaDeviceDone(false))
                }else {
                    Log.d(TAG, "Registration of dahua camera succeeded")
                    post(EventRegisterDahuaDeviceDone(true))
                }
            }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetLiveVideoStream) {
        network.dahuaApi.getDahuaLiveVideoStream(event.panelId, event.deviceId,
            event.videoType).promise()
                .fail {
                    Log.d(TAG, "Registration of dahua camera failed", it)
                    post(EventGetLiveVideoStreamDone(false, null, it.message))
                }
                .done {
                    if(it == null){
                        Log.d(TAG, "Failed to get live video stream")
                        post(EventGetLiveVideoStreamDone(false))
                    }else {
                        Log.d(TAG, "Succeeded to get live video stream")
                        post(EventGetLiveVideoStreamDone(true, it))
                    }
                }
    }
}