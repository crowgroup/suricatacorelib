package com.crowcloud.gateway.suricataCore.components


import android.annotation.SuppressLint

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import android.net.wifi.WifiNetworkSpecifier
import android.os.Build
import android.os.Handler
import android.text.TextUtils
import com.crowcloud.gateway.suricataCore.events.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class ComponentWifi : Component() {

    companion object {
        const val TAG: String = "ComponentWifi"
    }

    data class WifiScanRequestData(var prefix: String, var timeout: Long)

    private var previousSSID: String? = null
    private var isScanning: Boolean = false
    private var networkPrefixFilter: String = ""
    private lateinit var wifiManager: WifiManager
    private val map: HashMap<String, android.net.wifi.ScanResult> = HashMap()//address to descriptor

//    private val wifiReceiver: BroadcastReceiver = object : BroadcastReceiver() {
//        override fun onReceive(context: Context, intent: Intent) {
//            if (checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//                for (scan in wifiManager.scanResults) {
//                    if (scan.SSID.startsWith(networkPrefixFilter)) {
//                        map[scan.SSID] = scan
//                        post(EventReceivedData(DataType.WifiNetworks, ArrayList(map.values)))
//                    }
//                }
//            } else {
//                // Handle the missing permission or request the permission from the user.
//            }
//
//            // If you wish to unregister the receiver after a condition
//            // if (map.isNotEmpty()) {
//            //     App.get()!!.unregisterReceiver(this)
//            // }
//        }
//    }

    private fun checkSelfPermission(context: Context, permission: String): Int {
        return context.checkSelfPermission(permission)
    }

    override fun onStart() {
        super.onStart()
       // wifiManager = com.crowcloud.gatewaymobile.App.get()?.applicationContext?.getSystemService(WIFI_SERVICE) as WifiManager
    }

    @SuppressLint("NewApi")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventRequestData) {
//        if (event.type == DataType.WifiNetworks && !isScanning) {
//            isScanning = true
//            ensureWifiEnabled()
//
//            val data = event.requestData as WifiScanRequestData
//            networkPrefixFilter = data.prefix
//
//            withLocationService({
//                //com.crowcloud.gatewaymobile.App.get()?.registerReceiver(wifiReceiver, IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))
//
//                wifiManager.startScan()
//
//                Handler(Looper.getMainLooper()).postDelayed({
//                   // com.crowcloud.gatewaymobile.App.get()?.unregisterReceiver(wifiReceiver)
//                    post(EventReceivedData(DataType.WifiNetworks, ArrayList(map.values)))
//                    map.clear()
//                    isScanning = false
//                }, data.timeout)
//            }, {
//
//            })
//
//        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventConnectToPreviousWifiNetwork) {
        ensureWifiEnabled()
        previousSSID?.let {
            post(EventConnectToWifiNetwork(SSID = it))
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventRequestCurrentConnectedWifiNetworkSsid) {
        ensureWifiEnabled()
        post(EventRequestCurrentConnectedWifiNetworkSsidDone(SSID = getCurrentSsid()))
    }

    fun getCurrentSsid(): String? {
        if (wifiManager.connectionInfo != null &&
                !TextUtils.isEmpty(wifiManager.connectionInfo.ssid)) {
            return wifiManager.connectionInfo.ssid
        }
        return null
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventConnectToWifiNetwork) {
        ensureWifiEnabled()

        val ssid = getCurrentSsid()
        if (ssid != previousSSID) {
            previousSSID = ssid
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val wifiNetworkSpecifier = WifiNetworkSpecifier.Builder()
                    .setSsid(event.SSID)//event.SSID
                    //.setWpa2Passphrase(passphrase)
                    //.setBssid(mac)
                    .build()
            val networkRequest = NetworkRequest.Builder()
                    .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                    .setNetworkSpecifier(wifiNetworkSpecifier)
                    .build()
            //val connectivityManager =
                    //com.crowcloud.gatewaymobile.App.get()?.applicationContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
           // connectivityManager?.requestNetwork(networkRequest, ConnectivityManager.NetworkCallback())

            val networkCallback = object : ConnectivityManager.NetworkCallback() {
                override fun onUnavailable() {
                    super.onUnavailable()
                    post(EventConnectToWifiNetworkDone(event, Exception("Failed to connect")))
                }
                override fun onLosing(network: Network, maxMsToLive: Int) {
                    super.onLosing(network, maxMsToLive)

                }
                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    //connectivityManager?.bindProcessToNetwork(network)
                    post(EventConnectToWifiNetworkDone(event))
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                    post(EventConnectToWifiNetworkDone(event, Exception("Failed to connect")))
                }
            }
            //connectivityManager?.requestNetwork(networkRequest, networkCallback)
        }else {
            val conf = WifiConfiguration()
            conf.SSID = "\"" + event.SSID + "\""   // Please note the quotes. String should contain ssid in quotes
            conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE)
            wifiManager.addNetwork(conf)

//            if (com.crowcloud.gatewaymobile.App.get()?.applicationContext?.let {
//                    ActivityCompat.checkSelfPermission(
//                        it,
//                        android.Manifest.permission.ACCESS_FINE_LOCATION)
//                } != android.content.pm.PackageManager.PERMISSION_GRANTED) {
//                // TODO: Consider calling
//                //    ActivityCompat#requestPermissions
//                // here to request the missing permissions, and then overriding
//                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                //                                          int[] grantResults)
//                // to handle the case where the user grants the permission. See the documentation
//                // for ActivityCompat#requestPermissions for more details.
//
//            }
//            for (i in wifiManager.configuredNetworks) {
//                if (i.SSID != null && i.SSID == "\"" + event.SSID + "\"") {
//                    wifiManager.disconnect()
//                    wifiManager.enableNetwork(i.networkId, true)
//                    wifiManager.reconnect()
//                    break
//                }
//            }
            Handler().postDelayed({
                if ("\"" + event.SSID + "\"" == wifiManager.connectionInfo.ssid) {
                    post(EventConnectToWifiNetworkDone(event))
                } else {
                    post(EventConnectToWifiNetworkDone(event, Exception("Failed to connect")))
                }
            }, 10000)
        }
    }

    private fun ensureWifiEnabled() {
//        wifiManager = com.crowcloud.gatewaymobile.App.get()?.applicationContext?.getSystemService(WIFI_SERVICE) as WifiManager
//        if (!wifiManager.isWifiEnabled) {
//            Toast.makeText(com.crowcloud.gatewaymobile.App.get(), "WiFi is disabled. We need to enable it", Toast.LENGTH_LONG).show()
//            wifiManager.isWifiEnabled = true
//        }
    }
}