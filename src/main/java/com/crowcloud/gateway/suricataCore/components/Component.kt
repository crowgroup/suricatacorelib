package com.crowcloud.gateway.suricataCore.components

import org.greenrobot.eventbus.EventBus


abstract class Component {

    open fun onStart() {

    }

    open fun onStop() {

    }

    protected fun post(event: Any?) {
       EventBus.getDefault().post(event)
    }

    protected fun postSticky(event: Any?) {
       EventBus.getDefault().postSticky(event)
    }

}