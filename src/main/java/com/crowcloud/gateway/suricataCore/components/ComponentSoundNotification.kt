package com.crowcloud.gateway.suricataCore.components


import android.media.MediaPlayer
import android.os.Handler
import android.os.Looper
import android.speech.tts.TextToSpeech
import com.crowcloud.gateway.suricataCore.R
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.events.EventReceivedData
import com.crowcloud.gateway.suricataCore.events.EventSelectedPanel
import com.crowcloud.gateway.suricataCore.events.EventToggleSiren
import com.crowcloud.gateway.suricataCore.extensions.currentPanelData
import com.crowcloud.gateway.suricataCore.network.model.response.Area
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class ComponentSoundNotification : Component() {

    companion object {
        const val TAG: String = "ComponentSoundNotification"

        var disarmedTemplateVoiceResId: Int = R.string.disarmed_template
        var armedTemplateVoiceResId: Int = R.string.armed_template
        var stayArmedTemplateVoiceResId: Int = R.string.stay_armed_template
        var usePanelName: Boolean = false

        fun initialize(usePanelName: Boolean, disarmedTemplateVoiceResId: Int, armedTemplateVoiceResId: Int, stayArmedTemplateVoiceResId: Int) {
            this.disarmedTemplateVoiceResId = disarmedTemplateVoiceResId
            this.armedTemplateVoiceResId = armedTemplateVoiceResId
            this.stayArmedTemplateVoiceResId = stayArmedTemplateVoiceResId
            this.usePanelName = usePanelName
        }
    }

    private var textToSpeech: TextToSpeech? = null
    private var areas: HashMap<Int, Area> = HashMap()

    override fun onStart() {
        super.onStart()
        areas.putAll(currentPanelData()?.areasMap ?: HashMap())
//        textToSpeech = TextToSpeech(com.crowcloud.gatewaymobile.App.get(), TextToSpeech.OnInitListener {})
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onAlarm(event: EventToggleSiren) {
        if (event.isOn) {
            initAndStartSound()
        } else {
            stopSiren()
        }
    }

    private var mp: MediaPlayer? = null

    private fun initAndStartSound() {
//        val am = com.crowcloud.gatewaymobile.App.get()?.getSystemService(Context.AUDIO_SERVICE) as AudioManager
//        if (am.ringerMode != AudioManager.RINGER_MODE_NORMAL) {
//            return
//        }
//        mp = MediaPlayer()
//
//        mp?.setAudioStreamType(AudioManager.STREAM_RING)
//
//        mp?.setVolume(am.getStreamVolume(AudioManager.STREAM_RING).toFloat(), am.getStreamVolume(AudioManager.STREAM_RING).toFloat())
//        val audio = Uri.parse("android.resource://" + com.crowcloud.gatewaymobile.App.get()?.packageName + "/" + R.raw.siren)
//
//        mp?.isLooping = true
//        mp?.setOnPreparedListener { startSiren() }
//        try {
//            mp?.setDataSource(com.crowcloud.gatewaymobile.App.get()!!, audio)
//        } catch (e: IOException) {
//            e.printStackTrace()
//            mp?.release()
//            mp = null
//        }
//
//        mp?.prepareAsync()
    }


    private fun startSiren() {
        if (mp?.isPlaying == false) {
            mp?.start()
            Handler(Looper.getMainLooper()).postDelayed({ this.stopSiren() }, (60 * 1000).toLong())
        }
    }

    fun stopSiren() {
        mp?.release()
        mp = null
    }


    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventReceivedData) {
        when (event.type) {
            DataType.Area -> if (event.data is Area) {
                announceNewAreaStateIfNeeded(event.data as Area)
            }
            DataType.Areas -> if (event.data is List<*>) {
                (event.data as List<*>).forEach { announceNewAreaStateIfNeeded(it as Area) }
            }
            else -> {}
        }
    }

    private fun announceNewAreaStateIfNeeded(newAreaData: Area) {
        if (areas[newAreaData.id] != null && newAreaData.state != areas[newAreaData.id]?.state) {
            val templateResId = when (newAreaData.state) {
                Area.AreaState.Disarmed -> disarmedTemplateVoiceResId
                Area.AreaState.Armed -> armedTemplateVoiceResId
                Area.AreaState.StayArmed1, Area.AreaState.StayArmed2 -> stayArmedTemplateVoiceResId
                else -> -1
            }
//            textToSpeech?.speak(getString(templateResId,
//                if (usePanelName)
//                    DataRepo.instance.currentPanel?.name ?: getString(R.string.panel)
//                else
//                    newAreaData.name ?: getString(R.string.area)),
//                TextToSpeech.QUEUE_ADD, null, null)

        }
        areas[newAreaData.id] = newAreaData
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSelectedPanel) {
        areas.clear()
        areas.putAll(currentPanelData()?.areasMap ?: HashMap())
    }

}