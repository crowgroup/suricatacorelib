package com.crowcloud.gateway.suricataCore.components

import android.location.Location
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.crowcloud.gateway.suricataCore.events.*
import com.crowcloud.gateway.suricataCore.exception.CrowNetworkException
import com.crowcloud.gateway.suricataCore.extensions.*
import com.crowcloud.gateway.suricataCore.network.NetworkManager
import com.crowcloud.gateway.suricataCore.network.model.request.*
import com.crowcloud.gateway.suricataCore.network.model.response.*
import com.google.gson.JsonObject
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jdeferred.Promise
import org.jdeferred.impl.DeferredObject

class ComponentActions : Component() {

    companion object {
        const val TAG: String = "ComponentActions"
    }

    private val network = NetworkManager.getInstance()

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().register(this)
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetLatestMeasurementsByZone){
        network.measurementsApi.getLatestMeasurementsByZone(currentControlPanel()?.id!!).promise()
            .done {
                currentPanelData()?.measurementsMap = it
                post(EventGetLatestMeasurementsByZoneDone())
                Log.d(TAG, "EventGetLatestMeasurementsByZone: getLatestMeasurementsByZone")
            }.fail {
                post(EventGetLatestMeasurementsByZoneDone(it.message))
            }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetLatestPicturesByZone){
        network.picturesApi.getLatestPicturesByZone(currentControlPanel()?.id!!).promise()
            .done {
                currentPanelData()?.latestPicturesMap = it
                post(EventGetLatestPicturesByZoneDone(true))
            }.fail {
                post(EventGetLatestPicturesByZoneDone(false, it.message))
            }
    }

    //ACTIONS
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventCreateOrUpdatePanelUser) {
        if (event.user.id != null) {
            network.internalUsersApi.updateUserOfPanel(event.userCode, currentControlPanel()?.id!!,
                event.user.id!!, event.user).promise()
                .done {
                    post(EventUpdatePanelUserDone(it))
                }.fail {
                    post(EventUpdatePanelUserDone(event.user, error = it))
                }
        } else {
            network.internalUsersApi.createUserOfPanel(
                event.userCode, currentControlPanel()?.id!!, event.user).promise()
                .done {
                    post(EventAddPanelUserDone(user = it))
                }.fail {
                     post(EventAddPanelUserDone(event.user, error = it))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetMaintenanceState) {
        currentControlPanel()?.run {
            network.panelsApi.getMaintenanceState(id!!).promise()
                .done {
                    currentPanelData()?.maintenanceUntil = it.maintenanceUntil
                    postSticky(EventGetMaintenanceStateDone(true, null))
                }
                .fail {
                    postSticky(EventGetMaintenanceStateDone(false, it.message))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventCloseMaintenance) {
        currentControlPanel()?.run {
            network.panelsApi.closeMaintenance(id!!).promise()
                .done {
                    currentPanelData()?.maintenanceUntil = it.maintenanceUntil
                    postSticky(EventCloseMaintenanceDone(true, null))
                }
                .fail {
                    postSticky(EventCloseMaintenanceDone(false, it.message))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSetMaintenanceDuration) {
        currentControlPanel()?.run {
            network.panelsApi.setMaintenanceDuration(mac!!, event.requestedTime).promise()
                .done {
                    currentPanelData()?.maintenanceUntil = it.maintenanceUntil
                    postSticky(EventSetMaintenanceDurationDone(true, null))
                }
                .fail {
                    postSticky(EventSetMaintenanceDurationDone(false, it.message))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventResetMaintenanceDuration) {
        currentControlPanel()?.run {
            network.panelsApi.closeMaintenance(id!!).promise()
                .done {
                    network.panelsApi.setMaintenanceDuration(mac!!, event.requestedTime).promise()
                        .done {
                            currentPanelData()?.maintenanceUntil = it.maintenanceUntil
                            postSticky(EventResetMaintenanceDurationDone(true, null))
                        }
                        .fail {
                            postSticky(EventResetMaintenanceDurationDone(false, it.message))
                        }
                }
                .fail {
                    postSticky(EventResetMaintenanceDurationDone(false, it.message))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventBypassRequest) {
        currentControlPanel()?.run {
            network.zonesApi.bypassZone(id!!, event.zone.id, BypassRequest(!event.zone.bypass)).promise()
                .done { zone ->
                    if(zone != null) {
                        post(EventBypassZoneDone(zone))
                    }else{
                        post(EventByPassFailed(event.zone))
                    }
                }
                .fail {
                    post(EventBypassZoneDone(event.zone, it))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventToggleLockRequest) {
        currentControlPanel()?.run {
            Log.d(TAG, "EventToggleLockRequest 1")
            network.locksApi.toggleLock(id!!, event.doorLock.id,
                ToggleLockRequest(!event.doorLock.isLocked)).promise()
                .done { doorLock ->
                    if(doorLock != null) {
                        Log.d(TAG, "EventToggleLockRequest success")
                        post(EventToggleDoorLockDone(true, doorLock, event.index))
                    }else{
                        Log.d(TAG, "EventToggleLockRequest failed")
                        post(EventToggleDoorLockDone(false, event.doorLock, event.index))
                    }
                }
                .fail {
                    Log.d(TAG, "EventToggleLockRequest failed " + it.message)
                    post(EventToggleDoorLockDone(false, event.doorLock, event.index, it.message))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetDoorLockById) {
        currentControlPanel()?.run {
            network.locksApi.getDoorLock(id!!, event.id).promise()
                .done { doorLock ->
                    if(doorLock != null) {
                        post(EventGetDoorLockByIdDone(true, doorLock, event.index))
                    }else{
                        Log.d(TAG, "get door lock request failed")
                        post(EventGetDoorLockByIdDone(false, null, event.index))
                    }
                }
                .fail {
                    Log.d(TAG, "get door lock request failed: " + it.message)
                    post(EventGetDoorLockByIdDone(false, null, event.index, it.message))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventRequestTakePicture) {
        Log.d(TAG, "EventRequestTakePicture")
        currentControlPanel()?.run {
            network.picturesApi.takePicture(id!!, event.selectedCamera?.id!!, JsonObject()).promise()
                .done {
                    Log.d(TAG, "EventRequestTakePicture done")
                    post(EventTakePictureDone(event.selectedCamera!!, it))
                }
                .fail { error ->
                    Log.d(TAG, "EventRequestTakePicture fail, error: ${error.message}")
                    post(EventTakePictureDone(event.selectedCamera!!, null, error.message))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventCancelTakePicture) {
        currentControlPanel()?.run {
            currentControlPanel()?.id?.let {
                network.picturesApi.cancelTakePicture(it, event.zoneId).promise()
                    .done {
                    }
                    .fail {
                        // TODO: 2/18/21
                    }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventDeleteCameraPicture){
        currentControlPanel()?.run {
            network.picturesApi.deletePictureById(id!!, event.pictureId).promise()
                .done {
                    post(EventDeleteCameraPictureDone(event.pictureId, event.isLatestPicture))
                }
                .fail {
                    post(EventDeleteCameraPictureDone(null))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetUserRegistrationInAssaInfo){
        network.locksApi.getUserRegistrationInAssaInfo().promise()
            .done {
                if(it == null) {
                    post(EventGetUserRegistrationInAssaInfoDone(null))
                }else {
                    post(EventGetUserRegistrationInAssaInfoDone(it))
                }
            }
            .fail {
                post(EventGetUserRegistrationInAssaInfoDone(null))
            }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventRegisterUserInAssa){
        network.locksApi.registerUserInAssa(LocaleRequest(event.locale)).promise()
            .done {
                if(it == null) {
                    post(EventRegisterUserInAssaDone(null))
                }else {
                    post(EventRegisterUserInAssaDone(it))
                }
            }
            .fail {
                post(EventRegisterUserInAssaDone(null))
            }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetLocks){
        network.locksApi.getLocks(event.panelId!!).promise()
            .done {
                if(it == null) {
                    post(EventGetLocksDone(null))
                }else {
                    post(EventGetLocksDone(it as ArrayList<DoorLock>))
                }
            }
            .fail {
                post(EventGetLocksDone(null))
            }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetLocksInAssa){
        network.locksApi.getYaleLocks().promise()
            .done {
                if(it == null) {
                    post(EventGetLocksInAssaDone(null))
                }else {
                    post(EventGetLocksInAssaDone(it.result))
                }
            }
            .fail {
                post(EventGetLocksInAssaDone(null))
            }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventAddLock){
        network.locksApi.addYaleLockToAssa(event.mac, SerialNumber(event.serial)).promise()
            .done {
                if(it == null) {
                    post(EventAddLockDone(null))
                }else {
                    post(EventAddLockDone(event.mac))
                }
            }
            .fail {
                post(EventAddLockDone(null))
            }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventDeleteLock){
        network.locksApi.deleteYaleLockById(event.lockId).promise()
            .done {
                if(it == null) {
                    post(EventDeleteLockDone(null))
                }else {
                    post(EventDeleteLockDone(event.lockId))
               }
            }
            .fail {
                post(EventDeleteLockDone(null))
            }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetAssaAccessRequest){
        network.locksApi.getAccessRequest(event.accessRequestId).promise()
            .done {
                if(it == null) {
                    post(EventGetAssaAccessRequestDone(null))
                }else {
                    post(EventGetAssaAccessRequestDone(it.result))
                }
            }
            .fail {
                post(EventGetAssaAccessRequestDone(null))
            }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventAcceptAssaRequest){
        network.locksApi.acceptAccessRequest(event.accessRequestId, AcceptAccessRequest(event.lockId)).promise()
            .done {
                if(it == null) {
                    post(EventAcceptAssaRequestDone(null))
                }else {
                    post(EventAcceptAssaRequestDone(event.lockId))
                }
            }
            .fail {
                post(EventAcceptAssaRequestDone(null))
            }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventRejectAssaRequest){
        network.locksApi.rejectAccessRequest(event.accessRequestId).promise()
            .done {
                if(it == null) {
                    post(EventRejectAssaRequestDone(null))
                }else {
                    post(EventRejectAssaRequestDone(event.accessRequestId))
                }
            }
            .fail {
                post(EventRejectAssaRequestDone(null))
            }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventRequestPanicAlarm) {
        currentControlPanel()?.run {
            val location: Location? = null//Util.getLastKnownLocation()
            Log.e("TEST_PANIC", "try to request panic alarm long/lat = ${LongLat(location?.longitude
                    ?: 0.0, location?.latitude ?: 0.0)}")
            network.panelsApi.panic(id!!,
                PanicRequestBody(LongLat(location?.longitude ?: 0.0, location?.latitude
                        ?: 0.0))).promise()
                .done {
                    Log.e("TEST_PANIC", "done request panic alarm response = $it")
                    if (it.panicAlarm) {
                        Handler(Looper.getMainLooper()).postDelayed({
                            post(EventRequestData(DataType.Troubles, useCache = false))
                        }, 3000)
                    } else {
                        Log.e("TEST_PANIC", "failed to panic")
                    }
                }
                .fail {
                    Log.e("TEST_PANIC", "failed to panic", it)
                }
        }
    }

//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    fun onEvent(event: EventRequestUpdateAreaState) = currentPanel()?.run {
//        var area = currentPanelData()?.areasMap?.get(event.areaId)
//        if ((area?.isReadyToArm == false && event.newState == AreaStateRequest.State.ARM && !event.force)
//                || (area?.isReadyToStay == false && event.newState == AreaStateRequest.State.STAY && !event.force)) {
//            post(EventRequestUpdateAreaStateNotReady(event))
//        } else
//            network.areasApi.setAreaState(id!!, event.areaId, AreaStateRequest(event.newState, event.force)).promise()
//                .done {
//
//                    area = currentPanelData()?.areasMap?.get(event.areaId)
//                    val delay = when (event.newState) {
//                        AreaStateRequest.State.ARM -> {
//                            getArmDelayTSPref(event.areaId)?.set(System.currentTimeMillis() + ((area?.exitDelay?.toLong()
//                                    ?: 0L) * 1000))
//                            area?.exitDelay
//                        }
//                        AreaStateRequest.State.DISARM -> 0
//                        AreaStateRequest.State.STAY -> {
//                            getStayDelayTSPref(event.areaId)?.set(System.currentTimeMillis() + ((area?.stayExitDelay?.toLong()
//                                    ?: 0L) * 1000))
//                            area?.stayExitDelay
//                        }
//                    }
//
//                    post(EventReceivedData(DataType.Area, it))
//
//                    val start = System.currentTimeMillis()
//                    Handler(Looper.getMainLooper()).postDelayed({
//                        post(EventRequestData(DataType.Area, requestData = event.areaId, useCache = false))
//                        post(EventRequestData(DataType.Troubles, useCache = false))
//                        Log.i("TEST", "request areas again after ${event.newState} after ${(System.currentTimeMillis() - start) / 1000} seconds")
//                    }, (delay?.toLong() ?: 0L) * 1000)
//
//                }
//                .fail { throwable ->
////                    post(EventReceivedData(DataType.Area, throwable))
////
////                    //update data
////                    post(EventRequestData(DataType.Area, requestData = event.areaId, useCache = false))
////                    post(EventRequestData(DataType.Troubles, useCache = false))
////
////                    currentPanelData()?.areasMap?.get(event.areaId)?.let {
////                        post(EventActionDone(data = it, error = throwable, event = it))
////                    }
//                }
//    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventUpdateNotificationFilters) {
//        currentControlPanel()?.run {
//            network.panelsApi.updatePanelById(panelUser!!, event.body).promise()
//                .done {
//                    currentPanelInfo()?.notificationsFilter = it.notificationsFilter
//                    currentPanelInfo()?.receivePictures = it.receivePictures
//                   // post(EventRequestData(DataType.CurrentPanel))
//                    post(EventActionDone(data = it, event = it))
//                }
//                .fail {
//                    post(EventActionDone(data = event.body, event = event.body, error = it))
//                }
//        }
    }

//    @SuppressLint("MissingPermission")
//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    fun onEvent(event: EventUpdatePanelLocation) {
//        if (event.location != null) {
//            updatePanelLocation(event.location)
//        } else {
//            withLocationService(onLocationGranted = {
//                val lm = com.crowcloud.gatewaymobile.App.get()?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
//                val location = lm.getLastKnownLocation(
//                        lm.getBestProvider(Criteria().apply { accuracy = Criteria.ACCURACY_COARSE; isCostAllowed = true },
//                                false)!!)
//
//                location?.let { updatePanelLocation(it) }
//            })
//        }
//    }

    private fun updatePanelLocation(location: Location) {
//        currentPanel()?.run {
//            network.panelsApi.updatePanelById(panelUser!!,
//                Panel().apply {
//                    userCode = currentPanel()?.userCode
//                    mac = currentPanel()?.mac
//                    remoteAccessPassword = currentPanel()?.remoteAccessPassword
//                    longitude = location.longitude
//                    latitude = location.latitude
//
//                }).promise()
//                .done {
//                    currentPanel()?.longitude = it.longitude
//                    currentPanel()?.latitude = it.latitude
//                    post(EventRequestData(DataType.CurrentPanel))
//                    post(EventActionDone(data = it, event = it))
//                }
//                .fail {
//                    post(EventActionDone(currentPanel()!!, error = it, data = currentPanel()!!))
//                }
//        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventRegisterCameraDevice) {
        currentControlPanel()?.run {
            network.camerasApi.registerCamera(id!!, RegisterCameraRequest(event.serialNumber, event.name)).promise()
                .done {
                    post(EventActionDone(event = it, data = it))
                }
                .fail {
                    if (it is CrowNetworkException) {
                        //removed 409 code
                        if (it.code == 423) {//Camera already exists - try updating instead
                            network.camerasApi.updateCamera(id!!, event.serialNumber, RegisterCameraRequest(name = event.name)).promise()
                                .done {
                                    post(EventActionDone<RegisterCameraResponse, RegisterCameraResponse>(event = RegisterCameraResponse(), data = null))
                                }
                                .fail {
                                    post(EventActionDone<RegisterCameraResponse, RegisterCameraResponse>(event = RegisterCameraResponse(), error = it))
                                }
                        } else {
                            post(EventActionDone<RegisterCameraResponse, RegisterCameraResponse>(event = RegisterCameraResponse(), error = it))
                        }
                    } else {
                        post(EventActionDone<RegisterCameraResponse, RegisterCameraResponse>(event = RegisterCameraResponse(), error = it))
                    }
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventUpdateCamera) {
        currentControlPanel()?.run {
            network.camerasApi.updateCamera(id!!, event.camera.device?.deviceUid!!,
                    RegisterCameraRequest(name = event.name)).promise()
                .done {
                    post(EventUpdateCameraDone())
                }
                .fail {
                    post(EventUpdateCameraDone(error = it))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventZoomInIpCamera) {
        currentControlPanel()?.run {
            network.camerasApi.zoomInIpCamera(id!!, event.uid, event.zoomVideoRequest).promise()
                .done {
                    post(EventZoomInIpCameraDone())
                }
                .fail {
                    post(EventZoomInIpCameraDone(error = it))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventZoomOutIpCamera) {
        currentControlPanel()?.run {
            network.camerasApi.zoomOutIpCamera(id!!, event.uid).promise()
                .done {
                    post(EventZoomOutIpCameraDone())
                }
                .fail {
                    post(EventZoomOutIpCameraDone(error = it))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventDeleteCamera) {
        currentControlPanel()?.run {
            network.camerasApi.deleteCamera(id!!, event.camera.device?.deviceUid!!).promise()
                .done {
                    post(EventDeleteIpCameraDone())
                }
                .fail {
                    post(EventDeleteIpCameraDone(error = it))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventArmIpCamera) {
        currentControlPanel()?.run {
            network.camerasApi.armIpCamera(id!!, event.camera.device?.deviceUid!!).promise()
                .done {
                    post(EventArmIpCameraDone())
                }
                .fail {
                    post(EventArmIpCameraDone(error = it))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventDisarmIpCamera) {
        currentControlPanel()?.run {
            network.camerasApi.disarmIpCamera(id!!, event.camera.device?.deviceUid!!).promise()
                .done {
                    post(EventDisarmIpCameraDone())
                }
                .fail {
                    post(EventDisarmIpCameraDone(error = it))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventUpgradeIpCamera) {
        currentControlPanel()?.run {
            network.camerasApi.upgradeIpCamera(id!!, event.camera.device?.deviceUid!!).promise()
                .done {
                    post(EventUpgradeIpCameraDone())
                }
                .fail {
                    post(EventUpgradeIpCameraDone(error = it))
                }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventDeleteNotificationsEmail) {
        currentControlPanel()?.run {
            //TODO: add EventDeleteNotificationsEmailDone
            network.panelsApi.deleteNotificationsEmail(id!!, event.emailId).promise().actionDone(event)
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventUpdateNotificationsEmail) {
        currentControlPanel()?.run {
            //TODO: add EventUpdateNotificationsEmailDone
            network.panelsApi.updateNotificationsEmail(id!!, event.email.id!!, event.email).promise().actionDone(event)
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventAddNotificationsEmail) {
        currentControlPanel()?.run {
            //TODO: add EventAddNotificationsEmailDone
            network.panelsApi.addNotificationsEmail(id!!, event.email).promise().actionDone(event)
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventRegisterIotDevice) {
        currentControlPanel()?.run {
            network.iotApi.registerIotDevice(id!!, IotDevice(mac = event.mac,
                    name = event.name,
                    type = event.type,
                    uid = event.uid)).promise().actionDone(event)
        }

        if (currentControlPanel() == null) {
            network.iotApi.registerIotDevice(event.panelId, IotDevice(mac = event.mac,
                    name = event.name,
                    type = event.type,
                    uid = event.uid)).promise().actionDone(event)
        }
    }

    fun <T, E> Promise<T, Throwable, Void>.actionDone(event: E): Promise<T, Throwable, Void> {
        val deferred = DeferredObject<T, Throwable, Void>()
        this.done {
            post(EventActionDone<E, T>(event = event, data = it))
            deferred.resolve(it)
        }.fail {
            post(EventActionDone<E, T>(event = event, error = it))
            Log.d(TAG, "Post Error " + it.message)
            deferred.reject(it)
        }
        return deferred.promise()
    }

}