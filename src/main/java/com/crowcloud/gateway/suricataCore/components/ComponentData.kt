package com.crowcloud.gateway.suricataCore.components


import androidx.annotation.StringRes
import com.crowcloud.gateway.suricataCore.events.EventRequestData
import com.crowcloud.gateway.suricataCore.extensions.*
import com.crowcloud.gateway.suricataCore.network.NetworkManager
import com.crowcloud.gateway.suricataCore.network.model.response.*
import com.crowcloud.gateway.suricataCore.network.services.IpCamerasApi
import com.crowcloud.gateway.suricataCore.network.services.MeasurementsApi
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.Serializable

class ComponentData : Component() {

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().register(this)
    }

    data class PagingInfo(val page: Int,
                          val pageSize: Int)

    data class GeneralTrouble(val device: BaseDevice? = null,
                              @StringRes val descResId: Int)

//    data class MeasurementStatsRequestData(val measurement: Measurement,
//                                           val time: Int,
//                                           val timeUnit: MeasurementsApi.TimeUnit,
//                                           val offset: Int = 0)

    data class ShortcutData(val device: BaseDevice? = null,
                            val panelUser: PanelUser? = null) : Serializable {
        override fun equals(other: Any?): Boolean {
            if (other !is ShortcutData) return false
            return panelUser?.equals(other.panelUser) != false // if both are null return true
                    && device?.equals(other.device) != false
        }

        override fun hashCode(): Int {
            return ("" + panelUser?.hashCode() + device?.hashCode()).hashCode()
        }
    }

    data class StreamRequestData(val streamType: IpCamerasApi.StreamType? = null,
                                 val deviceUid: String? = null,
                                 val eventId: String? = null,
                                 val videoName: String? = null)

    companion object {
        const val TAG: String = "ComponentData"
    }

    private val network = NetworkManager.getInstance()

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onRequestData(event: EventRequestData) {
//        val panelId = currentPanel()?.id ?: -1
//        when (event.type) {
//            DataType.Panels -> {
//                postCachedData(event) { DataRepo.instance.panels }
//                event.pagingInfo?.let {
//                    network.panelsApi.getPanels(it.page, it.pageSize).promise().sendData(prefsStorage, panelId, event.type) {
//                        if (it.previous == null) DataRepo.instance.panels.clear()
//                        DataRepo.instance.panels.addAll(it.results ?: ArrayList())
//
//                        //remove duplicates
////                        val idSet: HashSet<Int> = HashSet()
////                        val removelist : ArrayList<Panel> = ArrayList()
////                        DataRepo.get().panels.forEach {
////                            if(idSet.contains(it.id!!)){
////                                removelist.add(it)
////                            }
////                            idSet.add(it.id!!)
////                        }
////
////                        DataRepo.get().panels.removeAll(removelist)
//                    }
//                }
//            }
//            DataType.PanelConnectionState -> requestByPanelId(event, network.panelsApi::connectToPanel)
//
//            DataType.Areas -> {
//                postCachedData(event) { currentPanelData()?.areasMap?.values?.sortedBy { it.id } }
//                requestByPanelId(event, network.areasApi::getAreasForPanel) {
//                    currentPanelData()?.areas = it as ArrayList<Area>
//                    it.forEach { area -> currentPanelData()?.areasMap?.put(area.id, area) }
//                }
//            }
//
//            DataType.Area -> if (currentPanel() != null && event.requestData is Int) {//requestData -> area Id
//                postCachedData(event) { currentPanelData()?.areasMap?.get(event.requestData) }
//                network.areasApi.getAreasForPanelByAreaId(currentPanel()?.id!!, event.requestData).promise()
//                        .sendData(panelId, event.type) { currentPanelData()?.areasMap?.put(it.id, it) }
//            }
//            DataType.Troubles -> {
//                postCachedData(event) { currentPanelData()?.troubles }
////                requestByPanelId(event, network.troublesApi::getTroubles) { currentPanelData()?.troubles = it }
//            }
//            DataType.AllTroubles -> {
//                post(EventReceivedData(DataType.AllTroubles, generateGeneralTroubles(currentPanelData()?.troubles,
//                        currentPanelData()?.zones,
//                        currentPanelData()?.outputs)))
////                JavaUtil.waitToPromises(
////                        requestByPanelId(EventRequestData(DataType.Troubles), network.troublesApi::getTroubles) { currentPanelData()?.troubles = it },
////                        requestByPanelId(EventRequestData(DataType.Zones), network.zonesApi::getZonesForPanel) { currentPanelData()?.zones = ArrayList(it) },
////                        requestByPanelId(EventRequestData(DataType.Outputs), network.outputsApi::getOutputsForPanel) { currentPanelData()?.outputs = ArrayList(it) })
////                        .done {
////                            post(EventReceivedData(DataType.AllTroubles, generateGeneralTroubles(currentPanelData()?.troubles,
////                                    currentPanelData()?.zones,
////                                    currentPanelData()?.outputs)))
////                        }
////                        .fail {
////                            post(EventReceivedData(DataType.AllTroubles, it.reject as Throwable))
////                        }
//            }
//            DataType.Measurements -> {
//                postCachedData(event) { currentPanelData()?.measurementsMap }
//                requestByPanelId(event, network.measurementsApi::getLatestMeasurementsByZone) { currentPanelData()?.measurementsMap = it }
//            }
//            DataType.MeasurementStats -> {
//                if (event.requestData is MeasurementStatsRequestData && currentPanel()?.id != null) {
//                    val key = "${event.requestData.time}${event.requestData.timeUnit.name}"
//
//                    postCachedData(event) { currentPanelData()?.measureStatsMap?.get(key) }
//
//                    val data = event.requestData.measurement.data
//                    network.measurementsApi.getMeasurementsByPeriod(currentPanel()?.id!!,
//                            data?.deviceType ?: Measurement.DeviceType.Zone,
//                            data?.deviceId ?: 0,
//                            data?.dectInterface ?: 0,
//                            "${event.requestData.time}${event.requestData.timeUnit}",
//                            event.requestData.offset)
//                            .promise()
//                            .sendData(panelId, DataType.MeasurementStats) { currentPanelData()?.measureStatsMap?.put(key, it) }
//                }
//            }
//            DataType.CurrentPanel -> {
//                postCachedData(event) { currentPanel() }
//            }
//            DataType.Devices -> {}
//            DataType.LocksInAssa -> {
//                postCachedData(event) { currentPanelData()?.locksInAssa }
//                requestWithoutPanelId(event, network.locksApi::getYaleLocks) {
//                    currentPanelData()?.locksInAssa = it.result }
//            }
//            DataType.Zones -> {
//                postCachedData(event) { currentPanelData()?.zones }
//                requestByPanelId(event, network.zonesApi::getZonesForPanel) { currentPanelData()?.zones = ArrayList(it) }
//            }
//            DataType.Outputs -> {
//                postCachedData(event) { currentPanelData()?.outputs }
//                requestByPanelId(event, network.outputsApi::getOutputsForPanel) { currentPanelData()?.outputs = ArrayList(it) }
//            }
//            DataType.Keypads -> {
//                postCachedData(event) { currentPanelData()?.keypads }
//                requestByPanelId(event, network.panelsApi::getKeypads) { currentPanelData()?.keypads = it }
//            }
//            DataType.LoginCreds -> {/*handled in ComponentLogin*/
//            }
//            DataType.Events -> {}
//            DataType.Pictures -> {
//                postCachedData(event) { currentPanelData()?.pictures }
//                val camera: Zone? = event.requestData as Zone?
//                if (camera == null) {
//                    Log.d(TAG, "camera is null")
//                } else {
//                    requestPaginatedDataByItemId(event, currentPanelData()?.pictures, camera.id,
//                            network.picturesApi::getZonePictures)
//                    Log.d(TAG, "size of pictures: " + currentPanelData()?.pictures?.size)
//                }
//            }
//            DataType.LatestPictures -> {
//                postCachedData(event) { currentPanelData()?.latestPicturesMap }
//                requestByPanelId(event, network.picturesApi::getLatestPicturesByZone) {
//                    currentPanelData()?.latestPicturesMap = it }
//            }
//            DataType.Users -> {
//                postCachedData(event) { currentPanelData()?.users }
//                //requestPaginatedData(event, currentPanelData()?.users, network.panelsApi::getPanelUserAccounts)
//            }
//            DataType.UsersOfPanel -> {
//                postCachedData(event) { currentPanelData()?.internalUsers }
//                requestByPanelId(event, network.internalUsersApi::getUsersForPanel) {
//                    currentPanelData()?.internalUsers = it }
//            }
//            DataType.CurrentUser -> {
//                postCachedData(event) { DataRepo.instance.currentUser }
//                network.authApi.user().promise().sendData(panelId, event.type) { DataRepo.instance.currentUser = it }
//            }
//            DataType.MaintenanceState -> {
//                postCachedData(event) { currentPanelData()?.maintenanceUntil }
//                requestByPanelId(event, network.panelsApi::getMaintenanceState) {
//                    currentPanelData()?.maintenanceUntil = it.maintenanceUntil }
//            }
//            DataType.SirenEnabledState -> {
//                postCachedData(event) { EncryptedPrefsJava.Bool.IS_SIREN_ENABLED.get() }
//            }
//            DataType.CurrentUserOfPanel -> TODO()
//            DataType.NotificationFilters -> TODO()
//            DataType.NotificationsEmails -> {
//                postCachedData(event) { currentPanelData()?.notificationsEmails }
//                requestPaginatedData(event, currentPanelData()?.notificationsEmails,
//                        network.panelsApi::getNotificationsEmails)
//            }
//            DataType.Shortcuts -> {
//                postCachedData(event) { constructShortcuts() }
//                JavaUtil.waitToPromises(
//                        requestByPanelId(EventRequestData(DataType.UsersOfPanel), network.internalUsersApi::getUsersForPanel) { currentPanelData()?.internalUsers = it },
//                        requestByPanelId(EventRequestData(DataType.Zones), network.zonesApi::getZonesForPanel) { currentPanelData()?.zones = ArrayList(it) },
//                        requestByPanelId(EventRequestData(DataType.Outputs), network.outputsApi::getOutputsForPanel) { currentPanelData()?.outputs = ArrayList(it) })
//                        .done {
//                            post(EventReceivedData(DataType.Shortcuts, constructShortcuts()))
//                        }
//                        .fail {
//                            post(EventReceivedData(DataType.Shortcuts, it.reject as Throwable))
//                        }
//            }
//            DataType.Zone -> {
//                val id: Int = event.requestData as Int
//                postCachedData(event) { currentPanelData()?.zones?.find { it.id == id } }
//                requestByCurrentPanelAndItemId(event, id, network.zonesApi::getZone) {
//                    val idx = currentPanelData()?.zones?.indexOfFirst { it.id == id }
//                    idx?.apply {
//                        currentPanelData()?.zones?.removeAt(idx)
//                        currentPanelData()?.zones?.add(idx, it)
//                    }
//                }
//            }
//            DataType.Output -> {
//                val id: Int = event.requestData as Int
//                postCachedData(event) { currentPanelData()?.outputs?.find { it.id == id } }
//                requestByCurrentPanelAndItemId(event, id, network.outputsApi::getOutput) {
//                    val idx = currentPanelData()?.outputs?.indexOfFirst { it.id == id }
//                    idx?.apply {
//                        currentPanelData()?.outputs?.removeAt(idx)
//                        currentPanelData()?.outputs?.add(idx, it)
//                    }
//                }
//            }
//            DataType.AccessRequests -> {
//                postCachedData(event) { currentPanelData()?.lockAccessRequests }
//                requestWithoutPanelId(event, network.locksApi::getAccessRequestsOld) {
//                    currentPanelData()?.lockAccessRequests = it.result
//                }
//            }
//            DataType.IpCameras -> {
//                postCachedData(event) { currentPanelData()?.ipCameras }
//                requestPaginatedData(event, currentPanelData()?.ipCameras, network.camerasApi::getCameras)
//            }
//            DataType.MeariAlbum -> {
//                val cameraInfo = event.requestData as CameraInfo
//                val items = ArrayList<Any?>()
//                post(EventReceivedData(event.type, items))
//            }
//
//            DataType.MeariDevices -> {
//                postCachedData(event) { currentPanelData()?.ipMeariCameras }
//                MeariUser.getInstance().getDeviceList(object : IDevListCallback {
//                    override fun onSuccess(dev: MeariDevice) {
//                        // Select according to the device you access
//                        val cameraInfos: ArrayList<CameraInfo> = ArrayList()
//                        cameraInfos.addAll(dev.nvrs)
//                        cameraInfos.addAll(dev.ipcs)
//                        cameraInfos.addAll(dev.doorBells)
//                        cameraInfos.addAll(dev.batteryCameras)
//                        cameraInfos.addAll(dev.voiceBells)
//                        cameraInfos.addAll(dev.fourthGenerations)
//                        cameraInfos.addAll(dev.flightCameras)
//                        cameraInfos.addAll(dev.chimes)
//
//                        currentPanelData()?.ipMeariCameras?.clear()
//                        currentPanelData()?.ipMeariCameras?.addAll(cameraInfos)
//
//                        com.crowcloud.gateway.suricataCore.extensions.post(
//                            EventReceivedData(
//                                event.type,
//                                currentPanelData()?.ipMeariCameras
//                            )
//                        )
//                    }
//
//                    override fun onError(code: Int, error: String) {
//                        Log.d(TAG, error)
//                    }
//                })
//            }
//            DataType.StreamData -> {
//                val reqData = event.requestData as StreamRequestData
//                if (reqData.deviceUid != null) {
//                    postCachedData(event) { currentPanelData()?.streamDataMap?.get(reqData.deviceUid) }
//                    network.camerasApi.getCameraStream(currentPanel()?.id!!, reqData.deviceUid, reqData.streamType?.value)
//                            .promise()
//                            .sendData(panelId, event.type) {
//                                currentPanelData()?.streamDataMap?.put(reqData.deviceUid, it)
//                            }
//                } else if (reqData.videoName != null && reqData.eventId != null) {
//                    postCachedData(event) { currentPanelData()?.streamDataMap?.get(reqData.eventId + reqData.videoName) }
//                    network.camerasApi.getCameraStream(eventId = reqData.eventId, videoName = reqData.videoName)
//                            .promise()
//                            .sendData(panelId, event.type) {
//                                currentPanelData()?.streamDataMap?.put(reqData.eventId + reqData.videoName, it)
//                            }
//                }
//            }
//            DataType.VideoEvents -> {
//                val deviceId: String = event.requestData as String
//                val deviceUid = currentPanelData()?.ipCameras?.filter {
//                    it.device?.deviceId.equals(deviceId) }?.first()?.device?.deviceUid
//                deviceUid?.let {
//                    postCachedData(event) { currentPanelData()?.videoEventsMap?.getOrPut(it) { ArrayList() } }
//                    requestPaginatedDataByItemId(event, currentPanelData()?.videoEventsMap?.get(deviceId),
//                        it, network.camerasApi::getCameraVideoEvents)
//                }
//            }
//            DataType.IpCamera -> {}
//            DataType.BleIpCameras -> {}
//            DataType.WifiNetworks -> {}
//            DataType.IThinkDeviceInfo -> {}
//            DataType.IThinkDeviceVisibleWifiList -> {}
//            DataType.Locks -> {}
//            DataType.RadioInfo -> {}
//            DataType.EthernetInfo -> {}
//            DataType.GsmInfo -> {}
//            DataType.WifiInfo -> {}
//        }
    }

    private fun generateGeneralTroubles(troubles: Troubles?, zones: List<Zone>?, outputs: List<Output>?): List<GeneralTrouble> {
        val gTroubles = ArrayList<GeneralTrouble>()
        //TODO
//        troubles?.troubles?.forEach { gTroubles.add(GeneralTrouble(device = null, descResId = it)) }
//        zones?.forEach {
//            val zone = it
//            it.troublesList.forEach { gTroubles.add(GeneralTrouble(device = zone, descResId = it)) }
//        }
//        outputs?.forEach {
//            val output = it
//            it.troublesList.forEach { gTroubles.add(GeneralTrouble(device = output, descResId = it)) }
//        }
        return gTroubles
    }

    private fun constructShortcuts(): List<ShortcutData> {
        val models: ArrayList<ShortcutData> = ArrayList()

        currentPanelData()?.internalUsers?.forEach { models.add(ShortcutData(panelUser = it)) }
        currentPanelData()?.zones?.forEach { models.add(ShortcutData(device = it)) }
        currentPanelData()?.outputs?.forEach { models.add(ShortcutData(device = it)) }

        return models
    }

}