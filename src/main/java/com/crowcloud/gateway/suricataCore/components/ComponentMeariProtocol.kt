package com.crowcloud.gateway.suricataCore.components

import android.util.Log
import com.crowcloud.gateway.suricataCore.events.*
import com.meari.sdk.MeariUser
import com.meari.sdk.bean.*
import com.meari.sdk.callback.*
import com.meari.sdk.listener.MeariDeviceListener
import com.meari.sdk.listener.MeariDeviceTalkVolumeListener
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class ComponentMeariProtocol  : Component() {

    companion object {
        const val TAG: String = "ComponentMeariProtocol"
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().register(this)
    }


//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    fun onEvent(event: EventLoginMeariCloud) {
//         MeariUser.getInstance().loginWithAccount(event.countryCode, event.regionCode, event.meariUID, 70,
//            object : ILoginCallback {
//            override fun onSuccess(user: UserInfo) {
//                post(EventLoginMeariCloudDone(true, user, null, null))
//            }
//            override fun onError(errorCode: Int, errStr: String?) {
//                Log.d(TAG, "EventLoginMeariCloud, onError, errStr: $errStr")
//                post(EventLoginMeariCloudDone(false, null, errorCode, errStr))
//            }
//        })
//    }

//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    fun onEvent(event: EventConnectMeariDevice) {
//        event.deviceController?.startConnect(object : MeariDeviceListener {
//            override fun onSuccess(successMsg: String?) {
//                post(EventConnectMeariDeviceDone(true, successMsg, null))
//            }
//            override fun onFailed(errorMsg: String?) {
//                post(EventConnectMeariDeviceDone(false, null, errorMsg))
//                Log.d(TAG, "Failed to connect device $errorMsg")
//            }
//        })
//    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventDeleteMeariDevice) {
        MeariUser.getInstance().deleteDevice(event.cameraInfo?.deviceID, event.cameraInfo?.devTypeID!!,
            object : IResultCallback {
                override fun onSuccess() {
                    post(EventDeleteMeariDeviceDone(true, event.cameraInfo.deviceName,
                            null, null))
                }
                override fun onError(errorCode: Int, error: String?) {
                    post(EventDeleteMeariDeviceDone(false, event.cameraInfo.deviceName,
                            errorCode, error))
                }
            })
    }

//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    fun onEvent(event: EventGetMeariDeviceParams) {
//        MeariUser.getInstance().getDeviceParams(object : IGetDeviceParamsCallback {
//            override fun onSuccess(params: DeviceParams) {
//                post(EventGetMeariDeviceParamsDone(true, params, null, null))
//            }
//            override fun onFailed(errorCode: Int, errorMsg: String?) {
//                post(EventGetMeariDeviceParamsDone(false, null, errorCode, errorMsg))
//            }
//        })
//    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventRenameMeariDevice) {
        MeariUser.getInstance().renameDevice(event.meariDeviceUiModel?.id,
            event.meariDeviceUiModel?.type!!,
            event.newName, object : IResultCallback {
                override fun onSuccess() {
                    post(EventRenameMeariDeviceDone(true, event.newName, null, null))
                }
                override fun onError(errorCode: Int, errorMsg: String?) {
                    post(EventRenameMeariDeviceDone(false, "", errorCode, errorMsg))
                }
            })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSwitchMotionDetection) {
        // 0-close; 1-open;
        MeariUser.getInstance().setMotionDetection(event.enable, event.sensitivity,
            object : ISetDeviceParamsCallback {
                override fun onSuccess() {
                    post(EventSwitchMotionDetectionDone(true, event.enable, null, null))
                }
                override fun onFailed(errorCode: Int, errorMsg: String?) {
                    post(EventSwitchMotionDetectionDone(false, 0, errorCode, errorMsg))
                }
            })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSwitchFlightSiren) {
        // 0-close; 1-open;
        MeariUser.getInstance().setFlightSirenEnable(event.status,
            object : ISetDeviceParamsCallback {
                override fun onSuccess() {
                    post(EventSwitchFlightSirenDone(true, null, null))
                }
                override fun onFailed(errorCode: Int, errorMsg: String?) {
                    post(EventSwitchFlightSirenDone(false, errorCode, errorMsg))
                }
            })
    }

    /*  0-low; 1-mid; 2-high
    Speed 6S, from low to high sensitivity: 4, 6, 8m
    Flight 3/5S,from low to high sensitivity: 6, 8,12m
    Snap 12/15/16S, PIR motion from low to high sensitivity: 4.5~9m
    Bell 15/19S, PIR motion from low to high sensitivity: 2.5~7m
     */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSetMotionDetSensitivity) {
        Log.d(TAG, "EventSetMotionDetSensitivity, event.motionDetSensitivity: " + event.motionDetSensitivity!!)
        MeariUser.getInstance().setMotionDetection(event.motionDetEnable!!, event.motionDetSensitivity,
            object : ISetDeviceParamsCallback {
                override fun onSuccess() {
                    post(EventSetMotionDetSensitivityDone(true, event.motionDetSensitivity, null,null))
                }
                override fun onFailed(errorCode: Int, errorMsg: String?) {
                    post(EventSetMotionDetSensitivityDone(false, event.motionDetSensitivity, errorCode, errorMsg))
                }
            })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSwitchPirDetection) {
        // 0-close; 1-open;
        MeariUser.getInstance().setPirDetection(event.enable, event.sensitivity,
            object : ISetDeviceParamsCallback {
                override fun onSuccess() {
                    post(EventSwitchPirDetectionDone(true, event.enable, null, null))
                }
                override fun onFailed(errorCode: Int, errorMsg: String?) {
                    post(EventSwitchPirDetectionDone(false, 0, errorCode, errorMsg))
                }
            })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSetPirDetSensitivity) {
        MeariUser.getInstance().setPirDetectionSensitivity(event.sensitivity,
            object : ISetDeviceParamsCallback {
                override fun onSuccess() {
                    Log.d(TAG, "Succeeded to set PIR detection sensitivity to $event.sensitivity")
                    post(EventSetPirDetSensitivityDone(true, event.sensitivity, null, null))
                }
                override fun onFailed(errorCode: Int, errorMsg: String?) {
                    Log.d(TAG, "Failed to set PIR detection sensitivity to $event.sensitivity, Error $errorMsg")
                    post(EventSetPirDetSensitivityDone(false, event.sensitivity, errorCode, errorMsg))
                }
            })
    }

    /**
    * Intercom volume setting of Device
    *
    * @param volume intercom volume 0-100
    * @param callback Function callback
    */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSetSpeakerVolume) {
        MeariUser.getInstance().setSpeakVolume(event.volume, object : ISetDeviceParamsCallback {
            override fun onSuccess() {
                post(EventSetSpeakerVolumeDone(true, null, null))
            }
            override fun onFailed(errorCode: Int, errorMsg: String?) {
                post(EventSetSpeakerVolumeDone(false, errorCode, errorMsg))
            }
        })
    }

    /**
     * Day and night mode setting of device
     *
     * @param mode mode
     * @param callback Function callback
     *
     * int dayNightMode; day-night mode: 0-automatic; 1-day mode; 2-night mode;
     */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSetDayNightMode) {
        MeariUser.getInstance().setDayNightMode(event.mode, object : ISetDeviceParamsCallback {
            override fun onSuccess() {
                post(EventSetDayNightModeDone(true,null, null))
            }
            override fun onFailed(errorCode: Int, errorMsg: String?) {
                post(EventSetDayNightModeDone(false, errorCode, errorMsg))
            }
        })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSetHumanDetectionDay) {
        MeariUser.getInstance().setHumanDetectionDay(event.status, object : ISetDeviceParamsCallback {
            override fun onSuccess() {
                post(EventSetHumanDetectionDayDone(true, null, null))
            }
            override fun onFailed(errorCode: Int, errorMsg: String?) {
                post(EventSetHumanDetectionDayDone(false, errorCode, errorMsg))
            }
        })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSetHumanDetectionNight) {
        MeariUser.getInstance().setHumanDetectionNight(event.status, object : ISetDeviceParamsCallback {
            override fun onSuccess() {
                post(EventSetHumanDetectionNightDone(true, null, null))
            }
            override fun onFailed(errorCode: Int, errorMsg: String?) {
                post(EventSetHumanDetectionNightDone(false, errorCode, errorMsg))
            }
        })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetSdCardInfo) {
        MeariUser.getInstance().getSDCardInfo(object : ISDCardInfoCallback {
            override fun onSuccess(sdCardInfo: SDCardInfo) {
                Log.d(TAG, "Succeeded to get SDCardInfo")
                post(EventGetSdCardInfoDone(true, sdCardInfo, null, null))
            }
            override fun onFailed(errorCode: Int, errorMsg: String?) {
                Log.d(TAG, "Failed to get SDCardInfo: $errorMsg, $errorCode")
                post(EventGetSdCardInfoDone(false, null, errorCode, errorMsg))
            }
        })
    }

    /**
     * Set whether the video is mirrored
     *
     * @param mirrorEnable 0-normal;1-mirror
     * @param callback Function callback
     *
     * public void setMirror(int mirrorEnable, ISetDeviceParamsCallback callback);
     */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSetMirrorScreen) {
        MeariUser.getInstance().setMirror(event.mirrorEnable, object : ISetDeviceParamsCallback {
            override fun onSuccess() {
                Log.d(TAG, "Succeeded to flip image")
                post(EventSetMirrorScreenDone(true, null, null))
            }
            override fun onFailed(errorCode: Int, errorMsg: String?) {
                Log.d(TAG, "Failed to flip image")
                post(EventSetMirrorScreenDone(false, errorCode, errorMsg))
            }
        })
    }

    /*
    DeviceUpgradeInfo
      -boolean forceUpgrade; whether to force upgrade
      -int upgradeStatus; whether it can be upgraded 0-No; 1-Yes
      -String newVersion; New version
      -String upgradeDescription; New version description
      -String upgradeUrl; new version address

      / **
       * Check if the device has a new version
       *
       * @param devVersion device version
       * @param lanType language type ("en")
       * @param callback callback
       * /
       public void checkNewFirmwareForDev(String devVersion, String lanType, ICheckNewFirmwareForDevCallback callback);
     */
//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    fun onEvent(event: EventCheckNewFirmware) {
//        MeariUser.getInstance().checkNewFirmwareForDev(event.firmwareVersion, event.snNum, event.tp,
//            object : ICheckNewFirmwareForDevCallback {
//                override fun onSuccess(info: DeviceUpgradeInfo) {
//                    //mUpgradeInfo = info
//                }
//                override fun onError(code: Int, error: String?) {
//                    //toastFailed()
//                }
//            })
//    }

//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    fun onEvent(event: EventStartDeviceUpgrade) {
//        MeariUser.getInstance().startDeviceUpgrade(event.upgradeUrl, event.newVersion,
//            object : IDeviceUpgradeCallback {
//                override fun onSuccess() {
//                    Log.d(TAG, "upgrade success")
//                    post(EventStartDeviceUpgradeDone(true,0, null))
//                }
//                override fun onFailed(errorCode: Int, errorMsg: String?) {
//                    Log.d(TAG, "upgrade failed, error code $errorCode, error message: $errorMsg")
//                    post(EventStartDeviceUpgradeDone(false, errorCode, errorMsg))
//                }
//            })
//    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetDeviceUpgradePercent) {
        MeariUser.getInstance().getDeviceUpgradePercent(object : IDeviceUpgradePercentCallback {
            override fun onSuccess(percent: Int) {
                //runOnUiThread(Runnable { showUpgradePercent(percent) })
            }
            override fun onFailed(errorCode: Int, errorMsg: String?) {
                //toastFailed()
            }
        })
    }

//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    fun onEvent(event: EventSetLedEnable) {
//        // 0-close; 1-open;
//        MeariUser.getInstance().setLED(event.enable, object : ISetDeviceParamsCallback {
//            override fun onSuccess() {
//                Log.d(TAG, "EventSetLedEnable: success")
//            }
//            override fun onFailed(errorCode: Int, errorMsg: String?) {
//                Log.d(TAG, "EventSetLedEnable: failure: $errorMsg")
//            }
//        })
//    }

    /**
     * Set sound and light alarm mode
     *
     * @param alarmType alarmType
     * @param callback Function callback
     */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSetFloodVoiceLightAlarmType) {
        MeariUser.getInstance().setFloodCameraVoiceLightAlarmType(event.alarmType,
            object : ISetDeviceParamsCallback {
            override fun onSuccess() {
                post(EventSetFloodVoiceLightAlarmTypeDone(true))
            }
            override fun onFailed(errorCode: Int, errorMsg: String?) {
                post(EventSetFloodVoiceLightAlarmTypeDone(false, errorCode, errorMsg))
            }
        })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSetFloodVoiceLightAlarmEnable) {
        MeariUser.getInstance().setFloodCameraVoiceLightAlarmEnable(event.enable,
            object : ISetDeviceParamsCallback {
                override fun onSuccess() {
                    post(EventSetFloodVoiceLightAlarmEnableDone(true))
                }
                override fun onFailed(errorCode: Int, errorMsg: String?) {
                    post(EventSetFloodVoiceLightAlarmEnableDone(false, errorCode, errorMsg))
                }
            })
    }

    /* Get alarm messages of all devices
    DeviceMessageStatus
    -long deviceID; device ID
    -String deviceName; device name
    -String snNum; device SN
    -String deviceIcon; device icon
    -boolean hasMessage; whether the device has an alarm message */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetAlertMessageStatusList) {
        MeariUser.getInstance().getDeviceMessageStatusList(object : IDeviceMessageStatusCallback {
            override fun onSuccess(deviceMessageStatusList: List<DeviceMessageStatus>) {
                post(EventGetAlertMessageStatusListDone(deviceMessageStatusList))
            }
            override fun onError(errorCode: Int, errorMsg: String?) {
                post(EventGetAlertMessageStatusListFailed(errorCode, errorMsg))
            }
        })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetDaysWithAlertMessages) {
        MeariUser.getInstance().getAlertMsgHas(event.deviceId, object:IBaseModelCallback<List<String>>
        {
            override fun onSuccess(days: List<String>) {
                post(EventGetDaysWithAlertMessagesDone(days, 0, ""))
            }

            override fun onFailed(errorCode: Int, errorMsg: String?) {
                post(EventGetDaysWithAlertMessagesDone(null, errorCode, errorMsg))
            }
        })
    }

//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    fun onEvent(event: EventPostPushToken) {
//        MeariUser.getInstance().postPushToken(1, event.pushToken, object : IResultCallback {
//            override fun onSuccess() {
//                post(EventPostPushTokenDone(true, null, null))
//            }
//            override fun onError(errorCode: Int, errorMsg: String?) {
//                post(EventPostPushTokenDone(false, errorCode, errorMsg))
//            }
//        })
//    }

    /**
     * Set the Alarm open or close
     *  deviceId device id
     *  status   0：open  1: close
     *  callback  */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventReceiveDeviceAlarmPush) {
        MeariUser.getInstance().closeDeviceAlarmPush(event.deviceId, event.status,
            object : IPushStatusCallback {
            override fun onSuccess(status: Int) {
                post(EventReceiveDeviceAlarmPushDone(true, event.status, null, null))
            }
            override fun onError(errorCode: Int, errorMsg: String?) {
                post(EventReceiveDeviceAlarmPushDone(false, null, errorCode, errorMsg))
            }
        })
    }

    /**
     * Light switch
     *
     * status  0-off; 1-on
     * callback
     */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSetFlightLightStatus) {
        MeariUser.getInstance().setFlightLightStatus(event.status,
            object : ISetDeviceParamsCallback {
                override fun onSuccess() {
                    post(EventSetLightStatusDone(true))
                }

                override fun onFailed(errorCode: Int, errorMsg: String) {
                    post(EventSetLightStatusDone(false, errorCode, errorMsg))
                }
            })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventStartVoiceTalk) {
        event.deviceController?.startVoiceTalk(event.talkType!!, object : MeariDeviceListener {
            override fun onSuccess(successMsg: String?) {
               post(EventStartVoiceTalkDone(true, successMsg))
            }
            override fun onFailed(errorMsg: String?) {
                post(EventStartVoiceTalkDone(false, errorMsg))
            }
        }, object : MeariDeviceTalkVolumeListener {
            override fun onTalkVolume(volume: Int) {
                // Intercom volume
                //TODO
                Log.d(TAG, "Intercom volume: $volume")
            }
            override fun onFailed(errorMsg: String?) {
                // No microphone found
                Log.d(TAG, "No microphone found")
                post(EventStartVoiceTalkDone(false, errorMsg))
            }
        })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventStopVoiceTalk) {
        event.deviceController?.stopVoiceTalk(object : MeariDeviceListener {
            override fun onSuccess(successMsg: String?) {
                Log.d(TAG, "stopVoiceTalk: $successMsg")
                post(EventStopVoiceTalkDone(true, successMsg))
            }

            override fun onFailed(errorMsg: String?) {
                Log.d(TAG, "stopVoiceTalk: $errorMsg")
                post(EventStopVoiceTalkDone(false, errorMsg))
            }
        })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventStartVideoRecording) {
        event.deviceController?.startRecordMP4(event.fullPath, object : MeariDeviceListener {
            override fun onSuccess(successMsg: String?) {
                post(EventStartVideoRecordingDone(true, successMsg))
            }
            override fun onFailed(errorMsg: String?) {
                post(EventStartVideoRecordingDone(false, errorMsg))
            }
        }) { i ->
            if (i > 0) {
                //toastSuccess()
            } else {
                //toastFailed()
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventStopVideoRecording) {
        event.deviceController?.stopRecordMP4(object : MeariDeviceListener {
            override fun onSuccess(successMsg: String?) {
                post(EventStopVideoRecordingDone(true, successMsg))
            }
            override fun onFailed(errorMsg: String?) {
                post(EventStopVideoRecordingDone(false, errorMsg))
            }
        })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSetSdRecordingDuration) {
        MeariUser.getInstance().setPlaybackRecordVideo(event.type, event.duration,
                object : ISetDeviceParamsCallback {
            override fun onSuccess() {
                post(EventSetSdRecordingDurationDone(true, event.duration, null, null))
            }
            override fun onFailed(errorCode: Int, errorMsg: String?) {
                post(EventSetSdRecordingDurationDone(false, event.duration, errorCode, errorMsg))
            }
        })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSetSdRecordingType) {
        MeariUser.getInstance().setPlaybackRecordVideo(event.recordingType, event.sdRecordDuration!!,
            object : ISetDeviceParamsCallback {
                override fun onSuccess() {
                    post(EventSetSdRecordingTypeDone(true, null, null))
                }
                override fun onFailed(errorCode: Int, errorMsg: String?) {
                    post(EventSetSdRecordingTypeDone(false, errorCode, errorMsg))
                }
            })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventFormatSdCard) {
        Log.d(TAG, "EventFormatSdCard")
        MeariUser.getInstance().startSDCardFormat(object : ISDCardFormatCallback {
            override fun onSuccess() {
                Log.d(TAG, "startSDCardFormat: success")
              post(EventFormatSdCardDone(true, null, null))
            }
            override fun onFailed(errorCode: Int, errorMsg: String) {
                Log.d(TAG, "startSDCardFormat failed: $errorMsg")
                post(EventFormatSdCardDone(false, errorCode, errorMsg))
            }
        })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetSdCardFormatPercent) {
        Log.d(TAG, "EventGetSdCardFormatPercent")
        MeariUser.getInstance().getSDCardFormatPercent(object : ISDCardFormatPercentCallback {
            override fun onSuccess(percent: Int) {
                post(EventGetSdCardFormatPercentDone(true, percent, null, null))
            }
            override fun onFailed(errorCode: Int, errorMsg: String?) {
                post(EventGetSdCardFormatPercentDone(false, null, errorCode, errorMsg))
            }
        })
    }

}

