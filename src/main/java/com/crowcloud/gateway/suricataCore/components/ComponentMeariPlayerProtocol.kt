package com.crowcloud.gateway.suricataCore.components

import android.util.Log
import com.crowcloud.gateway.suricataCore.events.*
import com.meari.sdk.callback.IPlaybackDaysCallback
import com.meari.sdk.listener.MeariDeviceListener
import com.meari.sdk.listener.MeariDeviceVideoSeekListener
import com.meari.sdk.listener.MeariDeviceVideoStopListener
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

class ComponentMeariPlayerProtocol : Component() {

    companion object {
        const val TAG: String = "ComponentMPlayerProtocol"
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().register(this)
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventStartPreview) {
        Log.d(TAG, "EventStartPreview")
        event.deviceController?.startPreview(event.videoSurfaceView, event.videoId,
            object : MeariDeviceListener {
                override fun onSuccess(successMsg: String?) {
                    post(EventStartPreviewDone(true, successMsg))
                }
                override fun onFailed(errorMsg: String?) {
                    Log.d(TAG, "startPreview failed: $errorMsg")
                    post(EventStartPreviewDone(false, errorMsg))
                }
            }, object : MeariDeviceVideoStopListener {
                override fun onVideoClosed(code:Int){
                    Log.d(TAG, "startPreview : Video closed")
                    post(EventVideoClosed())
                }
            })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventStopPreview) {
        event.deviceController?.stopPreview(object : MeariDeviceListener {
            override fun onSuccess(successMsg: String?) {
                Log.d(TAG, "Succeeded to stop preview")
                // Disconnect, you must disconnect when exiting preview and playback
                event.deviceController.stopConnect(object : MeariDeviceListener {
                    override fun onSuccess(successMsg: String?) {
                        Log.d(TAG, "Succeeded to disconnect")
                    }
                    override fun onFailed(errorMsg: String?) {
                        Log.d(TAG, "Failed to disconnect: $errorMsg")
                    }
                })
            }
            override fun onFailed(errorMsg: String?) {
                Log.d(TAG, "Failed to stop preview: $errorMsg")
            }
        })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSwitchResolution) {
        event.deviceController?.changeVideoResolution(event.videoSurfaceView, event.videoId,
            object : MeariDeviceListener {
                override fun onSuccess(successMsg: String?) {
                    Log.d(TAG, "Succeeded to switch resolution: $successMsg")
                    post(EventSwitchResolutionDone(true, event.videoId, null))
                }
                override fun onFailed(errorMsg: String?) {
                    Log.d(TAG, "Failed to switch resolution: $errorMsg")
                    post(EventSwitchResolutionDone(false, event.videoId, errorMsg))
                }
            }) {}
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSnapshot) {
        event.deviceController?.snapshot(event.fullPath, object : MeariDeviceListener {
            override fun onSuccess(msg: String?) {
                Log.d(TAG, "snapshot--preview--success: $msg")
                post(EventSnapshotDone(true, msg))
            }
            override fun onFailed(error: String?) {
                Log.d(TAG, "snapshot--preview--failed: $error")
                post(EventSnapshotDone(false, error))
            }
        })
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetPlaybackVideoDaysInMonth) {
        Log.d(TAG, "EventGetPlaybackVideoDaysInMonth")
        event.deviceController?.getPlaybackVideoDaysInMonth(event.year, event.month, event.videoId,
            object : IPlaybackDaysCallback {
                override fun onSuccess(playbackDays: ArrayList<Int>) {
                    post(EventGetPlaybackVideoDaysInMonthDone(
                        event.year, event.month, playbackDays, true, null))
                }

                override fun onFailed(errorMsg: String?) {
                    post(EventGetPlaybackVideoDaysInMonthDone(
                        event.year, event.month, null, false, errorMsg))
                }
            })
    }

    /*
     * Get all video clips of the day
     * @param year
     * @param month
     * @param videoId video definition
     * @param callback callback
     * successMsg means the json array of startTime and endTime
        for example:
        [“000000-110330”,“135157-163706"]
        hhmmss(start time)-hhmmss(end time)
     */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventGetPlaybackVideoTimesInDay) {
        event.deviceController?.getPlaybackVideoTimesInDay(
            event.year, event.month, event.day, event.videoId, object : MeariDeviceListener {
                override fun onSuccess(msg: String) {
                    Log.d(TAG, "GetPlaybackVideoTimesInDay--success: $msg")
                    post(EventGetPlaybackVideoTimesInDayDone(msg, true))
                }
                override fun onFailed(error: String) {
                    Log.d(TAG, "GetPlaybackVideoTimesInDay--failed: $error")
                    post(EventGetPlaybackVideoTimesInDayDone(error, false))
                }
            })
    }


 /* Start playing video
 *
 * @param ppsGLSurfaceView video control
 * @param videoId video resolution 0-HD; 1-SD
 * @param startTime video start time
 * @param deviceListener operation listen
 * @param videoStopListener
 */
//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    fun onEvent(event: EventStartPlaybackSDCard) {
//        event.deviceController?.startPlaybackSDCard(event.ppsGLSurfaceView, event.videoId,
//            event.startTime, object : MeariDeviceListener {
//                override fun onSuccess(msg: String) {
//                    Log.d(TAG, "SD card playback--video--success: $msg")
//                }
//
//                override fun onFailed(error: String) {
//                    Log.d(TAG, "SD card playback--video--failed: $error")
//                }
//            }, object : MeariDeviceVideoStopListener {
//            override fun onVideoClosed(code:Int){
//                Log.d(TAG, "SD card playback : Video closed")
//            }
//        })
//    }

    /**
    * Drag to change playback video
    * @param seekTime the time the video started
    * @param deviceListener operation listen
    * @param videoStopListener
    */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventSeekPlaybackSDCard) {
        event.deviceController?.seekPlaybackSDCard(event.seekTime, object : MeariDeviceListener {
            override fun onSuccess(msg: String) {
                Log.d(TAG, "SD card playback--video--success: $msg")
            }

            override fun onFailed(error: String) {
                Log.d(TAG, "SD card playback--video--failed: $error")
            }
        }, object : MeariDeviceVideoSeekListener {
            override fun onVideoSeek(){
                Log.d(TAG, "SD card playback : Video seek")
            }
        })
    }

    /**
    * Pause video
    * @param deviceListener operation listen
    */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventPausePlaybackSDCard) {
        event.deviceController?.pausePlaybackSDCard(object : MeariDeviceListener {
            override fun onSuccess(successMsg: String) {
                Log.d(TAG, "SD card playback--video--success: $successMsg")
            }

            override fun onFailed(errorMsg: String) {
                Log.d(TAG, "SD card playback--video--failed: $errorMsg")
            }
        })
    }

    /**
    * Replay video after pause
    *
    * @param deviceListener operation listen
    */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventResumePlaybackSDCard) {
        event.deviceController?.resumePlaybackSDCard(object : MeariDeviceListener {
            override fun onSuccess(successMsg: String) {
                Log.d(TAG, "SD card playback--video--success: $successMsg")
            }

            override fun onFailed(errorMsg: String) {
                Log.d(TAG, "SD card playback--video--failed: $errorMsg")
            }
        })
    }

    /**
    * Stop playing video
    *
    * @param deviceListener operation listen
    */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventStopPlaybackSDCard) {
        event.deviceController?.stopPlaybackSDCard(object : MeariDeviceListener {
            override fun onSuccess(successMsg: String) {
                Log.d(TAG, "SD card playback--video--success: $successMsg")
            }

            override fun onFailed(errorMsg: String) {
                Log.d(TAG, "SD card playback--video--failed: $errorMsg")
            }
        })
    }
}