package com.crowcloud.gateway.suricataCore.components

import com.crowcloud.gateway.suricataCore.auth.LoginManager
import com.crowcloud.gateway.suricataCore.events.EventChangePassword
import com.crowcloud.gateway.suricataCore.events.EventFingerPrintScanDone
import com.crowcloud.gateway.suricataCore.events.EventRecoverPassword
import com.crowcloud.gateway.suricataCore.events.EventRequestData
import com.crowcloud.gateway.suricataCore.events.EventTryAutoLogin
import com.crowcloud.gateway.suricataCore.events.EventTryLogin
import com.crowcloud.gateway.suricataCore.events.EventTrySignUp
import com.crowcloud.gateway.suricataCore.network.services.AuthApi
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class ComponentLogin(
    private val authApi: AuthApi,
    private val loginManager: LoginManager,
) : Component() {

    companion object {
        const val TAG: String = "ComponentLogin"
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().register(this)
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventRequestData) {
//        when (event.type) {
//            DataType.LoginCreds -> if (loginManager.hasAccount()) {
//                postSticky(EventReceivedData(DataType.LoginCreds,
//                        LoginManager.LoginCreds(loginManager.account().name, prefsStorage.getPassword())))
//            }
//            else -> {}
//        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEvent(event: EventChangePassword) {
//        authApi.changePassword(ChangePasswordRequest(event.oldPassword,
//            event.newPassword, event.newPassword))
//                .promise().done {
//                    if(it == null){
//                        post(EventChangePasswordDone(false, error = it))
//                    }else {
//                        post(EventChangePasswordDone(true, it))
//                    }
//                }.fail {
//                    post(EventChangePasswordDone(false, error = it))
//                }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventTryLogin) {
       // prefsStorage.saveSelectedPanelId(-1) // Clear saved data
        if (event.rememberPassword) {
           // prefsStorage.savePassword(event.password)
        }

     //   loginManager.login(event.userName, event.password)
//                .done {
//                    if (event.loadSavedPanel) {
//                        post(EventLoadSavedPanel())
//                    }
//                    post(EventLoginDone(true, intent = it))
//                }
//                .fail {
//                    post(EventLoginDone(false, error = it))
//                }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventTrySignUp) {
//        loginManager.signUp(event.email, event.pass, event.passRetype, event.fName, event.lName)
//                .done { post(EventSignUpDone(true)) }
//                .fail { post(EventSignUpDone(false, it)) }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onRequestPasswordRecovery(event: EventRecoverPassword) {
//        authApi.resetPassword(ResetPasswordRequest(event.email)).promise()
//                .done { post(EventRecoverPassDone(true)) }
//                .fail { post(EventRecoverPassDone(false, it)) }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onFingerPrintComplete(event: EventFingerPrintScanDone) {
        if (event.isSuccess) {
          //  tryAutoLogin()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventTryAutoLogin) {
       // tryAutoLogin()
    }

//    private fun tryAutoLogin() {
//        loginManager.refreshTokenIfNeededAsync()
//                .done {
//                    post(EventTryAutoLoginDone(true))
//                    //TODO post(EventLoadSavedPanel())
//                }
//                .fail { post(EventTryAutoLoginDone(false)) }
//    }

}