package com.crowcloud.gateway.suricataCore.components

import android.os.SystemClock
import android.util.Log
import org.apache.commons.net.telnet.TelnetClient
import java.io.InputStream
import java.io.OutputStream
import java.net.InetAddress
import kotlin.collections.HashMap

class ComponentIotDeviceProtocol : Component() {

    companion object {
        const val TAG: String = "ComponentIotDeviceProto"
        const val URL = "http://192.168.123.1:23"
        val IP_ADDRESS = byteArrayOf(192.toByte(), 168.toByte(), 123, 1)
        const val PORT = 23
        const val TIMEOUT = 10000
        const val WAIT_INTERVAL_MS = 0L
    }

//    @SuppressLint("NewApi")
//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    fun onEvent(event: EventRequestIotInfo) = syncOnWifiOnly {
//        telnetRequest(IP_ADDRESS, PORT, TIMEOUT) { tc, t ->
//            if (tc != null) {
//                Log.i(TAG, "writing to socket: info\n\r")
//                val map = writeCommandForResult(tc, "info")
//                post(EventRequestIotInfoDone(map))
//                Arrays.sort("hi".toCharArray())
//            }
//        }
//    }

    private fun telnetRequest(ip: ByteArray, port: Int, timeoutMs: Int, callback: (tc: TelnetClient?, t: Throwable?) -> Unit) {
        val tc = TelnetClient()
        tc.connectTimeout = timeoutMs
        try {
            tc.connect(InetAddress.getByAddress(ip), port)
            callback(tc, null)
        } catch (t: Throwable) {
            Log.e(TAG, "Telnet error ", t)
            callback(null, t)
        } finally {
            tc.disconnect()
        }
    }

    private fun writeCommandForResult(tc: TelnetClient, command: String): HashMap<String, String> {
        writeCommand(tc.outputStream, command)
        SystemClock.sleep(WAIT_INTERVAL_MS)
        return readTextAndParse(tc.inputStream)
    }

    private fun readTextAndParse(tc: TelnetClient) = readTextAndParse(tc.inputStream)

    private fun readTextAndParse(inputStream: InputStream): HashMap<String, String> {
        val res = readText(inputStream)
        Log.i(TAG, "received data : $res")
        val map: HashMap<String, String> = parseIotResponse(res)
        return map
    }

    private fun writeCommand(tc: TelnetClient, command: String) = writeCommand(tc.outputStream, command)

    private fun writeCommand(outputStream: OutputStream, command: String) {
        outputStream.write("$command\r\n".toByteArray())
        outputStream.flush()
    }

    private fun readText(inputStream: InputStream): String {
        val sb = StringBuilder()
        var res: String
        do {
            val bytes = ByteArray(64)
            inputStream.read(bytes)
            res = String(bytes)
            sb.append(res)
            Log.i(TAG, "received raw data : $res")
        } while (!res.contains("OK"))


        Log.i(TAG, "received raw collected data : $sb")

        res = sb.toString().filter { it.isLetterOrDigit() || it.isWhitespace() || it == '_' || it == '-' || it == '\n' || it == '\r' || it == '.' }

        Log.i(TAG, "raw collected data : $res")

        return res
    }

    private fun parseIotResponse(res: String?): HashMap<String, String> {
        val map = HashMap<String, String>()

        res?.replace("\r", "")?.split("\n")?.forEach {
            val words = it.split(" ")
            if (words.size == 1) {
                map.put(words[0], "")
            } else if (words.size == 2) {
                map.put(words[0], words[1])
            }
        }

        return map
    }

//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    fun onEvent(event: EventRequestIotDeviceBind) = syncOnWifiOnly {
//        telnetRequest(IP_ADDRESS, PORT, TIMEOUT) { tc, t ->
//            if (tc != null) {
//                writeCommand(tc, "name ${event.name}")
//                writeCommand(tc, "wifi.ssid ${event.wifiSsid}")
//                writeCommand(tc, "wifi.password ${event.wifiPassword}")
//                writeCommand(tc, "settings.uid ${event.settingUid}")
//
//                SystemClock.sleep(WAIT_INTERVAL_MS)
//                val map = readTextAndParse(tc)
//                SystemClock.sleep(WAIT_INTERVAL_MS)
//                try {
//                    writeCommand(tc, "save")
//                } catch (t: Throwable) {
//                    Log.e(TAG, "Bind error ", t)
//                }
//                SystemClock.sleep(250L)
//                post(EventRequestIotDeviceBindDone(map))
//            }
//        }
//    }
}