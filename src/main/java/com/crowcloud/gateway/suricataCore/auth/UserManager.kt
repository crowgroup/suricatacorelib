package com.crowcloud.gateway.suricataCore.auth

import android.content.Context
import com.crowcloud.gateway.suricataCore.network.model.response.User
import com.crowcloud.gateway.suricataCore.persistence.SecuredPrefs

class UserManager private constructor(context: Context) {
    private val securedPrefs = SecuredPrefs.getInstance(context)

    companion object {
        @Volatile
        private var instance: UserManager? = null

        fun initialize(context: Context) {
            if (instance == null) {
                synchronized(this) {
                    if (instance == null) {
                        instance = UserManager(context)
                    }
                }
            }
        }

        fun getInstance(): UserManager {
            return instance ?: throw IllegalStateException("UserManager is not initialized")
        }

        private const val KEY_USER_PROFILE = "user_profile"
    }

    var currentUser: User?
        get() = securedPrefs.getObject(KEY_USER_PROFILE, User::class.java)
        set(value) = value?.let { securedPrefs.saveObject(KEY_USER_PROFILE, it) }
            ?: securedPrefs.removeKeys(listOf(KEY_USER_PROFILE))

    fun clearUserData() {
        securedPrefs.clearAll()
    }
}
