package com.crowcloud.gateway.suricataCore.auth


import android.util.Log
import com.crowcloud.gateway.suricataCore.network.NetworkManager
import com.crowcloud.gateway.suricataCore.network.services.AuthApi
import com.crowcloud.gateway.suricataCore.util.AccessTokenProvider

class LoginManager private constructor() : AccessTokenProvider {

    private var isRefreshing = false
    private var authApi: AuthApi? = null
    private var config: LoginManagerConfig? = null
    private val sessionManager = SessionManager.getInstance()

    companion object {
        private const val TAG = "LoginManager"

        @Volatile private var instance: LoginManager? = null
        fun getInstance() = instance ?: synchronized(this) {
            instance ?: LoginManager().also { instance = it }
        }

        fun initialize(config: LoginManagerConfig) {
            getInstance().apply {
                this.authApi = NetworkManager.getInstance().authApi
                this.config = config
                sessionManager
            }
        }
    }

    override fun getCurrentAccessToken(): String? {
        return sessionManager.accessToken
    }

    override fun hasValidAccessToken(): Boolean {
        return !sessionManager.isAccessTokenExpired()
    }

    override fun hasRefreshToken(): Boolean {
        val refreshTokenExists = !sessionManager.refreshToken.isNullOrEmpty()
        Log.d(TAG, "hasRefreshToken: $refreshTokenExists")
        return refreshTokenExists
    }

    override suspend fun refreshTokenIfNeeded(): Boolean {
        Log.d(TAG, "refreshTokenIfNeeded called")

        if (isRefreshing) {
            Log.d(TAG, "Refresh already in progress, skipping")
            return true
        }

        if (hasValidAccessToken()) {
            Log.d(TAG, "Access token is valid, skipping refresh")
            return true // No refresh needed
        }

        if (!hasRefreshToken()) {
            Log.e(TAG, "No refresh token available. Cannot refresh access token.")
            return false
        }

        isRefreshing = true
        try {
            Log.d(TAG, "Calling refreshToken")
            return refreshToken()
        } finally {
            isRefreshing = false
            Log.d(TAG, "Refresh completed, isRefreshing reset")
        }
    }

    private suspend fun refreshToken(): Boolean {
        Log.d(TAG, "Refreshing token started")
        val refreshToken = sessionManager.refreshToken
        if (refreshToken.isNullOrEmpty()) {
            Log.e(TAG, "No refresh token available, returning false")
            return false
        }

        return try {
            Log.d(TAG, "Preparing refresh token request")
            val response = config?.clientId?.let { clientId ->
                config?.clientSecret?.let { clientSecret ->
                    Log.d(TAG, "Sending refresh token request with token: $refreshToken")
                    NetworkManager.getInstance().authApi.refreshToken(
                        grantType = "refresh_token",
                        refreshToken = refreshToken,
                        clientId = clientId,
                        clientSecret = clientSecret
                    )
                }
            }

            if (response?.isSuccessful == true) {
                Log.d(TAG, "Token refresh successful")
                response.body()?.let {
                    Log.d(TAG, "Saving refreshed token")
                    sessionManager.saveLoginData(
                        it.accessToken, it.refreshToken, it.expiresIn, sessionManager.email ?: ""
                    )
                }
                true
            } else {
                Log.e(TAG, "Token refresh failed: ${response?.errorBody()?.string()}")
                clearSession()
                false
            }
        } catch (e: Exception) {
            Log.e(TAG, "Exception during token refresh", e)
            false
        }
    }

    override fun clearSession() {
        Log.d(TAG, "Clearing session")
        sessionManager.clearSession()
    }
}



