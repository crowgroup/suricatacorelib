package com.crowcloud.gateway.suricataCore.auth

data class LoginManagerConfig(val clientId: String, val clientSecret: String, val accountType: String)
