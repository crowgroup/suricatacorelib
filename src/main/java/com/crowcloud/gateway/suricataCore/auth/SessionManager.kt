package com.crowcloud.gateway.suricataCore.auth

import android.content.Context
import android.util.Log
import com.crowcloud.gateway.suricataCore.persistence.SecuredPrefs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class SessionManager private constructor(context: Context) {

    private val securedPrefs = SecuredPrefs.getInstance(context)

    companion object {
        @Volatile
        private var instance: SessionManager? = null

        // This should only be called in the App class, during app startup
        fun initialize(context: Context) {
            if (instance == null) {
                synchronized(this) {
                    if (instance == null) {
                        instance = SessionManager(context)
                    }
                }
            }
        }

        fun getInstance(): SessionManager {
            return instance ?: throw IllegalStateException("SessionManager is not initialized")
        }

        private const val KEY_ACCESS_TOKEN = "access_token"
        private const val KEY_REFRESH_TOKEN = "refresh_token"
        private const val KEY_EXPIRATION_TIME = "expires_in"
        private const val KEY_EMAIL = "email"
        private const val KEY_REMEMBER_ME = "remember_me"
        private const val TOKEN_REFRESH_THRESHOLD = 60 * 60 * 1000 // 1 hour in milliseconds
    }

    var accessToken: String?
        get() = runBlocking { getSecureString(KEY_ACCESS_TOKEN) }
        set(value) = runBlocking { saveSecureString(KEY_ACCESS_TOKEN, value) }

    var refreshToken: String?
        get() = runBlocking { getSecureString(KEY_REFRESH_TOKEN) }
        set(value) = runBlocking { saveSecureString(KEY_REFRESH_TOKEN, value) }

    private suspend fun getSecureString(key: String): String? {
        return withContext(Dispatchers.IO) {
            securedPrefs.getString(key, null) // Perform on background thread
        }
    }

    private suspend fun saveSecureString(key: String, value: String?) {
        withContext(Dispatchers.IO) {
            value?.let { securedPrefs.saveString(key, it) }
                ?: securedPrefs.removeKeys(listOf(key))
        }
    }

    private var expirationTime: Long
        get() = securedPrefs.getLong(KEY_EXPIRATION_TIME, 0L)
        set(value) = securedPrefs.saveLong(KEY_EXPIRATION_TIME, value)

    var email: String?
        get() = securedPrefs.getString(KEY_EMAIL, null)
        set(value) = value?.let { securedPrefs.saveString(KEY_EMAIL, it) }
            ?: securedPrefs.removeKeys(listOf(KEY_EMAIL))

    var rememberMe: Boolean
        get() = securedPrefs.getBoolean(KEY_REMEMBER_ME, true)
        set(value) = securedPrefs.saveBoolean(KEY_REMEMBER_ME, value)

    fun isAccessTokenExpired(): Boolean {
        val currentTime = System.currentTimeMillis()
        val timeLeft = expirationTime - currentTime

        Log.d("SessionManager", "isAccessTokenExpired called")
        Log.d("SessionManager", "Current Time: $currentTime")
        Log.d("SessionManager", "Access Token Expiration Time: $expirationTime")
        Log.d("SessionManager", "Time Left: $timeLeft ms")

        val isExpired = timeLeft <= TOKEN_REFRESH_THRESHOLD
        Log.d("SessionManager", "Access Token Expired: $isExpired")

        return isExpired
    }


    fun saveLoginData(
        accessToken: String?,
        refreshToken: String?,
        expiresIn: Int,
        email: String
    ) {
        this.accessToken = accessToken
        this.refreshToken = refreshToken
        this.expirationTime = System.currentTimeMillis() + expiresIn * 1000 // Access token expiration
        this.email = email
    }

    fun clearSession() {
        // Retain the last saved email
        val savedEmail = email

        // Clear all preferences
        securedPrefs.clearAll()

        // Restore the saved email
        if (savedEmail != null) {
            email = savedEmail
        }
    }
}

