package com.crowcloud.gateway.suricataCore.persistence.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.crowcloud.gateway.suricataCore.util.AlertType
import com.crowcloud.gateway.suricataCore.util.ImgAlertType


@Entity(tableName = "alert_table", indices = [Index(value = ["deviceID", "createDate"])])
data class AlertDB(@PrimaryKey @ColumnInfo(name = "alertID") val alertID: Long,
                   @ColumnInfo(name = "deviceID") val deviceID: Long,
                   @ColumnInfo(name = "deviceUuid") val deviceUID: String,
                   @ColumnInfo(name = "alertType") val alertType: AlertType,   // message type (sound / motion detection)
                   @ColumnInfo(name = "imageAlertType") val imgAlertType: ImgAlertType, // Alarm type (PIR and Motion)
                   @ColumnInfo(name = "createDate") val createDate: String,
                   @ColumnInfo(name = "isRead") val isRead: Boolean,
                   @ColumnInfo(name = "userID") val userID: Long,
                   @ColumnInfo(name = "userIDS") val userIDs: Long,
                   @ColumnInfo(name = "tumbnailPic") val thumbnailUrl: String,
                   @ColumnInfo(name = "decibel") val decibel: String)


//class DeviceAlarmMessage:
//    -long deviceID; // device ID
//-String deviceUuid; // device unique identifier (deviceID and deviceUuid are the same)
//-String imgUrl; // Alarm picture address
//-int imageAlertType; // Alarm type (PIR and Motion)
//-int msgTypeID; // message type (sound / motion detection)
//-long userID; // User Id
//-long userIDS;// if it's a shared device, the value is 0, or it will be user id
//-String createDate; // wear time
//-String isRead; // whether read
//-String tumbnailPic; // Thumbnails
//-String decibel; // dB
//-long msgID; // Message Id

