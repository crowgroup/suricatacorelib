package com.crowcloud.gateway.suricataCore.persistence

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.google.gson.Gson
import java.io.IOException
import java.security.GeneralSecurityException

class SecuredPrefs private constructor(context: Context) {

    private val sharedPreferences: SharedPreferences
    private val gson = Gson()

    companion object {
        private const val TAG = "SecuredPrefs"
        private const val ENCRYPTED_PREFS_NAME = "encrypted_prefs"
        private const val FALLBACK_PREFS_NAME = "general_params"

        @Volatile
        private var instance: SecuredPrefs? = null

        fun getInstance(context: Context): SecuredPrefs =
            instance ?: synchronized(this) {
                instance ?: SecuredPrefs(context).also { instance = it }
            }
    }

    init {
        val masterKeyAlias = MasterKey.Builder(context)
            .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
            .build()

        sharedPreferences = try {
            EncryptedSharedPreferences.create(
                context,
                ENCRYPTED_PREFS_NAME,
                masterKeyAlias,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            )
        } catch (e: GeneralSecurityException) {
            Log.e(TAG, "Error creating EncryptedSharedPreferences", e)
            context.getSharedPreferences(FALLBACK_PREFS_NAME, Context.MODE_PRIVATE)
        } catch (e: IOException) {
            Log.e(TAG, "IO Error when creating EncryptedSharedPreferences", e)
            context.getSharedPreferences(FALLBACK_PREFS_NAME, Context.MODE_PRIVATE)
        }
    }

    fun saveString(key: String, value: String) {
        sharedPreferences.edit().putString(key, value).apply()
    }

    fun getString(key: String, defaultValue: String? = null): String? {
        return sharedPreferences.getString(key, defaultValue)
    }

    fun saveInt(key: String, value: Int) {
        sharedPreferences.edit().putInt(key, value).apply()
    }

    fun getInt(key: String, defaultValue: Int = 0): Int {
        return sharedPreferences.getInt(key, defaultValue)
    }

    fun saveBoolean(key: String, value: Boolean) {
        sharedPreferences.edit().putBoolean(key, value).apply()
    }

    fun getBoolean(key: String, defaultValue: Boolean = false): Boolean {
        return sharedPreferences.getBoolean(key, defaultValue)
    }

    fun saveFloat(key: String, value: Float) {
        sharedPreferences.edit().putFloat(key, value).apply()
    }

    fun getFloat(key: String, defaultValue: Float = 0.0f): Float {
        return sharedPreferences.getFloat(key, defaultValue)
    }

    fun saveLong(key: String, value: Long) {
        sharedPreferences.edit().putLong(key, value).apply()
    }

    fun getLong(key: String, defaultValue: Long = 0L): Long {
        return sharedPreferences.getLong(key, defaultValue)
    }

    fun <T> saveObject(key: String, obj: T) {
        sharedPreferences.edit().putString(key, gson.toJson(obj)).apply()
    }

    fun <T> getObject(key: String, clazz: Class<T>): T? {
        val json = sharedPreferences.getString(key, null) ?: return null
        return gson.fromJson(json, clazz)
    }

    fun removeKeys(keys: List<String>) {
        val editor = sharedPreferences.edit()
        keys.forEach { key ->
            editor.remove(key)
        }
        editor.apply()
    }

    fun clearAll() {
        sharedPreferences.edit().clear().apply()
    }
}
