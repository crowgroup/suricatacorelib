package com.crowcloud.gateway.suricataCore.persistence

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.crowcloud.gateway.suricataCore.events.DataType
import com.crowcloud.gateway.suricataCore.network.model.response.AccessRequest
import com.crowcloud.gateway.suricataCore.network.model.response.Area
import com.crowcloud.gateway.suricataCore.network.model.response.ArmData
import com.crowcloud.gateway.suricataCore.network.model.response.BaseDevice
import com.crowcloud.gateway.suricataCore.network.model.response.ControlPanel
import com.crowcloud.gateway.suricataCore.network.model.response.DoorLock
import com.crowcloud.gateway.suricataCore.network.model.response.EthernetInfo
import com.crowcloud.gateway.suricataCore.network.model.response.Event
import com.crowcloud.gateway.suricataCore.network.model.response.GsmInfo
import com.crowcloud.gateway.suricataCore.network.model.response.IpCamera
import com.crowcloud.gateway.suricataCore.network.model.response.IpDeviceModule
import com.crowcloud.gateway.suricataCore.network.model.response.LockInAssa
import com.crowcloud.gateway.suricataCore.network.model.response.MeariDeviceUiModel
import com.crowcloud.gateway.suricataCore.network.model.response.Measurement
import com.crowcloud.gateway.suricataCore.network.model.response.MeasuresByZone
import com.crowcloud.gateway.suricataCore.network.model.response.NotificationsEmailData
import com.crowcloud.gateway.suricataCore.network.model.response.Output
import com.crowcloud.gateway.suricataCore.network.model.response.Panel
import com.crowcloud.gateway.suricataCore.network.model.response.PanelConnectionState
import com.crowcloud.gateway.suricataCore.network.model.response.PanelInfo
import com.crowcloud.gateway.suricataCore.network.model.response.PanelUser
import com.crowcloud.gateway.suricataCore.network.model.response.Picture
import com.crowcloud.gateway.suricataCore.network.model.response.RadioInfo
import com.crowcloud.gateway.suricataCore.network.model.response.StreamData
import com.crowcloud.gateway.suricataCore.network.model.response.Troubles
import com.crowcloud.gateway.suricataCore.network.model.response.User
import com.crowcloud.gateway.suricataCore.network.model.response.UserAccount
import com.crowcloud.gateway.suricataCore.network.model.response.VideoEvent
import com.crowcloud.gateway.suricataCore.network.model.response.WifiInfo
import com.crowcloud.gateway.suricataCore.network.model.response.Zone
import com.crowcloud.gateway.suricataCore.util.toPanel
import com.crowcloud.gateway.suricataCore.util.toPanelInfo
import com.meari.sdk.bean.CameraInfo
import java.util.Date

class DataRepo private constructor() {

    companion object {
        const val TAG = "DataRepo"
        val instance: DataRepo by lazy(LazyThreadSafetyMode.SYNCHRONIZED) { DataRepo() }
    }

    var apiVersion: Int = 1
        private set
    fun updateApiVersion(version: Int) {
        apiVersion = version
    }
    var role: Int = 0
    private val _roleLiveData = MutableLiveData<Int>(0)
    val roleLiveData: LiveData<Int> get() = _roleLiveData
    fun updateRole(newRole: Int) {
        role = newRole
        _roleLiveData.postValue(newRole)
    }

    var needLogoutMeariCloud: Boolean = false

    val panels: ArrayList<Panel> = ArrayList()
    var currentPanelInfo: PanelInfo? = null
        private set
    fun updateCurrentPanelInfo(panelInfo: PanelInfo) {
        currentPanelInfo = panelInfo
    }

    var currentPanelName: String? = null
    fun updateCurrentPanelName(name: String?) {
        currentPanelName = name
    }

    var currentControlPanel: ControlPanel? = null
        private set
    fun updateCurrentControlPanel(controlPanel: ControlPanel) {
        currentControlPanel = controlPanel
    }

    var currentPanelId: Int? = null
        private set
    fun updateCurrentPanelId(panelId: Int) {
        currentPanelId = panelId
    }

    var currentPanelVersion: String? = currentControlPanel?.version
        private set
    fun updateCurrentPanelVersion(version: String?) {
        currentPanelVersion = version
    }

    var currentMacAddress: String? = currentControlPanel?.mac
        private set
    fun updateCurrentMacAddress(mac: String?) {
        currentMacAddress = mac
    }

    var currentPanelState: Panel.PanelState = Panel.PanelState.Offline
    var currentUser: User? = null

    var armDataMap: HashMap<Int, ArmData> = HashMap()

    var muteSpeakerMap: HashMap<String, Boolean> = HashMap()

    // Update the Panel list
    fun updatePanelList(panelInfos: List<PanelInfo>) {
        panels.clear()
        panels.addAll(panelInfos.map { it.toPanel() })
    }

    // Retrieve PanelInfo list
    fun getPanelInfoList(): List<PanelInfo> {
        return panels.map { it.toPanelInfo() }
    }

    // Update the current PanelInfo and ControlPanel
    fun updateCurrentPanelInfo(panel: Panel) {
        currentPanelInfo = panel.toPanelInfo()
        currentControlPanel = panel.controlPanel
    }

    fun savePanels(panelInfos: List<PanelInfo>) {
        Log.d(TAG, "Saving ${panelInfos.size} panels in DataRepo")
        panelInfos.forEach { panelInfo ->
            val panelId = panelInfo.id ?: return@forEach
            panelDataMap[panelId] = PanelData().apply {
                updateFromPanelInfo(panelInfo)
            }
        }
    }

    fun getPanelInfoById(panelId: Int): PanelInfo? {
        return panelDataMap[panelId]?.toPanelInfo()
    }

    fun getAllPanels(): List<PanelInfo> {
        return panelDataMap.values.mapNotNull { it.toPanelInfo() }
    }

    fun resetData() {
        needLogoutMeariCloud = false
        panels.clear()
        currentPanelInfo = null
        currentControlPanel = null
        currentPanelId = null
        currentPanelState = Panel.PanelState.Offline
        currentUser = null
        armDataMap.clear()
        muteSpeakerMap.clear()

        // Reset all PanelData stored in the map
        panelDataMap.values.forEach { it.clearCache() }
        panelDataMap.clear()
    }

    class PanelData {
        var areasMap: HashMap<Int, Area> = HashMap()
        var areas: ArrayList<Area> = ArrayList()
        var areaUiMap: HashMap<Int, AreaUI> = HashMap()
        val events: ArrayList<Event> = ArrayList()
        var pictures: ArrayList<Picture> = ArrayList()
        var latestPicturesMap: HashMap<String, Picture> = HashMap()
        val users: ArrayList<UserAccount> = ArrayList()
        val ipCameras: ArrayList<IpCamera> = ArrayList()
        var ipMeariCameras: ArrayList<CameraInfo> = ArrayList()
        var doorbells: ArrayList<MeariDeviceUiModel> = ArrayList()
        var ipMeariDeviceUiModels: ArrayList<IpDeviceModule> = ArrayList()
        var devices: ArrayList<DeviceMultiItem> = ArrayList()
        var outputsUI: ArrayList<DeviceMultiItem> = ArrayList()
        var rgbLights: ArrayList<DeviceMultiItem> = ArrayList()
        var doorLocksUI: ArrayList<DeviceMultiItem> = ArrayList()
        var temperatureLights: ArrayList<DeviceMultiItem> = ArrayList()
        var dimmableLights: ArrayList<DeviceMultiItem> = ArrayList()
        var zonesUI: ArrayList<DeviceMultiItem> = ArrayList()
        var pirCamerasUI: ArrayList<DeviceMultiItem> = ArrayList()
        var keypadsUI: ArrayList<DeviceMultiItem> = ArrayList()
        var locks: ArrayList<DeviceMultiItem> = ArrayList()
        var doorLocks: ArrayList<DoorLock> = ArrayList()
        var locksInAssa: ArrayList<LockInAssa> = ArrayList()
        var isLoggedInUserRegisteredInAssa: Boolean = false
        var lockAccessRequests: ArrayList<AccessRequest> = ArrayList()
        var zones: ArrayList<Zone>? = null
        var outputs: ArrayList<Output>? = null
        var troubles: Troubles? = null
        var keypads: List<BaseDevice>? = null
        var connectionState: PanelConnectionState? = null
        var radioInfo: RadioInfo? = null
        var ethernetInfo: EthernetInfo? = null
        var wifiInfo: WifiInfo? = null
        var gsmInfo: GsmInfo? = null
        var internalUsers: List<PanelUser>? = null
        var maintenanceUntil: Date? = null
        var measurementsMap: HashMap<String, MeasuresByZone> = HashMap()
        var measureStatsMap: HashMap<String, List<Measurement>> = HashMap()
        val streamDataMap: HashMap<String, StreamData> = HashMap()//device_uid to StreamData
        val videoEventsMap: HashMap<String, ArrayList<VideoEvent>> = HashMap()//device_id to VideoEvent list map
        val notificationsEmails: ArrayList<NotificationsEmailData> = ArrayList()

        fun clearCache() {
            areasMap.clear()
            events.clear()
            pictures.clear()
            latestPicturesMap.clear()
            users.clear()
            measureStatsMap.clear()
            ipCameras.clear()
            ipMeariCameras.clear()
            streamDataMap.clear()
            videoEventsMap.clear()
            notificationsEmails.clear()
            devices.clear()
            doorLocks.clear()
            locksInAssa.clear()
            isLoggedInUserRegisteredInAssa = false
            lockAccessRequests.clear()
            zones = null
            troubles = null
            outputs = null
            keypads = null
            radioInfo = null
            internalUsers = null
            maintenanceUntil = null
            measurementsMap.clear()
        }

        fun get(type: DataType): Any? = when (type) {
            DataType.LoginCreds -> null
            DataType.Panels -> instance.panels
            DataType.PanelConnectionState -> null
            DataType.Areas -> ArrayList<Area>().apply { addAll(areasMap.values) }
            DataType.Troubles -> troubles
            DataType.Measurements -> measurementsMap
            DataType.CurrentPanel -> instance.currentControlPanel
            DataType.Devices -> devices
            DataType.Locks -> doorLocks
            DataType.LocksInAssa -> locksInAssa
            DataType.AccessRequests -> lockAccessRequests
            DataType.Zones -> zones
            DataType.Area -> null
            DataType.Events -> events
            DataType.Pictures -> pictures
            DataType.LatestPictures -> latestPicturesMap
            DataType.Outputs -> outputs
            DataType.Keypads -> keypads
            DataType.AllTroubles -> null
            DataType.RadioInfo -> radioInfo
            DataType.EthernetInfo -> ethernetInfo
            DataType.GsmInfo -> gsmInfo
            DataType.WifiInfo -> wifiInfo
            DataType.Users -> users
            DataType.UsersOfPanel -> internalUsers
            DataType.CurrentUser -> instance.currentUser
            DataType.CurrentUserOfPanel -> null
            DataType.NotificationFilters -> null
            DataType.NotificationsEmails -> null
            DataType.MaintenanceState -> null
            DataType.SirenEnabledState -> null
            DataType.MeasurementStats -> measureStatsMap
            DataType.Shortcuts -> null
            DataType.Zone -> zones
            DataType.Output -> outputs
            DataType.IpCameras -> ipCameras
            DataType.MeariDevices -> ipMeariCameras
            DataType.StreamData -> TODO()
            DataType.BleIpCameras -> TODO()
            DataType.VideoEvents -> TODO()
            else -> null
        }

        private var panelInfo: PanelInfo? = null

        fun updateFromPanelInfo(info: PanelInfo) {
            panelInfo = info
        }

        fun toPanelInfo(): PanelInfo? = panelInfo
    }

    val panelDataMap: HashMap<Int, PanelData> = HashMap()

    private fun forPanel(panelId: Int): PanelData {
        return panelDataMap.getOrPut(panelId) { PanelData() }
    }

    fun currentPanelData(): PanelData? {
        return currentPanelId?.let {
            panelDataMap[it] ?: run {
                Log.e(TAG, "PanelData not initialized for Panel ID: $it")
                null
            }
        }
    }

}