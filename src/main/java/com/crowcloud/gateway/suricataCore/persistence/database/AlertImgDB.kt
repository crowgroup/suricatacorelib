package com.crowcloud.gateway.suricataCore.persistence.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "alert_image_table")
data class AlertImgDB(@PrimaryKey @ColumnInfo(name = "id") val id: Long,
                      @ColumnInfo(name = "alertID") val alertID: Long,
                      @ColumnInfo(name = "index") val indexInUrlList: Int,
                      @ColumnInfo(name = "isEncrypted") val isEncrypted: Boolean,
                      @ColumnInfo(name = "remoteImgUrl") val remoteImgUrl: String,
                      @ColumnInfo(name = "localImgUrl") var localImgUrl: String?)
