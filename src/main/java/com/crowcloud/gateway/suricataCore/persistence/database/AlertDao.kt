package com.crowcloud.gateway.suricataCore.persistence.database

import androidx.room.*

/*
The Room Magic is in this file, where you map a method call to an SQL query.
When you are using complex data types, such as Date, you have to also supply type converters.
 */
@Dao
abstract class AlertDao : BaseDao<AlertDB> {

//    @Query("SELECT * FROM alert_table WHERE deviceID = :id")
//    abstract fun getAllByDeviceId(id: Long): Flow<PagingData<Alert>>

    @Query("SELECT * FROM alert_table WHERE deviceID = :id")
    abstract suspend fun getAllByDeviceId(id: Long): List<AlertDB>

    @Query("SELECT createDate FROM alert_table WHERE deviceID = :id")
    abstract suspend fun getAllDatesByDeviceId(id: Long): List<String>

    @Query("SELECT * FROM alert_table WHERE deviceID = :id AND createDate LIKE :date || '%'")
    abstract suspend fun getAllByDeviceIdAndDate(id: Long, date: String): List<AlertDB>

    @Transaction
    open suspend fun updateData(alerts: List<AlertDB>) {
        insertAll(alerts)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertAll(alerts: List<AlertDB>)

    @Update
    abstract suspend fun updateAlerts(vararg alerts: AlertDB)

    @Query("DELETE FROM alert_table")
    abstract suspend fun deleteAll()

    @Query("DELETE FROM alert_table WHERE alertID = :id")
    abstract fun deleteAlertById(id: Long)

    @Query("UPDATE alert_table SET isRead = :isReadByUser WHERE alertID = :id")
    abstract fun updateAlert(id: Long, isReadByUser: Boolean)

    @Delete
    abstract fun deleteAlert(alert: AlertDB)

    // The flow always holds/caches latest version of data. Notifies its observers when the
    // data has changed.

}