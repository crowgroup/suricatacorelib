package com.crowcloud.gateway.suricataCore.persistence

import com.chad.library.adapter.base.entity.MultiItemEntity
import com.crowcloud.gateway.suricataCore.network.model.response.BaseDevice

class DeviceMultiItem(device: BaseDevice) : MultiItemEntity{

    var data: BaseDevice = device

    override fun getItemType(): Int {
        return data.deviceType.ordinal
    }
}