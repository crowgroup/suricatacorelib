package com.crowcloud.gateway.suricataCore.persistence.database

import androidx.room.*

@Dao
abstract class AlertImgDao : BaseDao<AlertImgDB> {

    @Query("SELECT * FROM alert_image_table WHERE alertID = :id")
    abstract fun getAllByAlertId(id: Long): List<AlertImgDB>

    @Transaction
    open suspend fun updateData(alertImgs: List<AlertImgDB>) {
        insertAll(alertImgs)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertAll(alertImgs: List<AlertImgDB>)

    @Query("DELETE FROM alert_image_table")
    abstract suspend fun deleteAll()

    @Query("DELETE FROM alert_image_table WHERE alertID = :id")
    abstract fun deleteAlertImagesByAlertId(id: Long)

    @Delete
    abstract fun deleteAlertImage(alertImg: AlertImgDB)

}