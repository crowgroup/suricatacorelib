package com.crowcloud.gateway.suricataCore.persistence

import com.crowcloud.gateway.suricataCore.network.model.response.*
import com.crowcloud.gateway.suricataCore.util.DEFAULT_ACCESS_TAG_ID
import com.crowcloud.gateway.suricataCore.util.CrowDeviceType

class DeviceListParser constructor(devices: Devices){

    private val listToParse: Devices = devices

    fun getDeviceList(): ArrayList<BaseDevice> {
        val list: ArrayList<BaseDevice> = ArrayList()
//        if (listToParse.panel != null) {
//            list.add(listToParse.panel!!)
//        }
        for(item: Output in listToParse.outputs.orEmpty()) {
            when {
                item.isSiren() -> {
                    item.deviceType = CrowDeviceType.SIREN
                }
                item.isLock() -> {
                    item.deviceType = CrowDeviceType.DOOR_LOCK_OUTPUT
                }
                item.isZigbeeRgbBulb() -> {
                    item.deviceType = CrowDeviceType.ZIGBEE_RGB
                }
                item.isZigbeeDimmableBulb() -> {
                    item.deviceType = CrowDeviceType.ZIGBEE_DIMMABLE
                }
                item.isZigbeeTemperatureBulb() -> {
                    item.deviceType = CrowDeviceType.ZIGBEE_TEMPERATURE
                }
                item.isZigbeeAcPlug() -> {
                    item.deviceType = CrowDeviceType.AC_PLUG
                }
                item.isZigbeeDoorLockController() -> {
                    item.deviceType = CrowDeviceType.DOOR_LOCK_CONTROLLER
                }
                item.isAcPlug() -> {
                    item.deviceType = CrowDeviceType.AC_PLUG
                }
                item.isFogGenerator() -> {
                    item.deviceType = CrowDeviceType.FOG_GENERATOR
                }
                item.isWaterMeter() -> {
                    item.deviceType = CrowDeviceType.WATER_METER
                }
                else -> {
                    item.deviceType = CrowDeviceType.OUTPUT
                }
            }
            list.add(item)
        }
        for(item: Zone in listToParse.zones.orEmpty()) {
//            if(item.isCamera()) {
//                item.deviceType = CrowDeviceType.CAMERA
//            } else {
//                item.deviceType = CrowDeviceType.ZONE
//            }
            item.deviceType = CrowDeviceType.ZONE
            list.add(item)
        }
        for(item: Keypad in listToParse.keypads.orEmpty()) {
            list.add(item)
        }
        for(item: Extender in listToParse.extenders.orEmpty()) {
            list.add(item)
        }
        for(item: Repeater in listToParse.repeaters.orEmpty()) {
            list.add(item)
        }
        for(item: VoiceBox in listToParse.voiceboxes.orEmpty()) {
            list.add(item)
        }
        for (item in listToParse.users.orEmpty()) {
            item.takeIf { it.pendantAllow && it.pendantId?.let { id -> id > 0 } == true }?.let {
                it.name?.let { name -> list.add(Pendant(name, item.pendantBatteryLow)) }
            }
            item.takeIf {
                it.isAccessTagUser &&
                        it.accessTagId != "null" &&
                        it.accessTagId != DEFAULT_ACCESS_TAG_ID
            }?.let {
                it.name?.let { name ->
                    it.accessTagId?.let { id ->
                        list.add(AccessTag(CrowDeviceType.ACCESS_TAG, name, id))
                    }
                }
            }
        }
        return list
    }
}