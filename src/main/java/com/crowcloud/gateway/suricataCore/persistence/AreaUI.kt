package com.crowcloud.gateway.suricataCore.persistence

import com.crowcloud.gateway.suricataCore.network.model.response.Area
import com.crowcloud.gateway.suricataCore.util.TryProcessType

class AreaUI(private val area: Area) {

    val id: Int
        get() = area.id

    val name: String?
        get() = area.name

    val isFireAlarm: Boolean
        get() = area.isFireAlarm

    val isPanicAlarm: Boolean
        get() = area.isPanicAlarm

    val isZoneAlarm: Boolean
        get() = area.isZoneAlarm

    val isZone24HAlarm: Boolean
        get() = area.isZone24HAlarm

    val isEnAlarm: Boolean
        get() = area.isEnAlarm

    val isDuressAlarm: Boolean
        get() = area.isDuressAlarm

    val isCodeAttemptsAlarm: Boolean
        get() = area.isCodeAttemptsAlarm

    val isMedicalAlarm: Boolean
        get() = area.isMedicalAlarm

    val isEmergencyAlarm: Boolean
        get() = area.isEmergencyAlarm

    val isReadyToArm: Boolean
        get() = area.isReadyToArm

    val isReadyToStay: Boolean
        get() = area.isReadyToStay

    val isEmpty: Boolean
        get() = area.isEmpty

    val exitDelay: Int// = area.exitDelay
        get() = area.exitDelay

    val stayExitDelay: Int
        get() = area.stayExitDelay

    val areaState: Area.AreaState?
        get() = area.state

    var areaTryProcessType = TryProcessType.NONE

    var timeToArm: Int = 0
    var timeToStay: Int = 0

    private var isCheckBypassProcess = false
    private var userAssignedToArea: String? = null
    private var zoneAssignedToArea: String? = null

    private fun setZoneAssignedToArea(zoneAssignedToArea: String) {
        this.zoneAssignedToArea = zoneAssignedToArea
    }

    private fun setUserAssignedToArea(userAssignedToArea: String) {
        this.userAssignedToArea = userAssignedToArea
    }

    fun setIsCheckBypassProcess(isCheckBypassProcess: Boolean) {
        this.isCheckBypassProcess = isCheckBypassProcess
    }
}