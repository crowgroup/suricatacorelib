package com.crowcloud.gateway.suricataCore.persistence

object PreferenceKeys {
    const val CONTROL_PANEL_ID = "controlPanelId"
    const val LAST_SELECTED_PANEL_USER_ID = "last_selected_panel_user_id"
    const val CONTROL_PANEL_ID_FROM_NOTIFICATION = "controlPanelIdFromNotification"
}