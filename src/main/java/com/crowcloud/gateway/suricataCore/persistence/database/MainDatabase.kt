package com.crowcloud.gateway.suricataCore.persistence.database

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


@Database(entities = [AlertDB::class, AlertImgDB::class], version = 2, exportSchema = false)
@TypeConverters(Converters::class)
abstract class MainDatabase : RoomDatabase() {

    abstract fun alertDao(): AlertDao
    abstract fun alertImageDao(): AlertImgDao

    companion object {
        @Volatile
        private var INSTANCE: MainDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): MainDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MainDatabase::class.java,
                    "crow_database"
                )
                    //TODO .addMigrations(MIGRATION_1_2) and remove fallbackToDestructiveMigration
                        //https://medium.com/androiddevelopers/understanding-migrations-with-room-f01e04b07929
                    .fallbackToDestructiveMigration() // Wipes and rebuilds instead of migrating if no Migration object.
                    .addCallback(MainDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                instance
            }
        }

        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                //TODO("Not yet implemented")
            }
        }

        private class MainDatabaseCallback(
            private val applicationScope: CoroutineScope
        ) : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                applicationScope.launch(Dispatchers.IO) {
                    try {
                        INSTANCE?.let { database ->
                            populateDatabase(database.alertDao())
                        }
                    } catch (e: Exception) {
                        Log.e("MainDatabase", "Error populating database: ", e)
                        // Handle the error appropriately
                    }

                }
            }
        }

//        private class MainDatabaseCallback(
//            private val scope: CoroutineScope
//        ) : RoomDatabase.Callback() {
//            /**
//             * Override the onCreate method to populate the database.
//             */
//            override fun onCreate(db: SupportSQLiteDatabase) {
//                super.onCreate(db)
//                // If you want to keep the data through app restarts,
//                // comment out the following line.
//                INSTANCE?.let { database ->
//                    scope.launch(Dispatchers.IO) {
//                        populateDatabase(database.alertDao())
//                    }
//                }
//            }
//        }
        /**
         * Populate the database in a new coroutine.
         * If you want to start with more alerts, just add them.
         */
        suspend fun populateDatabase(alertDao: AlertDao) {
            // Start the app with a clean database every time.
            // Not needed if you only populate on creation.
            //alertDao.deleteAll()

//            var word = Word("Hello")
//            wordDao.insert(word)
//            word = Word("World!")
//            wordDao.insert(word)
        }
    }
}