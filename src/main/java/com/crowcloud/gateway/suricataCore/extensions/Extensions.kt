package com.crowcloud.gateway.suricataCore.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.util.SparseArray
import android.view.View
import androidx.fragment.app.Fragment
import com.crowcloud.gateway.suricataCore.persistence.DataRepo
import com.crowcloud.gateway.suricataCore.network.DeferredCallback
import org.greenrobot.eventbus.EventBus
import org.jdeferred.Promise
import retrofit2.Call
import java.text.SimpleDateFormat
import java.util.*
import kotlin.experimental.and


fun currentPanelId() = DataRepo.instance.currentPanelId

fun currentPanelVersion() = DataRepo.instance.currentPanelVersion

fun currentPanelState() = DataRepo.instance.currentPanelState

fun currentPanelInfo() = DataRepo.instance.currentPanelInfo

fun currentControlPanel() = DataRepo.instance.currentControlPanel

fun currentUser() = DataRepo.instance.currentUser
fun currentPanelData() = DataRepo.instance.currentPanelData()

fun <T> Call<T>.promise(): Promise<T, Throwable, Void> {
    val deferredCallback = DeferredCallback<T>()
    this.enqueue(deferredCallback)
    return deferredCallback.promise()
}

fun View.visible(isVisible: Boolean) {
    this.visibility = if (isVisible) View.VISIBLE else View.GONE
}

fun View.invisible(isInvisible: Boolean) {
    this.visibility = if (isInvisible) View.INVISIBLE else View.VISIBLE
}

fun View.disabled(isDisabled: Boolean) {
    this.alpha = if (isDisabled) 0.5f else 1.0f
    this.isEnabled = !isDisabled
}

fun post(event: Any?) {
    EventBus.getDefault().post(event)
}

fun postSticky(event: Any?) {
    EventBus.getDefault().postSticky(event)
}

// Extension function for Activity to register with EventBus
fun Activity.registerWithEventBus() {
    if (!EventBus.getDefault().isRegistered(this)) {
        EventBus.getDefault().register(this)
        // Use javaClass.simpleName for logging to dynamically get the class name
        Log.i(javaClass.simpleName, "${javaClass.simpleName} - register")
    }
}

// Extension function for Activity to unregister from EventBus
fun Activity.unregisterFromEventBus() {
    if (EventBus.getDefault().isRegistered(this)) {
        EventBus.getDefault().unregister(this)
        Log.i(javaClass.simpleName, "${javaClass.simpleName} - unregister")
    }
}

fun Fragment.registerWithEventBus() {
    if (!EventBus.getDefault().isRegistered(this)) {
        EventBus.getDefault().register(this)
        Log.d(javaClass.simpleName, "${javaClass.simpleName} - register")
    } else {
        Log.w(javaClass.simpleName, "Attempted to register ${javaClass.simpleName} with EventBus, but it is already registered.")
    }
}

fun Fragment.unregisterFromEventBus() {
    if (EventBus.getDefault().isRegistered(this)) {
        EventBus.getDefault().unregister(this)
        Log.d(javaClass.simpleName, "${javaClass.simpleName} - unregister")
    } else {
        Log.w(javaClass.simpleName, "Attempted to unregister ${javaClass.simpleName} from EventBus, but it was not registered.")
    }
}

private val hexArray = "0123456789ABCDEF".toCharArray()
fun ByteArray.toHexString(): String {
    val hexChars = CharArray(size * 2)
    for (j in indices) {
        val v = (this[j] and 0xFF.toByte()).toInt()

        hexChars[j * 2] = hexArray[v ushr 4]
        hexChars[j * 2 + 1] = hexArray[v and 0x0F]
    }
    return String(hexChars)
}

fun <E> SparseArray<E>.toArrayList(): MutableList<E>? {
    val arrayList = java.util.ArrayList<E>(size())
    for (i in 0 until size()) arrayList.add(valueAt(i))
    return arrayList
}

fun String.toDate(dateFormat: String = "yyyy-MM-dd'T'HH:mm:ss", timeZone: TimeZone = TimeZone.getTimeZone("UTC")): Date? {
    val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
    parser.timeZone = timeZone
    return parser.parse(this)
}

fun Date.formatTo(dateFormat: String, timeZone: TimeZone = TimeZone.getDefault()): String {
    val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())
    formatter.timeZone = timeZone
    return formatter.format(this)
}

fun Context.sendShareIntent(text: String) {
    startActivity(Intent().apply {
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_TEXT, text)
        type = "text/plain"
    })
}

